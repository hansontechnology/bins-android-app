package com.pixart.alg;

public class PXIALG {
 
	// 
	public static native void Open(int id);
	public static native void Close();
	public static native void SetData(int touch_flag, int channel, int[] input, int total_samples );
	//
 	public static native int DrvOpen();
	public static native int DrvClose();
	public static native int [] DrvReadAndProcessHRD();
	public static native int DrvGetStableTime();

	// Database table Data_HR, realtime
	public static native int GetHR();
	public static native int GetHRReadyFlag(); //return 0~100
	public static native int GetMotionFlag(); 
 
	// Database table Data_HRV, 120 sec
	public static native int SetAge(int age);
	public static native void HRVProcess(); //return 0~100
	public static native float GetHRVReadyFlag(); //return 0~100
	public static native int GetSDNN();
	public static native float GetLH();
	public static native float GetSDNNHR();
	public static native int GetStress(); 	//get SDNN result 0~3
											// 0 - Result : Perfect health, not at all strain
											// 1 - Result : Normal, not at all strain
      										// 2 - Result : Mildly Strain
      										// 3 - Result : Severely Strain
	public static native int [] GetRRI();
 
	// Database table Data_SpO2, realtime
	public static native float GetSpO2(); // 
	public static native float GetSpO2ReadyFlag(); //return 0~100
 
	// Database table Data_RR
	public static native float GetRR(); // BR == RR, 60 sec
	public static native float GetRRReadyFlag(); //return 0~100
 
	// Database table Data_SDPPG
	public static native int GetSDPPG(); // 90 sec
	public static native float GetSDPPGReadyFlag(); //return 0~100
 
 	public static native double [] GetDisplayBuffer();  //for waveform plot 
 
 
    static {
        System.loadLibrary("hrd_spo2");
    }    
}
    
    /*
	
	// 
	public static native void Open();
	public static native void Close();
	
	
	// Database table Data_HR, realtime
	public static native int GetHR();
	public static native int GetHRReadyFlag(); //return 0~100
	public static native int GetMotionFlag(); 
	
	// Database table Data_HRV, 120 sec
	public static native int SetAge(int age);
	public static native float GetHRVReadyFlag();	//return 0~100
	public static native int GetSDNN();
	public static native float GetLH();
	public static native float GetSDNNHR();
	public static native int GetStress();	//get SDNN result 0~3
											// 0 - Result : Perfect health, not at all strain
											// 1 - Result : Normal, not at all strain
											// 2 - Result : Mildly Strain
											// 3 - Result : Severely Strain
	public static native int [] GetRRI();
	
	// Database table Data_SpO2, realtime
	public static native float GetSpO2(); // 
	public static native int GetSpO2ReadyFlag();	//return 0~100
	
	// Database table Data_RR
	public static native float GetRR(); // BR == RR, 60 sec
	public static native int GetRRReadyFlag();	//return 0~100
	
	// Database table Data_SDPPG
	public static native int GetSDPPG(); // 90 sec
	public static native int GetSDPPGReadyFlag();	//return 0~100
	
	
	
	/////////
	public static native int SpO2SetData(int touch_flag, int ch, int[] input, int total_samples);
	public static native void HRDProcess(int touch_flag, int[] input, int total_samples);
	public static native void HRDSDNNProcess();
	public static native void SpO2Process();
	
	public static native void SpO2Start(); 
	public static native void SpO2Stop();
     */
