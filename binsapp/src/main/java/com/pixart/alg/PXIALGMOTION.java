package com.pixart.alg;

public class PXIALGMOTION {
 
	public static native void Open(int HZ);
	public static native void Close();
	public static native int GetReadyFlag();
	public static native int GetMotionFlag(); 
	public static native int GetVersion();	

	public static native int DrvOpen();
	public static native int DrvClose();
	public static native int [] DrvReadAndProcess();
	public static native float [] GetDisplayBuffer();
	public static native int DrvGetStableTime();
	
	public static native int GetTouchFlag(); 
	public static native int Process(char [] ppg_data, float [] mems_data);
	
	public static native String OpenLogFile(String file);
	public static native void CloseLogFile();

	public static native float GetSigGrade();
	public static native void SetSigGradeThrd(float thrd);
	public static native void EnableFastOutput(boolean enable);
	public static native void SetMemsScale(int scale);
	public static native void EnableMotionMode(boolean enable);
	public static native int GetHR();
    static {
        System.loadLibrary("paw8001motion");
    }    
}