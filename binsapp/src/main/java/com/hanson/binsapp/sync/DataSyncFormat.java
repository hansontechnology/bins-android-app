
package com.hanson.binsapp.sync;

import android.content.Context;
import android.util.Log;

import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.sync.DataSyncRoutine.ActivityStruct;

public class DataSyncFormat {
    public static final String TAG = "DataSyncFormat";

    public static void bleSet16(byte[] bytes, int val)
    {
    	Log.d(TAG, "bleSet16 : value =" + val);
 /*   	
    	bytes[0]= (byte)((val & 0xFF000000) >> 24) ;
    	bytes[1]= (byte)((val & 0x00FF0000) >> 16) ;
    	bytes[2]= (byte)((val & 0x0000FF00) >>  8) ;
    	bytes[3]= (byte)((val & 0x000000FF) >>  0) ;
 */
  /*
    	bytes[0]= (byte)((val & 0x0000FF00) >>  8) ;
    	bytes[1]= (byte)((val & 0x000000FF) >>  0) ;
 */
    	
    	bytes[1]= (byte)((val & 0x0000FF00) >>  8) ;
    	bytes[0]= (byte)((val & 0x000000FF) >>  0) ;

    	//-- bytes[0]= 0x77;
    	//-- bytes[1]= 0x66;
    	
    }
    
    public static String getDeviceAddress(Context context) {
        BinsDBUtils leDao = BinsDBUtils.getBluetoothLeDao(context);
        return leDao.getActiveDeviceAddress();
    }
    
    public static int parserBattery(byte[] bytes) {
        return bytes[0] & 0xFF;
    }

    public static int parserActivityValue(byte[] bytes) {
        return (bytes[0] & 0xFF) + ((bytes[1] & 0xFF) << 8);
    }

    public static int parserActivityType(byte[] bytes) {
        return bytes[0] & 0xFF;
    }

    public static int parserActivityTime(byte[] bytes) {
        return (bytes[0] & 0xFF) + ((bytes[1] & 0xFF) << 8);
    }
            
    public static int parserActivityTimeValue(byte[] bytes) {
        return (bytes[0] & 0xFF) 		+ ((bytes[1] & 0xFF) << 8) + 
        	   ((bytes[2] & 0xFF) << 16)	+ ((bytes[3] & 0xFF) << 24) ;
    }

    public static int parserInt32(byte[] bytes) {
        return (bytes[0] & 0xFF) 		+ ((bytes[1] & 0xFF) << 8) + 
        	   ((bytes[2] & 0xFF) << 16)	+ ((bytes[3] & 0xFF) << 24) ;
    }
 
    public static int parserInt16(byte[] bytes) {
        return (bytes[0] & 0xFF) + ((bytes[1] & 0xFF) << 8)  ;
    }
    
    public static void insertOrUpdateActivity(Context context, int user,
            ActivityStruct struct) {
  
        BinsDBUtils.getBluetoothLeDao(context).insertOrUpdateActivity(user, struct.mmActivityType,
                struct.mmActivityValue, struct.mmActivityTime);
    }

    public static int getActivityValueUser(Context context, int user, long time) {
    	return BinsDBUtils.getBluetoothLeDao(context).getUserActivity(user, 1, time);
    }
    
 
    public static void updateBatteryLevel(Context context, int device, int level) {
        BinsDBUtils leDao = BinsDBUtils.getBluetoothLeDao(context);
        int ret = leDao.updateBatteryLevel(device, level);
        Log.d(TAG, "Battery Level: " + level + ", ret = " + ret);
    }

    public static void updateSyncTime(Context context, int device, long time) {
        BinsDBUtils leDao = BinsDBUtils.getBluetoothLeDao(context);
        int ret = leDao.updateSyncTime(device, time);
        Log.d(TAG, "Update Time: " + time + ", ret = " + ret);
    }
    
    public static void updateConfiguration(int config) {
        BinsDBUtils leDao = BinsDBUtils.activeDB();
        leDao.setBinsConfiguration(config);
        Log.d(TAG, "Update Config: " + config);
    }
    
}
