
package com.hanson.binsapp.sync;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.hanson.binsapp.GattAttributes;
import com.hanson.binsapp.R;
import com.hanson.binsapp.fragment.BinsFragment;
import com.hanson.binsapp.provider.Bins;
import com.hanson.binsapp.provider.BinsDBUtils;

public class DataSyncRoutine {
    public static final String TAG = "DataSyncRoutine";
    public static final int READ_BATTERY = 0;
    public static final int READ_ACTIVITY_VALUE = 1;
    public static final int READ_ACTIVITY_TYPE = 2;
    public static final int READ_ACTIVITY_TIME = 3;
    public static final int READ_ACTIVITY_TIME_VALUE = 6;
    public static final int READ_CONFIGURATION = 8;    
    public static final int REFRESH_TIME = 4;
    public static final int CHECK_SLEEP_START_TIME = 10;
    public static final int UPLOAD_SLEEP_RECORDS = 11;
    public static final int READ_IBEACON_UUID = 12;
    
    //-- private HandlerThread mWorkThread;
    private BinsFragment mBinsFragment;

    private long mConnectedTime = 0;
    private int mCurrentDeviceID;
    private int mCurrentUserID;

    private Handler mHandler;
    private BluetoothGattCharacteristic mCharacterBattery;
    private BluetoothGattCharacteristic mCharacterValue;
    private BluetoothGattCharacteristic mCharacterType;
    private BluetoothGattCharacteristic mCharacterTime;
    private BluetoothGattCharacteristic mCharacterTimeValue;
    private BluetoothGattCharacteristic mCharacterUploadSleepRecords;
    private BluetoothGattCharacteristic mCharacterCheckSleepStartTime;
    private BluetoothGattCharacteristic mCharacterIbeaconUuid;

    //-- write characteristics 
    private BluetoothGattCharacteristic mCharacterLocalClock;
    private BluetoothGattCharacteristic mCharacterGoal;
    private BluetoothGattCharacteristic mCharacterAlarm;
    private BluetoothGattCharacteristic mCharacterConfiguration;
    
   	private boolean mIsWroteLocalClock;
	private boolean mIsWroteGoal;
	private boolean mIsWroteAlarm;
	private boolean mIsWroteConfiguration;

	private long mSleepRecordConnectTime=0;
	private long mSleepRecordSessionSaved=0;
	private long mSleepRecordSessionTotal=0;
	private long mSleepRecordNextTime=0;
	
    private static final int MAX_TRY_COUNTS = 3;
    private int mCurrentReadIndex = 0;
    private int mTryCounts = 0;
    
    private ActivityStruct mCached = new ActivityStruct();

    public boolean mIsFirstTimeSyncToDevice=false;
        
    private BinsDBUtils binsDB;
    private Activity mActivity=null;

    private ArrayList<ActivityStruct> mActivityStack = new ArrayList<ActivityStruct>();
 
    
    private void insertToStack(int activityType, int activityValue) {
        ActivityStruct theActivity=new ActivityStruct();

        theActivity.mmActivityType=activityType;
        theActivity.mmActivityValue=activityValue;
        
        mActivityStack.add(theActivity);
        
    }

    private ActivityStruct popFromStack() {
        
        int count=mActivityStack.size();
        
        if(count==0) return null;
        
        ActivityStruct theActivity;
        theActivity=mActivityStack.get(count-1);
        mActivityStack.remove(count-1);
        
        return theActivity;
    }

    private int countOfStack() {
        return mActivityStack.size();
    }

    private void resetStack() {
        mActivityStack.clear();
    }

    
    
    
    public DataSyncRoutine(BinsFragment binsFragment) {
        mBinsFragment = binsFragment;
        mActivity = mBinsFragment.getActivity();
    	binsDB=BinsDBUtils.getBluetoothLeDao(mActivity);

        //-- mWorkThread = new HandlerThread(TAG);
    }

    public void start(boolean isConnectOnly, BluetoothGattCharacteristic battery, BluetoothGattCharacteristic value,
            BluetoothGattCharacteristic type, BluetoothGattCharacteristic time, BluetoothGattCharacteristic timeValue,
            BluetoothGattCharacteristic localClock, 
            BluetoothGattCharacteristic goal, 
            BluetoothGattCharacteristic alarm,
            BluetoothGattCharacteristic config,
            BluetoothGattCharacteristic sleepTime,
            BluetoothGattCharacteristic sleepRecords,
            BluetoothGattCharacteristic ibeaconUuid          
    		) {
    	
    	binsDB=BinsDBUtils.getBluetoothLeDao(mActivity);
    	
        mCharacterBattery = battery;
        mCharacterValue = value;
        mCharacterType = type;
        mCharacterTime = time;
        mCharacterTimeValue = timeValue;
        mCharacterLocalClock = localClock;
        mCharacterGoal = goal;
        mCharacterAlarm = alarm;
        mCharacterConfiguration = config;
        mCharacterCheckSleepStartTime = sleepTime;
        mCharacterUploadSleepRecords = sleepRecords;
        mCharacterIbeaconUuid= ibeaconUuid;
         
   		mConnectedTime=(System.currentTimeMillis() / 1000 / 60); 
        mCurrentUserID=binsDB.getActiveUserID();
   	    mCurrentDeviceID=binsDB.getActiveDeviceID();
   	    
   		
        //-- mWorkThread.start();
        mHandler = new Handler(new DataSyncCallback());
        //-- 11.19 share UI thread, mHandler = new Handler(mWorkThread.getLooper(), new DataSyncCallback());

        // mHandler.sendEmptyMessage(READ_BATTERY);
        
        /*
        if(binsDB.getActiveLastSyncTime()!=0){
          //-- 11.14 mHandler.sendEmptyMessage(READ_CONFIGURATION); // 11.14
        }
        */
        
        if(isConnectOnly==true){	// 150712
        	mBinsFragment.endSyncNotify();
        	return;
        }
        // mHandler.sendEmptyMessage(READ_ACTIVITY_TIME_VALUE);
        mHandler.sendEmptyMessageDelayed(READ_ACTIVITY_TIME_VALUE, 500); //-- 09.14
        
        //-- 09.14 mHandler.sendEmptyMessage(READ_BATTERY);	// 07.21

    }

    public void stop() {
        //-- mWorkThread.quit();
        mCached.reset();
    }

    private void readBattery() {
        Log.d(TAG, "readBattery ...");
        mCurrentReadIndex = READ_BATTERY;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterBattery);
    }


    private void readActivityValue() {
        //Log.d(TAG, "readActivityValue ...");
        // Reset cache
        mCached.reset();

        mCurrentReadIndex = READ_ACTIVITY_VALUE;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterValue);
    }

    private void readActivityType() {
        //Log.d(TAG, "readActivityType ...");
        mCurrentReadIndex = READ_ACTIVITY_TYPE;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterType);
    }

    private void readActivityTime() {
        //Log.d(TAG, "readActivityTime ...");
        mCurrentReadIndex = READ_ACTIVITY_TIME;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterTime);
    }

    private void readBinsConfiguration() {
        Log.d(TAG, "readConfiguration ...");
        mCurrentReadIndex = READ_CONFIGURATION;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterConfiguration);
    }
    private void readActivityTimeValue() {
        Log.d(TAG, "readActivityTimeValue ...");
        mCurrentReadIndex = READ_ACTIVITY_TIME_VALUE;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterTimeValue);
    }
    
    private void readSleepStartTime() {
        Log.d(TAG, "read sleep start time ...");
        mCurrentReadIndex = CHECK_SLEEP_START_TIME;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterCheckSleepStartTime);
    }
    private void uploadSleepRecords() {
        Log.d(TAG, "continue read sleep records ...");
        mCurrentReadIndex = UPLOAD_SLEEP_RECORDS;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterUploadSleepRecords);
    }
    private void readIbeaconUuid() {
        Log.d(TAG, "read Ibeacon UUID ");
        mCurrentReadIndex = READ_IBEACON_UUID;
        mTryCounts++;
        mBinsFragment.readCharacteristic(mCharacterIbeaconUuid);
    }
    
    private void updateRefreshTime() {
        Log.d(TAG, "updateRefreshTime ...");       
        
        long lastSyncTime=BinsDBUtils.activeDB().getActiveUserLastSyncTime();
        
        if(lastSyncTime==0){ // means the first time sync
        	mIsFirstTimeSyncToDevice=true;
        }
        else{
        	mIsFirstTimeSyncToDevice=false;        	
        }
        
        binsDB.setActiveUserLastSyncTime(mConnectedTime);
        
        if(mBinsFragment.periodicScanSession()==true)
        {
        	Log.d(TAG, "end operation");
         	mBinsFragment.endSyncNotify();	//-- directly to end operation
        }
        else{  
        	// 150502s
        	if(mCharacterIbeaconUuid!=null){
          		mHandler.sendEmptyMessage(READ_IBEACON_UUID);
        	}
        	else
        	   writeConfigurations();	//-- write device configurations to device
        							//-- delay this end operation until writings finished - mBinsFragment.endRead();    
        }
    }
    
    private void writeConfigurations() {
    	byte[] bytes = new byte[8];
    	
    	Log.d(TAG,"writeConfiguration ...");
    	mIsWroteLocalClock=false;
    	mIsWroteGoal=false;
    	mIsWroteConfiguration=false;
    	
    	if(mCharacterAlarm==null) mIsWroteAlarm=true;
    	else mIsWroteAlarm=false;
    	
    	/* date formatter in local time zone */
    	Calendar cal = Calendar.getInstance();
        
    	int hour=cal.get(Calendar.HOUR_OF_DAY); //- 07.22
       	int minute=cal.get(Calendar.MINUTE);
       	
       	//-- 07.21, need to change to 24-hr format
       	// hour=15;
       	
       	int highV= (hour <<8);
       	int localClock= (highV + minute);
       	Log.d(TAG, "calendar time:"+ hour + ":" + minute + "=" + localClock + "," + highV);
       	DataSyncFormat.bleSet16(bytes, localClock);
    	mCharacterLocalClock.setValue(bytes);    	
    	mBinsFragment.writeCharacteristic(mCharacterLocalClock);
/*    	
    	int dailyGoal=0x1230;
    	DataSyncHelper.bleSet16(bytes, dailyGoal);
    	mCharacterGoal.setValue(bytes);
    	mBinsFragment.writeCharacteristic(mCharacterGoal);
    	
    	int deviceConfig=0x5410;
    	DataSyncHelper.bleSet16(bytes, deviceConfig);
    	mCharacterConfiguration.setValue(bytes);    	
    	mBinsFragment.writeCharacteristic(mCharacterConfiguration);
*/

    }
 
    public void onDataWrite(BluetoothGattCharacteristic character) {
        final String uuid = character.getUuid().toString();
        final byte[] bytes = character.getValue();
        if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_LOCAL_CLOCK)) {
            Log.d(TAG, "onDataWrite ... LOCAL CLOCK");
            mIsWroteLocalClock=true;
            
            int goal=BinsDBUtils.activeDB().getActiveUserDailyGoal();
            
        	DataSyncFormat.bleSet16(bytes, goal);
        	mCharacterGoal.setValue(bytes);
        	mBinsFragment.writeCharacteristic(mCharacterGoal);

            
        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_GOAL)) {
            Log.d(TAG, "onDataWrite ... GOAL");
            mIsWroteGoal=true;
            
            /* 11.14
           	int deviceConfig=BinsFragment.gConfiguration;
           	
           //--  gConfiguration=BinsDBUtils.getBluetoothLeDao(getActivity()).getBinsConfiguration();

        	DataSyncFormat.bleSet16(bytes, deviceConfig);
        	mCharacterConfiguration.setValue(bytes);    	
        	mBinsFragment.writeCharacteristic(mCharacterConfiguration);
        	*/
            
            mBinsFragment.writeBinsConfiguration(); // 11.14
            

        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ALARM_CLOCK)) {
            Log.d(TAG, "onDataWrite ... ALARM");
            mIsWroteAlarm=true;
        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_CONFIGURATION)) {
            Log.d(TAG, "onDataWrite ... CONFIGURATION");
            mIsWroteConfiguration=true;
            
            mBinsFragment.writeAlarm(); // 11.14                  
        }
        else{
        	return; //-- just return if else
        }
        
        if(mIsWroteLocalClock==true &&
        		mIsWroteGoal==true &&
        		mIsWroteConfiguration==true &&
        		mIsWroteAlarm==true				)
        {        	
        	Log.d(TAG, "end of writes");
        	
     	  	//-- 150914 verified, need to modify the unit ambient light sensing window too.
            //-- mBinsFragment.setSleepCheckPeriod(5); //-- every 5 minutes to report 1 sleep record.

         	mBinsFragment.endSyncNotify();
        }
    	 
    }
    
    public void insertHeartRateRecord(int rateHR)
    {
        mCached.mmActivityType=2;
        mCached.mmActivityTime = (int)( System.currentTimeMillis() / 1000);	//-- second

        mCached.mmActivityValue = rateHR;
        
        //-- 12.19 DataSyncFormat.insertOrUpdateActivity(mBinsFragment.getActivity(), mCurrentUserID, mCached);  
        
        BinsDBUtils.activeDB().insertOrUpdateActivity(
        		mCurrentUserID, 
        		mCached.mmActivityType, 
        		mCached.mmActivityValue, 
        		mCached.mmActivityTime);
    }    
    
    public void notifyBatteryLevel(int batteryLevel)
    {
    	if(mBinsFragment.mBinsType==0) return;	//-- 09.03 if Bins x1, ignore this part.
    	
		Log.d(TAG, "notifyBatteryLevel:"+batteryLevel);
		Activity theActivity=mActivity;
		
		Notification myNotification;
		myNotification= new NotificationCompat.Builder(theActivity.getApplicationContext())
		.setContentTitle(theActivity.getResources().getString(R.string.notification_title_battery_less))
		.setContentText(theActivity.getResources().getString(R.string.notification_content_battery_less)+" "+batteryLevel+"%")
		.setTicker(theActivity.getResources().getString(R.string.notification_ticker_battery))
		.setWhen(System.currentTimeMillis())
		.setDefaults(Notification.DEFAULT_SOUND)
		.setAutoCancel(true)
		.setSmallIcon(R.drawable.ic_bins)
		.build();

		NotificationManager notificationManager = 
		  (NotificationManager) theActivity.getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(0, myNotification); 
	}

    //BITS 15, 11, 4

    public int setShakeThresholdToConfig(int threshValue, int configOrigin)
    {
    	int config=configOrigin;
     
    	if((threshValue & 0x04)!=0) config |= 0x8000; else config &= (~0x8000);
    	if((threshValue & 0x02)!=0) config |= 0x0800; else config &= (~0x0800);
    	if((threshValue & 0x01)!=0) config |= 0x0010; else config &= (~0x0010);
     
    	return config;
    }
    
  //10.11
    public int getShakeThresholdFromConfig(int config) 
    {
     int threshValue=0;
        
     
     if((config & 0x8000)!=0) threshValue |= 0x4;
     if((config & 0x0800)!=0) threshValue |= 0x2;
     if((config & 0x0010)!=0) threshValue |= 0x1;
     
     return threshValue;
    }

    
  
    public void onCharacteristicChanged (BluetoothGattCharacteristic characteristic) {
        final String uuid = characteristic.getUuid().toString();
        final byte[] byteStream = characteristic.getValue();
 
        Log.d(TAG, "onCharacteristicChanged");
    	

    	if(uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_TIME_VALUE))
        {
     		mConnectedTime=(System.currentTimeMillis() / 1000 / 60); 
           
          	int readValue = DataSyncFormat.parserActivityTimeValue(byteStream);
    		int stepCount=(int)(readValue & 0x0000FFFF);             
            
            if(mBinsFragment.getBinsFirmwareRevisionMajor()<=2){            
            	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_STEPS;
            	mCached.mmActivityTime = (int)( mConnectedTime - 0);
            }
            else{
            	//-- mCached.mmActivityType=(int)(((readValue & 0x80000000)>>31)+1); //-- type = 1, 2 from 0, 1 
            	if((readValue & 0x80000000)!=0)
            		mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_SLEEP;
            	else
            		mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_STEPS;       
	            	
	           	  	mCached.mmActivityTime = ((readValue&0x7FFF0000)>>16);
	           	  	
	           	  	// Log.i(TAG, "shifted time:"+mCached.mmActivityTime);
	           	  	
            	mCached.mmActivityTime = (int)(mConnectedTime - mCached.mmActivityTime);
            }
            
            //-- 2015.01.15
            if(mCached.mmActivityType==Bins.BINS_ACTIVITY_TYPE_STEPS){
            	boolean stepWatchEnabled=BinsDBUtils.activeDB().getActiveUserStepWatchEnabled();
            	if(stepWatchEnabled==true){
            		long stepWatchStartTime=BinsDBUtils.activeDB().getActiveUserStepWatchStartTime();
            		if(mCached.mmActivityTime<stepWatchStartTime){
            			mCached.mmActivityTime=stepWatchStartTime;
            		}
            	}
            }
            /*
            Log.i(TAG, "onCharacteristicChanged revision:"+
            		mBinsFragment.getBinsFirmwareRevisionMajor()+ 
            		" ty:"+mCached.mmActivityType+" ti:"+mCached.mmActivityTime+
            		" va:"+stepCount);
            */
            
            if(mCached.mmActivityType==Bins.BINS_ACTIVITY_TYPE_STEPS){
                int currentValue=DataSyncFormat.getActivityValueUser(mActivity, mCurrentUserID, mConnectedTime);

            	if(mBinsFragment.mBinsType==0 && 
            		(mBinsFragment.getBinsFirmwareRevisionMajor()<=2 && mBinsFragment.getBinsFirmwareRevisionMinor()<=3) ){
            			//-- 07.28 
            			mCached.mmActivityValue = currentValue+1;	//-- that should be upgraded later from 0409 version
            	} else {	
            		//-- mCached.mmActivityValue = currentValue+stepCount;
            		// mCached.mmActivityValue = currentValue+1;
            		mCached.mmActivityValue = currentValue+stepCount;
            	}
            	
            } else { // not type 1
            	mCached.mmActivityValue = stepCount;	//-- 09.13 just overwrite
            }
            
            DataSyncFormat.insertOrUpdateActivity(mActivity, mCurrentUserID, mCached);
            mBinsFragment.mHadNewDataRead=true;
            mBinsFragment.updateActivityViews();
                   
        }
        else if(uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_HEARTRATE_STREAM)){
            mBinsFragment.updateHeartrateStream(byteStream);
        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_BATTERY)) {
            int newBatteryLevel = DataSyncFormat.parserBattery(byteStream);
            
            int batteryLevel = BinsDBUtils.getBluetoothLeDao(mActivity).getBatteryLevel(mCurrentDeviceID);
            
            if(newBatteryLevel<35 && batteryLevel>newBatteryLevel){
            	notifyBatteryLevel(newBatteryLevel);
            }
            
            DataSyncFormat.updateBatteryLevel(mActivity, mCurrentDeviceID, newBatteryLevel);
            Log.d(TAG, "onCharacteristicChanged ... BATTERY: " + newBatteryLevel);
        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_DEBUG_OUTPUT)) {
            int i;
            short x,y,z;
            i=0;
            x= (short) (byteStream[i*2] + (byteStream[i*2+1]<<8)); i++;
            y= (short) (byteStream[i*2] + (byteStream[i*2+1]<<8)); i++;
            z= (short) (byteStream[i*2] + (byteStream[i*2+1]<<8));
            
            //-- Log.d(TAG, "debug notified: " + Integer.toHexString(x) + "," + Integer.toHexString(y) +"," + Integer.toHexString(z));
            Log.d(TAG, "debug notified2: " + x + "," + y +"," + z);

        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_SLEEP_RECORDS)) {
            int sleepRecord;
          	sleepRecord=(byteStream[0] & 0xFF) + ((byteStream[1] & 0xFF) << 8);

     		mConnectedTime=(System.currentTimeMillis() / 1000 / 60); 

            if((sleepRecord & 0x8000)!=0)	// means ambient light record
            	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_LIGHT;
            else
            	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_SLEEP;       
	            	
            mCached.mmActivityTime = (int)(mConnectedTime);
            
            mCached.mmActivityValue = sleepRecord & 0x7FFF;
            
            Log.i(TAG, "Sleep record notified:"+
        			" ti:"+mCached.mmActivityTime+
        			" ty:"+mCached.mmActivityType+
        			" va:"+mCached.mmActivityValue);
			
        	DataSyncFormat.insertOrUpdateActivity(mActivity, mCurrentUserID, mCached);
        	
            if(mBinsFragment.isSleepRecordReadBypass==false){    //-- 20151104
            	binsDB.setActiveUserLastSyncTime(mConnectedTime);	//-- 20151021
            }
        }

    }
 
    public void onDataRead(BluetoothGattCharacteristic character) {
        // Reset
        mTryCounts = 0;

        Log.d(TAG, "onDataRead");
        
        final String uuid = character.getUuid().toString();
        final byte[] bytes = character.getValue();
        int readValue = 0;
        if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_BATTERY)) {
            int newBatteryLevel = readValue = DataSyncFormat.parserBattery(bytes);
            
            int batteryLevel = binsDB.getBatteryLevel(mCurrentDeviceID);
            
            if(newBatteryLevel<35 && batteryLevel>newBatteryLevel){
            	notifyBatteryLevel(newBatteryLevel);
            }

            binsDB.updateBatteryLevel(mCurrentDeviceID, readValue);
            Log.d(TAG, "onDataRead ... BATTERY: " + readValue);

        } else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_CONFIGURATION)) {
            Log.i(TAG, "onDataRead - configuration");
            int newConfig = readValue = DataSyncFormat.parserInt16(bytes);
            
            int shakeUiThresh=getShakeThresholdFromConfig(newConfig);
            BinsDBUtils.activeDB().setActiveShakeUiThreshold(shakeUiThresh);
            
            
        	DataSyncFormat.updateConfiguration(newConfig);
        	BinsFragment.gConfiguration=newConfig | 0xFFF0; // 11.14
        	
        	
        } else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_VALUE)) {
            //Log.d(TAG, "onDataRead ... Activity value");
            readValue = DataSyncFormat.parserActivityValue(bytes);
            if (readValue != 0) {
                mCached.mmActivityValue = readValue;
                //Log.d(TAG, "Continue read ...");
                mHandler.sendEmptyMessage(READ_ACTIVITY_TYPE);
            } else {
                Log.d(TAG, "Read end, will update refresh time ...");
                //-- obsoleted mHandler.sendEmptyMessage(REFRESH_TIME);
            }
        } else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_TYPE)) {
            //Log.d(TAG, "onDataRead ... Activity type");
            readValue = DataSyncFormat.parserActivityType(bytes);
            mCached.mmActivityType = readValue;
            mHandler.sendEmptyMessage(READ_ACTIVITY_TIME);
        } else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_TIME)) {
            //Log.d(TAG, "onDataRead ... Activity time");
            readValue = DataSyncFormat.parserActivityTime(bytes);
            mCached.mmActivityTime = (int) (mConnectedTime - readValue);
            DataSyncFormat.insertOrUpdateActivity(mActivity, mCurrentUserID, mCached);
            mHandler.sendEmptyMessage(READ_ACTIVITY_VALUE);
        } else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_ACTIVITY_TIME_VALUE)) {
        	int stepMinute;    
        	       	
            readValue = DataSyncFormat.parserActivityTimeValue(bytes);
            stepMinute=(int)(readValue & 0x0000FFFF);
        
            if(mBinsFragment.getBinsFirmwareRevisionMajor()<=2){ 
            	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_STEPS;
            	mCached.mmActivityTime = 
            		(int)( mConnectedTime - ((readValue & 0xFFFF0000) >> 16));
            }else{
            	if((readValue & 0x80000000)!=0)
             		mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_SLEEP;
             	else
             		mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_STEPS;  
            	
            	mCached.mmActivityTime = 
            		(int)( mConnectedTime - ((readValue & 0x7FFF0000) >> 16));
            	mCached.mmActivityTime = ((readValue&0x7FFF0000)>>16);
             	mCached.mmActivityTime = (int)(mConnectedTime - mCached.mmActivityTime);
            	
            }

            //-- 2015.01.15
            if(mCached.mmActivityType==Bins.BINS_ACTIVITY_TYPE_STEPS){
            	boolean stepWatchEnabled=BinsDBUtils.activeDB().getActiveUserStepWatchEnabled();
            	if(stepWatchEnabled==true){
            		long stepWatchStartTime=BinsDBUtils.activeDB().getActiveUserStepWatchStartTime();
            		if(mCached.mmActivityTime<stepWatchStartTime){
            			mCached.mmActivityTime=stepWatchStartTime;
            		}
            	}
            }
            
            //-- if there is an exist record with this time, then add it.
            int existStepValue=DataSyncFormat.getActivityValueUser(mActivity, mCurrentUserID , mCached.mmActivityTime);
            
            if(mCached.mmActivityType==Bins.BINS_ACTIVITY_TYPE_STEPS){

            	mCached.mmActivityValue = stepMinute+existStepValue;
            }
            else{ // not type 1
               	mCached.mmActivityValue = stepMinute;	//-- 09.13 not step actually, just overwrite            	
            }
            /*
            Log.i(TAG, "Read Time Activity- ty:" +
            		mCached.mmActivityType + " ti:" +
            		mCached.mmActivityTime + " va:" +
            		stepMinute + " ex:" + existStepValue);                      
            */
            if (stepMinute != 0 || mCached.mmActivityType!=Bins.BINS_ACTIVITY_TYPE_STEPS) {	//-- 09.13
            	
                DataSyncFormat.insertOrUpdateActivity(mActivity, mCurrentUserID, mCached);
                
                //-- ?? no meaning  0917 mCached.mmActivityValue = readValue;
                mBinsFragment.mHadNewDataRead=true;
                
                mHandler.sendEmptyMessage(READ_ACTIVITY_TIME_VALUE);
            } else {
            	
               	if(mCharacterCheckSleepStartTime!=null){           	
               		mHandler.sendEmptyMessage(CHECK_SLEEP_START_TIME);
               		
               	}else{  // if it is an old version wearable
               		mHandler.sendEmptyMessage(REFRESH_TIME);                             
               	}
                           
            }	
            
        }        
        else if(uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_SLEEP_START_TIME)){ 
        	int validBit;
        	int valueReceived=DataSyncFormat.parserInt16(bytes);
        	mSleepRecordConnectTime=(System.currentTimeMillis() / 1000 / 60);
                   	
        	validBit= valueReceived & 0x8000;
 
        	long sleepRecordTimeShift= ((~0x8000) & valueReceived)-1;

        	//-- Log.i(TAG, "Sleep record left:time="+validBit+":"+mSleepRecordStartTimeShift);

        	//-- 20151021 FIX BUG, 
        	//-- startTimeShift returned didn't consider the notifications occurred before. 
        	long lastSyncTime=binsDB.getActiveDeviceLastSyncTime();
        	if(lastSyncTime==0){
        		mSleepRecordNextTime=0;
        		resetStack();	
        	}else{
        		mSleepRecordNextTime=lastSyncTime;
        	}
        	if((mSleepRecordConnectTime-lastSyncTime)<sleepRecordTimeShift){
        		mSleepRecordSessionTotal=(int)(mSleepRecordConnectTime-lastSyncTime);        	
        	}else{
        		mSleepRecordSessionTotal=sleepRecordTimeShift;
        	}        	
    		mSleepRecordSessionSaved=0; //--20151205
            mBinsFragment.isSleepRecordReadBypass=false;
                    	
            Log.i(TAG, "Sleep record valid:time="+validBit+":"+sleepRecordTimeShift);
            
        	if(validBit!=0){  // valid bit
                //-- mHandler.sendEmptyMessage(REFRESH_TIME);      
        		//-- 150610 intermediate update, to avoid not updated after cancel
        		//-- 151103 will confuse sync binsDB.updateSyncTime(mCurrentDeviceID, mConnectedTime);

        		mHandler.sendEmptyMessage(UPLOAD_SLEEP_RECORDS);
        	}else{
      	    	Log.i(TAG, "Sleep time to Start refresh time");
      	    	binsDB.setActiveDeviceLastSyncTime(mSleepRecordConnectTime);
                mHandler.sendEmptyMessage(REFRESH_TIME);                              
        	}

        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_SLEEP_RECORDS)) {
            int sleepRecordArraySize=bytes.length-2;	// the first byte is the valid byte
            int i;
            int sleepRecord;

            //-- Log.i(TAG, "Sleep record size:"+sleepRecordArraySize+" dtag="+bytes[0]+" left:"+bytes[1]);
            // 0412 bytes[0] is valid indicator byte
            for(i=0; i<sleepRecordArraySize; i=i+2){
            	sleepRecord=(bytes[i+2] & 0xFF) + ((bytes[i+3] & 0xFF) << 8);
            	          		                                     
                if((sleepRecord & 0x8000)!=0){	// means ambient light record
                 	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_LIGHT;
                 	mCached.mmActivityTime = mSleepRecordNextTime;
                 	
                }else{
                 	mCached.mmActivityType=Bins.BINS_ACTIVITY_TYPE_SLEEP;   
                 	mCached.mmActivityTime = mSleepRecordNextTime; // vacant one to use
                 	mSleepRecordNextTime=binsDB.nextAvailableSleepTime(mSleepRecordNextTime);
                    if(mCached.mmActivityTime>0){
                    	binsDB.setActiveDeviceLastSyncTime(mSleepRecordNextTime);
                    		//-- to use updateSyncTime
                    	
                    }
                    mSleepRecordSessionSaved++;
                    
                    mBinsFragment.syncSleepRecordViewUpdate(mSleepRecordSessionSaved, mSleepRecordSessionTotal);
                    
                }    	
                 
                mCached.mmActivityValue = sleepRecord & 0x7FFF;
 
                //-- Log.i(TAG, "Sleep record:"+mCached.mmActivityValue+":"+mCached.mmActivityTime+":"+mSleepRecordConnectTime+":"+mSleepRecordStartTimeShift);
                //--Log.i(TAG, "Sleep record:"+bytes[0]+":"+bytes[1]+":"+sleepRecordArraySize+":"+mSleepRecordStartTimeShift);
                // bytes[1] return the length;
                
                
                Log.i(TAG, "Sleep record data:"+
                			mCached.mmActivityTime+":"+
                			mCached.mmActivityType+":"+
                			mCached.mmActivityValue);
                
                if(mCached.mmActivityTime>0){
                	DataSyncFormat.insertOrUpdateActivity(mActivity, mCurrentUserID, mCached);       
                }else{
                	insertToStack(mCached.mmActivityType, mCached.mmActivityValue);
                }
            }
             
      	    if(bytes[0]!=0 &&  mBinsFragment.isSleepRecordReadBypass==false){ // continue read from wearable
    	    	mHandler.sendEmptyMessage(UPLOAD_SLEEP_RECORDS);
      	    }else{      
      	    	if(mSleepRecordNextTime==0){
                    long sleepRecordTime;
                    mSleepRecordNextTime=mSleepRecordConnectTime;
                    
                    mSleepRecordSessionTotal=countOfStack();
                    mSleepRecordSessionSaved=mSleepRecordSessionTotal;
                    
                    
                    ActivityStruct theActivity;

                    while((theActivity=popFromStack())!=null){
                    	int activityType=theActivity.mmActivityType;
                    	int activityValue=theActivity.mmActivityValue;
                    	
                        if(activityType==Bins.BINS_ACTIVITY_TYPE_SLEEP){
                            sleepRecordTime=binsDB.previousAvailableSleepTime(mSleepRecordNextTime);
                        }else{
                            sleepRecordTime=mSleepRecordNextTime;
                        }
                        binsDB.insertOrUpdateActivity(activityType, activityValue, sleepRecordTime);
                                           
                        //-- sleepRecordRead: mSleepRecordSessionSaved-- of: mSleepRecordSessionTotal];
                        mBinsFragment.syncSleepRecordViewUpdate(mSleepRecordSessionSaved--, mSleepRecordSessionTotal);
                    }
      	    	}
                binsDB.setActiveDeviceLastSyncTime(mSleepRecordConnectTime);		
                
      	    	Log.i(TAG, "Sleep Record Ended and request refresh time");
                mHandler.sendEmptyMessage(REFRESH_TIME);                              
      	    }
            
        }
        else if (uuid.equalsIgnoreCase(GattAttributes.CHARACTERISTIC_IBEACON_UUID)) {
        	
            //-- Log.i(TAG, "Ibeacon uuid:"+bytes.length); // 150502s
            
            
            StringBuffer hexString = new StringBuffer();
            
            int i;
            for(i=0; i<16; i++){
            	 //-- Log.i(TAG, "Ibeacon:"+i+":"+Integer.toHexString(0xFF & bytes[i]));
            	 StringBuilder sb = new StringBuilder();
            	 sb.append(Integer.toHexString(0xFF & bytes[i]));
            	 if(sb.length()<2) sb.insert(0, '0');
            	 hexString.append(sb);
            	 if(i==3 || i==5 || i==7 || i==9){
            		 hexString.append("-");
            	 }
            }
            Log.i(TAG, "Ibeacon uuid string:"+hexString); // 150502s
                        
    		BinsDBUtils.activeDB().setIbeaconUuid(hexString.toString());
                 		
            // continue connection process, by writings
       	    writeConfigurations();	//-- write device configurations to device
			//-- delay this end operation until writings finished - mBinsFragment.endRead();    

        }
    	else {
            Log.d(TAG, "onDataRead ... should not run here");
        }
    }

    public void stopRead(){
        if(mHandler!=null) mHandler.removeMessages(READ_ACTIVITY_TIME_VALUE);        
    }
    public void readAgain() {
        if (mTryCounts < MAX_TRY_COUNTS) {
            Log.w(TAG, "found Error when read data " + mCurrentReadIndex + "[" + mTryCounts
                    + " times], will read again");
	    	mBinsFragment.resetBluetooth();	    	
            // -- 12.13, move to run after bluetooth reset finished 
	    	// -- mHandler.sendEmptyMessageDelayed(mCurrentReadIndex, 300); 
            				//-- mCurrentReadIndex is assigned then same as the previous command when first read command
            				//-- do it again
        } else {
            Log.e(TAG, "There is still error when read data" + mCurrentReadIndex + ", will exit!");
            mBinsFragment.meetError();
        }  
    }

    private class DataSyncCallback implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case READ_BATTERY:
                    readBattery();
                    return true;
                case READ_ACTIVITY_VALUE:
                    readActivityValue();
                    return true;
                case READ_ACTIVITY_TYPE:
                    readActivityType();
                    return true;
                case READ_ACTIVITY_TIME:
                    readActivityTime();
                    return true;
                case READ_ACTIVITY_TIME_VALUE:
                    readActivityTimeValue();
                    return true;
                case CHECK_SLEEP_START_TIME:
                	readSleepStartTime();
                	return true;
                case UPLOAD_SLEEP_RECORDS:
                	uploadSleepRecords();
                	return true;
                case READ_CONFIGURATION:
                	readBinsConfiguration();
                	return true;
                case READ_IBEACON_UUID:
                	readIbeaconUuid();
                	return true;
                case REFRESH_TIME:
                    updateRefreshTime();
                    //-- normally it is the end of the session
                    //-- but more extra from writings
                    //-- mBinsFragment.close();
                    return true;
                default:
                    return false;
            }
        }
    }

    public static class ActivityStruct {
        public int mmActivityType;
        public int mmActivityValue;
        public long mmActivityTime;

        public ActivityStruct() {
            reset();
        }

        public void reset() {
            mmActivityType = 0;
            mmActivityValue = 0;
            mmActivityTime = 0;
        }

        public String toString() {
            return "Activity Value = " + mmActivityValue + "\n" +
                    "Activity  Type = " + mmActivityType + "\n" +
                    "Activity  Time = " + mmActivityTime;
        }
    }
    
    

}



