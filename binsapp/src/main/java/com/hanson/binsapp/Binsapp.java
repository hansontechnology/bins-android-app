package com.hanson.binsapp;

import java.util.Collection;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

public class Binsapp extends Application implements BootstrapNotifier, RangeNotifier {
	private static final String TAG = "Binsapp Application";
	private BeaconManager mBeaconManager;
	private Region mAllBeaconsRegion;
	private RegionBootstrap mRegionBootstrap;
	
	@Override 
	public void onCreate() {
		mAllBeaconsRegion = new Region("all beacons", null, null, null);
		//-- mBeaconManager.setDebug(true); // 150503 for test
        mBeaconManager = BeaconManager.getInstanceForApplication(this);
        
        // set the duration of the scan to be 5 seconds
        mBeaconManager.setBackgroundScanPeriod(5000); 
        // set the time between each scan to be 5 seconds
        mBeaconManager.setBackgroundBetweenScanPeriod(5000); 
		
		
        mBeaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        
        mRegionBootstrap = new RegionBootstrap(this, mAllBeaconsRegion);
       
	
        // By default the AndroidBeaconLibrary will only find AltBeacons.  If you wish to make it
        // find a different type of beacon, you must specify the byte layout for that beacon's
        // advertisement with a line like below.  The example shows how to find a beacon with the
        // same byte layout as AltBeacon but with a beaconTypeCode of 0xaabb
        //
        // beaconManager.getBeaconParsers().add(new BeaconParser().
        //        setBeaconLayout("m:2-3=aabb,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
        //
        // In order to find out the proper BeaconLayout definition for other kinds of beacons, do
        // a Google search for "setBeaconLayout" (including the quotes in your search.)
	}
	
	@Override
	public void didRangeBeaconsInRegion(Collection<Beacon> ibeacons, Region arg1) {
		Log.d(TAG, "didRangeBeaconsInRegion");
		
        if (ibeacons.size() > 0) {
            Beacon thisBeacon = ibeacons.iterator().next();
            Log.i(TAG, "beacon is:"+thisBeacon);
        }

		
	}

	@Override
	public void didDetermineStateForRegion(int arg0, Region arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void didEnterRegion(Region arg0) {
		/*
		try {
			Log.d(TAG, "entered region.  starting ranging");
			mBeaconManager.startRangingBeaconsInRegion(mAllBeaconsRegion);
			mBeaconManager.setRangeNotifier(this);
		} catch (RemoteException e) {
			Log.e(TAG, "Cannot start ranging");
		}
		*/
	     Log.d(TAG, "Got a didEnterRegion call");
	        // This call to disable will make it so the activity below only gets launched the first time a beacon is seen (until the next time the app is launched)
	        // if you want the Activity to launch every single time beacons come into view, remove this call.  
	        mRegionBootstrap.disable();
	        Intent intent = new Intent(this, BinsActivity.class);
	        // IMPORTANT: in the AndroidManifest.xml definition of this activity, you must set android:launchMode="singleInstance" or you will get two instances
	        // created when a user launches the activity manually and it gets launched from here.
	        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        this.startActivity(intent);
	}

	@Override
	public void didExitRegion(Region arg0) {		
		Log.d(TAG, "didExitRegion");

	}
	
}
