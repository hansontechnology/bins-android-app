
package com.hanson.binsapp.widget;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;

import com.hanson.binsapp.R;

public class AmbientLightView extends View {
	   
    private static final String TAG = "AmbientLightView";
    
    private final int LIGHT_STAGE_INVALID = -1;
    
    private final Paint mTextPaint;
    private final Paint mLinePaint;
    private final Paint mContentPaint;

    private final Paint mLinePaintActivity;
    private Paint mLinePaintSleepEnds;
    private final Paint mTextPaintHR;
    private final Paint mLinePaintHR;
    //-- private int[] mLightStages=new int[axisCountX];
    private int[] mLightStages;
    private int mNumberOfBars=0;
    private RectF mTmpRectF = new RectF();
    
    public AmbientLightView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Resources resource = context.getResources();
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(resource.getColor(android.R.color.darker_gray));
        mTextPaint.setTextSize(resource.getDimension(R.dimen.sleep_view_text_size));

        mTextPaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaintHR.setColor(resource.getColor(android.R.color.tertiary_text_dark));      
        mTextPaintHR.setTextSize(resource.getDimension(R.dimen.steps_view_coordinate_text_size));

        mLinePaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintHR.setColor(resource.getColor(android.R.color.holo_red_dark));
        mLinePaintHR.setStrokeWidth(2);
        mLinePaintHR.setStyle(Paint.Style.STROKE);

        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setColor(resource.getColor(android.R.color.darker_gray));
        mLinePaint.setStrokeWidth(2);
        mLinePaint.setStyle(Paint.Style.STROKE);

        mLinePaintActivity = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintActivity.setColor(resource.getColor(android.R.color.holo_orange_dark));
        mLinePaintActivity.setStrokeWidth(2);
        mLinePaintActivity.setStyle(Paint.Style.STROKE);

        
        mLinePaintSleepEnds = new Paint(Paint.ANTI_ALIAS_FLAG);        
        mLinePaintSleepEnds.setAntiAlias(true);        
        mLinePaintSleepEnds.setColor(resource.getColor(android.R.color.holo_red_light));
        mLinePaintSleepEnds.setStrokeWidth(8);
        mLinePaintSleepEnds.setStyle(Paint.Style.STROKE);
        PathEffect effects = new DashPathEffect(new float[] {10,20}, 0); 
        mLinePaintSleepEnds.setPathEffect(effects); 

        mContentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mContentPaint.setColor(resource.getColor(android.R.color.holo_orange_light));
        mContentPaint.setStyle(Paint.Style.FILL);

    }

    public void displayUpdate(int samplePeriod, int sampleTimeWindow, int[] lightLevels) {
	   	      
       //-- mNumberOfBars=axisCountX; 
       
       Log.i(TAG, "light displayUpdate");
       
 	   //-- int minutesPerBar= (lightLevels.length*sampleTimeWindow) / mNumberOfBars;
 	  	
       mNumberOfBars=lightLevels.length;
       
       mLightStages=lightLevels;
       
       /*
	   Arrays.fill(mLightStages,LIGHT_STAGE_INVALID);
	   	   
 	   for(int i=0; i<lightLevels.length;i++){
 		   
 		   int index = (int)((i*sampleTimeWindow) / minutesPerBar); 
 		   
 		   if(mLightStages[index]<0){ 
 			   mLightStages[index]=lightLevels[i]; 
 		   }
 		   else{
 			   mLightStages[index]+=lightLevels[i]; 		
 		   }
 		   
 	   }
 	   */
   }
   
    
  
    private static class LevelToBarAttr {
    	int barColor;

    	private LevelToBarAttr( int color) {
            this.barColor=color;
          }
    }
    
    static LevelToBarAttr[] levelToBarAttr = {
    		new LevelToBarAttr(0xff0066CC),	// 0, not a valid value    	
    		new LevelToBarAttr(0xff0066CC),	// 1, deep sleep
    		new LevelToBarAttr(0xff00bfff),	// 2, Restless Sleep
    		new LevelToBarAttr(0xff7ac5cd),	// 3, Resting
    		new LevelToBarAttr(0xffcdc673),	// 4, Couch
    		new LevelToBarAttr(0xffcdc673),	// 5, Desk Work  // 0xffbc8f8f
    		new LevelToBarAttr(0xffffe485),	// 6, Walking
    		new LevelToBarAttr(0xffffe485),	// 7, Jogging
    		new LevelToBarAttr(0xff00fa9a),	// 8, Running
    		new LevelToBarAttr(0xff00fa9a),	// 9, Spiriting
    		new LevelToBarAttr(0xff00fa9a),	// 10, Error condition  
       		new LevelToBarAttr(0xff00fa9a)			// 11, not a valid value 		      	 
    };
    
    @Override
    protected void onDraw(Canvas canvas) {
    	
        super.onDraw(canvas); 
        
        final int startX = getPaddingLeft();
        final int startY = 0; 
        final int width = getWidth() - getPaddingLeft() - getPaddingRight();
        final int height = getHeight() ; 
        final float cellWidth = (float) width / mNumberOfBars;
        
        //-- Log.i(TAG, "light onDraw: startY="+startY+":width="+width+":height="+height);
        // Draw content
        
        Float lightDivider=FloatMath.sqrt(0x7FFF);
        float middleY=(float)startY+ (float)height/2;            

        for (int i = 0; i < mNumberOfBars; i++) 
        {       	
        	
        	mTmpRectF.left = startX + i * cellWidth;
            mTmpRectF.right = startX + (i+1) * cellWidth-2;
            
            int unitColor;
           
            float heightFactor=0;
             
            int lightLevel=mLightStages[i];
            
            if(lightLevel==LIGHT_STAGE_INVALID) continue;

            if(lightLevel>0x7FFF) lightLevel=0x7FFF;
                        
            Float lightStrength=FloatMath.sqrt((float)(lightLevel));
            // Float lightStrength=(float)(lightLevel);
            // Float lightDivider=(float)0x7FFF;
                        
            heightFactor=(lightStrength/lightDivider);
                                    
            float scaleRange=(float)(height/2)*heightFactor;
            
            if(scaleRange<=2){
            	unitColor=levelToBarAttr[(int)(scaleRange)].barColor;
            }else{
            	unitColor=0xffff4500;
            }
            
            mTmpRectF.top = 	middleY-2-scaleRange;            
           	mTmpRectF.bottom = 	middleY+2+scaleRange; 
                        
            //-- Log.i(TAG, "light draw data:"+i+":"+mLightStages[i]+":"+mTmpRectF.left+":"+startY+":"+mTmpRectF.top+":"+heightFactor+":"+unitColor);
            if(mLightStages[i]!=LIGHT_STAGE_INVALID)
            {		           
            	mContentPaint.setColor(unitColor);
            	canvas.drawRect(mTmpRectF, mContentPaint);
            }
            
        } 	//-- end of for loop for every bar
 
        
        // Draw midnight Y axis
        canvas.drawLine(startX+width/2, startY, 
        				startX+width/2, startY + height, mLinePaintActivity);

        return;

        
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d(TAG, "onSaveInstanceState ...");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        if(savedState!=null){
        	savedState.values = mLightStages;
        }
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState ...");

        if(state==null){
        	Log.w(TAG,  "Abnormal restore, null...");
        	return;
        }

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mLightStages = savedState.values;
        invalidate();
    }

    static class SavedState extends BaseSavedState {
        int[] values;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel in) {
            super(in);
            if(values!=null) in.readIntArray(values); // 150909
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeIntArray(values);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
