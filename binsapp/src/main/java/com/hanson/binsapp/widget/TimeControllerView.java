
package com.hanson.binsapp.widget;

import java.util.Calendar;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hanson.binsapp.R;

public class TimeControllerView extends FrameLayout implements View.OnClickListener {
    public static final String TAG = "TimeControllerView";
    public static final String DATE_FORMAT = "EEEE, MMM d, yyyy";
    public static final String DATE_FORMAT_SIMPLE = "EEEE, MMM d";
    public static final String PARCEL_CURRENT_TIME = "current_time";
    public static final int VIEW_MODE_UNIT_DAY    =  0;
    public static final int VIEW_MODE_UNIT_MONTH  =  1;
    public static final int VIEW_MODE_UNIT_HOUR   =  2;

    public static final int DAY_MINUTES = 24 * 60;
    public static final int MONTH_MINUTES = 24 * 60 * 30;

    private int mViewMode=VIEW_MODE_UNIT_DAY;
    private Calendar mThisMonthCalendar;
    
    private Calendar mTodayCalendar;
    private Calendar mCurrentCalendar;
    private ImageView mPrevView;
    private ImageView mNextView;
    private TextView mCurrentDay;

    private OnDateChangedListener mOnDateChangedListener;

    public interface OnDateChangedListener {
        void OnDateChanged(TimeControllerView timeView, long timeInMillis);
    }

    public TimeControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.time_controller, this, true);
        mPrevView = (ImageView) findViewById(R.id.time_prev);
        mNextView = (ImageView) findViewById(R.id.time_next);
        mCurrentDay = (TextView) findViewById(R.id.time_current);

        mPrevView.setOnClickListener(this);
        mNextView.setOnClickListener(this);
        
        setToday();
        
        mCurrentCalendar = (Calendar)(mTodayCalendar.clone());

        setToday();
        /*
        Log.d(TAG, "TODAY  : "
                + DateFormat.format("yyyy-MM-dd HH:mm:ss", mTodayCalendar).toString());
        Log.d(TAG, "CURRENT: "
                + DateFormat.format("yyyy-MM-dd HH:mm:ss", mCurrentCalendar).toString());
        */
        updateViews();
    }

    public void setOnDateChangedListener(OnDateChangedListener listener) {
        mOnDateChangedListener = listener;
    }

    public int getDisplayMode(){
    	return mViewMode;
    }
 
    public void setDisplayMode(int dmode) {
    	mViewMode=dmode;
    	changeDate(0);
    }  
    
    public Calendar startOfToday() {
    	
    	Calendar startToday=(Calendar)mTodayCalendar.clone();
    	
    	startToday.set(Calendar.HOUR_OF_DAY, 0);
    	startToday.set(Calendar.MINUTE, 0);
    	startToday.set(Calendar.SECOND, 0);

    	return startToday;   	
    }
    
    private void updateActionButtons(){
        Calendar nextDate=createNewDate(mCurrentCalendar,1);
        boolean isNextEnable=false;
        /*
        Log.d(TAG,
                "newDate: "
                        + DateFormat.format("yyyy-MM-dd",nextDate).toString()+":"+DateFormat.format("yyyy-MM-dd",mTomorrowCalendar).toString());
   
         */
        if(mViewMode==VIEW_MODE_UNIT_MONTH){
        	isNextEnable=nextDate.after(mThisMonthCalendar);       	              	 
        }
        else if(mViewMode==VIEW_MODE_UNIT_DAY){
        	isNextEnable=nextDate.after(mTodayCalendar);
        }
        
        if(isNextEnable==true) isNextEnable=false;
        else isNextEnable=true;
        
        mNextView.setEnabled(isNextEnable);
    }
    private void updateViews() {
        //-- Log.d(TAG, "updateViews ...mode="+mViewMode);
              
        updateActionButtons();
        
        if(mViewMode==VIEW_MODE_UNIT_MONTH){
            mCurrentDay.setText(DateFormat.format("MMMM,  yyyy", mCurrentCalendar));
        	
        } else if(mViewMode==VIEW_MODE_UNIT_DAY){
           long intervalMilli = mCurrentCalendar.getTimeInMillis() - mTodayCalendar.getTimeInMillis();
           int xcts = (int) (intervalMilli / (24 * 60 * 60 * 1000));
           // -1：yesterday 0：today 1：tomorrow out：display time
           switch (xcts) {
            case -1:
                mCurrentDay.setText(getResources().getString(R.string.Yesterday)
                		+" - "+DateFormat.format(DATE_FORMAT_SIMPLE, mCurrentCalendar));
                break;
            case 0:
                mCurrentDay.setText(getResources().getString(R.string.Today)
                		+" - "+DateFormat.format(DATE_FORMAT_SIMPLE, mCurrentCalendar));
                 break;
            case 1:
                // mCurrentDay.setText("Tomorrow");
                // break;
            default:
                mCurrentDay.setText(DateFormat.format(DATE_FORMAT, mCurrentCalendar));
                break;
           }
        }


    }

    public long getTimeInMillis() {
        return mCurrentCalendar.getTimeInMillis();
    }

    public void setTimeInMillis(long milliseconds) {
        mCurrentCalendar.setTimeInMillis(milliseconds);
        
        setToday();
        
        updateViews();
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick ...");
             
        setToday();
 
        if (v.getId() == R.id.time_prev) {
            mCurrentCalendar=createNewDate(mCurrentCalendar, -1);

        } else if (v.getId() == R.id.time_next) {
            mCurrentCalendar=createNewDate(mCurrentCalendar, 1);       	
        }
        /*
        Log.d(TAG,
                "onClick ... CURRENT: "
                        + DateFormat.format("yyyy-MM-dd HH:mm:ss", mCurrentCalendar).toString());
        */
        updateViews();
        if (mOnDateChangedListener != null) {
            mOnDateChangedListener.OnDateChanged(this, mCurrentCalendar.getTimeInMillis());
        }
    }
    
    private void setToday() {
    	
        //-- update today, for overnight case
        mTodayCalendar = Calendar.getInstance();
        mTodayCalendar.set(Calendar.AM_PM, Calendar.AM);
        mTodayCalendar.set(Calendar.HOUR, 23);
        mTodayCalendar.set(Calendar.MINUTE, 59);
        mTodayCalendar.set(Calendar.SECOND, 59);
        mTodayCalendar.set(Calendar.MILLISECOND, 0);
        
    	Calendar gc = Calendar.getInstance(); // new GregorianCalendar();
    	gc.add(Calendar.DATE, 1);
    	gc.set(Calendar.HOUR_OF_DAY, 0);
    	gc.set(Calendar.MINUTE, 0);
    	gc.set(Calendar.SECOND, 0);
    	
     	Calendar gc2 = Calendar.getInstance(); // new GregorianCalendar();
    	gc2.add(Calendar.MONTH, 1);	// this month
      	gc2.set(Calendar.DAY_OF_MONTH, 1);  	
    	gc2.set(Calendar.HOUR_OF_DAY, 0);
    	gc2.set(Calendar.MINUTE, 0);
    	gc2.set(Calendar.SECOND, 0);
    	
    	mThisMonthCalendar=gc2;   
   	
    }

    public int actualDaysOfTheMonth() {
    	int days = getDate().getActualMaximum(Calendar.DAY_OF_MONTH);
    	return days;
    }
    public Calendar startOfTheMonth() {
    	Calendar date=getDate();
       	date.set(Calendar.DAY_OF_MONTH, 1);
       	date.set(Calendar.HOUR_OF_DAY, 0);
     	date.set(Calendar.MINUTE, 0);
    	date.set(Calendar.SECOND, 0);
    	return date;
    }
    
    public Calendar startOfTheDay() {
    	Calendar date=getDate();
       	date.set(Calendar.HOUR_OF_DAY, 0);
     	date.set(Calendar.MINUTE, 0);
    	date.set(Calendar.SECOND, 0);
    	return date;
    }
    
    public Calendar endOfTheDay() {
    	Calendar date=getDate();
       	date.set(Calendar.HOUR_OF_DAY, 23);
     	date.set(Calendar.MINUTE, 59);
    	date.set(Calendar.SECOND, 0);
    	return date;
    }
    
    public Calendar getDate(){
    	  
    	return (Calendar)(mCurrentCalendar.clone());
    }
    public void moveToToday() {
    	setToday();
    	setDate(mTodayCalendar);
    }
    public void setDate(Calendar newDate) {
    	 	
        setToday();

        boolean isEnable=newDate.after(mTodayCalendar);
       
        // Log.i(TAG, "enable="+isEnable);
        
        if(isEnable==true) return;
     	
    	mCurrentCalendar=(Calendar)(newDate.clone());
    	       
        // Log.i(TAG,"New date:"+mCurrentCalendar.get(Calendar.DATE));
        
        updateViews();
        
        if (mOnDateChangedListener != null) {
            mOnDateChangedListener.OnDateChanged(this, mCurrentCalendar.getTimeInMillis());
        }  	
    	
    }
    Calendar createNewDate(Calendar date, int period){
      
 /*    
        long intervalMilli = mCurrentCalendar.getTimeInMillis() - mTodayCalendar.getTimeInMillis();
        int xcts = (int) (intervalMilli / (24 * 60 * 60 * 1000));
        // -1：yesterday 0：today 1：tomorrow out：display time
 
        if (dayShift<0) {
            mCurrentCalendar.setTimeInMillis(mCurrentCalendar.getTimeInMillis() - 24 * 60 * 60
                    * 1000L);
        } else if ( xcts!=0 && dayShift>0) {
            mCurrentCalendar.setTimeInMillis(mCurrentCalendar.getTimeInMillis() + 24 * 60 * 60
                    * 1000L);
        }
  */
        
        // TODO Gregorian
    	Calendar gc = (Calendar)(date.clone());   // new GregorianCalendar();
    	    	   
    	// Log.i(TAG,"period="+period);
    	
        if(mViewMode==VIEW_MODE_UNIT_DAY){
         	gc.add(Calendar.DATE, period);       	
        }
        else if(mViewMode==VIEW_MODE_UNIT_MONTH){
        	gc.add(Calendar.MONTH, period);
        }        
        
        // if(gc.after(mTomorrowCalendar)){
        // 	return date;
        // }
        
       //  Log.d(TAG,
       //         "newDate: "
       //                 + DateFormat.format("yyyy-MM-dd",gc).toString()+":"+DateFormat.format("yyyy-MM-dd",mCurrentCalendar).toString());
       
        return gc;
        
    }
 /*   
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        NSDateComponents *currentDateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];

        
        if(unit==VIEW_MODE_UNIT_DAY){
            
            currentDateComponents.day+=period;
            
            NSDate *newDate = [calendar dateFromComponents:currentDateComponents];
            
            if([newDate compare:tomorrowDate]==NSOrderedAscending){
                confirmedDate=newDate;
             }
        }
        else if(unit==VIEW_MODE_UNIT_MONTH){
            
            currentDateComponents.month+=period;
            
            NSDate *newDate = [calendar dateFromComponents:currentDateComponents];
            
            if([newDate compare:nextMonthDate]==NSOrderedAscending){
                confirmedDate=newDate;
            }
        }
        
        return confirmedDate;
    }

    - (void)changeDate: (int) periodShift {
        
        [self setToday];
        
        currentDate=[self createNewDate:currentDate shiftUnit:periodShift withUnitOf:browseUnitMode];
        
        if(browseUnitMode==VIEW_MODE_UNIT_DAY){
            self.text=[NSDateFormatter localizedStringFromDate:currentDate  dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];

        } else if(browseUnitMode==VIEW_MODE_UNIT_MONTH){
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM, yyyy"];
            
            self.text=[formatter stringFromDate:currentDate];
        }
        
        [self.delegate timeControllerView:self timeDidChange:currentDate];
    }
 */   
    public void changeDate(int timeShift) {
        // Log.d(TAG, "timeShift:"+timeShift);
               
        setToday();
        
        mCurrentCalendar=createNewDate(mCurrentCalendar, timeShift);
        
        // Log.i(TAG,"New date:"+mCurrentCalendar.get(Calendar.DATE));
        
        updateViews();
        
        if (mOnDateChangedListener != null) {
            mOnDateChangedListener.OnDateChanged(this, mCurrentCalendar.getTimeInMillis());
        }
    }
    
    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d(TAG, "onSaveInstanceState ...");
        // return super.onSaveInstanceState();
        Parcelable parcelable = super.onSaveInstanceState();
        SavedState saveState = new SavedState(parcelable);
        if(saveState!=null && mCurrentCalendar!=null){
        	saveState.currentTime = mCurrentCalendar.getTimeInMillis();
        }
        return saveState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        // super.onRestoreInstanceState(state);
        Log.d(TAG, "onRestoreInstanceState ...");
        if(state==null){
        	Log.w(TAG,  "Abnormal restore, null...");
        	return;
        }
        
        SavedState timeState = (SavedState) state;
        super.onRestoreInstanceState(timeState.getSuperState());
        long currentTime = timeState.currentTime;
        mCurrentCalendar.setTimeInMillis(currentTime);
        updateViews();
    }

    static class SavedState extends BaseSavedState {
        long currentTime;

        public SavedState(Parcelable parcel) {
            super(parcel);
        }

        public SavedState(Parcel parcel) {
            super(parcel);
            currentTime = parcel.readLong();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeLong(currentTime);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
