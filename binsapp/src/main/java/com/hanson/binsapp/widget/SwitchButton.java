package com.hanson.binsapp.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.CheckBox;

import com.hanson.binsapp.R;

public class SwitchButton extends CheckBox {
    private static final int MAX_ALPHA = 255;
    private static final float EXTENDED_OFFSET_Y = 15;

    private Paint mPaint;
    private ViewParent mParent;

    private Bitmap mThumbCurrent;
    private Bitmap mBackground;
    private Bitmap mThumbPressed;
    private Bitmap mThumbNormal;
    private Bitmap mFrame;
    private Bitmap mMask;

    private RectF mSaveLayerRectF;
    private PorterDuffXfermode mXfermode;
    private float mFirstDownX;
    private float mFirstDownY;
    private float mRealPos;     // The position the background will be drawn
    private float mThumbPos;
    private float mThumbOnPos;
    private float mThumbOffPos;
    private float mMaskWidth;
    private float mMaskHeight;
    private float mThumbWidth;
    private float mThumbInitPos;
    private int mClickTimeout;
    private int mTouchSlop;

    private int mAlpha = MAX_ALPHA;
    private boolean mChecked = false;
    private boolean mBroadcasting;
    private boolean mTurningOn;
    private PerformClick mPerformClick;
    private OnCheckedChangeListener mOnCheckedChangeListener;
    private OnCheckedChangeListener mOnCheckedChangeWidgetListener;
    private float mExtendOffsetY; // The extend offset of Y-axis which is used to enlarge the touching zone.
    private ValueAnimator mThumbAnimator;
    private AnimatorUpdateListener mOnUpdateListener = new AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            final float position = (Float) animation.getAnimatedValue();
            doAnimation(position);
        }
    };

    public SwitchButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.checkboxStyle);
    }

    public SwitchButton(Context context) {
        this(context, null);
    }

    public SwitchButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        Resources resources = context.getResources();

        // get viewConfiguration
        mClickTimeout = ViewConfiguration.getPressedStateDuration()
                + ViewConfiguration.getTapTimeout();
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();

        // get Bitmap
        mBackground = BitmapFactory.decodeResource(resources, R.drawable.switch_background);
        mThumbPressed = BitmapFactory.decodeResource(resources, R.drawable.switch_thumb_pressed);
        mThumbNormal = BitmapFactory.decodeResource(resources, R.drawable.switch_thumb_normal);
        mFrame = BitmapFactory.decodeResource(resources, R.drawable.switch_frame);
        mMask = BitmapFactory.decodeResource(resources, R.drawable.switch_mask);
        mThumbCurrent = mThumbNormal;

        mThumbWidth = mThumbPressed.getWidth();
        mMaskWidth = mMask.getWidth();
        mMaskHeight = mMask.getHeight();

        mThumbOffPos = mThumbWidth / 2;
        mThumbOnPos = mMaskWidth - mThumbWidth / 2;

        mThumbPos = mChecked ? mThumbOnPos : mThumbOffPos;
        mRealPos = getRealPos(mThumbPos);

        final float density = getResources().getDisplayMetrics().density;
        mExtendOffsetY = (int) (EXTENDED_OFFSET_Y * density + 0.5f);

        mSaveLayerRectF = new RectF(0, mExtendOffsetY,
                mMask.getWidth(), mMask.getHeight() + mExtendOffsetY);
        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void setEnabled(boolean enabled) {
        mAlpha = enabled ? MAX_ALPHA : MAX_ALPHA / 2;
        super.setEnabled(enabled);
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void toggle() {
        setChecked(!mChecked);
    }

    /**
     * Internal method to set the state of checked. To ensure the smooth of animation,
     * this method will call callbacks with delay.
     * 
     * @param checked
     */
    private void setCheckedDelayed(final boolean checked) {
        this.postDelayed(new Runnable() {
            @Override
            public void run() {
                setChecked(checked);
            }
        }, 10);
    }

    /**
     * <p>
     * Changes the checked state of this button.
     * </p>
     * 
     * @param checked true to check the button, false to uncheck it
     */
    public void setChecked(boolean checked) {

        if (mChecked != checked) {
            mChecked = checked;

            mThumbPos = checked ? mThumbOnPos : mThumbOffPos;
            mRealPos = getRealPos(mThumbPos);
            invalidate();

            // Avoid infinite recursions if setChecked() is called from a
            // listener
            if (mBroadcasting) {
                return;
            }

            mBroadcasting = true;
            if (mOnCheckedChangeListener != null) {
                mOnCheckedChangeListener.onCheckedChanged(SwitchButton.this, mChecked);
            }
            if (mOnCheckedChangeWidgetListener != null) {
                mOnCheckedChangeWidgetListener.onCheckedChanged(SwitchButton.this, mChecked);
            }

            mBroadcasting = false;
        }
    }

    /**
     * Register a callback to be invoked when the checked state of this button
     * changes.
     * 
     * @param listener the callback to call on checked state change
     */
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    /**
     * Register a callback to be invoked when the checked state of this button
     * changes. This callback is used for internal purpose only.
     * 
     * @param listener the callback to call on checked state change
     * @hide
     */
    void setOnCheckedChangeWidgetListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeWidgetListener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        float deltaX = Math.abs(x - mFirstDownX);
        float deltaY = Math.abs(y - mFirstDownY);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                attemptClaimDrag();
                mFirstDownX = x;
                mFirstDownY = y;
                mThumbCurrent = mThumbPressed;
                mThumbInitPos = mChecked ? mThumbOnPos : mThumbOffPos;
                break;
            case MotionEvent.ACTION_MOVE:
                float time = event.getEventTime() - event.getDownTime();
                mThumbPos = mThumbInitPos + event.getX() - mFirstDownX;
                if (mThumbPos >= mThumbOffPos) {
                    mThumbPos = mThumbOffPos;
                }
                if (mThumbPos <= mThumbOnPos) {
                    mThumbPos = mThumbOnPos;
                }
                mTurningOn = mThumbPos > (mThumbOffPos - mThumbOnPos) / 2 + mThumbOnPos;

                mRealPos = getRealPos(mThumbPos);
                break;
            case MotionEvent.ACTION_UP:
                mThumbCurrent = mThumbNormal;
                time = event.getEventTime() - event.getDownTime();
                if (deltaY < mTouchSlop && deltaX < mTouchSlop && time < mClickTimeout) {
                    if (mPerformClick == null) {
                        mPerformClick = new PerformClick();
                    }
                    if (!post(mPerformClick)) {
                        performClick();
                    }
                } else {
                    startAnimation(!mTurningOn);
                }
                break;
        }

        invalidate();
        return isEnabled();
    }

    private final class PerformClick implements Runnable {
        public void run() {
            performClick();
        }
    }

    @Override
    public boolean performClick() {
        startAnimation(!mChecked);
        return true;
    }

    /**
     * Tries to claim the user's drag motion, and requests disallowing any
     * ancestors from stealing events in the drag.
     */
    private void attemptClaimDrag() {
        mParent = getParent();
        if (mParent != null) {
            mParent.requestDisallowInterceptTouchEvent(true);
        }
    }

    /**
     * Transfer thumbPos to realPos
     * 
     * @param thumbPos
     * @return
     */
    private float getRealPos(float thumbPos) {
        return thumbPos - mThumbWidth / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.saveLayerAlpha(mSaveLayerRectF, mAlpha, Canvas.MATRIX_SAVE_FLAG
                | Canvas.CLIP_SAVE_FLAG | Canvas.HAS_ALPHA_LAYER_SAVE_FLAG
                | Canvas.FULL_COLOR_LAYER_SAVE_FLAG | Canvas.CLIP_TO_LAYER_SAVE_FLAG);
        // draw the mask
        canvas.drawBitmap(mMask, 0, mExtendOffsetY, mPaint);
        mPaint.setXfermode(mXfermode);

        // draw background
        canvas.drawBitmap(mBackground, mRealPos, mExtendOffsetY, mPaint);
        mPaint.setXfermode(null);

        // draw the outline
        canvas.drawBitmap(mFrame, 0, mExtendOffsetY, mPaint);

        // draw the thumb
        canvas.drawBitmap(mThumbCurrent, mRealPos, mExtendOffsetY, mPaint);
        canvas.restore();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension((int) mMaskWidth, (int) (mMaskHeight + 2 * mExtendOffsetY));
    }

    private void startAnimation(boolean turnOn) {
        final float destination = turnOn ? mThumbOnPos : mThumbOffPos;
        if (null != mThumbAnimator) mThumbAnimator.cancel();
        mThumbAnimator = ValueAnimator.ofFloat(mThumbPos, destination);
        mThumbAnimator.addUpdateListener(mOnUpdateListener);
        mThumbAnimator.start();
    }

    private void doAnimation(float animationPosition) {
        if (animationPosition <= mThumbOnPos) {
            animationPosition = mThumbOnPos;
            setCheckedDelayed(true);
        } else if (animationPosition >= mThumbOffPos) {
            animationPosition = mThumbOffPos;
            setCheckedDelayed(false);
        }
        moveView(animationPosition);
    }

    private void moveView(float position) {
        mThumbPos = position;
        mRealPos = getRealPos(mThumbPos);
        invalidate();
    }
}
