
package com.hanson.binsapp.widget;

import java.util.Arrays;
import java.util.Random;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hanson.binsapp.R;

public class StepsView extends View {
    private static final String TAG = "StepsView";
    private static final boolean DEBUG = false;
    // 30 minutes
    private static final int AXIS_COUNT_X = 62;
    private static final int AXIS_COUNT_Y = 10;
    private static final int axisCountX[] = { 48, 62, 60};
    private final Paint mTextPaint;
    private final Paint mLinePaint;
    private final Paint mContentPaint;
    private final Paint mTextPaintYCoordinate;

    private final Paint mTextPaintActivity;
    private final Paint mLinePaintActivity;
    
    private final Paint mTextPaintHR;
    private final Paint mLinePaintHR;
    
    private int mMaxValue = 0; //-- Integer.MIN_VALUE;
    private int mMinValue = Integer.MAX_VALUE;
    private int mMaxIndex = 0;
    private int mMinIndex = 0;
    
    private int mHeartRateMaxValue = 0; //-- Integer.MIN_VALUE;
    private int mHeartRateMinValue = Integer.MAX_VALUE;
    
    private int[] mValues = new int[AXIS_COUNT_X];
    private int[] mHeartRateValues = new int[AXIS_COUNT_X];
    private int[] mHeartRateLowValues = new int[AXIS_COUNT_X];
    
    private RectF mTmpRectF = new RectF();
    
    private int mViewMode=0; 
    private int mPageUnit=0;

    private TextView mPulseTitleView=null;
    
    public StepsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Resources resource = context.getResources();
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(resource.getColor(android.R.color.darker_gray));
        mTextPaint.setTextSize(resource.getDimension(R.dimen.steps_view_text_size));
        
        mTextPaintYCoordinate = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaintYCoordinate.setColor(resource.getColor(android.R.color.darker_gray));      
        mTextPaintYCoordinate.setTextSize(resource.getDimension(R.dimen.steps_view_Ycoordinate_text_size));

        mTextPaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaintHR.setColor(resource.getColor(android.R.color.holo_red_dark));      
        mTextPaintHR.setTextSize(resource.getDimension(R.dimen.steps_view_Ycoordinate_text_size));

        mLinePaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintHR.setColor(resource.getColor(android.R.color.holo_red_dark));
        mLinePaintHR.setStrokeWidth(2);
        mLinePaintHR.setStyle(Paint.Style.STROKE);

        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setColor(resource.getColor(android.R.color.darker_gray));
        mLinePaint.setStrokeWidth(2);
        mLinePaint.setStyle(Paint.Style.STROKE);



        mTextPaintActivity = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaintActivity.setColor(resource.getColor(android.R.color.holo_blue_dark));      
        mTextPaintActivity.setTextSize(resource.getDimension(R.dimen.steps_view_text_size));

        mLinePaintActivity = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintActivity.setColor(resource.getColor(android.R.color.holo_blue_dark));
        mLinePaintActivity.setStrokeWidth(2);
        mLinePaintActivity.setStyle(Paint.Style.STROKE);
        
        mContentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mContentPaint.setColor(resource.getColor(android.R.color.holo_orange_light));
        mContentPaint.setStyle(Paint.Style.FILL);

        resetSteps(0,0); //-- initial view mode is 0
    }

    // For test
    public void initValues() {
        int minValue = 0;
        Random random = new Random();
        for (int i = 0; i < mValues.length; i++) {
            mValues[i] = random.nextInt(1000);
            if (mMaxValue < mValues[i]) {
                mMaxValue = mValues[i];
                mMaxIndex = i;
            }
            if (minValue > mValues[i]) {
                minValue = mValues[i];
                mMinIndex = i;
            }
        }

        mMaxValue = (mMaxValue / 10 + 1) * 10;
    }

    public void resetSteps(int vm, int pu, TextView tv) {
        
        mPulseTitleView=tv; 
               
        resetSteps(vm, pu);
    }


    public void resetSteps(int vm, int pu) {
        //-- Log.d(TAG, "resetStep ...");
        mMaxValue = 0; //-- Integer.MIN_VALUE;
        mMinValue = Integer.MAX_VALUE;
        mMaxIndex = 0;
        mMinIndex = 0;

        mHeartRateMaxValue = 0; //-- Integer.MIN_VALUE;
        mHeartRateMinValue = Integer.MAX_VALUE;
        
        Arrays.fill(mValues, 0);
        
        Arrays.fill(mHeartRateValues, 0);
        Arrays.fill(mHeartRateLowValues, 400);
        
        mViewMode=vm;
        mPageUnit=pu;
    }

    private int getIndexOfBarGraph(int time){
        int stepWidth;
        
        if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_DAY) 
        	stepWidth= TimeControllerView.DAY_MINUTES / axisCountX[mViewMode];
        else if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_MONTH) 
        	stepWidth= TimeControllerView.MONTH_MINUTES / axisCountX[mViewMode];  
        else{
        	stepWidth= 60/axisCountX[mViewMode];	// 60 minutes in one hour
        }
        int index = time / stepWidth;
         
       // Log.e(TAG, "setSteps more: index="+index+" stepWidth="+stepWidth+" time="+time);
        if(index>=axisCountX[mViewMode]) index=(axisCountX[mViewMode]-1);
        if(index<0){ //-- 08.14
        	index=0;
        	Log.e(TAG, "setSteps: wrong time assigned, time was:"+time);
        }
        
 
    	return index;
    }
    public void setSteps(int time, int steps) {
    	
    	int index=getIndexOfBarGraph(time);
    	
        mValues[index] += steps;
        
        if (DEBUG) 
        {
            Log.d(TAG, "setSteps ... index = " + index + ", steps = " + steps);
        }
    }
    
    public void setHeartRate(int time, int hr){
        
       	int index=getIndexOfBarGraph(time);
              
        // 8.20
        if(hr<40) hr=40;
        if(hr>200) hr=200;
        
        if(hr>mHeartRateValues[index]) mHeartRateValues[index]=hr;
        if(hr<mHeartRateLowValues[index]) mHeartRateLowValues[index]=hr;

        if (DEBUG) {
            Log.d(TAG, "setHeartRate ... index = " + index + ", hr = " + hr);
        }    	
    }
    public void calculateMaxAndMinValue() {
        //-- Log.d(TAG, "calculateMaxAndMinValue ...");
        for (int i = 0; i < mValues.length; i++) {
            if (DEBUG) {
                Log.d(TAG, "mValues[" + i + "] = " + mValues[i]);
            }
            if (mMaxValue < mValues[i]) {
                mMaxValue = mValues[i];
                mMaxIndex = i;
            }
            if (mMinValue > mValues[i] && mValues[i] != 0) {
                mMinValue = mValues[i];
                mMinIndex = i;
            }
        }
        /*
        Log.d(TAG, "mMinIndex = " + mMinIndex);
        Log.d(TAG, "mMaxIndex = " + mMaxIndex);
        Log.d(TAG, "mMinValue = " + mMinValue);
        Log.d(TAG, "mMaxValue = " + mMaxValue);
        */
        mMaxValue += mMaxValue / 10;
        
        if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_DAY) mMaxValue = ((mMaxValue+100)/100)*100;  //-- times of 100
        else if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_MONTH) mMaxValue = ((mMaxValue+100)/100)*100;  //-- times of 100
        else mMaxValue = ((mMaxValue+5)/10)*10; 
        
        //-- for heart rate
        for (int i = 0; i < mHeartRateValues.length; i++) {
            if (mHeartRateMaxValue < mHeartRateValues[i]) {
                mHeartRateMaxValue = mHeartRateValues[i];
                if(mHeartRateMaxValue>200) mHeartRateMaxValue=200;
            }
            if (mHeartRateMinValue > mHeartRateValues[i] && mHeartRateValues[i] != 0) {
                mHeartRateMinValue = mHeartRateValues[i];
                
                if(mHeartRateMinValue<40) mHeartRateMinValue=40;

            }
        }

        if(mHeartRateMaxValue==0) return;
        
        mHeartRateMaxValue += mHeartRateMaxValue / 10;
        
        mHeartRateMaxValue = ((mHeartRateMaxValue+10)/10)*10;  //-- times of 10
                
    }

    public boolean isLoaded() {
        return mMinValue != Integer.MAX_VALUE && mMaxIndex != Integer.MIN_VALUE;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
           
        if(mPulseTitleView==null) return; //-- 08.11, not assigned user yet

        final int textHeight = (int) -mTextPaint.getFontMetrics().top;

        final int defaultPaddingLeft=getPaddingLeft();
        final int defaultPaddingTop=getPaddingTop();
        final int defaultPaddingRight=getPaddingRight();
        final int defaultPaddingBottom=getPaddingBottom();
        
        // setPadding(30,30,40,40);
        
        int activePaddingLeft=defaultPaddingLeft;
        int activePaddingTop=defaultPaddingTop;
        int activePaddingRight=defaultPaddingRight;
        int activePaddingBottom=defaultPaddingBottom;
        
        activePaddingLeft+=40;
        
        final int startX = activePaddingLeft;
        final int startY = activePaddingTop;
        final int width = getWidth() - activePaddingLeft - activePaddingRight;
        final int height = getHeight() - activePaddingBottom - activePaddingTop - 2 * textHeight;
        final float cellWidth = (float) width / axisCountX[mViewMode];
        final float cellHeight = (float) height / AXIS_COUNT_Y;
        if (cellWidth == 0 || cellHeight == 0) {
            Log.e(TAG, "Error: onDraw ... cellWidth = " + cellWidth + ", cellHeight = "
                    + cellHeight);
        }

        // Draw axis
        for (int i = 1; i <= AXIS_COUNT_Y; i++) {
            canvas.drawLine(startX, startY + i * cellHeight, startX + width, startY
                     + i * cellHeight, mLinePaint);
        }

        //-- textHeight
        canvas.drawLine(startX, startY+ textHeight / 1.5f, 
        				startX, startY + AXIS_COUNT_Y * cellHeight, mLinePaintActivity);

        
        if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_DAY)
        {    
        	int startX2= startX + width / 12;
        
        	// Draw axis text
        	canvas.drawText( "2", startX2 ,                 startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText( "6", startX2 + width / 6 ,     startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("10", startX2 + width * 2 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("14", startX2 + width * 3 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("18", startX2 + width * 4 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("22", startX2 + width * 5 / 6  , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                 mTextPaint);
        	
        	canvas.drawText(getResources().getString(R.string.page_day_view), width /2 - 20 , startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint); 
        } else if(mViewMode==TimeControllerView.VIEW_MODE_UNIT_MONTH)
        {    
        	float locX= startX + 5;
        
        	float locY=startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight;
        	
        	// Draw axis text
        	canvas.drawText( "1", locX ,                 locY, mTextPaint);
        	canvas.drawText( "6", locX + width / 6 ,     locY, mTextPaint);
        	canvas.drawText("11", locX + width * 2 / 6 , locY, mTextPaint);
        	canvas.drawText("16", locX + width * 3 / 6 , locY, mTextPaint);
        	canvas.drawText("21", locX + width * 4 / 6 , locY, mTextPaint);
        	canvas.drawText("26", locX + width * 5 / 6 , locY, mTextPaint);
        	    	
        	canvas.drawText(getResources().getString(R.string.page_month_view), width /2 - 20 , startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint); 
        	
	
        } else{	//-- hour based resolution
        
            int startX2= startX + width / 12;
            
            // Draw axis text
            canvas.drawText( "10", startX2 ,                 startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint);
            canvas.drawText( "20", startX2 + width / 6 ,     startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint);
            canvas.drawText("30", startX2 + width * 2 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint);
            canvas.drawText("40", startX2 + width * 3 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint);
            canvas.drawText("50", startX2 + width * 4 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint);
            canvas.drawText("60", startX2 + width * 5 / 6  , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                     mTextPaint);
      	
            canvas.drawText(getResources().getString(R.string.page_hour_view)+" "+mPageUnit+":00", width /2 - 20 , startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                    mTextPaint); 
        }
    
        
        int step=mMaxValue/5 ; // mMaxValue/5;
        if(step<50) step=50;
        float indexMax=mMaxValue/step ; // mMaxValue/step;
        int startYCoordinate=startY+height;
        int startXCoordinate=defaultPaddingLeft-3;
        float stepYCoordinate=height/indexMax;
        {
          float j=0;
          for (int i=0; i<= 9000 ; i=i+step, j=j+stepYCoordinate) {
             String strLabel=String.valueOf(i);
            
            Rect bounds = new Rect();
            mTextPaintYCoordinate.getTextBounds(strLabel, 0, 1, bounds);
            float heightAdjustLabel=mTextPaintYCoordinate.measureText(strLabel)/2;
            
            canvas.save();
            if(i!=0) canvas.rotate(-70, startXCoordinate, startYCoordinate-j+heightAdjustLabel);

            canvas.drawText(strLabel, startXCoordinate, startYCoordinate-j+heightAdjustLabel, mTextPaintYCoordinate);           

            // String strHeight=String.valueOf(bounds.width());
            // Log.i(TAG, strHeight);
            canvas.restore();
          }
        }

        
        

        //-- canvas.drawText(TIME_STOP, startX + width - mTextPaint.measureText(TIME_STOP), startY + 2
        //--         * textHeight + AXIS_COUNT_Y * cellHeight,
        //--         mTextPaint);
        //-- canvas.drawText(String.valueOf(mMaxValue), startX, startY + textHeight / 1.5f, mTextPaint);
        // canvas.drawText(String.valueOf(mMaxValue), startX, startY, mTextPaintActivity);

        // Draw content
        final int contentStartY = startY + (int) (AXIS_COUNT_Y * cellHeight);
        final float contentCellHeight = (float) mMaxValue / height;
        for (int i = 0; i < mValues.length; i++) {
            mTmpRectF.left = startX + i * cellWidth + cellWidth / 2 - cellWidth / 3;
            mTmpRectF.top = startY + height - mValues[i] / contentCellHeight;
            mTmpRectF.right = startX + i * cellWidth + cellWidth / 2 + cellWidth / 3;
            mTmpRectF.bottom = contentStartY;
            if (i == mMaxIndex) {
                mContentPaint.setColor(getContext().getResources().getColor(
                        android.R.color.holo_blue_dark));
            } else if (i == mMinIndex) {
                mContentPaint.setColor(getContext().getResources().getColor(
                        android.R.color.holo_blue_light));
            } else {
                mContentPaint.setColor(getContext().getResources().getColor(
                        android.R.color.holo_blue_light));
            }
            canvas.drawRect(mTmpRectF, mContentPaint);
        }       

        
        final float contentCellHeightHR = (float) (mHeartRateMaxValue-40) / height;
 
        if(mHeartRateMaxValue!=0){       		
        	
            mPulseTitleView.setVisibility(View.VISIBLE);

            canvas.drawLine(startX+width, startY+ textHeight / 1.5f, 
    				startX+width, startY + AXIS_COUNT_Y * cellHeight, mLinePaintHR);

            /* 20151115 
            if(String.valueOf(mHeartRateMaxValue).length()<3){
            	canvas.drawText(String.valueOf(mHeartRateMaxValue), startX+width-30, startY, mTextPaintHR);
            }
            else{
            	canvas.drawText(String.valueOf(mHeartRateMaxValue), startX+width-60, startY, mTextPaintHR);            	
            }
            */
        	  
            Paint mTextPaintBG = new Paint(Paint.ANTI_ALIAS_FLAG);
            mTextPaintBG.setColor(getResources().getColor(android.R.color.background_light));   
 
            

            int pulseGap=mHeartRateMaxValue/5;
            if(pulseGap<20) pulseGap=20;
            float pulseIndexMax=mHeartRateMaxValue/pulseGap ; // mMaxValue/step;
            float pulseYCoordinate=height/pulseIndexMax;
            {
              float j=0;            
              for (int i=0; i<=mHeartRateMaxValue; i=i+pulseGap, j=j+pulseYCoordinate) {
            	
            	if(i==0) continue;
                String strLabel=String.valueOf(i);
                float heightAdjustLabel=mTextPaintHR.measureText(strLabel)/2;                             
                float yPos=startYCoordinate-j+heightAdjustLabel;
                
                int xGap=20;
                if(strLabel.length()>2){
                	xGap+=10;
                }
        
                canvas.save();
                
                canvas.drawCircle(
                		startX+width-xGap+22, 
                		yPos-15, 
                		40, mTextPaintBG);
            
                canvas.drawText(strLabel, 
                		startX+width-xGap, 
                		yPos, 
                		mTextPaintHR);
                
                canvas.restore();
              }  
                //-- (startY+ height - (60-40)/contentCellHeightHR)
            }
        }
        else{
            mPulseTitleView.setVisibility(GONE);        	
        }
        
        float lastCx=0, lastCy=0;
        
        for (int i = 0; i < mHeartRateValues.length; i++) {
        	
        	if(mHeartRateValues[i]==0) continue;
        	
            float cx = startX + i * cellWidth + cellWidth / 2 - cellWidth / 3;
            float cy = startY + height - ((mHeartRateValues[i]-40)/contentCellHeightHR);

            mContentPaint.setColor(getContext().getResources().getColor(
                        android.R.color.holo_red_dark));
            canvas.drawCircle(cx, cy, 5, mContentPaint);

            
            if((mHeartRateLowValues[i]>0)) //- && (mHeartRateValues[i]-mHeartRateLowValues[i])>10)
            {
            	
                float ccx = startX + i * cellWidth + cellWidth / 2 - cellWidth / 3;
                float ccy = startY + height - ((mHeartRateLowValues[i]-40)/contentCellHeightHR);

                Resources resource2 = getContext().getResources();
                Paint mLinePaintHR2 = new Paint(Paint.ANTI_ALIAS_FLAG);
                mLinePaintHR2.setColor(resource2.getColor(android.R.color.holo_red_light));
                mLinePaintHR2.setStrokeWidth(5);               
                
                mContentPaint.setColor(getContext().getResources().getColor(
                        android.R.color.holo_red_light));
                canvas.drawCircle(ccx, ccy, 5, mContentPaint);
                
                canvas.drawLine(ccx, ccy, cx, cy, mLinePaintHR2);           	            	
            }
            
            if(i>0 && !(lastCx==0 && lastCy==0)){
                //-- canvas.drawLine(lastCx, lastCy, cx, cy, mLinePaintHR);           	
            }
            
            lastCx=cx;
            lastCy=cy;
            
            canvas.drawLine(startX, startY+ textHeight / 1.5f, 
    				startX, startY + AXIS_COUNT_Y * cellHeight, mLinePaintActivity);
           
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d(TAG, "onSaveInstanceState ...");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        if(savedState!=null){
        	savedState.minValue = mMinValue;
        	savedState.maxValue = mMaxValue;
        	savedState.minIndex = mMinIndex;
        	savedState.maxIndex = mMaxIndex;
        	savedState.values = mValues;
        }
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState ...");

        if(state==null){
        	Log.w(TAG,  "Abnormal restore, null...");
        	return;
        }

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mMinValue = savedState.minValue;
        mMaxValue = savedState.maxValue;
        mMinIndex = savedState.minIndex;
        mMaxIndex = savedState.maxIndex;
        mValues = savedState.values;
        invalidate();
    }

    static class SavedState extends BaseSavedState {
        int minValue;
        int maxValue;
        int minIndex;
        int maxIndex;
        int[] values;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel in) {
            super(in);
            minValue = in.readInt();
            maxValue = in.readInt();
            minIndex = in.readInt();
            maxIndex = in.readInt();
            if(values!=null) in.readIntArray(values);	//-- 150615 avoid fault
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(minValue);
            dest.writeInt(maxValue);
            dest.writeInt(minIndex);
            dest.writeInt(maxIndex);
            dest.writeIntArray(values);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
