
package com.hanson.binsapp.widget;

import java.util.Arrays;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.hanson.binsapp.R;

public class SleepView extends View {
	
	
    public int mLightSleepTime;
    public int mDeepSleepTime;
    public int mTotalSleepTime;

   
    private static final String TAG = "SleepView";
    // 30 minutes
    private static final int AXIS_COUNT_Y = 10;
    private int mViewMode=0; 
    private static final int axisCountX[] = { 48, 72, 96};
    
	//-- 150616 must to match to LevelToBarAttr table
	public static final int SLEEP_STAGE_DEEP_SLEEP = 1;
	public static final int SLEEP_STAGE_LIGHT_SLEEP = 2;
	public static final int SLEEP_STAGE_LIGHT_WAKE =  3;
	public static final int SLEEP_STAGE_NORMAL_WAKE = 4;	
	public static final int SLEEP_STAGE_ACTIVE = 7;
	public static final int SLEEP_STAGE_INVALID = 100;
	
    private final Paint mTextPaint;
    private final Paint mLinePaint;
    private final Paint mContentPaint;

    private final Paint mLinePaintActivity;
    private Paint mLinePaintSleepEnds;
    
    
    private final Paint mTextPaintHR;
    private final Paint mLinePaintHR;
       
    
    private int[] mThresholds=null;
    private int	  mMinutesPerBar=0;
    private int[] mSleepStages=new int[100];
 
    private int mSleepStartBarIndex=-1;
    private int mSleepEndBarIndex=-1;

    private int mNumberOfBars=0;
    
    private RectF mTmpRectF = new RectF();
    
    public SleepView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Resources resource = context.getResources();
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setColor(resource.getColor(android.R.color.darker_gray));
        mTextPaint.setTextSize(resource.getDimension(R.dimen.sleep_view_text_size));

        mTextPaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaintHR.setColor(resource.getColor(android.R.color.tertiary_text_dark));      
        mTextPaintHR.setTextSize(resource.getDimension(R.dimen.steps_view_coordinate_text_size));

        mLinePaintHR = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintHR.setColor(resource.getColor(android.R.color.holo_red_dark));
        mLinePaintHR.setStrokeWidth(2);
        mLinePaintHR.setStyle(Paint.Style.STROKE);

        mLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaint.setColor(resource.getColor(android.R.color.darker_gray));
        mLinePaint.setStrokeWidth(2);
        mLinePaint.setStyle(Paint.Style.STROKE);

        mLinePaintActivity = new Paint(Paint.ANTI_ALIAS_FLAG);
        mLinePaintActivity.setColor(resource.getColor(android.R.color.holo_orange_dark));
        mLinePaintActivity.setStrokeWidth(2);
        mLinePaintActivity.setStyle(Paint.Style.STROKE);

        
        mLinePaintSleepEnds = new Paint(Paint.ANTI_ALIAS_FLAG);        
        mLinePaintSleepEnds.setAntiAlias(true);        
        mLinePaintSleepEnds.setColor(resource.getColor(android.R.color.holo_red_light));
        mLinePaintSleepEnds.setStrokeWidth(8);
        mLinePaintSleepEnds.setStyle(Paint.Style.STROKE);
        PathEffect effects = new DashPathEffect(new float[] {10,20}, 0); 
        mLinePaintSleepEnds.setPathEffect(effects); 

        mContentPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mContentPaint.setColor(resource.getColor(android.R.color.holo_orange_light));
        mContentPaint.setStyle(Paint.Style.FILL);

    }

       
   private boolean checkSleepEnd(int index){
	   int wakeSum=0;
	   
	   for(int i=0; i<3; i++){
		   /*
		   if(mSleepStages[i+index]<=SLEEP_STAGE_LIGHT_SLEEP && 
				   mSleepStages[i+index]!=SLEEP_STAGE_INVALID) sleepSum++;
		   */
		   if(mSleepStages[i+index]>=SLEEP_STAGE_LIGHT_WAKE && 
				   mSleepStages[i+index]!=SLEEP_STAGE_INVALID) wakeSum++;
	   }
	   
	   if(wakeSum>=2) return true;
	   
	   return false;
   }
   
   private void setThresholds(int[] thresholds, int minutesPerBar) {
	   mThresholds=thresholds;
	   mMinutesPerBar=minutesPerBar;
	   if(mMinutesPerBar<=0){ Log.i(TAG, "wrong minutes per bar:"+mMinutesPerBar); mMinutesPerBar=1;}
   }
   private int getThreshIdFromMovements(int movements){
	   int movPerMinute=(movements/mMinutesPerBar);	// the input is for a total of the minutes per bar
	   
	   if(movements<0) return -1;
	   
	   int idFound=-1;
	   int i;
	   for(i=1; i<mThresholds.length; i++){
		   if(mThresholds[i]>movPerMinute) break;
		   if(mThresholds[i]==-1) break;
	   }
	   if(mThresholds[i]==-1) i--;
	   idFound=i;
	   
	   //-- Log.i(TAG, "sleep thresh="+movPerMinute+":"+idFound+":");
	   return idFound;
   }
   
   public void displayUpdate(int samplePeriod, int sampleTimeWindow, int[] thresholds, int[] activityMovements) {
	   	   
	   mViewMode=2;	// force to set to 2, more bars 
	   
       mNumberOfBars=axisCountX[mViewMode]; 
       
       mSleepStartBarIndex=-1;
       mSleepEndBarIndex=-1;
       
       mLightSleepTime=mDeepSleepTime=mTotalSleepTime=0; 
   

 	   int minutesPerBar= (activityMovements.length*sampleTimeWindow) / mNumberOfBars;
 	  	
	   setThresholds(thresholds, minutesPerBar);
	   
	   int[] mSleepQualityRecords= new int[mNumberOfBars+1];
	   
	   Arrays.fill(mSleepQualityRecords, -1);
	   Arrays.fill(mSleepStages,SLEEP_STAGE_INVALID);
	   
	   //-- Log.i(TAG, "sleep sample window:"+activityMovements.length+":"+sampleTimeWindow+":"+minutesPerBar);

	   
	   
 	   for(int i=0; i<activityMovements.length;i++){
 		   
 		   int index = (int)((i*sampleTimeWindow) / minutesPerBar); 
 		   
  		   
 		   if(mSleepQualityRecords[index]<0){ 
 			   mSleepQualityRecords[index]=activityMovements[i]; 
 		   }
 		   else{
 			   mSleepQualityRecords[index]+=activityMovements[i]; 		
 		   }
 		   
		   //-- Log.i(TAG, "sleep quality="+i+":"+index+":"+mSleepQualityRecords[index]+":"+activityMovements[i]);

 	   }
 	   
 	   for(int i=0; i<mNumberOfBars;i++){
 		   if(mSleepQualityRecords[i]>=0){
 	            int normalizedMovements= (int)((float)(mSleepQualityRecords[i]/mMinutesPerBar)*sampleTimeWindow);

 	            mSleepStages[i]=getThreshIdFromMovements(normalizedMovements);
 	            //-- Log.i(TAG, "sleep stages="+i+":"+normalizedMovements);
 		   }
 	   }
 	   
 	   updateSleepQuality();
   }
   
    
   public void updateSleepQuality() { 
	   

       /* respect to every analysis */
   	   //-- normalize if only short period of time 
       for (int i = 0; i < mNumberOfBars; i++) {
           if(i<(mSleepStages.length-2) && 
           		mSleepStages[i]==mSleepStages[i+2] && 
           		mSleepStages[i+1]!=mSleepStages[i] ){
        	   
           		mSleepStages[i+1]=mSleepStages[i];
           		
           }
       } 
       
       
       for (int i = 0; i < mNumberOfBars; i++) {
           if(i>=2 && i<mNumberOfBars-2 && 
       /* 20150829
        		mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP &&
        		mSleepStages[i-2]>=SLEEP_STAGE_LIGHT_WAKE &&
        	    mSleepStages[i-1]>=SLEEP_STAGE_LIGHT_WAKE &&
        	    mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE &&
        	    mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE    
        */	    
              	mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP &&
            	mSleepStages[i-1]>=SLEEP_STAGE_LIGHT_WAKE &&
            	    mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE
        	)
        	   
           {
           		mSleepStages[i]=mSleepStages[i-1];
           }
       } 

       /*
       //-- 20150830
       for (int i = 0; i < mNumberOfBars; i++) {
           if(i>=2 && i<mNumberOfBars-2 && 
              	mSleepStages[i]<=SLEEP_STAGE_DEEP_SLEEP &&
            	mSleepStages[i-1]>SLEEP_STAGE_DEEP_SLEEP &&
            	    mSleepStages[i+1]>SLEEP_STAGE_DEEP_SLEEP
        	)
        	   
           {
           		mSleepStages[i]=mSleepStages[i-1];
           }
       } 
	   */
       
       mSleepStartBarIndex=-1;
       mSleepEndBarIndex=-1;
       
       //-- if not in a sleep time, then it only can be light wake-up, no sleep stages assigned
       for (int i = 0; i < mNumberOfBars; i++) {
           if(mSleepStartBarIndex<0 &&
        		   mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE &&
        		   (	
        			  ((i<(mNumberOfBars-1) && mSleepStages[i+1]==SLEEP_STAGE_DEEP_SLEEP) || 
        		   		(i<(mNumberOfBars-2) && 
        				  mSleepStages[i+1]==SLEEP_STAGE_LIGHT_SLEEP && 
        				  (mSleepStages[i+2]<=SLEEP_STAGE_LIGHT_SLEEP	  ))
        			  )
        		   ) 
        	  )
           {
        	   mSleepStartBarIndex=i+1;
           }
                               
       }       
       
       /* For early sleep situation, double confirm if it is true
        * 
        */
       int barsPerHour=mNumberOfBars/24; // how many bars in one hour
       if(mSleepStartBarIndex<=(barsPerHour*9)){ // if before 9PM, early sleep, re-confirming 
    	   for (int i = mSleepStartBarIndex+1; i < (barsPerHour*12); i++) {
    		   if(	mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE && 
    				mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE &&
    				mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE &&
    				mSleepStages[i+3]<=SLEEP_STAGE_LIGHT_SLEEP &&
    				mSleepStages[i+3]<=SLEEP_STAGE_LIGHT_SLEEP 		)
    		   {      
    			   mSleepStartBarIndex=i+3;
    		   }
    	   }
       }
       
       if(mSleepStartBarIndex<0){
    	   mSleepStartBarIndex=0; 
       }
       
       for (int i = mSleepStartBarIndex; i < mNumberOfBars; i++) {  
    	       	   
           //-- 150618
           if(
     			  mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE && 
          				  mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE	&&
          				 i>=(barsPerHour*19) )		//-- morning time
           {       	   
        	   break;
           }  
           
  		   if(!(mSleepStages[i]==SLEEP_STAGE_DEEP_SLEEP || 
		   		(i>=1 && 
				  mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP && 
 				  mSleepStages[i-1]<=SLEEP_STAGE_LIGHT_WAKE	 &&
 				  mSleepStages[i]!=0))
 				)
  		   {
  			   continue;
  		   }
			  
  		   //-- Log.i(TAG, "sleep-end 2:"+i+":"+mSleepStages[i]+":"+mSleepStages[i+1]+":"+mSleepStages[i+2]);
  		   
  		   boolean foundSleepEnd=false;
  		   
           if(	(i<(mNumberOfBars-2) && (mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE || mSleepStages[i+2]==0))
        		   &&
        		(i<(mNumberOfBars-1) && (mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE || mSleepStages[i+1]==0))
        	)
           {
        	   foundSleepEnd=true;
           }
           
           if(i<(mNumberOfBars-3) && checkSleepEnd(i+1)==true){
        	   foundSleepEnd=true;
           }
           
           if(i<(mNumberOfBars-2) && mSleepStages[i+1]==0 && mSleepStages[i+2]==0){
        	   foundSleepEnd=true;
           }
        	
           if(foundSleepEnd==true)
           {
        	   mSleepEndBarIndex=i;
           }           
       		
    
       }
       
       //-- if not in a sleep time, then it only can be light wake-up, no sleep stages assigned
       mLightSleepTime=mDeepSleepTime=mTotalSleepTime=0;

              
	   // Log.i(TAG,"sleep period :"+sleepPeriodStartTimeIndex+":"+sleepPeriodEndTimeIndex);

       for (int i = 0; i < mNumberOfBars; i++) {
    	       	   
    	   if(i<mSleepStartBarIndex) continue;
    	   if(i>mSleepEndBarIndex && mSleepEndBarIndex>0) continue;
    	   
    	   int threshId=mSleepStages[i];
    	   
    	   if(threshId==0) continue;
    	   
           if(threshId<=SLEEP_STAGE_DEEP_SLEEP){ 
           	 	mDeepSleepTime++;
           }
           else if(threshId<=SLEEP_STAGE_LIGHT_SLEEP){
       	   		mLightSleepTime++;
           }
       } 
       
       mDeepSleepTime *= mMinutesPerBar;
       mLightSleepTime *= mMinutesPerBar;
       mTotalSleepTime = mDeepSleepTime + mLightSleepTime;
              
    }

    private static class LevelToBarAttr {
    	int barColor;
    	float barHeight;
    	
    	private LevelToBarAttr( int color, float height) {
            this.barColor=color;
            this.barHeight=height;
          }
    }
    
    static LevelToBarAttr[] levelToBarAttr = {
    		new LevelToBarAttr(0xff0066CC, 1),	// 0, not a valid value    	
    		new LevelToBarAttr(0xff0066CC, 1),	// 1, deep sleep
    		new LevelToBarAttr(0xff00bfff, 8.5f/10.0f),	// 2, Restless Sleep
    		new LevelToBarAttr(0xff7ac5cd, 7.0f/10.0f),	// 3, Resting
    		new LevelToBarAttr(0xffcdc673, 5.5f/10.0f),	// 4, Couch
    		new LevelToBarAttr(0xffcdc673, 4.5f/10.0f),	// 5, Desk Work  // 0xffbc8f8f
    		new LevelToBarAttr(0xffffe485, 3.5f/10.0f),	// 6, Walking
    		new LevelToBarAttr(0xffffe485, 3.3f/10.0f),	// 7, Jogging
    		new LevelToBarAttr(0xff00fa9a, 2.8f/10.0f),	// 8, Running
    		new LevelToBarAttr(0xff00fa9a, 2.0f/10.0f),	// 9, Spiriting
    		new LevelToBarAttr(0xff00fa9a, 1.0f/10.0f),	// 10, Error condition  
       		new LevelToBarAttr(0xff00fa9a, 0)			// 11, not a valid value 		      	 
    };
    
    @Override
    protected void onDraw(Canvas canvas) {
    	
        super.onDraw(canvas); 
        Log.i(TAG, "SleepView onDraw start");
        	
        final int textHeight = (int) -mTextPaint.getFontMetrics().top;

        final int startX = getPaddingLeft();
        final int startY = getPaddingTop();
        final int width = getWidth() - getPaddingLeft() - getPaddingRight();
        final int height = getHeight() - getPaddingBottom() - getPaddingTop() - 2 * textHeight;
        final float cellWidth = (float) width / axisCountX[mViewMode];
        final float cellHeight = (float) height / AXIS_COUNT_Y;
        
        if (cellWidth == 0 || cellHeight == 0) {
            Log.e(TAG, "Error: onDraw ... cellWidth = " + cellWidth + ", cellHeight = "
                    + cellHeight);
        }

  
        canvas.drawLine(startX, startY + AXIS_COUNT_Y * cellHeight, startX + width, startY
                + AXIS_COUNT_Y * cellHeight, mLinePaint);

        canvas.drawLine(startX, startY, startX + width, startY, mLinePaint);

       	canvas.drawText(getResources().getString(R.string.deep_sleep), 
       			startX , startY - 1 * textHeight+20, mTextPaintHR); 

              
         {    
        	int startX2= startX + width / 12-20;
        
        	// Draw axis text
        	canvas.drawText( "14", startX2 ,                 startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText( "18", startX2 + width / 6 ,     startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("22", startX2 + width * 2 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("2", startX2 + width * 3 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("6", startX2 + width * 4 / 6 , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText("10", startX2 + width * 5 / 6  , startY + 1 * textHeight + AXIS_COUNT_Y * cellHeight,
                 mTextPaint);
        	        	
        	// Draw axis text
        	canvas.drawText(getResources().getString(R.string.sleep_pm),   	startX2 + width/12 ,  	startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText(getResources().getString(R.string.sleep_night), 	startX2 + width*5/12 ,   startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
        	canvas.drawText(getResources().getString(R.string.sleep_am), 		startX2 + width*9/12 , 	startY + 2 * textHeight + AXIS_COUNT_Y * cellHeight,
                mTextPaint);
         }
 
        // Stages result has been calculated through calculate SleepQuality called
        
        // Draw content
        final int contentStartY = startY + (int) (AXIS_COUNT_Y * cellHeight);
        for (int i = 0; i < mNumberOfBars; i++) 
        {       	
        	
        	mTmpRectF.left = startX + i * cellWidth;
            mTmpRectF.right = startX + (i+1) * cellWidth-2;
            mTmpRectF.bottom = contentStartY;
            
            int unitColor;
           
            float heightFactor=0;
             
            int threshId=mSleepStages[i];
            if(threshId>=levelToBarAttr.length) threshId=levelToBarAttr.length-1;
            if(threshId<0) threshId=SLEEP_STAGE_INVALID;
            
            if(threshId==SLEEP_STAGE_INVALID) continue;
            
            unitColor=levelToBarAttr[threshId].barColor;
            heightFactor=levelToBarAttr[threshId].barHeight;
            
           	mTmpRectF.top = startY + height * (1-heightFactor); 
                        
            //-- Log.i(TAG, "draw data:"+i+":"+mSleepStages[i]);
            if(mSleepStages[i]!=SLEEP_STAGE_INVALID)
            {		           
            	mContentPaint.setColor(unitColor);
            	canvas.drawRect(mTmpRectF, mContentPaint);
            }
            
            if(mSleepStartBarIndex==i && mSleepStartBarIndex!=mSleepEndBarIndex){                         	
            	Path path = new Path();
            	path.moveTo(mTmpRectF.left, mTmpRectF.bottom);
            	path.lineTo(mTmpRectF.left, startY);
            	canvas.drawPath(path, mLinePaintSleepEnds);            	
            }
        } 	//-- end of for loop for every bar
 
       
        // draw sleep end bar after all being done 
        if(mSleepEndBarIndex>0 && mSleepStartBarIndex!=mSleepEndBarIndex){
        	int i=mSleepEndBarIndex;
        	
            //-- Log.i(TAG, "sleep check 4 end:"+mSleepStartBarIndex+":"+mSleepEndBarIndex+":"+i);
            
        	mTmpRectF.left = startX + i * cellWidth;
            mTmpRectF.right = startX + (i+1) * cellWidth-2;
            mTmpRectF.bottom = contentStartY;
        	
         	Path path = new Path();	
        	path.moveTo(mTmpRectF.right, mTmpRectF.bottom);
        	path.lineTo(mTmpRectF.right, startY);
        	canvas.drawPath(path, mLinePaintSleepEnds);
 	
        }

        // Draw Y axis
        canvas.drawLine(startX, startY, 
        				startX, startY + AXIS_COUNT_Y * cellHeight, mLinePaintActivity);

        // Draw midnight Y axis
        canvas.drawLine(startX+width/2, startY, 
        				startX+width/2, startY + AXIS_COUNT_Y * cellHeight, mLinePaintActivity);

        Log.i(TAG, "SleepView onDraw end");
        
        return;

        
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d(TAG, "onSaveInstanceState ...");
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        if(savedState!=null){
        	savedState.values = mSleepStages;
        }
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState ...");

        if(state==null){
        	Log.w(TAG,  "Abnormal restore, null...");
        	return;
        }

        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        mSleepStages = savedState.values;
        invalidate();
    }

    static class SavedState extends BaseSavedState {
        int[] values;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel in) {
            super(in);
            if(values!=null) in.readIntArray(values); // 150909
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeIntArray(values);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
