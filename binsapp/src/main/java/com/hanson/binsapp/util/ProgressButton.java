package com.hanson.binsapp.util;


import com.hanson.binsapp.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.ImageButton;
    
@SuppressLint("DrawAllocation")
public class ProgressButton extends ImageButton {
	private static final int STROKE_WIDTH = 50;
	private static final int START_COLOR = 0xffffc1c1; // 0xffFFD700; // 0xffA6CE50;
	private static final int END_COLOR = 0xffff6a6a; // 0xffFFD700; // 0xff57A037;
	private int progress = 0;
	private int data = 0;
	private boolean stable = false;
	private Bitmap[] digitBitmap = new Bitmap[10];
	private Bitmap digitBitmap_0_gray;
	private boolean isMeasuring = false;
	
	public ProgressButton(Context context) {
		super(context);
		loadImageResource();
	}
	   
	public ProgressButton(Context context, AttributeSet attribute) {
		super(context, attribute);
		loadImageResource();
	}
	
	public void loadImageResource() {
		digitBitmap[0] = BitmapFactory.decodeResource(getResources(), R.drawable.number0_darkgray);		
		digitBitmap[1] = BitmapFactory.decodeResource(getResources(), R.drawable.number1_darkgray);
		digitBitmap[2] = BitmapFactory.decodeResource(getResources(), R.drawable.number2_darkgray);
		digitBitmap[3] = BitmapFactory.decodeResource(getResources(), R.drawable.number3_darkgray);
		digitBitmap[4] = BitmapFactory.decodeResource(getResources(), R.drawable.number4_darkgray);
		digitBitmap[5] = BitmapFactory.decodeResource(getResources(), R.drawable.number5_darkgray);
		digitBitmap[6] = BitmapFactory.decodeResource(getResources(), R.drawable.number6_darkgray);
		digitBitmap[7] = BitmapFactory.decodeResource(getResources(), R.drawable.number7_darkgray);
		digitBitmap[8] = BitmapFactory.decodeResource(getResources(), R.drawable.number8_darkgray);
		digitBitmap[9] = BitmapFactory.decodeResource(getResources(), R.drawable.number9_darkgray);
		digitBitmap_0_gray = BitmapFactory.decodeResource(getResources(), R.drawable.number0_gray);
		
		
		final int numberSizeWidth=23;
		final int numberSizeHeight=23; 
		for(int i=0;i<10; i++){
			digitBitmap[i] = Bitmap.createScaledBitmap(digitBitmap[i], numberSizeWidth, numberSizeHeight, true);
		}
		digitBitmap_0_gray = Bitmap.createScaledBitmap(digitBitmap_0_gray, numberSizeWidth, numberSizeHeight, true);

	}
	
	public int getProgress() {
		return progress;
	}
	
	public void setProgress(int progress) {
		if (this.progress!=progress) {
			this.progress = progress;
			invalidate();
		}
	}
	  
	public void setProgress(int current, int max) {
		int new_progress = (current*100)/max;
		if (this.progress!=new_progress) {
			this.progress = new_progress;
			invalidate();
		}
	}
	  
	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
		invalidate();
	}
	
	public boolean isStable() {
		return stable;
	}
	
	public void setStable(boolean stable) {
		this.stable = stable;
	}
	
	public void startMeasure() {
		isMeasuring = true;
		//-- setImageResource(R.drawable.upload_rate_circle_measuring);
		setBackgroundResource(R.drawable.upload_rate_circle_measuring);
		invalidate();
	}
	
	public void stopMeasure() {
		isMeasuring = false;
		setBackgroundResource(R.drawable.upload_rate_start_button_press);
		invalidate();
	}
	  
	@Override
	public void onDraw(Canvas canvas){
		super.onDraw(canvas);
		Paint paint = new Paint();
        
        if (isMeasuring) {
        	// Draw progress circle
        	Path path = new Path();
            RectF rect = new RectF();
            rect.left = STROKE_WIDTH/2;  
            rect.top = STROKE_WIDTH/2;
            rect.right = getWidth()-STROKE_WIDTH/2;
            rect.bottom = getHeight()-STROKE_WIDTH/2;
            path.addArc(rect, 270, 0-(int)(progress*3.6));
            paint.setStrokeWidth(STROKE_WIDTH);
            paint.setAntiAlias(true);
            paint.setPathEffect(null);
            paint.setShader(new LinearGradient(0, 0, 0, getHeight(), START_COLOR, END_COLOR, Shader.TileMode.CLAMP));
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(path, paint);
            
            if (!stable) {
            	paint.setAlpha(50);  
            }
            // Draw heart rate data
            
	        int digit2 = data/100;
	        int digit1 = (data%100)/10;
	        int digit0 = data%10;
	        if (digit2==0)
	        	canvas.drawBitmap(digitBitmap_0_gray, getWidth()/2-digitBitmap[0].getWidth()/2-digitBitmap[0].getWidth(), getHeight()/2-digitBitmap[0].getHeight()/2, paint);
	        else
	        	canvas.drawBitmap(digitBitmap[digit2], getWidth()/2-digitBitmap[0].getWidth()/2-digitBitmap[0].getWidth(), getHeight()/2-digitBitmap[0].getHeight()/2, paint);
	        if (digit2==0 && digit1==0)
	        	canvas.drawBitmap(digitBitmap_0_gray, getWidth()/2-digitBitmap[0].getWidth()/2, getHeight()/2-digitBitmap[0].getHeight()/2, paint);
	        else
	        	canvas.drawBitmap(digitBitmap[digit1], getWidth()/2-digitBitmap[0].getWidth()/2, getHeight()/2-digitBitmap[0].getHeight()/2, paint);
	        canvas.drawBitmap(digitBitmap[digit0], getWidth()/2-digitBitmap[0].getWidth()/2+digitBitmap[0].getWidth(), getHeight()/2-digitBitmap[0].getHeight()/2, paint);
       		
        }
        
        //-- paint.setAlpha(255);
        //-- canvas.drawBitmap(arrowBitmap, getWidth()/2-arrowBitmap.getWidth()/2, 0, paint);
	}
}
