package com.hanson.binsapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.xmlpull.v1.XmlPullParserException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.hanson.binsapp.R;
import com.hanson.binsapp.widget.SwitchButton;

public class SettingActivity extends BaseActivity {
    private static final String TAG = "SettingActivity";
    private static final String KEY_ID = "id"; // value: String
    private static final String KEY_DISPLAYNAME = "name"; // value: String
    private static final String KEY_GMT = "gmt"; // value: String
    private static final String KEY_OFFSET = "offset"; // value: int (Integer)
    private static final String XMLTAG_TIMEZONE = "timezone";
    private static final int HOURS_1 = 60 * 60000;
    private TextView mTimeZoneTv;
    private TextView mCountryTv;
    private SwitchButton mTimeZoneSwitch;
    private SwitchButton mCountrySwitch;
    private SimpleAdapter mTimeZoneAdapter;
    private ArrayAdapter<String> mCountryAdapter;

    private DialogInterface.OnClickListener mOnTimeZoneClickListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            @SuppressWarnings("unchecked")
            HashMap<String, Object> map = (HashMap<String, Object>) mTimeZoneAdapter.getItem(which);
            mTimeZoneTv.setText((CharSequence) map.get(KEY_DISPLAYNAME));
        }
    };

    private DialogInterface.OnClickListener mOnCountryClickListener
            = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String country = (String) mCountryAdapter.getItem(which);
            mCountryTv.setText(country);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        //-- mTimeZoneTv = (TextView) findViewById(R.id.setting_time_zone);
        mCountryTv = (TextView) findViewById(R.id.setting_country);
        mTimeZoneSwitch = (SwitchButton) findViewById(R.id.setting_time_zone_switch);
        mCountrySwitch = (SwitchButton) findViewById(R.id.setting_country_switch);
        setupTimeZoneAdapter();
        setupCountryAdapter();
        mTimeZoneTv.setText("Beijing");
        mCountryTv.setText("China");
    }

    private void setupTimeZoneAdapter() {
        final String[] from = new String[] { KEY_DISPLAYNAME, KEY_GMT };
        final int[] to = new int[] { android.R.id.text1, android.R.id.text2 };
        final MyComparator comparator = new MyComparator(KEY_OFFSET);
        final List<HashMap<String, Object>> sortedList = getZones(this);
        Collections.sort(sortedList, comparator);
        mTimeZoneAdapter = new SimpleAdapter(this, sortedList,
                android.R.layout.simple_list_item_2, from, to);
    }

    private void setupCountryAdapter() {
        String[] countries = new String[] {"China", "USA", "England", "Canada", "Australia"};
        mCountryAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, countries);
    }

    public void onChooseTimeZone(View view) {
        new AlertDialog.Builder(this).setAdapter(mTimeZoneAdapter,
                mOnTimeZoneClickListener).setTitle(R.string.choose_time_zone).create().show();
    }

    public void onSetTimeZoneAuto(View view) {
        mTimeZoneSwitch.performClick();
    }

    public void onChooseCountry(View view) {
        new AlertDialog.Builder(this).setAdapter(mCountryAdapter,
                mOnCountryClickListener).setTitle(R.string.choose_country).create().show();
    }

    public void onSetCountryAuto(View view) {
        mCountrySwitch.performClick();
    }

    private static List<HashMap<String, Object>> getZones(Context context) {
        final List<HashMap<String, Object>> myData = new ArrayList<HashMap<String, Object>>();
        final long date = Calendar.getInstance().getTimeInMillis();
        try {
            XmlResourceParser xrp = context.getResources().getXml(
                    R.xml.timezones);
            while (xrp.next() != XmlResourceParser.START_TAG)
                continue;
            xrp.next();
            while (xrp.getEventType() != XmlResourceParser.END_TAG) {
                while (xrp.getEventType() != XmlResourceParser.START_TAG) {
                    if (xrp.getEventType() == XmlResourceParser.END_DOCUMENT) {
                        return myData;
                    }
                    xrp.next();
                }
                if (xrp.getName().equals(XMLTAG_TIMEZONE)) {
                    String id = xrp.getAttributeValue(0);
                    String displayName = xrp.nextText();
                    addItem(myData, id, displayName, date);
                }
                while (xrp.getEventType() != XmlResourceParser.END_TAG) {
                    xrp.next();
                }
                xrp.next();
            }
            xrp.close();
        } catch (XmlPullParserException xppe) {
            Log.e(TAG, "Ill-formatted timezones.xml file");
        } catch (java.io.IOException ioe) {
            Log.e(TAG, "Unable to read timezones.xml file");
        }

        return myData;
    }

    private static void addItem(List<HashMap<String, Object>> myData,
            String id, String displayName, long date) {
        final HashMap<String, Object> map = new HashMap<String, Object>();
        map.put(KEY_ID, id);
        map.put(KEY_DISPLAYNAME, displayName);
        final TimeZone tz = TimeZone.getTimeZone(id);
        final int offset = tz.getOffset(date);
        final int p = Math.abs(offset);
        final StringBuilder name = new StringBuilder();
        name.append("GMT");

        if (offset < 0) {
            name.append('-');
        } else {
            name.append('+');
        }

        name.append(p / (HOURS_1));
        name.append(':');

        int min = p / 60000;
        min %= 60;

        if (min < 10) {
            name.append('0');
        }
        name.append(min);

        map.put(KEY_GMT, name.toString());
        map.put(KEY_OFFSET, offset);

        myData.add(map);
    }

    private static class MyComparator implements Comparator<HashMap<String, Object>> {
        private String mSortingKey;

        public MyComparator(String sortingKey) {
            mSortingKey = sortingKey;
        }

        @SuppressWarnings("unused")
        public void setSortingKey(String sortingKey) {
            mSortingKey = sortingKey;
        }

        @SuppressWarnings("unchecked")
        public int compare(HashMap<String, Object> map1, HashMap<String, Object> map2) {
            Object value1 = map1.get(mSortingKey);
            Object value2 = map2.get(mSortingKey);

            /*
             * This should never happen, but just in-case, put non-comparable
             * items at the end.
             */
            if (!isComparable(value1)) {
                return isComparable(value2) ? 1 : 0;
            } else if (!isComparable(value2)) {
                return -1;
            }

            return ((Comparable<Object>) value1).compareTo(value2);
        }

        private boolean isComparable(Object value) {
            return (value != null) && (value instanceof Comparable);
        }
    }
}
