package com.hanson.binsapp;

import java.util.HashMap;

/**
 * This class includes a small subset of standard GATT attributes for demonstration purposes.
 */
public class GattAttributes {
    private static HashMap<String, String> attributes = new HashMap<String, String>();
    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    public static String SERVICE_ACTIVITY_TRACKING = "12345678-1234-1234-1234-123456789abc";
    public static String SERVICE_DEVICE_INFORMATION = "0000180a-0000-1000-8000-00805f9b34fb";
    public static String SERVICE_BATTERY = "0000180f-0000-1000-8000-00805f9b34fb";

    public static String CHARACTERISTIC_ACTIVITY_VALUE = 		"20000002-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_TYPE = 		"20000001-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_TIME = 		"20000003-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_CONFIGURATION = 		"20000004-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_TIME_VALUE = 	"20000006-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_LOCAL_CLOCK = 	"20000010-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_TIMER =       	"20000011-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_GOAL =        	"20000012-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_UI_SENSE =    	"20000013-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ACTIVITY_LINK_LOSS = 	"20000014-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_HEARTRATE_STREAM = 		"20000015-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_HEARTRATE_HOST_UPDATE= 	"20000016-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_SLEEP_RECORDS = 		"20000017-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_SLEEP_START_TIME = 		"20000018-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_IBEACON_UUID = 			"20000019-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_DEBUG_OUTPUT = 			"2000001A-1234-1234-1234-123456789abc";
    public static String CHARACTERISTIC_ALARM_CLOCK = 			"2000001D-1234-1234-1234-123456789abc";
    
    
    public static String CHARACTERISTIC_DI_SERIAL_NUMBER = "00002a25-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_MODEL_NAME = "00002a24-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_SYSTEM_ID = "00002a23-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_HW_REVISION = "00002a27-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_FW_REVISION = "00002a26-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_SW_REVISION = "00002a28-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_MANUFACTURER = "00002a29-0000-1000-8000-00805f9b34fb";
    public static String CHARACTERISTIC_DI_PNP_ID = "00002a50-0000-1000-8000-00805f9b34fb";

    public static String CHARACTERISTIC_BATTERY = "00002a19-0000-1000-8000-00805f9b34fb";

    static {
        // Services.
        attributes.put("12345678-1234-1234-1234-123456789abc", "Activity Tracking Service");
        attributes.put("00001800-0000-1000-8000-00805f9b34fb", "Generic Access");
        attributes.put("00001801-0000-1000-8000-00805f9b34fb", "Generic Attribute");
        attributes.put("00001809-0000-1000-8000-00805f9b34fb", "Health Thermometer");
        attributes.put("0000180a-0000-1000-8000-00805f9b34fb", "Device Information Service");
        attributes.put("0000180f-0000-1000-8000-00805f9b34fb", "Battery Service");

        // Generic Access Characteristics
        attributes.put("00002a00-0000-1000-8000-00805f9b34fb", "Device Name");
        attributes.put("00002a01-0000-1000-8000-00805f9b34fb", "Appearance");
        attributes.put("00002a04-0000-1000-8000-00805f9b34fb", "Peripheral Preferred Connection Parameters");

        // Activity Characteristics.
        attributes.put("20000001-1234-1234-1234-123456789abc", "Activity Type");
        attributes.put("20000002-1234-1234-1234-123456789abc", "Activity Value");
        attributes.put("20000003-1234-1234-1234-123456789abc", "Activity Time");
        attributes.put("20000004-1234-1234-1234-123456789abc", "Configuration");
        attributes.put("20000006-1234-1234-1234-123456789abc", "Activity Time Value");
        attributes.put("20000010-1234-1234-1234-123456789abc", "Activity Local Clock");
        attributes.put("20000011-1234-1234-1234-123456789abc", "Activity Timer");
        attributes.put("20000012-1234-1234-1234-123456789abc", "Activity Goal");
        attributes.put("20000013-1234-1234-1234-123456789abc", "Activity UI Sense Strength");
        attributes.put("20000015-1234-1234-1234-123456789abc", "Heart Rate Stream");
        attributes.put("20000016-1234-1234-1234-123456789abc", "Heart Rate Host Update");

        // Device Information Characteristics.
        attributes.put("00002a25-0000-1000-8000-00805f9b34fb", "Serial Number");
        attributes.put("00002a24-0000-1000-8000-00805f9b34fb", "Model Number");
        attributes.put("00002a23-0000-1000-8000-00805f9b34fb", "System ID");
        attributes.put("00002a27-0000-1000-8000-00805f9b34fb", "Hardware Revision");
        attributes.put("00002a26-0000-1000-8000-00805f9b34fb", "Firmware Revision");
        attributes.put("00002a28-0000-1000-8000-00805f9b34fb", "Software Revision");
        attributes.put("00002a29-0000-1000-8000-00805f9b34fb", "Manufacturer Name");
        attributes.put("00002a50-0000-1000-8000-00805f9b34fb", "PnP ID");

        // Battery Characteristics.
        attributes.put("00002a19-0000-1000-8000-00805f9b34fb", "Battery Level");
    }

    public static String lookup(String uuid, String defaultName) {
        String name = attributes.get(uuid);
        return name == null ? defaultName : name;
    }
}
