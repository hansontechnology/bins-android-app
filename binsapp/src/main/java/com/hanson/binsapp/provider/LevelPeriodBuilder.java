package com.hanson.binsapp.provider;

import java.util.Arrays;

import android.database.Cursor;
import android.util.Log;

import com.hanson.binsapp.widget.AmbientLightView;
import com.hanson.binsapp.widget.SleepView;


public class LevelPeriodBuilder {
	private final String TAG = "LevelPeriodBuilder";
    private int[] mActivityMovements;
    private int[] mLightLevels;
    private final int MAX_THRESHOLDS=20;
    private final int MIN_TIME_WINDOW=5;   	//-- the minimum time unit to keep record
    										//-- the larger the number, the saving time and space during processing.
    private int[] mThresholds= new int[MAX_THRESHOLDS];
    private int   mTimeWindow;
    
    private long  mTimePeriodStart;
    private long  mTimePeriodEnd;
    private SleepView mSleepViewToDisplay;
    private AmbientLightView mLightViewToDisplay;
    
    private int mSamplePeriod;
    private int mActivityNumbers=0;
    private int mLightNumbers=0;
    
    private int mNumberActiveData=0;
    private int mActualSamplePeriod=0;
    
    
    public LevelPeriodBuilder(long timePeriodStart, long timePeriodEnd, Cursor cursor) {
    	if(timePeriodStart==0 || timePeriodEnd==0) return;
    	if(cursor==null) return;
    	
    	mTimeWindow=MIN_TIME_WINDOW;
    	mActivityNumbers=(int)((timePeriodEnd-timePeriodStart)+1)/mTimeWindow;
    	mLightNumbers=(int)((timePeriodEnd-timePeriodStart)+1)/mTimeWindow;

    	Log.i(TAG, "activity numbers:"+mActivityNumbers);
    	
    	mActivityMovements=new int[mActivityNumbers];
    	mLightLevels=new int[mLightNumbers];
        	
    	Arrays.fill(mActivityMovements, -1);
    	Arrays.fill(mLightLevels, -1);
    	
    	mTimePeriodStart=timePeriodStart;
    	mTimePeriodEnd=timePeriodEnd;
    	
    	
    	int samplePeriod=30; // max. from 30 minutes
    	long lastSampleTime=0;
    	mNumberActiveData=0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
           int movements = cursor.getInt(0);
           long sampleTime = cursor.getInt(1);
           
           int type= cursor.getInt(2);
           
           if(type==Bins.BINS_ACTIVITY_TYPE_LIGHT){
        	   if(sampleTime<timePeriodEnd){            	   
                   int timeIndex=(int)((sampleTime-mTimePeriodStart)/mTimeWindow);             
                   if(mLightLevels[timeIndex]==-1){
                       mLightLevels[timeIndex]=movements;
                   } else {
                       mLightLevels[timeIndex]+=movements;
                   }          
                   //-- Log.i(TAG, "light levels="+timeIndex+":"+mLightLevels[timeIndex]);
        	   }
           }else if(type==Bins.BINS_ACTIVITY_TYPE_SLEEP){                        
        	   if(sampleTime<timePeriodEnd){ 
            	   mNumberActiveData++; 	//-- 20151023
            	   int timeIndex=(int)((sampleTime-mTimePeriodStart)/mTimeWindow);             
            	   if(mActivityMovements[timeIndex]==-1){
            		   mActivityMovements[timeIndex]=movements;
            	   } else {
            		   mActivityMovements[timeIndex]+=movements;
            	   }
            	   //-- Log.i(TAG, "sleep movements="+timeIndex+":"+movements);
            	   if((sampleTime-lastSampleTime)<samplePeriod){
            		   //-- Log.i(TAG, "sleep sampleperiod="+timeIndex+":"+sampleTime+":"+lastSampleTime);        	   
            		   if(sampleTime-lastSampleTime>0){
            			   samplePeriod=(int)(sampleTime-lastSampleTime);        
            		   }
            	   }  
                   lastSampleTime=sampleTime;
        	   }else{
            	   Log.i(TAG, "WRONG sleep sampleTime:"+sampleTime+":"+timePeriodEnd);       		   
        	   }
           }
           cursor.moveToNext(); 
    	}
        cursor.close();
        
        mSamplePeriod=samplePeriod;
        mActualSamplePeriod=samplePeriod;
        if(mSamplePeriod<mTimeWindow) mSamplePeriod=mTimeWindow;
        
        normalizeMovements();            
        
        getThresholds();
        
        if(BinsDBUtils.activeDB().getFilterUnused()==1) removeDataForTimeUnused(SleepView.SLEEP_STAGE_LIGHT_WAKE);
    }
    public float getDataAvailability() {
    	
    	if(mActualSamplePeriod==0) return 0;
    	long dataShouldHave=((mTimePeriodEnd-mTimePeriodStart)+1)/mActualSamplePeriod;
    	//-- Log.i(TAG, "movement avail total="+mNumberActiveData+":"+dataShouldHave+":"+mActualSamplePeriod);
    	return ((float)mNumberActiveData/dataShouldHave);
    	
    }
    public int getCount() {
    	Log.i(TAG, "level period get count returns "+mActivityNumbers);
    	
    	return mActivityNumbers;
    }
    
    public int getTimeWindow() {
    	return mTimeWindow;
    }
    
    private void getThresholds() {
    	Cursor cursor=BinsDBUtils.activeDB().getThresholds();
    	
    	if(true) //-- if(cursor==null)	// 150623 for test, to get a reasonable values
    	{
       		mThresholds[1] =   6;			// deep sleep
    		mThresholds[2] =  16;			// light sleep
    		mThresholds[3] =  75;			// light wake
    		mThresholds[4] = 175;			// normal
    		mThresholds[5] = 250;			
    		mThresholds[6] = 350;
    		mThresholds[7] = 450;			// active
    		mThresholds[8] = 500;
    		mThresholds[9] = 900;			
    		mThresholds[10]=1200;
    		mThresholds[11]=-1;   
     		
    		// TODO local thresholds to use
    		return;
    	}    	
    	
    	int maxThresh=0;
    	cursor.moveToFirst();
    	while(!cursor.isAfterLast()){
            int threshId = cursor.getInt(0);
            int movements = cursor.getInt(1);
            if(threshId>maxThresh) maxThresh=threshId;
            if(threshId<mThresholds.length){
            	mThresholds[threshId]=movements;
            	//-- Log.i(TAG, "sleep thresholds="+threshId+":"+movements);
            } else {
            	Log.i(TAG, "Wrong Thresh ID:"+threshId);
            } 
            cursor.moveToNext();
    	} 
    	cursor.close();  
    	
    	mThresholds[maxThresh+1]=-1;
    }
    
    private void normalizeMovements() {
   	    int sampleTime;
   	    int lastSampleTime=-1;
   	    
   	    if(mSamplePeriod<=mTimeWindow) return; //-- no need to normalize

   	    float sampleFactor=mSamplePeriod/mTimeWindow;
        // mSamplePeriod should be multiple of mTimeWindow;
  	    
        for(int i=0; i<mActivityMovements.length; i++){        	
        	if(mActivityMovements[i]>=0){
        		int sampledMovements=mActivityMovements[i];  
        		
        		sampleTime=i;
        		
                //- boundary condition to improve efficiency
                // maximum granularity is 10 minutes
                if(((sampleTime-lastSampleTime)*mTimeWindow)>10)
                                            lastSampleTime=sampleTime-(10/mTimeWindow);
                
                if(lastSampleTime<=-1) lastSampleTime=sampleTime-(int)sampleFactor;
        		
        		int movementsPerTimeWindow;        	
        		for(int j=i; j>=(lastSampleTime+1) && j>=0; j--){
                    movementsPerTimeWindow=(int)((float)sampledMovements/sampleFactor);
                    
                    mActivityMovements[j]=movementsPerTimeWindow;
                	//-- Log.i(TAG, "sleep normalize="+j+":"+mActivityMovements[j]+":"+mSamplePeriod);
        		}
        		lastSampleTime=sampleTime;
        		
        	}      	        	
        }               
		
    }
    
    public void display(SleepView sleepView, AmbientLightView lightView) {
    	
    	mSleepViewToDisplay=sleepView;
    	
    	mLightViewToDisplay=lightView;
    	
    	Log.i(TAG, "sleep display update");
    	
    	mSleepViewToDisplay.displayUpdate(mSamplePeriod, mTimeWindow, mThresholds, mActivityMovements);        	
    	mLightViewToDisplay.displayUpdate(mSamplePeriod, mTimeWindow, mLightLevels);        	
    	
    }
    
    private int getThreshIdFromMovements(int movements){
    	int movePerMinute=(int)((float)movements/mTimeWindow);	// the input is for a total of the minutes per bar
 	   
 	   	if(movements<0) return -1;
 	   
 	   	int idFound=-1;
 	   	int i;
 	   	for(i=1; i<mThresholds.length; i++){
 	   		if(mThresholds[i]>movePerMinute) break;
 	   		if(mThresholds[i]==-1) break;
 	   	}
 	   	if(mThresholds[i]==-1) i--;
 	   	idFound=i;
 	   
 	   	// Log.i(TAG, "sleep thresh="+movPerMinute+":"+idFound+":");
 	   	return idFound;
    }
    
    public void storeToDB() {
        int i;
        long currentLevelStartIndex;
        long currentLevelEndIndex;
        long timePeriodStartIndex=mTimePeriodStart/mTimeWindow;
        
        for(i=0; i<mActivityMovements.length;) { 
            currentLevelStartIndex=timePeriodStartIndex+i;
            currentLevelEndIndex=currentLevelStartIndex; // initial to end at the same slot
            int movements=mActivityMovements[i];
            
            if(movements<0){ i++; continue;}
            int threshIdStart=getThreshIdFromMovements(movements);
            if(threshIdStart<1){
            	i++;
            	continue; // empty slot
            }
            
            int j;
            int threshIdNext=0;
            for(j=i+1; j<mActivityMovements.length ;j++){
                int moveCount=mActivityMovements[j];
                
                if(moveCount<0){ break; }
                               
                threshIdNext=getThreshIdFromMovements(moveCount);
                
            	if(threshIdNext==threshIdStart){
                    currentLevelEndIndex=timePeriodStartIndex+j;
            	}else{
            		break;
            	}
            }  

            long currentLevelStartTime=(currentLevelStartIndex*mTimeWindow);
            long currentLevelEndTime=(currentLevelEndIndex*mTimeWindow)+(mTimeWindow-1);
            
            BinsDBUtils.activeDB().insertCubeLevelPeriod(
            		currentLevelStartTime, currentLevelEndTime, threshIdStart);            
            i=j;                        
        } //-- end of for loop
    }
    
    
    public void removeDataForTimeUnused(int threshIdToCheck) {
    	int i; 
    	long numSamples=mActivityMovements.length;
    	for(i=0; i<numSamples;) { 
    	    int movements=mActivityMovements[i];
    	    if(movements<0){ i++; continue;}

    	    int threshIdStart=getThreshIdFromMovements(movements);
    	    if(threshIdStart<1){
    	    	i++;
    	    	continue; // empty slot
    	    }
    	 
    	    int j;
    	    Boolean isMovementZero=true;
    	    Boolean isWithoutUse=false;
    	    
    	    if(threshIdStart>2) isMovementZero=false;
    	    int threshIdNext=0;
    	    for(j=i+1; j<numSamples ;j++){
    	        int moveCount=mActivityMovements[j];
    	        
    	        if(moveCount<0){ break; }

    	        threshIdNext=getThreshIdFromMovements(moveCount);
    	        
      	        //-- Log.i(TAG, "movement count:"+i+":"+j+":"+moveCount+":"+isMovementZero+":"+threshIdNext);
      	        	        
    	    	if(threshIdNext!=threshIdStart){
     	    		break;
    	    	}
    	    } 

    		if(isMovementZero==true){
    	        if(threshIdNext>=threshIdToCheck || j>=numSamples){
    	        	long timePeriod=(j-i)*mTimeWindow;    					
    	        	if(timePeriod>90){ // more than 90 minutes then
    	        		isWithoutUse=true;
    	        	}
    	        }
    	 	}
    	    
    	    if(isWithoutUse==true){
    	    	for(long k=i; k<j; k++){
    	    		mActivityMovements[(int)(k)]=-1;
    	    	}
    	    }    	    
    	    i=j;
     	}  // for loop 
    }
}



