package com.hanson.binsapp.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class Bins {
    public static final String AUTHORITY = "com.hanson.binsapp";
    public static final String SCHEME = "content://";
    
    public static final int BINS_ACTIVITY_TYPE_STEPS =1;
    public static final int BINS_ACTIVITY_TYPE_SLEEP =3;
    public static final int BINS_ACTIVITY_TYPE_PULSE =2; 
    public static final int BINS_ACTIVITY_TYPE_LIGHT =4;  // 150410
    
    
    public static final int BINS_SLEEP_MEASURING_TIME_UNIT_MINUTES = 5;
    
    // This class cannot be instantiated
    private Bins() {
    }

    public static final class Activities implements BaseColumns {
        // This class cannot be instantiated
        private Activities() {}
        private static final String PATH_ACTIVITY = "/activities";
        private static final String PATH_ACTIVITY_ID = "/activities/";
        /** The table name offered by this provider */
        public static final String TABLE_NAME = "activities";
        /** The content:// style URL for this table */
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_ACTIVITY);
        /**
         * The content URI base for a single activity record. Callers must
         * append a numeric activity id to this Uri to retrieve a activity record
         */
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_ACTIVITY_ID);
        /**
         * The content URI match pattern for a single activity record.
         */
        public static final Uri CONTENT_ID_URI_PATTERN
            = Uri.parse(SCHEME + AUTHORITY + PATH_ACTIVITY_ID + "/#");
        /** Column name for activity type */
        public static final String COLUMN_ACTIVITY_TYPE = "type";
        /** Column name for activity value */
        public static final String COLUMN_ACTIVITY_VALUE = "value";
        /** Column name for activity time */
        public static final String COLUMN_ACTIVITY_TIME = "time";
        /** Column name for MAC address of the device measured this activity record */
        public static final String COLUMN_DEVICE_MAC = "mac";
        
        public static final String COLUMN_ACTIVITY_USER = "user";

        public static final String COLUMN_ACTIVITY_UPLOADED = "uploaded";

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of activities.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.activity";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single activity.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.activity";

        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = "time DESC";
        public static final String DEFAULT_SORT_ORDER_VALUE = "value ASC";
        public static final String DEFAULT_SORT_ORDER_TIME = "time ASC";

    }

    public static final class Device implements BaseColumns {
        private Device() {}
        private static final String PATH_DEVICE = "/device";
        private static final String PATH_DEVICE_ID = "/device/";
        public static final String TABLE_NAME = "device";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_DEVICE);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_DEVICE_ID);
        /** The device name */
        public static final String COLUMN_DEVICE_NAME = "deviceName";
        /** The serial number of the device */
        public static final String COLUMN_DEVICE_SN = "serialNumber";
        /** The model name of the device */
        public static final String COLUMN_DEVICE_MODEL = "model";
        /** The system id of the device */
        public static final String COLUMN_SYSTEM_ID = "systemID";
        /** The hardware revision of the device */
        public static final String COLUMN_HARDWARE_VERSION = "hardwareVersion";
        /** The software revision of the device */
        public static final String COLUMN_SOFTWARE_VERSION = "softwareVersion";
        /** The firmware revision of the device */
        public static final String COLUMN_FIRMWARE_VERSION = "firmwareVersion";
        /** The manufacturer of the device */
        public static final String COLUMN_MANUFACTURER = "manufacturer";
        /** The type of the device */
        public static final String COLUMN_DEVICE_TYPE = "deviceType";
        /** The battery level of the device */
        public static final String COLUMN_BATTERY_LEVEL = "batteryLevel";
        /** The time the device last synchronized */
        public static final String COLUMN_LAST_SYNC_TIME = "lastSyncTime";
        public static final String COLUMN_LAST_MEASUREMENT_TIME = "lastMeasurementTime";
        /** The mac address of the device */
        public static final String COLUMN_MAC_ADDRESS = "macAddress";
        public static final String COLUMN_IBEACON_UUID = "ibeaconUuid";
        
 
        public static final String COLUMN_SHAKE_THRESHOLD = "shakeThreshold";
        public static final String COLUMN_FINDER_THRESHOLD = "finderThreshold";
        public static final String COLUMN_CONFIGURATION = "deviceConfiguration" ;                   

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of device.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.device";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single device.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.device";

    }
    
    // 06.15"
    public static final class User implements BaseColumns {
        private User() {}  
        private static final String PATH_USER = "/user";
        private static final String PATH_USER_ID = "/user/";
        public static final String TABLE_NAME = "user2";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_USER);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_USER_ID);
        /** The device name */
        
        public static final String COLUMN_USER_FIRSTNAME = "userFirstName";
        public static final String COLUMN_USER_LASTNAME = "userLastName";

        public static final String COLUMN_USER_UID = "userUserId";	//-- 150820 Universal User ID
        
        public static final String COLUMN_USER_NAME = "userName";
        
        // 150516
         
        public static final String COLUMN_USER_DEVICE = "userDeviceAddress";

        public static final String COLUMN_USER_DOB = "userDOB";

        public static final String COLUMN_USER_WEIGHT = "userWeight";

        public static final String COLUMN_USER_HEIGHT = "userHeight";

        public static final String COLUMN_USER_GENDER = "userGender";

        public static final String COLUMN_USER_DAILYGOAL = "userGoal";
        public static final String COLUMN_USER_ALARM = "userAlarm";

        public static final String COLUMN_USER_STEPWATCH_STARTTIME = "userStepWatchTime";  // 01.09
        public static final String COLUMN_USER_STEPWATCH_ENABLED = "userStepWatchEnabled";
        public static final String COLUMN_USER_STEPWATCH_DELTA = "userStepWatchDelta";

        public static final String COLUMN_USER_GOAL_ACHIEVETIME = "userGoalAchievedTime";
        public static final String COLUMN_USER_GOAL_ACHIEVEVALUE = "userGoalAchievedValue";
        
        public static final String COLUMN_USER_FINDER_ENABLED = "userFinderEnabled";
        public static final String COLUMN_USER_SLEEP_ENABLED = "userSleepEnabled";
        public static final String COLUMN_USER_SLEEP_CHECK_PERIOD = "userSleepCheckPeriod";       
        public static final String COLUMN_USER_IS_FROM_NETWORK = "isUserFromNetwork";
        public static final String COLUMN_USER_PASSWORD ="userPassword";
        public static final String COLUMN_USER_SYNC_TIME = "userLastSyncTime";
        public static final String COLUMN_USER_START_TIME = "userStartTime";

        
        
        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of device.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.user";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single device.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.user";

    }
    
    
    // 150516
    public static final class SleepThreshold implements BaseColumns {
        private SleepThreshold() {}  
        private static final String PATH_SLEEP_THRESH = "/sleepThreshold";
        private static final String PATH_SLEEP_THRESH_ID = "/sleepThreshold/";
        public static final String TABLE_NAME = "sleepThreshold";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SLEEP_THRESH);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_SLEEP_THRESH_ID);
        public static final String COLUMN_SLEEP_THRESH_ID = "sleepThreshId";
        public static final String COLUMN_SLEEP_THRESH_LEVEL = "sleepThreshLevel";
        public static final String COLUMN_SLEEP_THRESH_LEVEL_INT = "sleepThreshLevelInteger";       
        public static final String COLUMN_SLEEP_THRESH_NAME =  "sleepThreshName";
        public static final String COLUMN_SLEEP_THRESH_MOVEMENTS_MIN = "sleepThreshValueMin";        
        public static final String COLUMN_SLEEP_THRESH_MOVEMENTS = "sleepThreshValue";
        public static final String COLUMN_SLEEP_THRESH_STATE = "sleepThreshState";
        
        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER_LEVEL = "sleepThreshValue ASC";
        public static final String DEFAULT_SORT_ORDER_MOVEMENTS = "sleepThreshValue ASC";
        public static final String CHECK_SORT_ORDER_LEVEL = "sleepThreshValue DESC";

        
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.sleepThreshold";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.sleepThreshold";

    }
    
    // 150516
    public static final class SleepTimeEffect implements BaseColumns {
        private SleepTimeEffect() {}  
        private static final String PATH_SLEEP_TIME_EFFECT= "/sleepTimeEffect";
        private static final String PATH_SLEEP_TIME_EFFECT_ID = "/sleepTimeEffect/";
        public static final String TABLE_NAME = "sleepTimeEffect";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_SLEEP_TIME_EFFECT);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_SLEEP_TIME_EFFECT_ID);
        public static final String COLUMN_SLEEP_TIME_EFFECT_THRESH_ID = "sleepTimeEffectId";
        public static final String COLUMN_SLEEP_TIME_EFFECT_HOUR =  "sleepTimeEffectHour";
        public static final String COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR = "sleepTimeEffectPercSleepFactor";
        public static final String COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR = "sleepTimeEffectPercWakeFactor";
        
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.sleepTimeEffect";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.sleepTimeEffect";

    }
      
    // 150516"
    public static final class Circadian implements BaseColumns {
        private Circadian() {}  
        private static final String PATH_CIRCADIAN = "/circadian";
        private static final String PATH_CIRCADIAN_ID = "/circadian/";
        public static final String TABLE_NAME = "circadian";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_CIRCADIAN);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_CIRCADIAN_ID);
        public static final String COLUMN_CIRC_CYCLE = "circCycle";
        public static final String COLUMN_CIRC_HOUR = "cirHour";
        public static final String COLUMN_CIRC_PERC_SLEEP_FACTOR = "circPercSleepFactor";
        public static final String COLUMN_CIRC_PERC_WAKE_FACTOR = "circPercWakeFactor";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.circadian";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.circadian";
        
    }
    
    // 150516"
    public static final class CubeLevelPeriods implements BaseColumns {
        private CubeLevelPeriods() {}  
        private static final String PATH_CUBELEVEL = "/cubeLevelPeriods";
        private static final String PATH_CUBELEVEL_ID = "/cubeLevelPeriods/";
        public static final String TABLE_NAME = "cubeLevelPeriods";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_CUBELEVEL);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_CUBELEVEL_ID);
        public static final String COLUMN_CUBELEVEL_USER = "userId";
        public static final String COLUMN_CUBELEVEL_THRESH_ID = "threshold";
        public static final String COLUMN_CUBELEVEL_DTM_START = "dtmStart";
        public static final String COLUMN_CUBELEVEL_DTM_START_ID = "dtmStartId";
        public static final String COLUMN_CUBELEVEL_DTM_END = "dtmEnd";
        public static final String COLUMN_CUBELEVEL_DTM_END_ID = "dtmEndId";
        public static final String COLUMN_CUBELEVEL_DEC_HOURS = "deHours";
        public static final String COLUMN_CUBELEVEL_NIGHT_SLEEP_START = "nightSleepStart";
        public static final String COLUMN_CUBELEVEL_NOTE = "note";
        public static final String COLUMN_CUBELEVEL_DELTA_CIRC = "deltaCirc";
        public static final String COLUMN_CUBELEVEL_DELTA_TIME_EFF = "deltaTimeEff";
        public static final String COLUMN_CUBELEVEL_DELTA_MENTAL_EFFECT= "deltaMentalEffect";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.cubeLevelPeriods";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.cubeLevelPeriods";
 
        public static final String DEFAULT_SORT_ORDER_THRESHOLD = "threshold DESC";
        public static final String SORT_ORDER_TIME_BEFORE = "dtmEnd DESC";
        public static final String SORT_ORDER_TIME_AFTER = "dtmStart ASC";
        
        
    }
     
    // 150615
    public static final class Actigraphy implements BaseColumns {
        private Actigraphy() {}  
        private static final String PATH_ACTIGRAPHY = "/actigraphy";
        private static final String PATH_ACTIGRAPHY_ID = "/actigraphy/";
        public static final String TABLE_NAME = "actigraphy";
        public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY + PATH_ACTIGRAPHY);
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse(SCHEME + AUTHORITY + PATH_ACTIGRAPHY_ID);
        public static final String COLUMN_ACTI_USER = "actiUser";
        public static final String COLUMN_ACTI_TIME = "actiTime";
        public static final String COLUMN_ACTI_MOVE_COUNTER = "actiMoveCounter";
        public static final String COLUMN_ACTI_CUBED = "actiCubed";
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hanson.actigraphy";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hanson.actigraphy";
        
    }
}
