package com.hanson.binsapp.provider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class UnlockingLifeReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(Context context, Intent intent)
    {
       Intent service1 = new Intent(context, UnlockingLifeService.class);
       context.startService(service1);      
    }   

}
