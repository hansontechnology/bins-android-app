package com.hanson.binsapp.provider;
 
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.*;
import com.amazonaws.mobileconnectors.dynamodbv2.dynamodbmapper.*;
import com.amazonaws.services.dynamodbv2.model.*;

 
public class BinsCloudService extends Service 
{
		
    public static final String TAG = "binsCloudService";
    
 	private final IBinder mBinder = new BinsCloudBinder();
	   // Random number generator
	private final Random mGenerator = new Random();

    public static final int MSG_CONNECT_TIMEOUT = 1;
    
    private Context mContext=null;
    
	//-- AWS
    CognitoCachingCredentialsProvider credentialsProvider=null;
	private AmazonDynamoDBAsyncClient mDdbClient=null;	
	
	private BinsDBUtils binsDB;
	
	private static boolean mInitiated=false;
	
	private String mLoadingUserUID=null;
	private String mLoadingUserName=null;	
	
	private String mActiveConnectUserUID=null;

	private ArrayList<DDBTableActivity> mActivityQueue = new ArrayList<DDBTableActivity>();
	   
	
    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class BinsCloudBinder extends Binder {
        public BinsCloudService getService() {
            // Return this instance of LocalService so clients can call public methods
            return BinsCloudService.this;
        }
    }
    
    @Override
    public IBinder onBind(Intent arg0)
    {
       // TODO Auto-generated method stub
        Log.i(TAG, "BinsCloud bonded");

        return mBinder;
    }
    /** method for clients */
    public int getRandomNumber() {
      return mGenerator.nextInt(100);
    }
 
    @Override
    public void onCreate() 
    {
    	// TODO Auto-generated method stub  
    	super.onCreate();
    	Log.i(TAG, "BinsCloud created");
       
    	int SDK_INT = android.os.Build.VERSION.SDK_INT;
    	if (SDK_INT > 8) 
    	{
    		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                   .permitAll().build();
    		StrictMode.setThreadPolicy(policy);
    		//your codes here
    	}
                 	  
    }
 
    
   @Override
   public int onStartCommand(Intent intent, int flags, int startId) 
   {
       Log.i(TAG, "BinsCloud start command");

       super.onStartCommand(intent, flags, startId);
          
       BinsDBUtils.getBluetoothLeDao(this);  
       binsDB=BinsDBUtils.activeDB();


       // We want this service to continue running until it is explicitly  
       // stopped, so return sticky.  
       return START_STICKY; 
   }
 
   @Override
   public void onDestroy() 
   {
        // TODO Auto-generated method stub
        super.onDestroy();
   }
 
  
   public Boolean isNetworkConnected() {
	
	   Boolean hasNetwork=false;
	  
	   ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
	  
	   if(cm==null){
		   Log.i(TAG, "isNetwotkConnected cm==null");		  
	   }else{
		   NetworkInfo ni = cm.getActiveNetworkInfo();
		   if (ni != null) {
			  // There are no active networks.			  
			  hasNetwork=true;
		   }
	   }
	
	   if(hasNetwork==false){
		   Log.i(TAG, "isNetwotkConnected: network is NOT connected.");
	   }
	   return hasNetwork;
   }

   public Boolean isInternetAvailable() {
    try {
        InetAddress ipAddr = InetAddress.getByName("www.yahoo.com"); //You can replace it with your name
        
        if (ipAddr.equals("")) {
            return false;
        } else {
            return true;
        }

    } catch (Exception e) {
        return false;
    }

   }

   public Boolean checkInternet() {
	if(isNetworkConnected()==true){ //-- && isInternetAvailable()){
		return true;
	}
    return false;
   }

   public Boolean loadUserProfile(final String userName, final String password) {
	
	   if(checkInternet()==false) return false;

	   Thread awsThread = new Thread( new Runnable() {
		@Override
		public void run(){
		    boolean foundExistingUser=false;
		    
		    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
		    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
		    
			Map<String, AttributeValue> expressionAttributeValues = 
				    new HashMap<String, AttributeValue>();		    
			
			expressionAttributeValues.put(":usr",  new AttributeValue(userName));
			expressionAttributeValues.put(":pwd",  new AttributeValue(password));
    	    Log.i(TAG, "cloud load username:"+userName);

		    scanExpression.setFilterExpression("userName = :usr AND userPassword = :pwd ");
		    //-- scanExpression.setFilterExpression("userName = :usr");
		    scanExpression.setExpressionAttributeValues(expressionAttributeValues);
		    		   
		    try {
		        PaginatedScanList<DDBTableUser> result = mapper.scan(
		                DDBTableUser.class, scanExpression);

		        //-- ArrayList<DDBTableUser> resultList = new ArrayList<DDBTableUser>();
		        for (DDBTableUser user : result) {
		            foundExistingUser=true;
		            
                    int isMale=0;
                    if(user.userGender.equals("M")){
                        isMale=1;
                    }  
            	    Log.i(TAG, "cloud load new names:"+user.userFirstName+":"+user.userLastName);

    		        mConnectEventListener.loadedUserProfile(
    		        		user.userName,
    		        		user.userId,
    		        		user.userFirstName,
    		        		user.userLastName,
    		        		user.userDOB,
    		        		user.userWeight,
    		        		user.userHeight,
    		        		isMale,
    		        		user.userLastSyncTime
    		        		);
 		            //-- resultList.add(up);
    		        Log.i(TAG, "loadUserProfile found user:"+user.userName+":"+user.userPassword);
		            break;
		        }
			    if(foundExistingUser==false){
    		        Log.i(TAG, "loadUserProfile cannot find");
			    }
		        
		    } catch (AmazonServiceException ex) {
		    	Log.i(TAG, "checkUserExist found error:"+ex);    	
		    } 
		    
		    if(foundExistingUser==false){
		    	mConnectEventListener.loadUserProfileFailed(userName);
		    }
		}	//-- end of run()
	   });		//-- end of thread

	   awsThread.start();
   
	   Log.i(TAG, "loadUserProfile successfully called");
    
	   return true;
   }

   public Boolean checkUserExist(final String userName) {
    	
	   if(checkInternet()==false) return false;
    
	   // Start up the thread running the service.  Note that we create a
	   // separate thread because the service normally runs in the process's
	   // main thread, which we don't want to block.  We also make it
	   // background priority so CPU-intensive work will not disrupt our UI.
	   Thread awsThread = new Thread( new Runnable() {
    		@Override
    		public void run(){
    		    boolean foundExistingUser=false;
    		    
    		    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
    		    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
    		    
    			Map<String, AttributeValue> expressionAttributeValues = 
    				    new HashMap<String, AttributeValue>();
    			
    			expressionAttributeValues.put(":user",  new AttributeValue(userName));

    		    scanExpression.setFilterExpression("userName = :user");
    		    scanExpression.setExpressionAttributeValues(expressionAttributeValues);
    		    		   
    		    try {
    		        PaginatedScanList<DDBTableUser> result = mapper.scan(
    		                DDBTableUser.class, scanExpression);

    		        for (DDBTableUser user : result) {
    		            foundExistingUser=true;
        		    	Log.i(TAG, "checkUserExist found user:"+user.userId);    	
    		            break;
    		        }
    		    } catch (AmazonServiceException ex) {
    		    	Log.i(TAG, "checkUserExist found error:"+ex);    	
    		    }     		    
		        mConnectEventListener.hasUser(foundExistingUser);
    		}
    	});

    	awsThread.start();
    
    	return true;  //-- always return true;
   }

   public String getNewUserId() {	    
 		long currentTimeMS=System.currentTimeMillis();  		 	    
	    return String.valueOf(currentTimeMS);	    
   }

   public boolean insertOrUpdateUserProfile(
		   				String userUID,
		   				String userName,
		   				String firstName,
		   				String lastName,
		   				String dob,
		   				int weight,
		   				int height,
		   				Boolean gender,
		   				String password) {
	   
	    if(checkInternet()==false){	       
	        return false;
	    }
	    
	    
	    String strGenderName="F";
	    if(gender==true) strGenderName="M";
	    
	    if(userUID==null || userUID.equals("")){
	        userUID=getNewUserId();
	    }

	    final DDBTableUser theUser= new DDBTableUser();
	    
	    Log.i(TAG, "cloud insert or update new names:"+firstName+":"+lastName);
	    
	    theUser.userId=mLoadingUserUID=userUID;	    
	    theUser.userFirstName=firstName;
	    theUser.userLastName=lastName;
	    theUser.userName=mLoadingUserName=userName;
	    theUser.userDOB=dob;
	    theUser.userWeight=weight;
	    theUser.userHeight=height;
	    theUser.userGender=strGenderName;
	    theUser.userPassword=password;
	    
	    Thread awsThread = new Thread( new Runnable() {
	    	@Override
	    	public void run(){
	    	    try {
		    	    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);	
	    	    	mapper.save(theUser);
	                mConnectEventListener.addedNewUser(true, mLoadingUserName, mLoadingUserUID);
	    	    } catch (AmazonServiceException ex) {
	    	    	Log.i(TAG, "insertOrUpdateUserProfile found error:"+ex);    	
	    	    }     		    	    	  
	    		 
	    	}
		});

		awsThread.start();
	  	   
		return true;
   }

   public void addUploadActivity( int actValue, int type, long time) {	
	    DDBTableActivity theActivity= new DDBTableActivity();
	    theActivity.userId=mActiveConnectUserUID;
	    theActivity.activityValue=actValue;
	    theActivity.activityType=type;
	    theActivity.activityTime=time;

	    mActivityQueue.add(theActivity);
 
   }


   public boolean sendUploadActivity() {
	   
	    if(mActivityQueue.size()==0){
	        return false;
	    }
	    
	    Thread awsThread = new Thread( new Runnable() {
	    	@Override
	    	public void run(){
	    		try {
    				DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);	
    			    DDBTableActivity theActivity;	    			    
	    			while(mActivityQueue.size()>0){
	    			    theActivity=mActivityQueue.get(0);
	    		        mActivityQueue.remove(0);
	    				mapper.save(theActivity);
	    			}
	    	        //-- By calling this to continuously trigger uploadTask
    		        mConnectEventListener.addedNewData(true, mActiveConnectUserUID);
	    		} catch (AmazonServiceException ex) {
	    			Log.i(TAG, "sendUploadActivity found error:"+ex);    	
	    		}     		    	    	      		 
	    	}
		});

		awsThread.start();
		
	    return true;
	}

   public boolean continueSendToCloud() {
	    boolean isSendSuccess= sendUploadActivity();
	    if(isSendSuccess==false){ // means no more buffer to send	        
	    }
	    
	    return isSendSuccess;
	}


   public void createUploadSession(String userUID, int type) {
	   //-- Log.i(TAG, "createUpoadSession UID="+userUID);
	   
	   mActiveConnectUserUID=userUID;
   
   }


   public Boolean startUploadSession(){

	   if(checkInternet()==false) return false;
   
	   Log.i(TAG, "startUploadSession");
   
	   if(mActivityQueue.size()==0){
	        return false;
	   }
	
	   return sendUploadActivity();		// if no more data, will return false   
   }


   public interface OnBinsCloudEventListener{
	   public void hasUser(Boolean isSuccess);   //method, which can have parameters
	   public void loadedUserProfile(String userName,String userUID, String firstName, String lastName,
									String dob, int weight, int height, int gender, long lastSyncTime);
	   public void loadUserProfileFailed(String userName);
	   public void addedNewUser(Boolean isSuccess, String userName, String userUID);
	   public void addedNewData(Boolean isSuccess, String userUID);
	   public void loadActivityCompleted(String userUID);
	   public void serviceTimeout();   
	   
   }

   public OnBinsCloudEventListener mConnectEventListener; //listener field

   public boolean isSupportUpdateUserProfile(){
		return true;
   }
   
   public boolean isSupportRemoteGuest(){
	   return true;
   }
   public void setConnection(Context ctext) {
	   
	mContext = ctext;
	
    Context appContext=mContext;
    
    credentialsProvider = new CognitoCachingCredentialsProvider(
       	    appContext,    /* get the context for the application */
       	    "us-east-1:c9792aff-127a-48c5-908f-ab5e137c02ae",    /* Identity Pool ID */
       	    Regions.US_EAST_1           /* Region for your identity pool--US_EAST_1 or EU_WEST_1*/
       	);

    mDdbClient = new AmazonDynamoDBAsyncClient(credentialsProvider);

    mDdbClient.setRegion(com.amazonaws.regions.Region.getRegion(Regions.US_EAST_1));

    if(mInitiated==false){
    	mInitiated=true;
    	//-- 20151209 download mental effectiveness tables
    	updateVitalityTables();
    }


    Log.i(TAG, "mDbClient has:"+ mDdbClient);      

   }

   //setting the listener
   public void setConnectEventListener(OnBinsCloudEventListener eventListener) {
	   this.mConnectEventListener=eventListener;
   }

   public void connectTimeoutHandler() {
	   if(this.mConnectEventListener!=null){
		// this.mConnectEventListener.onEvent(false);
		//TODO set the appropriate argument to let the service to return error properly to callbacks
		// mConnectResponseHandler.sendMessage(msg);
		   this.mConnectEventListener.serviceTimeout();
		}	
   }
   

   //-----------  DynamoDB Tables -------------------------
   //-- !!! NOTICE : The tables must be 'static' defined, otherwise exceptions with ... errors 
   //-- DynamoDB for Android is poorly documented, and implemented, need to work carefully.
   //-- 20151210
   
   //---- UserTable
   @DynamoDBTable(tableName = "BinsUserTable")
   public static class DDBTableUser {
	private String userId;
	private String userName;   //-- Email
	private String userFirstName;
	private String userLastName;
	private String userDOB;
	private int userWeight;
	private int userHeight;
	private String userGender;	//-- "F" or "M"
	private int userGoal;
	//-- private String userDevice;
	//-- private int alarmTime;
	private String userPassword;
	private int userLastSyncTime;
	
	@DynamoDBHashKey(attributeName = "userId")
    public String getUserId() {
        return userId;
    }
	public void setUserId(String userId) {
	        this.userId = userId;
	}
	
	@DynamoDBRangeKey(attributeName = "userName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        this.userName = name;
    }
    
    @DynamoDBAttribute(attributeName = "userFirstName")
    public String getUserFirstName() {
        return userFirstName;
    }
    public void setUserFirstName(String fname) {
        this.userFirstName = fname;
    }
    
    @DynamoDBAttribute(attributeName = "userLastName")
    public String getUserLastName() {
        return userLastName;
    }
    public void setUserLastName(String lname) {
        this.userLastName = lname;
    }
    
    @DynamoDBAttribute(attributeName = "userDOB")
    public String getUserDOB() {
        return userDOB;
    }
    public void setUserDOB(String userDOB) {
        this.userDOB = userDOB;
    }
    
    @DynamoDBAttribute(attributeName = "userWeight")
    public int getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(int userWeight) {
        this.userWeight = userWeight;
    }

    @DynamoDBAttribute(attributeName = "userHeight")
    public int getUserHeight() {
        return userHeight;
    }

    public void setUserHeight(int userHeight) {
        this.userHeight = userHeight;
    }

    @DynamoDBAttribute(attributeName = "userGender")
    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    @DynamoDBAttribute(attributeName = "userGoal")
    public int getUserGoal() {
        return userGoal;
    }

    public void setUserGoal(int userGoal) {
        this.userGoal = userGoal;
    }

    @DynamoDBAttribute(attributeName = "userPassword")
    public String getUserPassword() {
        return userPassword;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @DynamoDBAttribute(attributeName = "userLastSyncTime")
    public int getUserLastSyncTime() {
        return userLastSyncTime;
    }

    public void setUserLastSyncTime(int userLastSyncTime) {
        this.userLastSyncTime = userLastSyncTime;
    }

   }

   //--- ActivityTable  
   @DynamoDBTable(tableName = "BinsActivityTable")
   public static class DDBTableActivity {
	
	   	private String userId;
		private int activityType;
		private int activityValue;
		private long activityTime;
		
		@DynamoDBHashKey(attributeName = "userId")
	    public String getUserId() {
	        return userId;
	    }
		public void setUserId(String userId) {
		    this.userId = userId;
		}
		
		@DynamoDBRangeKey(attributeName = "activityTime")
	    public long getActivityTime() {
	        return activityTime;
	    }

	    public void setActivityTime(long time) {
	        this.activityTime = time;
	    }
    
    	@DynamoDBAttribute(attributeName = "activityValue")
    	public int getActivityValue() {
        	return activityValue;
    	}
    	public void setActivityValue(int  activityValue) {
        	this.activityValue = activityValue;
    	}
    
    	@DynamoDBAttribute(attributeName = "activityType")
    	public int getActivityType() {
        	return activityType;
    	}
    	public void setActivityType(int  activityType) {
        	this.activityType = activityType;
    	}
   }   
 
   //------   ThresholdTable
   @DynamoDBTable(tableName = "BinsSleepThresholdTable")
   public static class DDBTableSleepThreshold {	
	   private String userId;
	   private int thresholdId;
	   private String thresholdName;
	   private int movementCounts;
	   private int isStateSleep;
	
		@DynamoDBHashKey(attributeName = "userId")
    	public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
	
		@DynamoDBRangeKey(attributeName = "thresholdId")
    	public int getThresholdId() {
			return thresholdId;
    	}
    	public void setThresholdId(int thresholdId) {
    		this.thresholdId = thresholdId;
    	}
    
		@DynamoDBAttribute(attributeName = "thresholdName")
    	public String getThresholdName() {
			return thresholdName;
    	}
		public void setThresholdName(String thresholdName) {
			this.thresholdName = thresholdName;
		}

    	@DynamoDBAttribute(attributeName = "movementCounts")
    	public int getMovementCounts() {
    		return movementCounts;
    	}
    	public void setMovementCounts(int  movementCounts) {
    		this.movementCounts = movementCounts;
    	}
    
    	@DynamoDBAttribute(attributeName = "isStateSleep")
    	public int getIsStateSleep() {
        	return isStateSleep;
    	}
    	public void setIsStateSleep(int  isStateSleep) {
        	this.isStateSleep = isStateSleep;
    	}
   }  

   //------- CircadianTable ------------------------------
   @DynamoDBTable(tableName = "BinsCircadianTable")
   public static class DDBTableCircadian {
	
		private int circadianId;
		private int circadianHour24InDay;
		private float circadianPercSleepFactor;
		private float circadianPercWakeFactor;
		
		@DynamoDBHashKey(attributeName="circadianId")
	    public int getCircadianId() {
	        return circadianId;
	    }
		public void setCircadianId(int circadianId) {
		    this.circadianId = circadianId;
		}

		
		@DynamoDBRangeKey(attributeName = "circadianHour24InDay")
	    public int getCircadianHour24InDay() {
	        return  circadianHour24InDay;
	    }
	    public void setCircadianHour24InDay(int circadianHour24InDay) {
	        this.circadianHour24InDay = circadianHour24InDay;
	    }
	    
		@DynamoDBAttribute(attributeName = "circadianPercSleepFactor")
	    public float getCircadianPercSleepFactor() {
	        return circadianPercSleepFactor;
	    }
		public void setCircadianPercSleepFactor(float circadianPercSleepFactor) {
		    this.circadianPercSleepFactor = circadianPercSleepFactor;
		}

		@DynamoDBAttribute(attributeName = "circadianPercWakeFactor")
	    public float getCircadianPercWakeFactor() {
	        return circadianPercWakeFactor;
	    }
		public void setCircadianPercWakeFactor(float circadianPercWakeFactor) {
		    this.circadianPercWakeFactor = circadianPercWakeFactor;
		}  
   }  

   //------   TimeEffectTable
   @DynamoDBTable(tableName = "BinsTimeEffectTable")
   public static class DDBTableTimeEffect {
	
		private int threshId;
		private int timeEffectNthHour;
		private float recoveryFactor;
		
		@DynamoDBHashKey(attributeName="threshId")
	    public int getThreshId() {
	        return threshId;
	    }
		public void setThreshId(int threshId) {
		    this.threshId = threshId;
		}
		
		@DynamoDBRangeKey(attributeName = "timeEffectNthHour")
	    public int getTimeEffectNthHour() {
	        return  timeEffectNthHour;
	    }
	    public void setTimeEffectNthHour(int timeEffectNthHour) {
	        this.timeEffectNthHour = timeEffectNthHour;
	    }
	    
		@DynamoDBAttribute(attributeName = "recoveryFactor")
	    public float getRecoveryFactor() {
	        return recoveryFactor;
	    }
		public void setRecoveryFactor(float recoveryFactor) {
		    this.recoveryFactor = recoveryFactor;
		}

  }  

   //------- END of Vitality Tables ---------------------
   
   
   public void updateThresholdTable() {
	    
	    //-- final String userUID="9999";       //-- Temporarily use it for all;

	    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
	    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
	    		    
	    
	    Map<String, AttributeValue> expressionAttributeValues = 
	    				    new HashMap<String, AttributeValue>();
	    
	    AttributeValue av= new AttributeValue(); 
	    					//-- or simply AttributeValue("9999") for string
	    av.withS("9999");	//-- or withN for numbers
	    
	    expressionAttributeValues.put(":userUID",  av);
	    
	    scanExpression.setExpressionAttributeValues(expressionAttributeValues);
	    scanExpression.setFilterExpression("userId = :userUID");
	    		
	    Log.i(TAG, "threshold table start to load:"+mapper);
	    
	    try {
	    	PaginatedScanList<DDBTableSleepThreshold> result = mapper.scan(
	    		                DDBTableSleepThreshold.class, scanExpression);
    		Log.i(TAG, "threshold table result:"+result);

	    	for (DDBTableSleepThreshold thresh : result) {
	    		Log.i(TAG, "threshold table:"+thresh.thresholdId+":"+thresh.movementCounts+":"+thresh.isStateSleep+":"+thresh.thresholdName);

	    		binsDB.updateThresholdItem( 
	   	        		thresh.thresholdId, thresh.thresholdName, 
	   	        		thresh.movementCounts, thresh.isStateSleep);
	   	        
	    	}     
	    } catch (AmazonServiceException ex) {
	    	Log.i(TAG, "updateThresholdTable found error:"+ex);    	
	    }     		    
	    	    
	}


   	public void updateCircadianTable() {
	    
	    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
	    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
	    		    
	    
	    Map<String, AttributeValue> expressionAttributeValues = 
	    				    new HashMap<String, AttributeValue>();
	 
	    AttributeValue av= new AttributeValue(); 
	    av.withN("0");	//-- or withN, withS for numbers

	    expressionAttributeValues.put(":cirValue",  av);

	    scanExpression.setExpressionAttributeValues(expressionAttributeValues);
	    scanExpression.setFilterExpression("circadianId > :cirValue");

	    Log.i(TAG, "circadian table start to load:"+mapper);
	    
	    try {
	    	PaginatedScanList<DDBTableCircadian> result = mapper.scan(
	    		                DDBTableCircadian.class, scanExpression);
   		Log.i(TAG, "circadian table result:"+result);

	    	for (DDBTableCircadian circadian : result) {
	    		Log.i(TAG, "circadian table:"+circadian.circadianHour24InDay);
	    		
	    		binsDB.updateCircadianItem( 
	   	        		circadian.circadianId,
	   	        		circadian.circadianHour24InDay,
	   	        		circadian.circadianPercSleepFactor,
	   	        		circadian.circadianPercWakeFactor
	   	        		);
	   	        
	    	}     
	    } catch (AmazonServiceException ex) {
	    	Log.i(TAG, "updateCircadianTable found error:"+ex);    	
	    }     		    
	    	    
	}

   	public void updateTimeEffectTable() {
	    
        //-- follow 20151126 need to clear the table first
        binsDB.clearSleepTimeEffectTable();
   		
	    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
	    DynamoDBScanExpression scanExpression = new DynamoDBScanExpression();
	    		    
	    
	    Map<String, AttributeValue> expressionAttributeValues = 
	    				    new HashMap<String, AttributeValue>();
	 
	    AttributeValue av= new AttributeValue(); 
	    av.withN("0");	//-- or withN, withS for numbers

	    expressionAttributeValues.put(":threshValue",  av);

	    scanExpression.setExpressionAttributeValues(expressionAttributeValues);
	    scanExpression.setFilterExpression("threshId > :threshValue");

	    Log.i(TAG, "timeEffect table start to load:"+mapper);
	    
	    try {
	    	PaginatedScanList<DDBTableTimeEffect> result = mapper.scan(
	    		                DDBTableTimeEffect.class, scanExpression);
   		Log.i(TAG, "timeEffect table result:"+result);

	    	for (DDBTableTimeEffect timeEffect : result) {
	    		Log.i(TAG, "timeEffect table:"+timeEffect.threshId);
	    		
	    		binsDB.updateSleepTimeEffectItem( 
	    				timeEffect.threshId,
	    				timeEffect.timeEffectNthHour,
	    				timeEffect.recoveryFactor
	   	        		);	   	        
	    	}     
	    } catch (AmazonServiceException ex) {
	    	Log.i(TAG, "updateTimeEffectTable found error:"+ex);    	
	    }     		    
	    	    
	}

   public void updateVitalityTables() {
	   
	   	Log.i(TAG, "updateVitalityTables -");	   
	   	
	   	if(checkInternet()==false){
		   	Log.i(TAG, "internet is not connected..");	   	   		
	   		return;
	   	}
	   
	    Thread awsThread = new Thread( new Runnable() {	    	
    		@Override
    		public void run(){

    			updateCircadianTable();    
    							//-- 20151126 keep this sequence where updateCircadianTable is top
	                            //-- otherwise, multithreaded environment will keep sqlite3 crash
    			updateThresholdTable();
    			updateTimeEffectTable();
    		}
    	});
	    
	    awsThread.start();
	    
   }

   public boolean loadActivityRecordsStepPage(final String userUID, final long fromTime, final long toTime) {
	   
	   return loadActivityRecords(userUID, Bins.BINS_ACTIVITY_TYPE_STEPS, Bins.BINS_ACTIVITY_TYPE_PULSE, fromTime, toTime);
   }

   public boolean loadActivityRecordsSleepPage(final String userUID, final long fromTime, final long toTime) {
	   
	   return loadActivityRecords(userUID, Bins.BINS_ACTIVITY_TYPE_SLEEP, Bins.BINS_ACTIVITY_TYPE_LIGHT, fromTime, toTime);
   }
   
   public boolean loadActivityRecords(
		   			final String userUID,
		   			final int actType1,
		   			final int actType2,
		   			final long fromTime, 
		   			final long toTime							) {

	    Log.i(TAG, "Unlocking loadActivityRecords step page:"+userUID+":"+fromTime+" to "+toTime+":"+actType1+":"+actType2);

	    Thread awsThread = new Thread( new Runnable() {	    	
    		@Override
    		public void run(){
    		    
    		    DynamoDBMapper mapper = new DynamoDBMapper(mDdbClient);
    		    DynamoDBQueryExpression<DDBTableActivity> queryExp = 
    		    							new DynamoDBQueryExpression<DDBTableActivity>();
    		    		    	    
    		    Map<String, AttributeValue> expAttrs = new HashMap<String, AttributeValue>();
    		    
    		    expAttrs.put(":oneType", new AttributeValue().withN(String.valueOf(actType1)));
    		    expAttrs.put(":twoType", new AttributeValue().withN(String.valueOf(actType2)));

    		    DDBTableActivity queryActivity= new DDBTableActivity();
    		    queryActivity.userId=userUID;
    		    queryExp.setHashKeyValues(queryActivity);

    		    Map<String, Condition> conditions = new HashMap<String, Condition>();
    		    conditions.put("activityTime", 
    		    		new Condition()
    		    				.withComparisonOperator(ComparisonOperator.BETWEEN)
    		    				.withAttributeValueList(
    		    						new AttributeValue().withN(String.valueOf(fromTime)),
    		    						new AttributeValue().withN(String.valueOf(toTime))	
    		    						)
    		    		);	    
    		    queryExp.setRangeKeyConditions(conditions);
    		    queryExp.setExpressionAttributeValues(expAttrs);
    		    queryExp.setFilterExpression("activityType= :oneType OR activityType= :twoType");
    		    
    		    try {
    		    	PaginatedQueryList<DDBTableActivity> result = mapper.query(
    		    		                DDBTableActivity.class, queryExp);
    		    	Log.i(TAG, "Unlocking query activity result:"+result);

    		    	for (DDBTableActivity activity : result) {
    		    		Log.i(TAG, "Unlocking activity:"+activity.activityTime+":"+activity.activityType+":"+activity.activityValue);
    		    		
    		    		binsDB.insertOrUpdateActivitySetUploaded( 
    		    				activity.activityTime,
    		    		        activity.activityType,
    		    		        activity.activityValue
    		   	        		);	
    		   	           	        
    		    	} 
    		    	
    		        mConnectEventListener.loadActivityCompleted(userUID);

    		    } catch (AmazonServiceException ex) {
    		    	Log.i(TAG, "query activity found error:"+ex);    	
    		    }     		    
 	    
    		}
	    });
	    
	    awsThread.start();
	    
	    return true;
   }

}


