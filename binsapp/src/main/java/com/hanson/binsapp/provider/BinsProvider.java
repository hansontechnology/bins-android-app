package com.hanson.binsapp.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.hanson.binsapp.provider.Bins.Actigraphy;
import com.hanson.binsapp.provider.Bins.Activities;
import com.hanson.binsapp.provider.Bins.Circadian;
import com.hanson.binsapp.provider.Bins.CubeLevelPeriods;
import com.hanson.binsapp.provider.Bins.Device;
import com.hanson.binsapp.provider.Bins.SleepThreshold;
import com.hanson.binsapp.provider.Bins.SleepTimeEffect;
import com.hanson.binsapp.provider.Bins.User;
public class BinsProvider extends ContentProvider {
    // Used for debugging and logging
    private static final String TAG = "BinsProvider";

    /**
     * The database that the provider uses as its underlying data store
     */
    private static final String DATABASE_NAME = "bins.db";

    /**
     * The database version
     */
    private static final int DATABASE_VERSION = 1;

    private static final int ACTIVITY_BASE = 0;
    private static final int ACTIVITIES = ACTIVITY_BASE;
    private static final int ACTIVITY_ID = ACTIVITY_BASE + 1;

    private static final int DEVICE_BASE = 0x100;
    private static final int DEVICE = DEVICE_BASE;
    private static final int DEVICE_ID = DEVICE_BASE + 1;

    private static final int USER_BASE = 0x200;
    private static final int USER = USER_BASE;
    private static final int USER_ID = USER_BASE + 1;
 
    private static final int SLEEP_TIME_EFFECT_BASE = 0x300;
    private static final int SLEEP_TIME_EFFECT = SLEEP_TIME_EFFECT_BASE;
    private static final int SLEEP_TIME_EFFECT_ID = SLEEP_TIME_EFFECT_BASE + 1;
    
    private static final int CIRCADIAN_BASE = 0x400;
    private static final int CIRCADIAN = CIRCADIAN_BASE;
    private static final int CIRCADIAN_ID = CIRCADIAN_BASE + 1;
    
    private static final int CUBE_LEVEL_PERIODS_BASE = 0x500;
    private static final int CUBE_LEVEL_PERIODS = CUBE_LEVEL_PERIODS_BASE;
    private static final int CUBE_LEVEL_PERIODS_ID = CUBE_LEVEL_PERIODS_BASE + 1;
    
    private static final int SLEEP_THRESHOLD_BASE = 0x600;
    private static final int SLEEP_THRESHOLD = SLEEP_THRESHOLD_BASE;
    private static final int SLEEP_THRESHOLD_ID = SLEEP_THRESHOLD_BASE + 1;
    
    private static final int ACTIGRAPHY_BASE = 0x700;
    private static final int ACTIGRAPHY = ACTIGRAPHY_BASE;
    private static final int ACTIGRAPHY_ID = ACTIGRAPHY_BASE + 1;
    
    private static final int BASE_SHIFT = 8;
    /**
     *  TABLE_NAMES MUST remain in the order of the BASE constants above
     * (e.g. ACTIVITY_BASE = 0x000, DEVICE_BASE = 0x100, etc.) */
    private static final String[] TABLE_NAMES = {
        Activities.TABLE_NAME,
        Device.TABLE_NAME,
        User.TABLE_NAME,
        SleepTimeEffect.TABLE_NAME,
        Circadian.TABLE_NAME,
        CubeLevelPeriods.TABLE_NAME,
        SleepThreshold.TABLE_NAME,
        Actigraphy.TABLE_NAME
    };
    /** A UriMatcher instance */
    private static final UriMatcher sUriMatcher;

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        // Match activity
        sUriMatcher.addURI(Bins.AUTHORITY, "activities", ACTIVITIES);
        sUriMatcher.addURI(Bins.AUTHORITY, "activities/#", ACTIVITY_ID);

        // Match device
        sUriMatcher.addURI(Bins.AUTHORITY, "device", DEVICE);
        sUriMatcher.addURI(Bins.AUTHORITY, "device/#", DEVICE_ID);
        
        // Match users
        sUriMatcher.addURI(Bins.AUTHORITY, "user", USER);
        sUriMatcher.addURI(Bins.AUTHORITY, "user/#", USER_ID);

        
        sUriMatcher.addURI(Bins.AUTHORITY, "sleepTimeEffect", SLEEP_TIME_EFFECT);
        sUriMatcher.addURI(Bins.AUTHORITY, "sleepTimeEffect/#", SLEEP_TIME_EFFECT_ID);
        
        sUriMatcher.addURI(Bins.AUTHORITY, "circadian", CIRCADIAN);
        sUriMatcher.addURI(Bins.AUTHORITY, "circadian/#", CIRCADIAN_ID);
        
        sUriMatcher.addURI(Bins.AUTHORITY, "cubeLevelPeriods", CUBE_LEVEL_PERIODS);
        sUriMatcher.addURI(Bins.AUTHORITY, "cubeLevelPeriods/#", CUBE_LEVEL_PERIODS_ID);

        sUriMatcher.addURI(Bins.AUTHORITY, "sleepThreshold", SLEEP_THRESHOLD);
        sUriMatcher.addURI(Bins.AUTHORITY, "sleepThreshold/#", SLEEP_THRESHOLD_ID);

        sUriMatcher.addURI(Bins.AUTHORITY, "actigraphy", ACTIGRAPHY);
        sUriMatcher.addURI(Bins.AUTHORITY, "actigraphy/#", ACTIGRAPHY_ID);
        
    }

    private DatabaseHelper mOpenHelper;

    private class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            // calls the super constructor, requesting the default cursor factory.
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            
        }

        @Override
        public void onCreate(SQLiteDatabase db) {       	
        	
        	Log.d(TAG, "createUserTable");
        	  
            createActivityTable(db);
            createDeviceTable(db);
            createUserTable(db);
            createSleepTimeEffectTable(db);            
            createCircadianTable(db);
            createSleepCubeLevelPeriodsTable(db);
            createSleepThresholdTable(db);
            createActigraphyTable(db);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Logs that the database is being upgraded
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            // Kills the table and existing data
            db.execSQL("DROP TABLE IF EXISTS " + Bins.Activities.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + Bins.User.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + Bins.Device.TABLE_NAME);

            // Recreates the database with a new version
            onCreate(db);
        }

        private void createActivityTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Bins.Activities.TABLE_NAME + " ("
                    + Bins.Activities._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + Bins.Activities.COLUMN_ACTIVITY_TYPE + " INTEGER,"
                    + Bins.Activities.COLUMN_ACTIVITY_VALUE + " INTEGER,"
                    + Bins.Activities.COLUMN_ACTIVITY_TIME + " INTEGER,"
                    + Bins.Activities.COLUMN_DEVICE_MAC + " TEXT,"
                    + Bins.Activities.COLUMN_ACTIVITY_USER + " INTEGER,"
                    + Bins.Activities.COLUMN_ACTIVITY_UPLOADED + " INTEGER"
                    + ");");
        }

        private void createDeviceTable(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Bins.Device.TABLE_NAME + " ("
                    + Bins.Device._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + Bins.Device.COLUMN_DEVICE_NAME + " TEXT,"
                    + Bins.Device.COLUMN_DEVICE_SN + " TEXT,"
                    + Bins.Device.COLUMN_DEVICE_TYPE + " INTEGER,"
                    + Bins.Device.COLUMN_DEVICE_MODEL + " TEXT,"
                    + Bins.Device.COLUMN_SYSTEM_ID + " TEXT,"
                    + Bins.Device.COLUMN_HARDWARE_VERSION + " TEXT,"
                    + Bins.Device.COLUMN_SOFTWARE_VERSION + " TEXT,"
                    + Bins.Device.COLUMN_FIRMWARE_VERSION + " TEXT,"
                    + Bins.Device.COLUMN_MANUFACTURER + " TEXT,"
                    + Bins.Device.COLUMN_BATTERY_LEVEL + " INTEGER,"
                    + Bins.Device.COLUMN_LAST_SYNC_TIME + " DOUBLE,"
                    + Bins.Device.COLUMN_LAST_MEASUREMENT_TIME + " INTEGER,"
                    + Bins.Device.COLUMN_MAC_ADDRESS + " TEXT NOT NULL,"
                    + Bins.Device.COLUMN_SHAKE_THRESHOLD + " INTEGER,"
                    + Bins.Device.COLUMN_FINDER_THRESHOLD + " INTEGER,"
                    + Bins.Device.COLUMN_CONFIGURATION + " INTEGER,"
                    + Bins.Device.COLUMN_IBEACON_UUID + " TEXT"                                      
                    + ");");
        }
        
        private void createUserTable(SQLiteDatabase db) {
            Log.w(TAG, "createUserTable");

            db.execSQL("CREATE TABLE " + Bins.User.TABLE_NAME + " ("
                    + Bins.User._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + Bins.User.COLUMN_USER_UID + " TEXT,"                    
                    + Bins.User.COLUMN_USER_NAME + " TEXT,"
                    + Bins.User.COLUMN_USER_DEVICE + " INTEGER," 
                    + Bins.User.COLUMN_USER_FIRSTNAME + " TEXT,"  
                    + Bins.User.COLUMN_USER_LASTNAME + " TEXT,"  
                    + Bins.User.COLUMN_USER_DOB + " TEXT,"
                    + Bins.User.COLUMN_USER_ALARM + " INTEGER,"
                    + Bins.User.COLUMN_USER_WEIGHT + " INTEGER,"
                    + Bins.User.COLUMN_USER_HEIGHT + " INTEGER,"
                    + Bins.User.COLUMN_USER_GENDER + " INTEGER,"
                    + Bins.User.COLUMN_USER_DAILYGOAL + " INTEGER,"
                    + Bins.User.COLUMN_USER_FINDER_ENABLED + " INTEGER,"
                    + Bins.User.COLUMN_USER_SLEEP_ENABLED + " INTEGER,"
                    + Bins.User.COLUMN_USER_STEPWATCH_ENABLED + " INTEGER,"
                    + Bins.User.COLUMN_USER_STEPWATCH_STARTTIME + " DOUBLE,"
                    + Bins.User.COLUMN_USER_STEPWATCH_DELTA + " INTEGER,"
                    + Bins.User.COLUMN_USER_SLEEP_CHECK_PERIOD + " INTEGER,"
                    + Bins.User.COLUMN_USER_IS_FROM_NETWORK + " INTEGER,"
                    + Bins.User.COLUMN_USER_PASSWORD + " TEXT,"
                    + Bins.User.COLUMN_USER_SYNC_TIME + " DOUBLE,"
                    + Bins.User.COLUMN_USER_START_TIME + " DOUBLE"
                    + Bins.User.COLUMN_USER_GOAL_ACHIEVETIME + " DOUBLE,"
                    + Bins.User.COLUMN_USER_GOAL_ACHIEVEVALUE + " INTEGER"                    
                    + ");");
        }
        			// 01.09
    }

    // 150516
    public void createSleepCubeLevelPeriodsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Bins.CubeLevelPeriods.TABLE_NAME + " ("
                + Bins.CubeLevelPeriods._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_USER + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID+ " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START_ID + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END_ID + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DEC_HOURS + " DOUBLE," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_NIGHT_SLEEP_START + " INTEGER," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_NOTE + " TEXT," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DELTA_CIRC+ " DOUBLE," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DELTA_TIME_EFF+ " DOUBLE," 
                + Bins.CubeLevelPeriods.COLUMN_CUBELEVEL_DELTA_MENTAL_EFFECT+ " DOUBLE" 
               
                + ");");
  	
    }
    
    // 150516
    public void createCircadianTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Bins.Circadian.TABLE_NAME + " ("
                + Bins.Circadian._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Bins.Circadian.COLUMN_CIRC_CYCLE + " TEXT," 
                + Bins.Circadian.COLUMN_CIRC_HOUR + " INTEGER," 
                + Bins.Circadian.COLUMN_CIRC_PERC_SLEEP_FACTOR + " DOUBLE," 
                + Bins.Circadian.COLUMN_CIRC_PERC_WAKE_FACTOR  + " DOUBLE" 
                
                + ");");
  	
    }
    
    // 150516
    public void createSleepTimeEffectTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Bins.SleepTimeEffect.TABLE_NAME + " ("
                + Bins.SleepTimeEffect._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Bins.SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + " INTEGER," 
                + Bins.SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + " INTEGER," 
                + Bins.SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR + " DOUBLE," 
                + Bins.SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR  + " DOUBLE" 
                
                + ");");
  	
    }
  
    // 150516
    public void createSleepThresholdTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Bins.SleepThreshold.TABLE_NAME + " ("
                + Bins.SleepThreshold._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_ID + " INTEGER,"                
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_LEVEL + " TEXT," 
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_LEVEL_INT + " INTEGER,"                 
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_NAME + " TEXT," 
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS_MIN + " INTEGER," 
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS + " INTEGER," 
                + Bins.SleepThreshold.COLUMN_SLEEP_THRESH_STATE + " INTEGER" 
                
                + ");");
  	
    }  
    
    // 150615
    public void createActigraphyTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Bins.Actigraphy.TABLE_NAME + " ("
                + Bins.Actigraphy._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Bins.Actigraphy.COLUMN_ACTI_USER + " INTEGER,"                
                + Bins.Actigraphy.COLUMN_ACTI_TIME + " INTEGER," 
                + Bins.Actigraphy.COLUMN_ACTI_MOVE_COUNTER + " INTEGER,"                 
                + Bins.Actigraphy.COLUMN_ACTI_CUBED + " INTEGER"                   
                + ");");
  	
    }  
    
    
    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        
    	Log.d(TAG, "BinsProviderOnCreate");

        // SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        
 
        
        return true;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        final ContentResolver resolver = getContext().getContentResolver();
        final int matchId = sUriMatcher.match(uri);
        final int tableIndex = matchId >> BASE_SHIFT;
        final String tableName = TABLE_NAMES[tableIndex];
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        String finalWhere;
        String id = "0";
        int count = 0;
        switch (matchId) {
        case ACTIVITIES:
        case DEVICE:
        case USER:
        case SLEEP_TIME_EFFECT:
        case CIRCADIAN:
        case CUBE_LEVEL_PERIODS:
        case SLEEP_THRESHOLD:
        case ACTIGRAPHY:
       	
            count = db.delete(tableName, where, whereArgs);
            break;
        case ACTIVITY_ID:
        case DEVICE_ID:
        case USER_ID:
        case SLEEP_TIME_EFFECT_ID:
        case CIRCADIAN_ID:
        case CUBE_LEVEL_PERIODS_ID:
        case SLEEP_THRESHOLD_ID:
        case ACTIGRAPHY_ID:
        	
            id = uri.getPathSegments().get(1);
            finalWhere = BaseColumns._ID + " = " + id;
            if (where != null) {
                finalWhere = finalWhere + " AND " + where;
            }
            count = db.delete(tableName, finalWhere, whereArgs);
            break;
        }
        resolver.notifyChange(uri, null);
        return count;
    }

    @Override
    public String getType(Uri uri) {
        final int matchId = sUriMatcher.match(uri);
        String type = null;
        switch (matchId) {
        case ACTIVITIES:
            type = Activities.CONTENT_TYPE;
            break;
        case ACTIVITY_ID:
            type = Activities.CONTENT_ITEM_TYPE;
            break;
        case DEVICE:
            type = Device.CONTENT_TYPE;
            break;
        case DEVICE_ID:
            type = Device.CONTENT_ITEM_TYPE;
            break;
        case USER:
            type = User.CONTENT_TYPE;
            break;
        case USER_ID:
            type = User.CONTENT_ITEM_TYPE;
            break;
        case SLEEP_TIME_EFFECT:
            type = SleepTimeEffect.CONTENT_TYPE;
            break;
        case SLEEP_TIME_EFFECT_ID:
            type = SleepTimeEffect.CONTENT_ITEM_TYPE;
            break;
        case CUBE_LEVEL_PERIODS:
            type = CubeLevelPeriods.CONTENT_TYPE;
            break;
        case CUBE_LEVEL_PERIODS_ID:
            type = CubeLevelPeriods.CONTENT_ITEM_TYPE;
            break;
        case CIRCADIAN:
            type = Circadian.CONTENT_TYPE;
            break;
        case CIRCADIAN_ID:
            type = Circadian.CONTENT_ITEM_TYPE;
            break;
        case SLEEP_THRESHOLD:
            type = SleepThreshold.CONTENT_TYPE;
            break;
        case SLEEP_THRESHOLD_ID:
            type = SleepThreshold.CONTENT_ITEM_TYPE;
            break;
        case ACTIGRAPHY:
            type = SleepThreshold.CONTENT_TYPE;
            break;
        case ACTIGRAPHY_ID:
            type = SleepThreshold.CONTENT_ITEM_TYPE;
            break;

        }
        return type;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final int matchId = sUriMatcher.match(uri);
        final int tableIndex = matchId >> BASE_SHIFT;
        final String tableName = TABLE_NAMES[tableIndex];
        final ContentResolver resolver = getContext().getContentResolver();
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long id = db.insert(tableName, null, values);
        Uri resultUri = ContentUris.withAppendedId(uri, id);
        resolver.notifyChange(uri, null);
        return resultUri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        final int matchId = sUriMatcher.match(uri);
        final int tableIndex = matchId >> BASE_SHIFT;
        
        final String tableName = TABLE_NAMES[tableIndex];
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        Cursor result = null;
        String finalWhere;
        switch (matchId) {
        case ACTIVITIES:
        case DEVICE:
        case USER:
        case SLEEP_TIME_EFFECT:        	
        case CIRCADIAN:
        case CUBE_LEVEL_PERIODS:
        case SLEEP_THRESHOLD:	
        case ACTIGRAPHY:
            result = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
            break;
        case ACTIVITY_ID:
        case DEVICE_ID:
        case USER_ID:
        case SLEEP_TIME_EFFECT_ID:        	
        case CIRCADIAN_ID:
        case CUBE_LEVEL_PERIODS_ID:
        case SLEEP_THRESHOLD_ID:	
        case ACTIGRAPHY_ID:
            String id = uri.getPathSegments().get(1);
            finalWhere = BaseColumns._ID + "=" + id;
            if (null == selection) {
                finalWhere = finalWhere + " AND " + selection;
            }
            result = db.query(tableName, projection, finalWhere, selectionArgs, null, null, sortOrder);
            break;
        }
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where,
            String[] whereArgs) {
        final int matchId = sUriMatcher.match(uri);
        final int tableIndex = matchId >> BASE_SHIFT;
        final String tableName = TABLE_NAMES[tableIndex];
        final ContentResolver resolver = getContext().getContentResolver();
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int count = 0;
        String finalWhere;
        switch (matchId) {
        case ACTIVITIES:
        case DEVICE:
        case USER:
        case SLEEP_TIME_EFFECT:        	
        case CIRCADIAN:
        case CUBE_LEVEL_PERIODS:
        case SLEEP_THRESHOLD:	
            count = db.update(tableName, values, where, whereArgs);
            break;
        case ACTIVITY_ID:
        case DEVICE_ID:
        case USER_ID:
        case SLEEP_TIME_EFFECT_ID:        	
        case CIRCADIAN_ID:
        case CUBE_LEVEL_PERIODS_ID:
        case SLEEP_THRESHOLD_ID:	
        	
            String id = uri.getPathSegments().get(1);
            finalWhere = BaseColumns._ID + "=" + id;
            if (null == where) {
                finalWhere = finalWhere + " AND " + where;
            }
            count = db.update(tableName, values, finalWhere, whereArgs);
            break;
        }
        resolver.notifyChange(uri, null);
        return count;
    }

}
