package com.hanson.binsapp.provider;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;


public class ContactsUtils {
	
	public static int getContactId(Context context, String no) {
		final String TAG = "ContactsUtils";
		
		int contactId=-1;
        //Resolving the contact name from the contacts.
        Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(no));
        Cursor c = context.getContentResolver().query(lookupUri, new String[]{ContactsContract.Data.DISPLAY_NAME},null,null,null);
        if(c!=null) contactId=1;
        try {
            c.moveToFirst();
            String  displayName = c.getString(0);
            String ContactName = displayName;  
            Log.i(TAG, "Contact Name="+ContactName);
            //-- Toast.makeText(context, ContactName, Toast.LENGTH_LONG).show();
  
        } catch (Exception e) {
            // TODO: handle exception
        }finally{
            c.close();   
        }
        Log.i(TAG, "contactId:"+contactId);
        return contactId;
	}
}
