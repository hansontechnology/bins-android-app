package com.hanson.binsapp.provider;
 
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.hanson.binsapp.provider.BinsCloudService.OnBinsCloudEventListener;
import com.hanson.binsapp.util.CSVReader;
import org.json.JSONException;
import org.json.JSONObject;

 
public class UnlockingLifeService extends Service 
{
    public static final String TAG = "ulifeService";
    
	private final IBinder mBinder = new UnlockingLifeBinder();
		// Random number generator
	private final Random mGenerator = new Random();
	

    private static String mNewUserName=null;

    public static final String ACTION_SERVICE_START = "com.hanson.binsapp.unlockinglife.ACTION_SERVICE_START";

    public static final String URL_CIRCADIAN_TABLE = "https://dev.unlockinglife.com/lookups/circadian.csv";
    public static final String URL_THRESH_TABLE = "https://dev.unlockinglife.com/lookups/thresholds.csv";
    public static final String URL_TIME_EFFECT_TABLE = "https://dev.unlockinglife.com/lookups/timeeffect.csv";

    public static final String URL_USER_SEARCH = "https://dev.unlockinglife.com/apiv1/users/search/{\"keyword\":\"%s\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";

    public static final String URL_USER_ADD_NEW = "https://dev.unlockinglife.com/apiv1/users/new/{\"fname\":\"%s\",\"lname\":\"%s\",\"email\":\"%s\",\"dob\":\"%s\",\"gender\":\"%s\",\"password\":\"%s\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";
    
    public static final String URL_USER_ADD_DATA = "https://dev.unlockinglife.com/apiv1/add_bin_data/{\"sleepdata\":\"[%s]\",\"stepdata\":\"[%s]\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";

    public static final int MSG_CONNECT_TIMEOUT = 1;
    
    private static StringBuilder mUploadDataSleep=null;
    private static StringBuilder mUploadDataSteps=null;
    private static String mActiveConnectUserUID=null;
    
	private static String mActiveUrl;
	private static String mCommandUrl;
	    
    private Context mContext=null;
    
	private static boolean mInitiated=false;
	private static boolean mUserSearchForLoad=false;
    
    public class UnlockingLifeBinder extends Binder {
        public UnlockingLifeService getService() {
            // Return this instance of LocalService so clients can call public methods
            return UnlockingLifeService.this;
        }
    }
    
    /** method for clients */
    public int getRandomNumber() {
      return mGenerator.nextInt(100);
    }
    
    
    @Override
    public IBinder onBind(Intent arg0)
    {
       // TODO Auto-generated method stub
        Log.i(TAG, "Unlocking bonded");

        return mBinder;
    }
 
    @Override
    public void onCreate() 
    {
       // TODO Auto-generated method stub  
       super.onCreate();
       Log.i(TAG, "Unlocking created");
    }
 
   @Override
   public int onStartCommand(Intent intent, int flags, int startId) 
   {
       Log.i(TAG, "Unlocking start command");

       super.onStartCommand(intent, flags, startId);
       
       BinsDBUtils.getBluetoothLeDao(this);
             
       // We want this service to continue running until it is explicitly  
       // stopped, so return sticky.  
       return START_STICKY; 
    }
 
   	public void updateVitalityTables() {
        downloadCsvFromUrl(URL_CIRCADIAN_TABLE);	   
   	}
   	
    @Override
    public void onDestroy() 
    {
        // TODO Auto-generated method stub
        super.onDestroy();
    }
 
    public List<String[]> parseCSV(InputStream is)
    {
    	
    	//-- Log.i(TAG, "CSV parse:"+is);
    	String next[] = {};
        List<String[]> list = new ArrayList<String[]>();
        
        try {
            // CSVReader reader = new CSVReader(new InputStreamReader(getAssets().open("test.csv")));
            CSVReader reader = new CSVReader(new InputStreamReader(is));                 
        	while(true) {
                next = reader.readNext();
                if(next != null) {
                	//-- Log.i(TAG, "Unlocking csv:"+next);
                    list.add(next);
                } else {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return list;
    }
 
	
    public  List<String[]> downloadCsvFromUrl(String url)
    {
        
    	//-- Log.i(TAG,"CSV:"+url);
       
     	mCommandUrl=mActiveUrl=url;
     	
    	new Thread(networkTask).start(); 
    	
        return null; //-- strList;
    }
    
    
    Runnable networkTask = new Runnable() {  
    	
        @Override  
        public void run() {  
            // TODO  
        	BufferedReader input = null;
        	StringBuilder stringBuilder = null;
        	HttpParams params = new BasicHttpParams();
        	HttpConnectionParams.setConnectionTimeout(params, 10000);
        	HttpConnectionParams.setSoTimeout(params, 1);
        	
        	String currentUrl=mActiveUrl;
        	
            Message responseMessage = new Message();  
            
            HttpURLConnection con = null;
            try {
              URL url=new URL(currentUrl);
              try {
            	trustAllHosts();
    			HttpsURLConnection https = (HttpsURLConnection)url.openConnection();
    			if (url.getProtocol().toLowerCase().equals("https")) {
    				https.setHostnameVerifier(DO_NOT_VERIFY);
    				con = https;
    			} else {
    				con = (HttpURLConnection)url.openConnection();
    			}
    			input = new BufferedReader(new InputStreamReader(con.getInputStream()));
    			stringBuilder = new StringBuilder();
    			String s;
    			while ((s = input.readLine()) != null) {
    				//-- Log.i(TAG,  "unlocking read:"+s);
    				stringBuilder.append(s).append("\n");
    			}            	
              } catch (Exception e) {
            	Log.i(TAG, "unlocking exception:"+e);
              }	
        	} catch (MalformedURLException e1) {
        		e1.printStackTrace();
            
        	} finally {
        		// close buffered
        		if (input != null) {
        			try {
        				input.close();
        			} catch (IOException e) {
        				e.printStackTrace();
        			}
        		}
        		// disconnecting releases the resources held by a connection so they may be closed or reused
        		if (con != null) {
        			con.disconnect();
        		}
        	}
            
            
            Bundle responseData = new Bundle();  
            responseData.putString("url", mCommandUrl);  
        	                        
            String resultStr= (stringBuilder == null ? null : stringBuilder.toString());
            
            if(resultStr==null){
            	Log.i(TAG, "Unlocking return str==null");
            	return;
            }
            responseData.putString("csv", resultStr);   
            
            responseMessage.setData(responseData);  
            mConnectResponseHandler.sendMessage(responseMessage);  
        }  
    };  

    
    Handler mConnectResponseHandler = new Handler() {  
        @Override  
        public void handleMessage(Message responseMessage) {  
            super.handleMessage(responseMessage);  
            
            JSONObject jsonResponse=null;
            
            mHandler.removeMessages(MSG_CONNECT_TIMEOUT);
            
            Bundle responseData = responseMessage.getData();  
            
            String url = responseData.getString("url"); 
            String strCsv = responseData.getString("csv");
            
            List<String[]> strList=null;
            Log.i(TAG, "Unlocking-reply from command:"+url);
            
            if(strCsv!=null){
                try {            	
            	  // A Simple JSON Response Read           
            	  InputStream instream = new ByteArrayInputStream(strCsv.getBytes(StandardCharsets.UTF_8));
            	  strList=parseCSV(instream);
                  instream.close();
            	  
                } catch (Exception e) {
                  Log.i(TAG, "Unlocking+:"+e);	
                  return;
                }
            }
            
            if(strList==null){
            	Log.i(TAG, "Unlocking: retunred strList from CSV==null");
            	return;
            }
            
            if(url.equalsIgnoreCase(URL_USER_ADD_DATA)){
                Boolean isSuccess=true;
                //-- TODO check for complete or not
                
                Log.i(TAG, "Unlocking added data from user:"+mActiveConnectUserUID+", reply:"+strList);
                
                if(mConnectEventListener!=null){
                	mConnectEventListener.addedNewData(isSuccess,mActiveConnectUserUID);
                }
                return;
     
        	}else if(url.equalsIgnoreCase(URL_USER_SEARCH)){
            	Log.i(TAG, "Unlocking: user search response:"+strList.size());
            	
            	Boolean isExist=true;
            	if(strList.size()>=1){	//-- found more than one user account or null
              		String userUID=null ;
              		String userFirstName=null ;                                    		
              		String userLastName=null  ;                                    		
              		String userName=null  ;                                    		

            		String tokenFirst=strList.get(0)[0];	// first word of first row            		
            		Log.i(TAG, "user search returns:"+tokenFirst);
            		
            		if(tokenFirst.equals("null")){	//-- returns word of 'null'
            			Log.i(TAG, "user search found null return false");
            			isExist=false;
            			
            		}else{	//-- not 'null' means found something           			            	            			
                       	try{
                       		//-- String strResponseWithProcessed = strCsv.replace("\\","");
                       		
                       		String str="{\"row0\":\"{\"id\":\"8481\",\"first_name\":\"F\",\"last_name\":\"N\",\"email\":\"oo@oo.com\"}\"}";
                       	                     
                       		//-- 20151211 WORKAROUND - hard coded, to avoid BUG from server
                       		//-- it should be {"row0":{"id":8481","first_name":"F"...}} for formal JSON                     		
                       		str=str.substring(9,str.length()-2);
                       		
                    		Log.i(TAG, "Unlocking loaded response:"+str);		
                       		
                     		// jsonResponse= new JSONObject(str);                    		
                       		
                      		//- JSONObject form  = jsonResponse.getJSONObject("row0");
                    		JSONObject form  = new JSONObject(str);
                      		userUID =  form.getString("id");
                      		userFirstName  = form.getString("first_name");                                    		
                      		userLastName  = form.getString("last_name");                                    		
                      		userName  = form.getString("email");                                    		
                             		
                    		Log.i(TAG, "Unlocking user id/f/l/email:"+userUID+":"+userFirstName+":"+userLastName+":"+userName);

                    	}catch(JSONException ex){
                    		ex.printStackTrace();
                    	}
                      	isExist=true;
            		}
                	if(mUserSearchForLoad==false){
                		Log.i(TAG, "Unlocking hasUser sent");
                		mConnectEventListener.hasUser(isExist); 
                	}else{
                		Log.i(TAG, "Unlocking : calling loadedUserProfile");
                		
                		//-- if in case not found, then userUID==null to indicate
                		mConnectEventListener.loadedUserProfile(
                				userName, userUID, userFirstName, userLastName, 
                												null, -1, -1, -1, -1);
                		//-- 0 means not returned
                	}
            	}
            }else if(url.equalsIgnoreCase(URL_USER_ADD_NEW)){
            	/*
            	    {"Message":"You have successfully registered.","userid":"10723","name":"nigel.weeks@kar.."}
					Repeating it returns:
					{"Message":"Email address already registered. Please try to login with this email."}
            	 */           	  
            	           	
            	String userName=null;
            	String userUID=null;
            	Boolean isSuccess=true;
            	try{
            		            		
            		jsonResponse=new JSONObject(strCsv);
            		
            		//-- it returns first/last name, no need
            		// userName=jsonResponse.getString("name");
            		userName=mNewUserName;
            		mNewUserName=null;
            		
            		String strUserUID=jsonResponse.getString("userid");
            		if(strUserUID!=null){
            			userUID=strUserUID; // Integer.valueOf(strUserUID);
            		}else{
            			isSuccess=false;
            		}
            		
            		Log.i(TAG, "Unlocking added new user of name and id:"+userName+":"+userUID);

            	}catch(JSONException ex){
            		ex.printStackTrace();
            		isSuccess=false;
            	}finally{            	
            	}
            	
           		mConnectEventListener.addedNewUser(isSuccess, userName, userUID);
           	 
            }else if(url.equalsIgnoreCase(URL_CIRCADIAN_TABLE)){
                BinsDBUtils.activeDB().updateCircadianTable(strList);
                downloadCsvFromUrl(URL_THRESH_TABLE);
            }else if(url.equalsIgnoreCase(URL_THRESH_TABLE)){
            	BinsDBUtils.activeDB().updateThresholdTable(strList);
                downloadCsvFromUrl(URL_TIME_EFFECT_TABLE);
            }else if(url.equalsIgnoreCase(URL_TIME_EFFECT_TABLE)){
                BinsDBUtils.activeDB().updateTimeEffectTable(strList);
            }
        }  
    };    
    
    final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
    	 
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};


	/**
	* Trust every server - dont check for any certificate
	*/
	private void trustAllHosts() {
		final String TAG = "trustAllHosts";
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {

			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[] {};
			}

			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				Log.i(TAG, "checkClientTrusted");
			}

			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				Log.i(TAG, "checkServerTrusted");
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Boolean isNetworkConnected() {
		
		Boolean hasNetwork=false;
		  
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		  
		if(cm==null){
			Log.i(TAG, "isNetwotkConnected cm==null");		  
		}else{
			NetworkInfo ni = cm.getActiveNetworkInfo();
			if (ni != null) {
				  // There are no active networks.			  
				  hasNetwork=true;
			}
		}
		
		if(hasNetwork==false){
			Log.i(TAG, "isNetwotkConnected: network is NOT connected.");
		}
		return hasNetwork;
	}

	public Boolean isInternetAvailable() {
	    try {
	        InetAddress ipAddr = InetAddress.getByName("www.yahoo.com"); //You can replace it with your name
	        
	        if (ipAddr.equals("")) {
	            return false;
	        } else {
	            return true;
	        }

	    } catch (Exception e) {
	        return false;
	    }

	}

	public Boolean checkInternet() {
		if(isNetworkConnected()==true){ //-- && isInternetAvailable()){
			return true;
		}
	    return false;
	}

	public Boolean loadUserProfile( String userName, String password) {
	    
	    if(checkInternet()==false) return false;
	    
	    //-- 20151210 ignore password checking 
	    
	    String strUrl= String.format(URL_USER_SEARCH, userName);
	    
	    mActiveUrl=strUrl;
	    mCommandUrl=URL_USER_SEARCH;
	    
	    mUserSearchForLoad=true;	    
		mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIMEOUT, 10000); // as ms
		
	    new Thread(networkTask).start(); 
	    	    
	    return true;
	   
	}


	public Boolean checkUserExist( String userName) {
	    
	    if(checkInternet()==false) return false;
	    
	    String strUrl= String.format(URL_USER_SEARCH, userName);
	    
	    mActiveUrl=strUrl;
	    mCommandUrl=URL_USER_SEARCH;
	    
	    mUserSearchForLoad=false;	     	
		mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIMEOUT, 10000); // as ms
		
	    new Thread(networkTask).start(); 
	    	    
	    return true;
	   
	}
	
	public boolean insertOrUpdateUserProfile(
			String userUID,
			String userName,
			String firstName,
			String lastName,
			String dob,
			int weight,
			int height,
			Boolean gender,
			String password) 
	{

		   if(checkInternet()==false) return false;
			   
		    String strGender="F";
		    if(gender==true) strGender="M";
		    
		    if(userUID==null){
		        // TODO
		    }

		    String strUrl= String.format(URL_USER_ADD_NEW, 
		    		firstName, lastName, userName, dob, strGender, password);
		    
		    mActiveUrl=strUrl;
		    mCommandUrl=URL_USER_ADD_NEW;	    
		     	
		    mNewUserName=userName;
		    
			mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIMEOUT, 10000); // as ms
			
		    new Thread(networkTask).start(); 
		    	    
		    return true;
	}

	public boolean loadActivityRecordsStepPage(final String userUID, final long fromTime, final long toTime) {
		   
			//-- DO NOTHING
		   return false;
    }

	public boolean loadActivityRecordsSleepPage(final String userUID, final long fromTime, final long toTime) {
		   
			//-- DO NOTHING
		   	return false;
	}

	/*
	mUploadDataSleep=@"1||23||1423204200";
	mUploadDataSteps=@"1||23||1423204200";

	user, value, time
	*/


	public void addUploadActivity( int actValue, int type, long time) {
		
	   String strRecord=String.format("\"%s||%d||%d\";",mActiveConnectUserUID, actValue, time); 
	   
	   Log.i(TAG, "Unlocking: addUploadActivity:"+time+":"+type+":"+actValue);
	   
	   if(type==Bins.BINS_ACTIVITY_TYPE_SLEEP){
		   mUploadDataSleep.append(strRecord);
	   }else if(type==Bins.BINS_ACTIVITY_TYPE_STEPS){
	       mUploadDataSteps.append(strRecord);
	   }
	   //-- 20151210 currently only support those two activity types from Unlocking Life server
	   //-- ignore other type data
	   
	}


	public void createUploadSession(String userUID, int type) {
	   
	   Log.i(TAG, "Unlocking: createUploadSession UID="+userUID);
	   
	   mActiveConnectUserUID=userUID;

	   if(type==Bins.BINS_ACTIVITY_TYPE_SLEEP){
	       mUploadDataSleep= new StringBuilder("");
	   }else if(type==Bins.BINS_ACTIVITY_TYPE_STEPS){
	       mUploadDataSteps= new StringBuilder("");
	   }
	   
	}


	public Boolean startUploadSession(){
	   if(checkInternet()==false) return false;
	   
	   //-- NSLog(@"startUploadSession internet checked ok");
	   
	   if(mUploadDataSleep==null) return false;
	   
	   if(mUploadDataSleep.toString().equals("") &&
	       mUploadDataSteps.toString().equals("")     ){
	       
	       Log.i(TAG,"Unlocking: startUploadSession: no more data to upload");
	       return false;
	   }else{
		   //-- Log.i(TAG, "startUploadSession :"+mUploadDataSleep+" and "+mUploadDataSteps);
	   }
	    
	   
	   String strUrl= String.format(URL_USER_ADD_DATA, 
			   mUploadDataSleep.toString(),
	           mUploadDataSteps.toString());
	   
	   //-- Log.i(TAG, "startUploadSession strUrl:"+strUrl);
	   
	   mActiveUrl=strUrl;
	   mCommandUrl=URL_USER_ADD_DATA;	    
	    	   
	   mHandler.sendEmptyMessageDelayed(MSG_CONNECT_TIMEOUT, 10000); // as ms
		
	   new Thread(networkTask).start(); 
	   	    
	   Log.i(TAG, "startUploadSession");
	   
	    
	   return true;
	   
	}


	public OnBinsCloudEventListener mConnectEventListener; //listener field

	public boolean isSupportRemoteGuest(){
		return false;
	}
	 
	public boolean isSupportUpdateUserProfile(){
		return false;
	}
	
	public void setConnection(Context ctext) {	   
		mContext = ctext;
		
	    if(mInitiated==false){
	       	mInitiated=true;
	       	//-- 20151209 download mental effectiveness tables
	       	updateVitalityTables();
	    }

    }

	//setting the listener
	public void setConnectEventListener(OnBinsCloudEventListener eventListener) {
		this.mConnectEventListener=eventListener;
	}

	public void connectTimeoutHandler() {
		if(this.mConnectEventListener!=null){
			// this.mConnectEventListener.onEvent(false);
			// TODO set the appropriate argument to let the service to return error properly to callbacks
			// mConnectResponseHandler.sendMessage(msg);
			
       		mConnectEventListener.serviceTimeout();
       	    
		}	
	}

	private Handler mHandler = new Handler(new Handler.Callback() {

	    @Override
	    public boolean handleMessage(Message msg) {
	    	if(msg.what==MSG_CONNECT_TIMEOUT){
	    		connectTimeoutHandler();
	    	} 
	    	return true;
	    }
	});
    

}
