package com.hanson.binsapp.provider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;


public class TelephonyStateReceiver extends BroadcastReceiver {
    private Context mContext;
    private final String TAG = "TelephonyStateReceiver";

    // Retrieve SMS
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        int contactId = -1;

        
        if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(TelephonyManager.EXTRA_STATE_RINGING)) {
            // This code will execute when the phone has an incoming call
             
            // get the phone number 
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            contactId = ContactsUtils.getContactId(mContext, incomingNumber);

            if(contactId != -1){
            	Intent intentToSend = new Intent("com.data.telephony.RECEIVE").putExtra(
                         "call_event", "incoming "+contactId);
                context.sendBroadcast(intentToSend);
            }	
    
            Log.i(TAG, "incoming call:"+"incoming "+contactId); 
            
        } else if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                TelephonyManager.EXTRA_STATE_IDLE)
                || intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
                        TelephonyManager.EXTRA_STATE_OFFHOOK)) {
            // This code will execute when the call is disconnected
           	Intent intentToSend = new Intent("com.data.telephony.RECEIVE").putExtra(
                    "call_event", "hang");
           context.sendBroadcast(intentToSend);
 
           Log.i(TAG, "incoming call:"+"hang"); 

        }        

    }

    
}