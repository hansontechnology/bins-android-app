
package com.hanson.binsapp.provider;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.ContentResolver;
import android.content.ContentValues;  
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import com.hanson.binsapp.BinsActivity;
import com.hanson.binsapp.R;
import com.hanson.binsapp.provider.Bins.Activities;
import com.hanson.binsapp.provider.Bins.Circadian;
import com.hanson.binsapp.provider.Bins.CubeLevelPeriods;
import com.hanson.binsapp.provider.Bins.Device;
import com.hanson.binsapp.provider.Bins.SleepThreshold;
import com.hanson.binsapp.provider.Bins.SleepTimeEffect;
import com.hanson.binsapp.provider.Bins.User;
import com.hanson.binsapp.provider.BinsCloudService.OnBinsCloudEventListener;

public class BinsDBUtils {
	
	public static BinsCloudService mBinsCloudServer;
	//-- private static UnlockingLifeService mBinsCloudServer;	//-- HBP replacement
	
    private static final String TAG = BinsDBUtils.class.getSimpleName();
    public static final int DAY_TIME_LENGTH = 24 * 60 * 60 * 1000;
    public static final String HANSON_PREFERENCE = "hanson_preference";
    
    public static final String BINS_CONFIGURATION = "bins_configuration";
    
    public static final String ACTIVE_USER = "active_user_id";
    public static final String BACKGROUND_SYNC = "background_sync_mode";
    public static final String FILTER_DUMMY = "filter_dummy_data";
    
    
    
    public static final int BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK= (1<<0);
    public static final int BINS_CONFIG_COMMAND_HRD_START_MASK= (1<<1); 
    public static final int BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK= (1<<2); 
    public static final int BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK=(1<<3);
    public static final int BINS_CONFIG_STEP_WHILE_HRD_MASK= (1<<4);
    public static final int BINS_CONFIG_HEARTRATE2SLEEP_MASK= (1<<5);
    public static final int BINS_CONFIG_HEARTRATE_ENABLE_MASK= (1<<6);
    public static final int BINS_CONFIG_HRD_ALWAYS_ON_MASK= (1<<7); 

    public static final int BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK= (1<<9);
    public static final int BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK=   (1<<10);
    public static final int BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK= (1<<12);
    public static final int BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK=   (1<<13);
    public static final int BINS_CONFIG_ACTIVITY_NOTIFY_MASK= (1<<14);   
          
   //-- Shake UI threshold spreads on bits [15, 11, 8]
    
    public static final int BINS_ALERT_FINDER_MASK=  (0x80);	//-- 11.27 updated from 00
    public static final int BINS_ALERT_MESSAGE_MASK= (0x40);
    public static final int BINS_ALERT_COMMAND_SET=   (0x01);
    public static final int BINS_ALERT_COMMAND_RESET= (0x00);
    public static final int BINS_CONFIG_SETTING_ALL_MASK= (0xFFF0);    //-- 10.10 0xFFF0 to 0x7FE0

    
    public static final int  BINS_BATTERY_LOW_LEVEL = 35;
    public static final int  BINS_FINDER_LEVEL_MAX  = 6;
    public static final int  BINS_SHAKE_LEVEL_MAX = 8;

    public static final int  BINS_ACTIVITY_TYPE_STEPS =1;
    public static final int BINS_ACTIVITY_TYPE_SLEEP =3;
    public static final int BINS_ACTIVITY_TYPE_PULSE =2;

    public static final int BINS_MODEL_BINSX1 =  0;
    public static final int BINS_MODEL_BINSX2 =  1;

    public static final int DEFAULT_BINS_CONFIGURATION = 
    		BINS_CONFIG_ACTIVITY_NOTIFY_MASK | BINS_CONFIG_HEARTRATE_ENABLE_MASK | BINS_CONFIG_HEARTRATE2SLEEP_MASK |
    		BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK |
    		BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK; 
    						//-- 150412 default to enable sleep monitoring
    						//-- 150603 default to enable high brightness  
    
    public static final int DEFAULT_BINS_USER_GOAL = 10000;
    public static final int DEFAULT_BINS_FINDER_THRESHOLD =  (BINS_FINDER_LEVEL_MAX/2);
    public static final int DEFAULT_BINS_SHAKE_UI_THRESHOLD =  (BINS_SHAKE_LEVEL_MAX/2);   		

    public static final int DEFAULT_BINS_FINDER_ENABLE =  0;
    public static final int DEFAULT_BINS_SLEEP_ENABLE  =  1; // 150412 from 0 to 1

    private static BinsDBUtils sDao=null;
    private Context mContext;
    private static ContentResolver mResolver=null;
    
    private static int mActiveUserID=0;
    
    private static boolean mDataIsUploading=false;
        
    public OnBinsDBEventListener mBinsDBEventListener; //listener field

    public interface OnBinsDBEventListener{
 	   public void binsDataChanged();
    }
    //setting the listener
    public void setEventListener(OnBinsDBEventListener eventListener) {
 	   this.mBinsDBEventListener=eventListener;
    }
    
    private BinsDBUtils(Context context) {
        mContext = context;
        mResolver = context.getContentResolver();
                
        Log.i(TAG, "going to create BinsCloud service from DB");
        
    }
    
    public static void setBinsCloud(){ 	
        mBinsCloudServer=BinsActivity.mBinsCloudServer;
    }
    
    private OnBinsCloudEventListener onBinsCloudEventListner=new OnBinsCloudEventListener(){

        public void serviceTimeout(){
            if(mDataIsUploading==true) mDataIsUploading=false;        	
        }

        public void hasUser(Boolean isUserExist){
        	//-- do nothing
        }
        
    	public void loadedUserProfile(String userName,String userUID, String firstName, String lastName,
				String dob, int weight, int height, int isMale, long lastSyncTime){ 
    		
    		Log.i(TAG, "loadedUserProfile from DB");
     	}
    	
    	public void loadUserProfileFailed(String userName){
     	}
        
        public void addedNewUser(Boolean isSuccess, String userName, String userUID){
        	
        	Log.i(TAG, "addedNewUser called:"+isSuccess+":"+userName+":"+userUID);
        	
            if(isSuccess==false) return;
            
            
          	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_UID, userUID);

        	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User.COLUMN_USER_NAME + "=?",
        				new String[] {
        				userName
                	});
        	
        	} catch (Exception e) {
            	Log.i(TAG, "error found from addedNewUser");
        	}
 
        	mBinsDBEventListener.binsDataChanged();
        	
        }
        
        public void addedNewData(Boolean isSuccess, String userUID) {
            Log.i(TAG, "addedNewData of success, UID="+isSuccess+":"+userUID);
                 
            mDataIsUploading=false;

            if(isSuccess==true){                
                uploadTask();
            }           
        }
        
       	public void loadActivityCompleted(String userUID){
       		Log.i(TAG, "loadActivityCompleted called by BinsDBUtils");   
       		       		       		
       		BinsActivity.mBinsActivity.uiFragmentUpdate();      		
       		
    	}
        
    };	//-- END of Cloud Event Listener


    public void updateUserUID(String userName, String userUID){
    	        
      	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_UID, userUID);

    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User.COLUMN_USER_NAME + "=?",
    				new String[] {
    				userName
            	});
    	
    	} catch (Exception e) {
        	Log.i(TAG, "error found from updateUserUID");
    	}
    }

    public static BinsDBUtils getBluetoothLeDao(Context context) {
        if (null == sDao) {
            sDao = new BinsDBUtils(context);            
       }
        return sDao;
    }

    public static BinsDBUtils activeDB() {
    	return sDao;
    }
    
    public static void release() {
        BinsDBUtils.mResolver = null;
    }
    
    
    //----------------------------------------
    
    public Boolean insertNewUser(
    			String dUserName, 
    			String dFirstName,
    			String dLastName,
    			String 	dBirthday,
    			int 	dWeight,
    			int 	dHeight,
    			Boolean dGender,
    			String dPassword,
    			int dIsNetworkUser,
    			String dUserUID,
    			long dUserLastSyncTime
    			)
    {
    	Boolean isSuccessToCallCloud=false; 	
    	
    	if(dIsNetworkUser==1){
    		mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

    		isSuccessToCallCloud=mBinsCloudServer.insertOrUpdateUserProfile(
        			dUserUID, dUserName, dFirstName,dLastName,dBirthday,
        									dWeight,dHeight,dGender, dPassword	);
    		
	 	    Log.i(TAG, "save profile, cloud insertion called, and return:"+isSuccessToCallCloud);

    	}
    	
       	Cursor cursor=queryUserName(dUserName); 
        if (cursor != null && cursor.getCount()>0) {
            Log.d(TAG, "Already existed user name"); 
            cursor.close();
            return isSuccessToCallCloud;
        } 

        if(cursor!=null) cursor.close();
 
        
    	int aRemoteUser=dIsNetworkUser;
    	
        long currentTime=(System.currentTimeMillis() / 1000 / 60);

        ContentValues values = new ContentValues();        
        values.put(User.COLUMN_USER_NAME, dUserName);
        values.put(User.COLUMN_USER_FIRSTNAME, dFirstName);
        values.put(User.COLUMN_USER_LASTNAME, dLastName);        
        values.put(User.COLUMN_USER_DOB, dBirthday);
        values.put(User.COLUMN_USER_WEIGHT, dWeight);
        values.put(User.COLUMN_USER_HEIGHT, dHeight);
        values.put(User.COLUMN_USER_GENDER, dGender);
        values.put(User.COLUMN_USER_ALARM, (06<<8+30));
        values.put(User.COLUMN_USER_DAILYGOAL, 10000);
        values.put(User.COLUMN_USER_FINDER_ENABLED, DEFAULT_BINS_FINDER_ENABLE);
        values.put(User.COLUMN_USER_SLEEP_ENABLED, DEFAULT_BINS_SLEEP_ENABLE);
        values.put(User.COLUMN_USER_DEVICE, 0);	// no device assigned
        values.put(User.COLUMN_USER_PASSWORD, dPassword);
        values.put(User.COLUMN_USER_IS_FROM_NETWORK, aRemoteUser);
        values.put(User.COLUMN_USER_UID, dUserUID);
        values.put(User.COLUMN_USER_SYNC_TIME, dUserLastSyncTime);	
        values.put(User.COLUMN_USER_START_TIME, currentTime);	
        
        mResolver.insert(User.CONTENT_URI, values);
        
        
		int user=getUserIdByName(dUserName);
		
		setActiveUser(user);
		
        Log.i(TAG, "insertNewUser:" + dUserName +":"+ dBirthday + String.valueOf(dWeight)+":userID="+user);

		return isSuccessToCallCloud;
		
    }
    
    
   
// reuse the default user name if there is, and the record too.    
    public int insertNewUser()
    {
    	String defaultUserName;
    	defaultUserName=mContext.getResources().getString(R.string.default_user_name);
    	int user=getUserIdByName(defaultUserName);

    	if(user==0){
    
    		insertNewUser(
        			defaultUserName, 
        			"",
        			"",
        			"1968-04-30",
        			80,
        			172,
        			true,
        			"123456",
        			0,
        			"",
        			0
    			); 
    
    		user=getUserIdByName(defaultUserName);
    	}
    	
    	setActiveUser(user);
    	
    	return user;
    
	}

    public void updateUserDevice(
    		int user,
    		String deviceMac
			)
    {    	
    	
       	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_DEVICE, deviceMac);

    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(user)
            	});
    	
    	} catch (Exception e) {
        	// nothing
    	}
    
    }

    
    public boolean updateUser(
    		int user,
			String dUserName, 
			String dFirstName, 
			String dLastName, 			
			String dBirthday,
			int dWeight,
			int dHeight,
			Boolean dGender,
			String password,
			int isRemote,
			String userUID,
			long lastSyncTime
			)
    {

        boolean isSuccessToCallCloud=false;
        
        int intIsRemote=isRemote;
    	
    	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_NAME, dUserName);
    	values.put(User.COLUMN_USER_FIRSTNAME, dFirstName);
    	values.put(User.COLUMN_USER_LASTNAME, dLastName);
    	values.put(User.COLUMN_USER_DOB, dBirthday);
    	values.put(User.COLUMN_USER_WEIGHT, dWeight);
    	values.put(User.COLUMN_USER_HEIGHT, dHeight);
    	values.put(User.COLUMN_USER_GENDER, dGender);
    	values.put(User.COLUMN_USER_PASSWORD, password);
    	values.put(User.COLUMN_USER_IS_FROM_NETWORK, intIsRemote);
    	values.put(User.COLUMN_USER_SYNC_TIME, lastSyncTime);
    
    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(user)
            	});   	
    	} catch (Exception e) {
        	// nothing
    	}
 
        //-- 20151210
        if(isRemote==1){   // only a network user needs to update profile
        	
            mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

        	if(mBinsCloudServer.isSupportUpdateUserProfile()==true){
        		isSuccessToCallCloud=mBinsCloudServer.insertOrUpdateUserProfile(
        			userUID, dUserName, dFirstName,dLastName,dBirthday,
        									dWeight,dHeight,dGender, password		);
        	}
         }

        return isSuccessToCallCloud;
    }

    public void deleteUser(int user)
    {		
    	ContentValues values = new ContentValues();
    	values.put(User._ID, user);
    	
    	try{
    		int userCounts = mResolver.delete(User.CONTENT_URI,
                User._ID + "=?",
                new String[] {
                    String.valueOf(user)
                });
            Log.i(TAG, "Delete User of :" + String.valueOf(userCounts));        

   		} catch (Exception e) {
   			// nothing
   		}

    	try{
    		int activityCounts = mResolver.delete(Activities.CONTENT_URI,
                Activities.COLUMN_ACTIVITY_USER + "=?",
                new String[] {
                    String.valueOf(user)
                });
            Log.i(TAG, "Delete Activities :" + String.valueOf(activityCounts));        

   		} catch (Exception e) {
   			// nothing
   		}

    	
    }  

    
    public Cursor queryUserName(String username) {
    
        try {
        	return mResolver.query(
        		User.CONTENT_URI,
        		null,  //-- all columns
                User.COLUMN_USER_NAME + "=?",  //-- where
                new String[] {
        				username
        				},	//-- where arguments
                null	//-- order
                );
        } catch (Exception e) {
            // nothing
        }
        return null;
    }
    
    
    // to list all users 
    public Cursor getUsers() {
    
    	// try{
        	return mResolver.query(
        		User.CONTENT_URI,
        		null,  //-- all columns
                null,  //-- where
                null,	//-- where arguments
                null	//-- order
                );
    	//} catch (Exception e) {
    		// nothing
    	//}
    	//return null;
    }
    
    public void setBackgroundSync(int enable){
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(BACKGROUND_SYNC, enable).commit();   	
    }
    public int getBackgroundSync() {
        int status = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        status = sharedPref.getInt(BACKGROUND_SYNC, 0);
        return status;
    }
    
    
    public void setFilterUnused(int enable){
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(FILTER_DUMMY, enable).commit();   	
    }
    public int getFilterUnused() {
        int status = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        status = sharedPref.getInt(FILTER_DUMMY, 0);
        return status;
    }

    public void toggleFilterUnused() {
        int status=getFilterUnused();
        if(status==1) status=0;
        else status=1;
        
        setFilterUnused(status);
    }

    
    public int setActiveUser(int user){
    	
    	int userExisting=getActiveUserID();
    	if(userExisting==0){
    		setBackgroundSync(1);	//-- 12.07 means enable background sync as a default
    	}
    	
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(ACTIVE_USER, user ).commit();
    	mActiveUserID=user;
    	return user;    	
    }
    public int getActiveUserID() {
        int user = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        user = sharedPref.getInt(ACTIVE_USER, 0);
        
        mActiveUserID=user;
        
        return user;
    }
    
    public int getActiveDeviceID() {
        int device = 0;
        int user=0;
        
        user=getActiveUserID();
        device=getUserDevice(user);
       
        return device;
    }

    public void setLastDeviceMac(String deviceMac) {
    	
    	int user=getActiveUserID();    	
    	int device=getUserDevice(user);
    	
    	setDeviceAddress(device, deviceMac);

 /*   	
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        final String prevDeviceMac = sharedPref.getString(LAST_SYNC_DEVICE, null);
        
        //--
        if(prevDeviceMac!=null && prevDeviceMac.equals(deviceMac))
        	return;
        
        sharedPref.edit().putString(LAST_SYNC_DEVICE, deviceMac).commit();
  */
    	
    }

    
    //-- 01.09
    public long getActiveUserGoalAchieveTime() {
        long value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_GOAL_ACHIEVETIME
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return value;      	
    }   
    
    public class UserRecord {
    	public String userName;
    	public String userPassword;    	
    	public String firstName;
    	public String lastName;
    	public String userUID;
    	public String dob;
    	public int	weight;
    	public int  height;
    	public int 	gender;
    	public int isNetworkUser;
    	public long lastSyncTime;
    	
    	public UserRecord(
    			String dUserName, 
    			String dFirstName, 
    			String dLastName, 
    			String dDOB, 
    			int dWeight, 
    			int dHeight, 
    			int dGender, 
    			String dPassword, 
    			int dIsRemoteUser, 
    			String dUserUID, 
    			long dLastSyncTime){
    		
    		userName=dUserName;
    		firstName=dFirstName;
    		lastName=dLastName;
    		dob=dDOB;
    		weight=dWeight;
    		height=dHeight;
    		gender=dGender;
    		lastSyncTime=dLastSyncTime;
    		isNetworkUser=dIsRemoteUser;
    		userPassword=dPassword;
    		userUID=dUserUID;
    	}
    }
    
    public UserRecord getUserProfile(int userID) {
           
        UserRecord userRecord= new UserRecord(
        		"", 
        		"", 
        		"", 
        		"1970/01/01", 
        		70, 170, 0, 
        		"123456", 
        		0, 
        		"", 
        		0);
        
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_NAME,
                      User.COLUMN_USER_FIRSTNAME,
                      User.COLUMN_USER_LASTNAME,
                      User.COLUMN_USER_DOB,
                      User.COLUMN_USER_WEIGHT,
                      User.COLUMN_USER_HEIGHT,
                      User.COLUMN_USER_GENDER,
                      User.COLUMN_USER_IS_FROM_NETWORK,
                      User.COLUMN_USER_PASSWORD,
                      User.COLUMN_USER_SYNC_TIME,
                      User.COLUMN_USER_UID
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(userID)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				userRecord.userName = cursor.getString(0);
       				userRecord.firstName = cursor.getString(1);
       				userRecord.lastName = cursor.getString(2);
       				userRecord.dob = cursor.getString(3);
       				userRecord.weight = cursor.getInt(4);
       				userRecord.height = cursor.getInt(5);
       				userRecord.gender = cursor.getInt(6);
       				userRecord.isNetworkUser = cursor.getInt(7);
       				userRecord.userPassword = cursor.getString(8);
       				userRecord.lastSyncTime = cursor.getLong(9);
       				userRecord.userUID = cursor.getString(10);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return userRecord;
    }

    public int getActiveUserGoalAchieveValue() {
        int value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_GOAL_ACHIEVEVALUE
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return value;      	
    }   

    public void setActiveUserGoalAchieve(long aTime, int aValue) {
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_GOAL_ACHIEVETIME, aTime);
           	values.put(User.COLUMN_USER_GOAL_ACHIEVEVALUE, aValue);       	
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        	}

    }   
   
    public boolean getActiveUserStepWatchEnabled() {
        int value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_STEPWATCH_ENABLED
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}
       	if(value==0) return false;
       	
        return true;      	
    }    
    
    public long getActiveUserStepWatchStartTime() {
        long value=0;
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_STEPWATCH_STARTTIME
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(mActiveUserID)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

       	//-- Log.i(TAG, "getActiveUserStepWatchStartTime:"+value);
        return value;      	
    }   
    
    public int getActiveUserStepWatchDelta() {
        int value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_STEPWATCH_DELTA
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return value;      	
    }   

    public void resetActiveUserStepWatchDelta() {
    	ContentValues values = new ContentValues();
       	values.put(User.COLUMN_USER_STEPWATCH_DELTA, 0);       	
     	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(mActiveUserID)
            	});
    	
    	} catch (Exception e) {
        	// nothing
    	}

    }   
    public void setActiveUserStepWatchStartTime(long startTime) {
        	int deltaMinute=getUserActivity(getActiveUserID(), Bins.BINS_ACTIVITY_TYPE_STEPS, startTime);
        	Log.i(TAG, "setActiveUserStepWatchStartTime:"+startTime+":"+deltaMinute);
        	
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_STEPWATCH_STARTTIME, startTime);
           	values.put(User.COLUMN_USER_STEPWATCH_DELTA, deltaMinute);       	
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
        		Log.i(TAG, "StepWatch:"+e.getLocalizedMessage());
            	// nothing
        	}

    }   
    public void setActiveUserStepWatchEnable(boolean enable) {
        //-- update DB
        {   		
       	
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_STEPWATCH_ENABLED, enable);
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        	}
     
        }
        
        resetActiveUserStepWatchDelta();

    }

    public boolean getActiveSleepEnable() {
        int value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_SLEEP_ENABLED
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}
       	if(value==0) return false;
       	
        return true;      	
    }    
    public void setActiveSleepEnable(boolean enable) {
        //-- update DB
        {   		
       	  
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_SLEEP_ENABLED, enable);
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        	}
     
        }

    }
    public int getBinsConfiguration() {
    	/*
         SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        int config = sharedPref.getInt(BINS_CONFIGURATION, DEFAULT_BINS_CONFIGURATION); //-- return 2500 if not exist
        return config;
        */
    	
        // Get from DB
    	boolean isSuccess=false;
       	int value=0;
       	int device=getActiveDeviceID();
       	try{
       		Cursor cursor =mResolver.query(Device.CONTENT_URI,
           		new String[] {
                      Device.COLUMN_CONFIGURATION
                  },
                  Device._ID + "=? ",
                   new String[] {
                           String.valueOf(device)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       				isSuccess=true;
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}
       	
       	if(isSuccess==true){
       		boolean enable=getActiveSleepEnable();
       		if(enable==true){       			
            	value |= (BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK);   
            }else{
            	value &= (~(BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK));       		
       		}       		
       	}
       	
        return value;      	
    }    
    
    public void setBinsConfiguration(int config) {
    	/*
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        Log.d(TAG, "set Configuration "+ config);
        
        sharedPref.edit().putInt(BINS_CONFIGURATION, config ).commit();
		*/
    	
        //-- update DB
        {   		
        	int device=getActiveDeviceID();
        	
        	ContentValues values = new ContentValues();
        	values.put(Device.COLUMN_CONFIGURATION, config);
         	try{
        		mResolver.update(Device.CONTENT_URI,
        				values,
        				Device._ID + "=?",
        				new String[] {
        				String.valueOf(device)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        	}
     
        }
        
     	if((config & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0 ){
       		setActiveSleepEnable(true);
     	}else{
       		setActiveSleepEnable(false);     		
       	}

    }

    public void setActiveDailyGoal(int dgoal) {
        //-- update DB
        {   		
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_DAILYGOAL, dgoal);
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        	}
     
        }

    }
    
    public int getActiveUserDailyGoal() {
        // Get from DB
     	int value=0;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
         		new String[] {
                    User.COLUMN_USER_DAILYGOAL
                },
                User._ID + "=? ",
                 new String[] {
                         String.valueOf(mActiveUserID)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getInt(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     	}
         return value;
    }    

    public void setActiveUserAlarm(int alarmTime, boolean enable)
    {
    	int alarm=alarmTime & 0x7FFF;
    	if(enable) alarm |= 0x8000;
    	setActiveUserAlarm(alarm);
    }
    
    public void setActiveUserAlarmEnable(boolean enable) {
    	int alarm=(getActiveUserAlarm() & 0x7FFF);
    	if(enable==true) alarm |= 0x8000;
    	setActiveUserAlarm(alarm);
    }
    
    public void setActiveUserAlarm(int alarm) {
        //-- update DB
    	Log.i(TAG, "set alarm value is "+alarm);
        {   		
        	ContentValues values = new ContentValues();
        	values.put(User.COLUMN_USER_ALARM, alarm);
         	try{
        		mResolver.update(User.CONTENT_URI,
        				values,
        				User._ID + "=?",
        				new String[] {
        				String.valueOf(mActiveUserID)
                	});
        	
        	} catch (Exception e) {
            	// nothing
        		Log.i(TAG, "error from set alarm");
        		
        	}
     
        }

    }
    
    public int getActiveUserAlarm() {
        // Get from DB
     	int value=0;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
         		new String[] {
                    User.COLUMN_USER_ALARM
                },
                User._ID + "=? ",
                 new String[] {
                         String.valueOf(mActiveUserID)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getInt(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     		Log.i(TAG, "error from get alarm ");
     	}
     	Log.i(TAG, "get alarm value is "+value);
     	
         return value;
    }    

    public boolean getActiveUserAlarmEnable() {
    	boolean alarmEnable=false;
    	int alarm=getActiveUserAlarm();
    	if((alarm & 0x8000)!=0) alarmEnable=true;
    	return alarmEnable;
    }
    public int getActiveUserWeight() {
   
    /*
        int weight = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        weight = sharedPref.getInt(USER_WEIGHT, 60); 
    */
    	
        // Get from DB
    	int value=0;
    	try{
    		Cursor cursor =mResolver.query(User.CONTENT_URI,
         		new String[] {
                    User.COLUMN_USER_WEIGHT
                },
                User._ID + "=? ",
                new String[] {
                        String.valueOf(mActiveUserID)
                },
                null);
    		if (cursor != null) {
    			if (cursor.moveToFirst()) {
    				value = cursor.getInt(0);
    			} 
    			cursor.close();
    		}            	
    	} catch (Exception e) {
    		// nothing
    	}
        return value;
    } 
    public int getActiveUserHeight() {
    /*	
        int height = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        height = sharedPref.getInt(USER_HEIGHT, 165); 
        return height;
    	*/
    	
        // Get from DB
     	int value=0;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
         			new String[] {
                    User.COLUMN_USER_HEIGHT
                },
                User._ID + "=? ",
                 new String[] {
                         String.valueOf(mActiveUserID)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getInt(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     	}
     	return value;
    } 
    public void setActiveUserGender(boolean isMale) {
      	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_GENDER, isMale);
    
    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(mActiveUserID)
            	});
    	} catch (Exception e) {
    		// nothing
    	}

    }
    public boolean getActiveUserGender() {
    /*
        boolean isMale;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        isMale = sharedPref.getBoolean(USER_GENDER, true); 
        return isMale;
      */
    	
     	int value=0;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
         			new String[] {
                    User.COLUMN_USER_GENDER
                },
                User._ID + "=? ",
                 new String[] {
                         String.valueOf(mActiveUserID)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getInt(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     	}
     	
     	if(value==1) return true;
     	return false;
    } 
    
    public String getActiveUserName() {
    	return getUserName(mActiveUserID);
    }
    public String getUserName(int user){
    	/*
        String name;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);         	
        name = sharedPref.getString(USER_NAME, defaultUserName); 
   
        */
        // Get from DB
    	
     	String value=null;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
     			new String[] {
                    User.COLUMN_USER_NAME
                },
                User._ID + "=? ",
                new String[] {
                         String.valueOf(user)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getString(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     	}
        return value;

    }

    public String getUserUID(int user){    	
     	String value=null;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
     			new String[] {
                    User.COLUMN_USER_UID
                },
                User._ID + "=? ",
                new String[] {
                         String.valueOf(user)
                 },
                 null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getString(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     	}
        return value;
    }

    public int changeActiveUserByName(String name) {
        // Get from DB
   	
    	int value=0;
    	try{
    		Cursor cursor =mResolver.query(User.CONTENT_URI,
       		new String[] {
                   User._ID
               },
               User.COLUMN_USER_NAME + "=? ",
               new String[] {
                   name
               },
               null);
    		if (cursor != null) {
    			if (cursor.moveToFirst()) {
    				value = cursor.getInt(0);
    			} 
    			cursor.close();
    		}            	
    	} catch (Exception e) {
    		// nothing
    	}
       
    	int user=value;
    	
    	if(user!=0) setActiveUser(user);
    	
    	return user;

   }

    public int getUserIdByName(String name) {
         // Get from DB
    	
     	int value=0;
     	try{
     		Cursor cursor =mResolver.query(User.CONTENT_URI,
        		new String[] {
                    User._ID
                },
                User.COLUMN_USER_NAME + "=? ",
                new String[] {
                    name
                },
                null);
     		if (cursor != null) {
     			if (cursor.moveToFirst()) {
     				value = cursor.getInt(0);
     			} 
     			cursor.close();
     		}            	
     	} catch (Exception e) {
     		// nothing
     		Log.i(TAG, "getUserIdByName error="+e);
     	}
     	
    	Log.i(TAG, "getUserIdByName="+name+":"+value);

        return value;

    }
    
    /* depreciated to use preferences
     
    public String getUserName(String defaultName) {
        String name;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        name = sharedPref.getString(USER_NAME, defaultName); 
        return name;
    } 
 
	public String getUserBirth() {
        String strBirth;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        strBirth = sharedPref.getString(USER_DOB, "1980-04-30"); 
        return strBirth;
    } 
    
    public void setUserWeight(int weight) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(USER_WEIGHT, weight ).commit();
    }
    public void setUserHeight(int height) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putInt(USER_HEIGHT, height ).commit();
    }
    public void setUserName(String name) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putString(USER_NAME, name ).commit();
    }   
    public void setUserBirth(String strDate) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putString(USER_DOB, strDate ).commit();
    }
    public void setUserGender(boolean isMale) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        sharedPref.edit().putBoolean(USER_GENDER, isMale).commit();
    }
    
    public int getFinderSetting() {
        int onoff = 0;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        onoff = sharedPref.getInt(FINDER_SET, 0); //-- return false if not exist
        return onoff;
    }  
    
    public void setFinderSetting(int on) {
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                HANSON_PREFERENCE, Context.MODE_PRIVATE);
        Log.d(TAG, "setFinderSetting "+ on);
               
        sharedPref.edit().putInt(FINDER_SET, on).commit();
    }
*/
    //-- 20151205
    public void insertOrUpdateActivity(int aType, int aValue, long aTime) {
    	
    	int user=getActiveUserID();
    	
    	insertOrUpdateActivity(user, aType, aValue, aTime);
    }
    
    /**
     * Insert an activity record into database
     * 
     * @param deviceMac the device MAC address
     * @param aType activity type
     * @param aValue activity value
     * @param aTime activity time stamp
     */
    public void insertOrUpdateActivity(int user, int aType, int aValue, long aTime) {
        
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, null, Activities.COLUMN_ACTIVITY_USER
                + "=? AND "
                + Activities.COLUMN_ACTIVITY_TYPE + "=? AND " + Activities.COLUMN_ACTIVITY_TIME
                + "=?", new String[] {
                String.valueOf(user), String.valueOf(aType), String.valueOf(aTime)
        }, Activities.DEFAULT_SORT_ORDER);
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
        cursor.close();

        ContentValues values = new ContentValues();
        values.put(Activities.COLUMN_ACTIVITY_TYPE, aType);
        values.put(Activities.COLUMN_ACTIVITY_VALUE, aValue);
        values.put(Activities.COLUMN_ACTIVITY_TIME, aTime);
        values.put(Activities.COLUMN_ACTIVITY_USER, user);
        values.put(Activities.COLUMN_ACTIVITY_UPLOADED, 0);	//-- 20151210
        if (isExist) {
            //-- Log.d(TAG, "insertOrUpdateActivity ... UPDATE succeed: userName:"+user+" ti:"+aTime+" va:"+aValue);
            mResolver.update(Activities.CONTENT_URI, values, 
            		  Activities.COLUMN_ACTIVITY_USER + "=? AND "
                    + Activities.COLUMN_ACTIVITY_TYPE + "=? AND " 
                    + Activities.COLUMN_ACTIVITY_TIME
                    + "=?", new String[] {
                    String.valueOf(user), String.valueOf(aType), String.valueOf(aTime)
            });
        } else {
            //-- Log.d(TAG, "insertOrUpdateActivity ... UPDATE failed, will INSERT");
            mResolver.insert(Activities.CONTENT_URI, values);
        }
        
     }

    //-- equal to queryActivityRecords
    /**
     * Get the activities
     * 
     * @param deviceMac indicates the device
     * @param aType the activity type
     * @return
     */
    public Cursor getActivitiesByType(String deviceMac, int aType) {
        if (TextUtils.isEmpty(deviceMac)) {
            Log.e(TAG, "getActivitiesByType: the deviceMac must not be empty");
            return null;
        }

        return mResolver.query(Activities.CONTENT_URI,
                null,
                Activities.COLUMN_DEVICE_MAC + "=? AND "
                        + Activities.COLUMN_ACTIVITY_TYPE + "=?",
                new String[] {
                        deviceMac, String.valueOf(aType)
                },
                Activities.DEFAULT_SORT_ORDER);
    }

    public void setUserDeviceAddress(int user, String deviceAddress) {  
    	
    	int deviceID;
    	
    	deviceID=getDeviceIdByAddress(deviceAddress);
    	
    	if(deviceID==0){
    		deviceID=insertNewDevice(deviceAddress);
    	}
	    	
    	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_DEVICE, deviceID);   
    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(user)
            	});
    		
    	} catch (Exception e) {
    		// nothing
    		Log.i(TAG, "setUserDeviceAddress error");
    		
    	}
    	

    }
    
    public void setActiveFinderSetting(int onoff) {  
    	setUserFinderSetting(mActiveUserID, onoff);
    }
    	 
    public void setUserFinderSetting(int user, int onoff) {  
    	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_FINDER_ENABLED, onoff);   
    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(user)
            	});
    	} catch (Exception e) {
    		// nothing
    	}

    }
    
    public boolean getActiveFinderEnable() {
		
    	if(getActiveFinderSetting()==0){
    		return false;
    	}
    	
		return true;
	}
    public int getActiveFinderSetting(){
    	return getUserFinderSetting(mActiveUserID);
    }
    	 
    public int getUserFinderSetting(int user) {
        int onoff = 0;
        
  		Cursor cursor =mResolver.query(User.CONTENT_URI,
                new String[] {
  					User.COLUMN_USER_FINDER_ENABLED
  				},
                User._ID + "=? ",
                new String[] {
                        String.valueOf(user)
                },
                null);
    		if (cursor != null) {
    			if (cursor.moveToFirst()) {
    				onoff = cursor.getInt(0);
    			} else {
    				Log.w(TAG, "Failed to get finder set");
    			}
    			cursor.close();
    		} else {
    			Log.w(TAG, "failed to get finder set, null");
         	}    
        return onoff;
    }  
    
    public String getActiveUserBirth() {
    	return getUserBirth(mActiveUserID);
    	
    }
    public String getUserBirth(int user) {
        String dob = null;
        
  		Cursor cursor =mResolver.query(User.CONTENT_URI,
                new String[] {
  					User.COLUMN_USER_DOB
  				},
                User._ID + "=? ",
                new String[] {
                        String.valueOf(user)
                },
                null);
    		if (cursor != null) {
    			if (cursor.moveToFirst()) {
    				dob = cursor.getString(0);
    			} else {
    				Log.w(TAG, "Failed to get finder set");
    			}
    			cursor.close();
    		} else {
    			Log.w(TAG, "failed to get finder set, null");
         	}    
        return dob;
    }  
     
   public void setUserDevice(int user, int device) { 
	   
    	ContentValues values = new ContentValues();
    	values.put(User.COLUMN_USER_DEVICE, device);
    
    	try{
    		mResolver.update(User.CONTENT_URI,
    				values,
    				User._ID + "=?",
    				new String[] {
    				String.valueOf(user)
            	});
    	} catch (Exception e) {
    		// nothing
    	}

    }
   
   public void releaseUserDevice(int user) {
	   ContentValues values = new ContentValues();
   		values.put(User.COLUMN_USER_DEVICE, 0);
   
   		try{
   			mResolver.update(User.CONTENT_URI,
   				values,
   				User._ID + "=?",
   				new String[] {
   				String.valueOf(user)
           		});
   		} catch (Exception e) {
   			// nothing
   		}

   } 

    // get device ID from user
    public int getUserDevice(int user) {
    	int device=0;
    	
    	try{
    		Cursor cursor =mResolver.query(User.CONTENT_URI,
    	        new String[] {
                    User.COLUMN_USER_DEVICE
            	},
                User._ID + "=? ",
                new String[] {
                        String.valueOf(user)
                },
                null);
    		if (cursor != null) {
    			if (cursor.moveToFirst()) {
    				device = cursor.getInt(0);
    			} else {
    				Log.w(TAG, "Failed to get device ID");
    			}
    			cursor.close();
    		} else {
    			Log.w(TAG, "failed to get device ID, null");
         	}              	
    	} catch (Exception e) {
    		// nothing
    	}
        return device;
    } 
    
    
	int getDeviceIdByAddress(String deviceAddress) {
		
		int deviceID=0;
		// String deviceModel=null;
		
	     Cursor cursor = mResolver.query(
	                Device.CONTENT_URI,
	                new String[] {
	                    Device._ID
	                    //,
	                    // Device.COLUMN_DEVICE_MODEL
	                },
	                Device.COLUMN_MAC_ADDRESS + "=?",
	                new String[] {
	                    String.valueOf(deviceAddress)
	                },
	                null);
	        if(cursor != null) {
	            if (cursor.moveToFirst()) {
	                deviceID= cursor.getInt(0);
	                // deviceModel= cursor.getString(1);
	            } else {
	                Log.w(TAG, "getDeviceIdByAddress ... moveToFirst failed");
	            }
	            cursor.close();
	        } 
	        /*
	        if(deviceModel!=null){
	        	Log.i(TAG, "device model="+deviceModel);
	        }
	        */
	        return deviceID;
	
		
	}
	
	
	int insertNewDevice(String deviceAddress) {
		int deviceID;
		
		ContentValues values = new ContentValues();
		values.put(Device.COLUMN_MAC_ADDRESS, deviceAddress);
		values.put(Device.COLUMN_FINDER_THRESHOLD, DEFAULT_BINS_FINDER_THRESHOLD);
		values.put(Device.COLUMN_SHAKE_THRESHOLD, DEFAULT_BINS_SHAKE_UI_THRESHOLD);
		values.put(Device.COLUMN_LAST_SYNC_TIME, 0L);
		values.put(Device.COLUMN_CONFIGURATION, DEFAULT_BINS_CONFIGURATION);
		
		mResolver.insert(Device.CONTENT_URI, values);
		
		deviceID=getDeviceIdByAddress(deviceAddress);
		
		return deviceID;
	}
	 public void setActiveShakeUiThreshold(int thresh) {
		 
		 	int device = getActiveDeviceID();
		 	
	      	ContentValues values = new ContentValues();
	    	values.put(Device.COLUMN_SHAKE_THRESHOLD, thresh);
	    
	    	try{
	    		mResolver.update(Device.CONTENT_URI,
	    				values,
	    				Device._ID + "=?",
	    				new String[] {
	    				String.valueOf(device)
	            	});
	    	} catch (Exception e) {
	    		// nothing
	    	}

	}
	 
	 public void setActiveFinderThreshold(int thresh) {
		 
		 	int device = getActiveDeviceID();
		 	
	      	ContentValues values = new ContentValues();
	    	values.put(Device.COLUMN_FINDER_THRESHOLD, thresh);
	    
	    	try{
	    		mResolver.update(Device.CONTENT_URI,
	    				values,
	    				Device._ID + "=?",
	    				new String[] {
	    				String.valueOf(device)
	            	});
	    	} catch (Exception e) {
	    		// nothing
	    	}

	}
	 
	public int getActiveShakeUiThreshold() {
		
		 int device=getActiveDeviceID();
		 
	     if(device==0) return 0;
	        
	     	int value=0;
	        Cursor syncCursor = mResolver.query(
	                Device.CONTENT_URI,
	                new String[] {
	                    Device.COLUMN_SHAKE_THRESHOLD
	                },
	                Device._ID + "=?",
	                new String[] {
	                    String.valueOf(device)
	                },
	                null);
	        if (syncCursor != null) {
	            if (syncCursor.moveToFirst()) {
	                value = syncCursor.getInt(0);
	            } else {
	            }
	            syncCursor.close();
	        } else {
	        }
	        
	        return value;
	
	}
	
	public int getActiveFinderThreshold() {
		
		 int device=getActiveDeviceID();
		 
	     if(device==0) return 0;
	        
	     	int value=0;
	        Cursor syncCursor = mResolver.query(
	                Device.CONTENT_URI,
	                new String[] {
	                    Device.COLUMN_FINDER_THRESHOLD
	                },
	                Device._ID + "=?",
	                new String[] {
	                    String.valueOf(device)
	                },
	                null);
	        if (syncCursor != null) {
	            if (syncCursor.moveToFirst()) {
	                value = syncCursor.getInt(0);
	            } else {
	            }
	            syncCursor.close();
	        } else {
	        }
	        
	        return value;
	
	}
	
	//-- 150502
	
	public void setIbeaconUuid(String ibeaconUuid)
	{
		
		 	int device=getActiveDeviceID();

	        ContentValues values = new ContentValues();
	        values.put(Device.COLUMN_IBEACON_UUID, ibeaconUuid);
	        mResolver.update(Device.CONTENT_URI,
	                values,
	                Device._ID + "=?",
	                new String[] {
	                    String.valueOf(device)
	                });
	}
	    
	public String getIbeaconUuid() {		
		 int device=getActiveDeviceID();
		 
	     if(device==0) return null;
	     
	    	String ibeaconUuid=null;
	    		        
	        Cursor syncCursor = mResolver.query(
	                Device.CONTENT_URI,
	                new String[] {
	                    Device.COLUMN_IBEACON_UUID
	                },
	                Device._ID + "=?",
	                new String[] {
	                    String.valueOf(device)
	                },
	                null);
	        if (syncCursor != null) {
	            if (syncCursor.moveToFirst()) {
	                ibeaconUuid = syncCursor.getString(0);
	            } else {
	            }
	            syncCursor.close();
	        } else {
	        }
	        
	        return ibeaconUuid;
	}
	
	 public void setActiveUserLastSyncTime(long time) {	     
		 	int user=getActiveUserID();		 	
	    	if(user==0) return;
	    		        
	        ContentValues values = new ContentValues();
	        values.put(User.COLUMN_USER_SYNC_TIME, time);
	        mResolver.update(User.CONTENT_URI,
	                values,
	                User._ID + "=?",
	                new String[] {
	                    String.valueOf(user)
	                });
	 }
	 
	public void setActiveDeviceLastSyncTime(long time) {
		int device=getActiveDeviceID();
		
		updateSyncTime(device, time);		
	}
	
    /**
     * Update the last sync time of specific device
     * 
     * @param deviceMac indicates which device to be updated
     * @param time the time the device last sync, in unit of minute
     */
    public int updateSyncTime(int device, long time) {
     
    	Log.i(TAG, "updateSyncTime:"+device+":"+time);

    	if(device==0) return 0;
    	
        //-- 150523 wrong code, if (time <= 0) {
        //    time = System.currentTimeMillis();
        // }

        //-- long lastSyncTime=getActiveLastSyncTime();
        
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_LAST_SYNC_TIME, time);
        int retValue=mResolver.update(Device.CONTENT_URI,
                values,
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                });
        
        /* TODO maybe possible
        if(timeHour(time)-timeHour(lastSyncTime)>0){
        	updateCubeLevelPeriod(lastSyncTime,time);        	
        }
        */
        
        return retValue;
    }
    
    long timeHour(long aTime){
    	return (aTime / 60);
    }
    
    void updateCubeLevelPeriod(long startTime, long endTime){
        int userID=getActiveUserID();
        Cursor cursor = mResolver.query(Activities.CONTENT_URI,
                new String[] {
                        Activities.COLUMN_ACTIVITY_VALUE,
                        Activities.COLUMN_ACTIVITY_TIME
                }, 
            			Activities.COLUMN_ACTIVITY_USER + "==? AND " +
                        Activities.COLUMN_ACTIVITY_TYPE + "==? AND " +
                        Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
                        Activities.COLUMN_ACTIVITY_TIME + "<?", new String[] {
                        
                        String.valueOf(userID), String.valueOf(Bins.BINS_SLEEP_MEASURING_TIME_UNIT_MINUTES), String.valueOf(startTime), String.valueOf(endTime)
                }, Activities.DEFAULT_SORT_ORDER_TIME);
        
        if (cursor!= null) {
            cursor.moveToNext();
            while (!cursor.isAfterLast()) {
                int movements = cursor.getInt(0);
                int minuteTime = cursor.getInt(1);
                insertOrUpdateCubeLevelPeriod(minuteTime,getSleepThreshold(movements));
                cursor.moveToNext();
            }
            cursor.close();
        }
    }
    
    public void insertOrUpdateCubeLevelPeriod(long time, long threshLevel) {
    	//-- 150523 TODO 
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {
        		CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID,
        		CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
        		CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END,        		
        }, 
        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + "<=? AND " +
        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + ">=?"
        , 
        new String[] {                      
                String.valueOf(time), String.valueOf(time)
        }, null);

        if(cursor!=null && cursor.moveToFirst()){ // found one

        	 cursor.close();
        	 
             long cubeThreshLevel = cursor.getLong(0);
             long cubeStartTime=cursor.getLong(1);
             long cubeEndTime=cursor.getLong(1);
             
             if(cubeThreshLevel==threshLevel){
            	 
             }else{
            	 long leftEndTime=time-1;
            	 long rightEndTime=time+1;
            	 //-- TODO deleteCubeLevelPeriod(cursor);
            	 if(cubeStartTime<=leftEndTime){
            		 // createCubeLevelPeriod(cubeStartTime, leftEndTime);
            	 }
            	 if(cubeEndTime>=rightEndTime){
               		 // createCubeLevelPeriod(rightEndTime, cubeEndTime);                    		 
            	 }
            	 if(time==cubeEndTime || time==cubeStartTime){
            		 // use update statement to update time only            		 
            	 } else {            	 
            		 // createCubeLevelPeriod(time, time);   
            	 }
             }
            
        } else { // if cannot find one existing cube
        	// use query to find end == time, or start == time
        	// if found, then update
        	// else 
     		// createCubeLevelPeriod(time, time);          	
        }
        
    }
    public int getSleepThreshold(int movements){
    	int threshLevel=0;
    	
        Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI,
                new String[] {
                        SleepThreshold.COLUMN_SLEEP_THRESH_ID,
                }, 
            	SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS + ">=?" 
            	, 
                new String[] {                      
                        String.valueOf(movements), String.valueOf(movements)
                }, SleepThreshold.DEFAULT_SORT_ORDER_LEVEL);
    	
        if(cursor!=null){
            if (cursor.moveToFirst()) {
                threshLevel = cursor.getInt(0);
            } else {
                Log.w(TAG, "getSleepThreshold failed");
            }
            cursor.close();
        } else {
            Log.w(TAG, "getSleepThreshold failed , Cursor == null");
        }  
        
        return threshLevel;      
    }
    
    public float getMindEffect(long fromTime, long endTime) {
    	    	
        int userID=getActiveUserID();
        
    	float mindEffectFactor=0;
        Cursor cursor;
        cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {
        			CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
        			CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END,
        			CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID,
                },                
                CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "==? AND " +                
                CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + "<=? AND " +
                CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + ">=? "
                , 
                new String[] { 
        				String.valueOf(userID),
                        String.valueOf(fromTime), String.valueOf(endTime)
                }, SleepThreshold.DEFAULT_SORT_ORDER_LEVEL);
     	
        if (cursor==null) return 0;
        
        cursor.moveToNext();
        while (!cursor.isAfterLast()) {
        	int startHour = cursor.getInt(0);
            int periodHours = cursor.getInt(1);
            int isSleepInt = cursor.getInt(2);
            int threshId = cursor.getInt(3);
            int currentHour=0;
            float hourFraction=0;
            
            boolean isSleep=true;
            if(isSleepInt==0) isSleep=false;
            else isSleep=true;
            
            while(currentHour<periodHours){
            	if(periodHours-currentHour>1){
            		hourFraction=1;
            	} else {
            		hourFraction=periodHours-currentHour;            		
            	}
                mindEffectFactor +=getCircadianFactor(isSleep,startHour+currentHour,hourFraction);
            	mindEffectFactor +=getTimeEffectFactor(threshId,startHour+currentHour, hourFraction);
            	
               	currentHour+=1;
         
                cursor.moveToNext();
            }
            cursor.close();
        }
    	
    	return mindEffectFactor;
    }
    
    
    public float getCircadianFactor(boolean isSleepOrWake, long hourTime, float hourFraction) {
    	
    	if(hourTime>=24){ 
    		hourTime-=24;    	
    	}
    	
        Cursor cursor = mResolver.query(Circadian.CONTENT_URI,
                new String[] {
                        Circadian.COLUMN_CIRC_PERC_SLEEP_FACTOR,
                        Circadian.COLUMN_CIRC_PERC_WAKE_FACTOR,
                        Circadian.COLUMN_CIRC_HOUR
                }, 
                Circadian.COLUMN_CIRC_HOUR + "==? "     
            	, 
                new String[] {                      
                        String.valueOf(hourTime)
                }, null);
    	
        long circWakeFactor=0, circSleepFactor=0;
        float circDelta=0;
        if(cursor!=null){
            if (cursor.moveToFirst()) {
                circWakeFactor = cursor.getLong(1);
                circSleepFactor = cursor.getLong(0);
            } else {
                Log.w(TAG, "getCircadian failed");
            }
            cursor.close();
        } else {
            Log.w(TAG, "getCircadian failed , Cursor == null");
        }  

        if(isSleepOrWake==true){
        	if(circSleepFactor!=0){
        		circDelta= hourFraction * circSleepFactor;
        	}
        } else {
        	if(circWakeFactor!=0){ 
        		circDelta= hourFraction * circWakeFactor;
        		
        	}
        }
    	return circDelta;	
    }
    
    
    public float getTimeEffectFactor(int threshId, long hourTime, float hourFraction) {
    	
    	if(hourTime>=24){ 
    		hourTime-=24;    	
    	}
    	
        Cursor cursor = mResolver.query(Circadian.CONTENT_URI,
        		new String[] {
                        SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR,
                        SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR,
                   }, 
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "==? AND " +                      
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "==? "     
            	, 
                new String[] {                      
        			String.valueOf(threshId), String.valueOf(hourTime)
                }, null);
    	
        long teffectWakeFactor=0, teffectSleepFactor=0;
        float teffectDelta=0;
        if(cursor!=null){
            if (cursor.moveToFirst()) {
                teffectWakeFactor = cursor.getLong(1);
                teffectSleepFactor = cursor.getLong(0);
            } else {
                Log.w(TAG, "getSleepTimeEffect failed");
            }
            cursor.close();
        } else {
            Log.w(TAG, "getSleepTimeEffect failed , Cursor == null");
        }  

        if(teffectSleepFactor!=0){
        	teffectDelta= hourFraction * teffectSleepFactor;
        	
        } 
        if(teffectWakeFactor!=0){ 
        	teffectDelta= hourFraction * teffectWakeFactor;       		
        }
    	return teffectDelta;	
    }
    
    public long getActiveDeviceLastSyncTime(){
    	int device=getUserDevice(mActiveUserID);
    	
    	return getDeviceLastSyncTime(device);
    }
    
    public long getActiveUserLastSyncTime(){
    	int user=getActiveUserID();
    	
    	return getUserLastSyncTime(user);
    }
   
    public long getUserLastSyncTime(int user){
        long time = 0;
        
        if(user==0) return 0;
        
        Cursor syncCursor = mResolver.query(
                User.CONTENT_URI,
                new String[] {
                    User.COLUMN_USER_SYNC_TIME
                },
                User._ID + "=?",
                new String[] {
                    String.valueOf(user)
                },
                null);
        if (syncCursor != null) {
            if (syncCursor.moveToFirst()) {
                time = syncCursor.getLong(0);
            } else {
                Log.w(TAG, "getUserLastSyncTime ... moveToFirst failed");
            }
            syncCursor.close();
        } else {
            Log.w(TAG, "getUserLastSyncTime ... syncCursor == null");
        }
        
        return time;
    }
    
    /**
     * Get the last sync time of the specific device
     * 
     * @param deviceMac
     * @return
     */
    public long getDeviceLastSyncTime(int device) {
        long time = 0;
  
        if(device==0) return 0;
        
        Cursor syncCursor = mResolver.query(
                Device.CONTENT_URI,
                new String[] {
                    Device.COLUMN_LAST_SYNC_TIME
                },
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                },
                null);
        if (syncCursor != null) {
            if (syncCursor.moveToFirst()) {
                time = syncCursor.getLong(0);
            } else {
                Log.w(TAG, "getLastSyncTime ... moveToFirst failed");
            }
            syncCursor.close();
        } else {
            Log.w(TAG, "getLastSyncTime ... syncCursor == null");
        }
        
        //--Log.d(TAG, "getLastSyncTime ..."+time);

        return time;
    }

  
  
    /**
     * Update the battery level of the specific device
     * 
     * @param deviceMac indicates which device to be updated
     * @param bLevel the battery level of the specific device
     */
    public int updateBatteryLevel(int device, int bLevel) {
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_BATTERY_LEVEL, bLevel);
        return mResolver.update(Device.CONTENT_URI,
                values,
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                });
    }

    public int getActiveBatteryLevel() {
    	int device=getActiveDeviceID();
    	return getBatteryLevel(device);
    	
    }
    /**
     * Get the battery level of the specific device
     * 
     * @param deviceMac
     * @return
     */
    public int getBatteryLevel(int device) {
        Log.d(TAG, "getBatteryLevel ...");
 
        if(device==0) return 0;

        int batteryLevel = 0;
        Cursor batteryCursor = mResolver.query(
                Device.CONTENT_URI,
                new String[] {
                    Device.COLUMN_BATTERY_LEVEL
                },
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                },
                null);
        if (batteryCursor != null) {
            if (batteryCursor.moveToFirst()) {
                return batteryCursor.getInt(0);
            } else {
                Log.w(TAG, "getBatteryLevel ... moveToFirst failed");
            }
            batteryCursor.close();
        } else {
            Log.w(TAG, "getBatteryLevel ... batteryCursor == null");
        }
        return batteryLevel;
    }

    public void setDeviceAddress(int device, String deviceMac)
    {
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_MAC_ADDRESS, deviceMac);
        mResolver.update(Device.CONTENT_URI,
                values,
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                });
    }
    
    public String getActiveDeviceAddress(){
    	int device = getActiveDeviceID();
    	
    	String deviceAddress=null;
    	
    	if(device==0) return null;
    	
        Cursor cursor = mResolver.query(
                Device.CONTENT_URI,
                new String[] {
                    Device.COLUMN_MAC_ADDRESS
                },
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                },
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                deviceAddress=cursor.getString(0);
            } else {
                Log.w(TAG, "get device address failed");
            }
            cursor.close();
        } else {
            Log.w(TAG, "get device address failed, cursor == null");
        }
        
        return deviceAddress;   	
    	
    }
    /**
     * Update the information of the specific device
     * 
     * @param deviceMac indicates which device to be updated
     * @param values contains info about the specific device
     */
    public void updateDeviceInfo(String deviceMac, ContentValues values) {
        mResolver.update(Device.CONTENT_URI,
                values,
                Device.COLUMN_MAC_ADDRESS + "=?",
                new String[] {
                    deviceMac
                });
    }

    public String getDeviceName(String deviceMac) {
        Cursor cursor = mResolver.query(Device.CONTENT_URI, new String[] {
                Device.COLUMN_DEVICE_NAME
        }, Device.COLUMN_MAC_ADDRESS + "=?", new String[] {
                deviceMac
        }, null);

        String name = "Unknown";
        if (null != cursor && cursor.moveToFirst()) {
            name = cursor.getString(0);
        }
        if (cursor != null) {
            cursor.close();
        }
        return name;
    }

    /**
     * Insert a new device into database
     * 
     * @param dMac device MAC address
     * @param dName device name
     * @param dSn device serial number
     * @param dType device type
     * @param dModel device model
     * @param systemId system id
     * @param hwv hardware version
     * @param swv software version
     * @param fwv firmware version
     * @param manufacturer the device manufacturer
     */
    public void insertNewDevice(String dMac, String dName,
            String dSn, String dType, String dModel, String systemId,
            String hwv, String swv, String fwv, String manufacturer) {
        if (TextUtils.isEmpty(dMac))
            return;
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_MAC_ADDRESS, dMac);
        values.put(Device.COLUMN_DEVICE_NAME, dName);
        values.put(Device.COLUMN_DEVICE_SN, dSn);
        values.put(Device.COLUMN_DEVICE_TYPE, dType);
        values.put(Device.COLUMN_DEVICE_MODEL, dModel);
        values.put(Device.COLUMN_SYSTEM_ID, systemId);
        values.put(Device.COLUMN_HARDWARE_VERSION, hwv);
        values.put(Device.COLUMN_SOFTWARE_VERSION, swv);
        values.put(Device.COLUMN_FIRMWARE_VERSION, fwv);
        values.put(Device.COLUMN_MANUFACTURER, manufacturer);
        mResolver.insert(Device.CONTENT_URI, values);
    }

   
    // DAY_TIME_LENGTH
    
    public Cursor queryUserActivities(int user, int type, long time) {
    	return queryUserActivities(user,type,time,0);
    }
    
    public Cursor queryActivitiesForUpload( int user, int type, int limit) {

    	Cursor cursor;
    	
        if(type>=0){
    		cursor = mResolver.query(Activities.CONTENT_URI,
    				new String[] {
            		Activities.COLUMN_ACTIVITY_TIME,
            		Activities.COLUMN_ACTIVITY_TYPE,
            		Activities.COLUMN_ACTIVITY_VALUE,
            		Activities._ID           		
            		},
                    Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                            Activities.COLUMN_ACTIVITY_TYPE + "=? " 
                    		+ " AND "
                            + Activities.COLUMN_ACTIVITY_UPLOADED + "=?"
                            ,
                    new String[] {
                           	String.valueOf(user),
                            String.valueOf(type)
                            ,
                            "0"
                    },
                    Activities._ID + " ASC" +
                    " LIMIT "+limit);
    		
        }else{
       		cursor = mResolver.query(Activities.CONTENT_URI,
    				new String[] {
            		Activities.COLUMN_ACTIVITY_TIME,
            		Activities.COLUMN_ACTIVITY_TYPE,
            		Activities.COLUMN_ACTIVITY_VALUE
            		},
                    Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                            Activities.COLUMN_ACTIVITY_TYPE + "=? AND " +
                             Activities.COLUMN_ACTIVITY_UPLOADED + "=?",
                    new String[] {
                           	String.valueOf(user),
                            String.valueOf(type),
                            "0"
                    },
                    Activities._ID + " ASC" +
                    " LIMIT "+limit);
         }
        
        return cursor;
    }

    public Cursor queryUserActivities(int user, int type, long time, long period) {
    	
    	Cursor cursor;
    	
    	if(period==0){
    		cursor = mResolver.query(Activities.CONTENT_URI,
                    null,
                    Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                            Activities.COLUMN_ACTIVITY_TYPE + "=? AND " +
                            Activities.COLUMN_ACTIVITY_TIME + "=?",
                    new String[] {
                           	String.valueOf(user),
                            String.valueOf(type),
                            String.valueOf(time)
                    },
                    Activities.COLUMN_ACTIVITY_TIME + " DESC");  		
    		
    	}
    	else{
    		cursor = mResolver.query(Activities.CONTENT_URI,
                null,
                Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                        Activities.COLUMN_ACTIVITY_TYPE + "=? AND " +
                        Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
                        Activities.COLUMN_ACTIVITY_TIME + "<=?",
                new String[] {
                       	String.valueOf(user),
                        String.valueOf(type),
                        String.valueOf(time),
                        String.valueOf(time+period)
                },
                Activities.COLUMN_ACTIVITY_TIME + " DESC");
    	}
        return cursor;
    }
    
    public int getUserActivity(int user, int type, long time) {
        int values = 0;
        Cursor cursor = queryUserActivities(user, type, time);
        if (null != cursor) {
            final int index = cursor.getColumnIndex(Activities.COLUMN_ACTIVITY_VALUE);
            while (cursor.moveToNext()) {
                values += cursor.getInt(index);
            }
            cursor.close();
        }
        return values;
    }
    
    public void setFirmwareRevision(String fwRevision)
    {
    	int device=getActiveDeviceID();
    	
    	if(device==0) return;
    	
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_FIRMWARE_VERSION, fwRevision);
        mResolver.update(Device.CONTENT_URI,
                values,
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                });
    }
  
    //-- 2015.01.13
    public void setActiveDeviceType(int type)
    { 
    	int device=getActiveDeviceID();  
    	
    	if(device==0) return;
    	
        ContentValues values = new ContentValues();
        values.put(Device.COLUMN_DEVICE_TYPE, type);
        mResolver.update(Device.CONTENT_URI,
                values,
                Device._ID + "=?",
                new String[] {
                    String.valueOf(device)
                });
    }

    public int getActiveDeviceType()
    {    	
    	int type=BINS_MODEL_BINSX1;
    	int device=getActiveDeviceID();  
    	
    	if(device==0) return type;
    	
        Cursor cursor = mResolver.query(Device.CONTENT_URI, new String[] {
        		Device.COLUMN_DEVICE_TYPE
        		}, Device._ID + "=?", new String[] {
        		String.valueOf(device)
         		}, null);

        if (null != cursor && cursor.moveToFirst()) {
             type= cursor.getInt(0);
        }
        if (cursor != null) {
           cursor.close();
        }
     	
    	return type;
    }

    public String getBinsFirmwareRevision() {
    	
    	String fwRevision=null;
    	
    	int device=getActiveDeviceID();
    	
    	
    	if(device==0) return null;
    	
    	
        Cursor cursor = mResolver.query(Device.CONTENT_URI, new String[] {
        		Device.COLUMN_FIRMWARE_VERSION
        		}, Device._ID + "=?", new String[] {
        		String.valueOf(device)
         		}, null);

        if (null != cursor && cursor.moveToFirst()) {
            fwRevision = cursor.getString(0);
        }
        if (cursor != null) {
           cursor.close();
        }
 
        return fwRevision;
    }
    
    public int getBinsFirmwareRevisionMajor()
    {
    	Integer version;
    	
    	String binsFirmwareRevision=getBinsFirmwareRevision();
    	if(binsFirmwareRevision==null) return 0;
 
    	//-- Log.i(TAG, "Firmware Major rev:"+binsFirmwareRevision);

    	if(binsFirmwareRevision.length()<21){
    		version=3;	//-- 09.21, 0, assume it is the latest one
    	}
    	else{
    		version=Integer.parseInt(binsFirmwareRevision.substring(16,17));
    	}
    	Log.i(TAG, "Firmware Major:"+version);
    	
    	return version;
    }
    
    public int getBinsFirmwareRevisionMinor()
    {
    	String binsFirmwareRevision=getBinsFirmwareRevision();
    	
    	if(binsFirmwareRevision==null) return 0;
    	
    	if(binsFirmwareRevision.length()<21) return 0;
    	
    	Integer version=Integer.parseInt(binsFirmwareRevision.substring(18,19));
    	    	
    	// Log.i(TAG, "Firmware Minor:"+version);

    	return version;
    }
 
    
    
    public void splitCubeLevelPeriod(int entry, long time, int threshId){
    	Cursor cursor;
    	
        cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, 
                        CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID, 
                        
                }, 
                CubeLevelPeriods._ID + "==?", new String[] {                        
                	String.valueOf(entry)  
                }, 
                null);
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
        
        if(isExist==false){
        	return; // exception, wrong to be here
        }

        cursor.moveToFirst();
        long startTime = cursor.getInt(0);	
        long endTime = cursor.getInt(1);	
        int currentThreshId = cursor.getInt(2);        
      	
        if(cursor!=null) cursor.close();

        cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                	CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID
                	}, 
                	CubeLevelPeriods._ID + "=?",
                    new String[] {
                            String.valueOf(entry)
           		}, null);
                	
         
        ContentValues values = new ContentValues();
        values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, time);
        if(startTime==time){
            values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START, time);       	
        }
        mResolver.update(CubeLevelPeriods.CONTENT_URI,
                values,
                CubeLevelPeriods._ID + "=?",
                new String[] {
                    String.valueOf(entry)
                });
        insertCubeLevelPeriod(time, time, threshId);
        
        if(endTime>time){
        	insertCubeLevelPeriod(time+1, endTime, currentThreshId);        	
        }
    }
    
    public void insertCubeLevelPeriod(long startTime, long endTime, int threshId) {
    	
    	
    	//-- Log.i(TAG, "sleep insertCubeLevelPeriod:"+startTime+":"+endTime+":"+threshId);
        int user=getActiveUserID();
    	ContentValues values = new ContentValues();
        values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_USER, user);        
        values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID, threshId);        
        values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START, startTime);        
        values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, endTime);
        
        mResolver.insert(CubeLevelPeriods.CONTENT_URI, values);
        
    }
    public int getEntryFromCubeLevelPeriods(long time){
    	int user=getActiveUserID();
    	int entry=0;
    	
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods._ID
                }, 
                		CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +                
            			CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + ">=? AND " +
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + "<=?", new String[] {
                        
                        String.valueOf(user), String.valueOf(time), String.valueOf(time)
                }, null);

        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                entry = cursor.getInt(0);	
        	}       	
        	cursor.close();
        }        		    		
    	return entry;
            
    }
    public void expandCubeLevelPeriod(long time, long newTime)
    {
       	int user=getActiveUserID();
    	int entry=0;
    	long startTime=0, endTime=0;
    	
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods._ID,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END
                }, 
                		CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +                
            			CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + "=? OR " +
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + "=?" 
                , new String[] {
                        
                        String.valueOf(user), String.valueOf(time), String.valueOf(time)
                }, null);

        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                entry = cursor.getInt(0);
                startTime = cursor.getInt(1);	
                endTime = cursor.getInt(2);	
                
              	//-- Log.i(TAG,"expandCubeLevelPeriod:"+startTime+":"+endTime);

        	}       	
        	cursor.close();
        }else {
        	Log.i(TAG, "expand error");
        	return;
        }
        
        ContentValues values = new ContentValues();
        
    	if(newTime<startTime){
    		startTime=newTime;
    	    values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START, newTime);  
    	    
        	//-- Log.i(TAG,"expandCubeLevelPeriod-"+newTime+":"+endTime);

    		
    	}else if(endTime<newTime){
    		endTime=newTime;
    	    values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, newTime); 
    	    
        	//-- Log.i(TAG,"expandCubeLevelPeriod+"+startTime+":"+newTime);

    	}
    	
      	//-- Log.i(TAG,"expandCubeLevelPeriod:"+startTime+":"+endTime);

        mResolver.update(CubeLevelPeriods.CONTENT_URI,
                values,
                CubeLevelPeriods._ID + "=?",
                new String[] {
                    String.valueOf(entry)
                });    	   	
    }
    
    public void expandCubeLevelPeriod(int entry, long startTime, long endTime)
    {
    	
    	//-- Log.i(TAG, "sleep expandCube:"+entry+":"+startTime+":"+endTime);
        ContentValues values = new ContentValues();
        if(startTime!=0) values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START, startTime);
        if(endTime!=0) values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, endTime);
        
        mResolver.update(CubeLevelPeriods.CONTENT_URI,
                values,
                CubeLevelPeriods._ID + "=?",
                new String[] {
                    String.valueOf(entry)
                });    	   	
    }
    
    public void insertCubeLevelPeriod(long time, int threshId)
    {
       	int user=getActiveUserID();
    	//-- Log.i(TAG,"insertCubeLevelPeriod:"+time+":"+threshId);    	
        
        ContentValues values = new ContentValues();
     	values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_USER, user);   
     	values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID, threshId); 
     	values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START, time);        
    	values.put(CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END, time); 
    	
        mResolver.insert(CubeLevelPeriods.CONTENT_URI,
                values
                );    	   	
    }
    
    public class ThreshPair {
    	public int threshId;
    	public int entry;
    	public long startTime;
    	public long endTime;
    	public ThreshPair(int t, int e, long st, long en){
    		threshId=t;
    		entry=e;
    		startTime=st;
    		endTime=en;
    	}
    }
    
    
    public ThreshPair getThreshFromCubeLevelPeriodsAfter(long time){
    	int user=getActiveUserID();
    	int threshId=0;
    	int entry=0;
    	long stTime=0;
    	long enTime=0;
    	
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID,
                        CubeLevelPeriods._ID,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END
                }, 
                		CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +                
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + ">=?"
                        , new String[] {
                        
                        String.valueOf(user), String.valueOf(time)
                }, CubeLevelPeriods.SORT_ORDER_TIME_AFTER);

        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                threshId = cursor.getInt(0);
                entry = cursor.getInt(1);
                stTime = cursor.getInt(2);
                enTime = cursor.getInt(3);
        	}       	
        	cursor.close();
        }        		    	
        
    	return new ThreshPair(threshId, entry, stTime, enTime);
    }
    
    
    
    public ThreshPair getThreshFromCubeLevelPeriodsBefore(long time){
    	int user=getActiveUserID();
    	int threshId=0;
    	int entry=0;
       	long stTime=0;
    	long enTime=0;
    	
    	
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID,
                        CubeLevelPeriods._ID,
                        
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END

                }, 
                		CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +                
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + "<=?"
                        , new String[] {
                        
                        String.valueOf(user), String.valueOf(time)
                }, CubeLevelPeriods.SORT_ORDER_TIME_BEFORE);

        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                threshId = cursor.getInt(0);
                entry = cursor.getInt(1);
                stTime = cursor.getInt(2);
                enTime = cursor.getInt(3);
        	}       	
        	cursor.close();
        }        		    	
        
    	return new ThreshPair(threshId, entry, stTime, enTime);
    }
    
    
    public int getThreshFromCubeLevelPeriods(long time){
    	int user=getActiveUserID();
    	int threshId=0;
    	
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID
                }, 
                		CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +                
            			CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + "<=? AND " +
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + ">=?"
                        , new String[] {
                        
                        String.valueOf(user), String.valueOf(time), String.valueOf(time)
                }, CubeLevelPeriods.DEFAULT_SORT_ORDER_THRESHOLD);

        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                threshId = cursor.getInt(0);	
        	}       	
        	cursor.close();
        }        		    	
        //-- Log.i(TAG, "getThreshFromCubeLevel:"+threshId+":"+time+":"+cursor.getCount());
        
    	return threshId;
            
    }
    	
    public boolean readyThresholdData() {
    	if(getThresholdId(50)>0){ return true; }
    	
    	return false;
    }
    
    
    public Cursor getThresholds() {
        Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, 
        		new String[] { SleepThreshold.COLUMN_SLEEP_THRESH_ID, 
        		SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS },
        		null, null, SleepThreshold.DEFAULT_SORT_ORDER_LEVEL);
        		    		                            
    	return cursor;
    }
    
    
    /*
    private int threshTableSize=0;
    private int[] threshIdTable= new int[30];
    private int[] threshMovementTable= new int[30];
    private boolean isThreshTableLoaded=false;
    */
    public int getThresholdId(int movements)
    {
    	int threshId=0;    	
  /*  	
    	if(isThreshTableLoaded==false){
            Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, 
            		new String[] { 
            			SleepThreshold.COLUMN_SLEEP_THRESH_ID, 
            			SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS,  
            			},
                    null, null, SleepThreshold.DEFAULT_SORT_ORDER_MOVEMENTS);
            
            if(cursor!=null && cursor.moveToFirst()){
            	for(int i=0; i<cursor.getCount(); i++){
            		threshIdTable[i]=cursor.getInt(0);
            		threshMovementTable[i]=cursor.getInt(1);
            		cursor.moveToNext();
            	}
            	threshTableSize=cursor.getCount();
            }
        	if(cursor!=null) cursor.close();

    		isThreshTableLoaded=true;
    		
    	}
    	if(isThreshTableLoaded==true){
    		int i;
    		for(i=0; i<threshTableSize; i++){
    			if(threshMovementTable[i]>=movements) break;
    		}
    		if(i==threshTableSize && movements>0) i=threshTableSize-1;
    		
    		return threshIdTable[i];
    	}
 */   	
    	
    	//-- Log.i(TAG, "sleep getThreshId :"+movements);
    	
        Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, 
        		new String[] { SleepThreshold.COLUMN_SLEEP_THRESH_ID },
                SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS + ">=?", new String[] {
                String.valueOf(movements)
        		}, SleepThreshold.DEFAULT_SORT_ORDER_LEVEL);
        		    		                    
        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                threshId = cursor.getInt(0);
        	}       
        	else{
        		//  Log.i(TAG, "threshId no entry:"+movements);
        		threshId=10; // TODO forced assignment
        	}

        	cursor.close();
        }else{
        	Log.i(TAG, "threshId cannot found:"+movements);
        
        }
        
    	return threshId;
    }
    public int getSleepStateFromThreshold(int threshId) {
    	int sleepState=0;
        Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, new String[] {
        		SleepThreshold.COLUMN_SLEEP_THRESH_STATE } , 

                SleepThreshold.COLUMN_SLEEP_THRESH_ID + "=?", new String[] {
                  String.valueOf(threshId)
        		}, null);
        		    		                    
        if(cursor!=null){
        	if(cursor.moveToFirst()) {
                sleepState = cursor.getInt(0);	
        	}       	
        	cursor.close();
        }        		
        
        return sleepState;
    }
    public void updateThresholdTable(List<String[]> strList)
    {
    	//-- the first row is for names
    	//-- the last row is out of boundary, don't know why
    	int i;
    	for(i=1; i<strList.size()-1; i++){ 
    		if(strList.get(i).length<4) continue;    		
    		String tokenThreshId=strList.get(i)[0];
     		String tokenName=strList.get(i)[1];
     		String tokenMovements=strList.get(i)[2];     	    		
     		String tokenSleepState=strList.get(i)[3];     	    		
     		
            Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, null, 
                    SleepThreshold.COLUMN_SLEEP_THRESH_ID + "=?", new String[] {
                    tokenThreshId 
            		}, null);
            		    		
            boolean isExist = (cursor != null && cursor.getCount() > 0);
                        
            if(cursor!=null) cursor.close();

            ContentValues values = new ContentValues();
            values.put(SleepThreshold.COLUMN_SLEEP_THRESH_ID, tokenThreshId);
            values.put(SleepThreshold.COLUMN_SLEEP_THRESH_NAME, tokenName);
            values.put(SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS, tokenMovements);
            values.put(SleepThreshold.COLUMN_SLEEP_THRESH_STATE, tokenSleepState);
            if (isExist) {
                mResolver.update(SleepThreshold.CONTENT_URI, values, 
                			SleepThreshold.COLUMN_SLEEP_THRESH_ID + "=?"
                		    , new String[] {
                        		tokenThreshId
                			});
            } else {
                mResolver.insert(SleepThreshold.CONTENT_URI, values);
            }
            //-- Log.i(TAG, "threshold table:"+tokenThreshId+":"+tokenMovements+":"+tokenSleepState);
            
    	}

    }
    public Cursor getTimeEffect(int threshId, int hour) {
        Cursor cursor = mResolver.query(SleepTimeEffect.CONTENT_URI, new String[] {
    			SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR,
    			SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR
    			}, 
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "=? AND " +
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "=?", 
                new String[] {
                   String.valueOf(threshId), String.valueOf(hour) 
        		}, null);
    	
        return cursor;    	
    }
    public void updateTimeEffectTable(List<String[]> strList)
    {
    	//-- the first row is for names
    	//-- the last row is out of boundary, don't know why
    	int i;
    	for(i=1; i<strList.size(); i++){     		
    		if(strList.get(i).length<4) continue;    		
    		String tokenThreshId=strList.get(i)[0];
     		String tokenHour=strList.get(i)[1];
     		String tokenPercSleep=strList.get(i)[2];     	    		
     		String tokenPercWake=strList.get(i)[3];     	    		

            Cursor cursor = mResolver.query(SleepTimeEffect.CONTENT_URI, null, 
                    SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "=? AND " +
                    SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "=?  ", new String[] {
                    tokenThreshId, tokenHour 
            		}, null);
            		    		
            boolean isExist = (cursor != null && cursor.getCount() > 0);
                        
            if(cursor!=null) cursor.close();

            ContentValues values = new ContentValues();
            values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID, tokenThreshId);
            values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR, tokenHour);
            values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR, tokenPercSleep);
            values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR, tokenPercWake);
            if (isExist) {
                mResolver.update(SleepTimeEffect.CONTENT_URI, values, 
                			SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "=? AND "+
                		    SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "=?"
                		    , new String[] {
                        		tokenThreshId, tokenHour
                			});
            } else {
                mResolver.insert(SleepTimeEffect.CONTENT_URI, values);
            }
            //-- Log.i(TAG, "time effect table tresh:hr:sl:wa="+tokenThreshId+":"+tokenHour+":"+tokenPercSleep+":"+tokenPercWake);
    	}

    }
    
    public Cursor getCircadian(int hour) {
        Cursor cursor = mResolver.query(Circadian.CONTENT_URI, new String[]{
        		Circadian.COLUMN_CIRC_PERC_SLEEP_FACTOR,
        		Circadian.COLUMN_CIRC_PERC_WAKE_FACTOR
        		},
                Circadian.COLUMN_CIRC_HOUR + "=?", new String[] {
                	String.valueOf(hour), 
        		}, null);
        
        return cursor;
    }
    public void updateCircadianTable(List<String[]> strList)
    {
     	//-- the first row is for names
    	//-- the last row is out of boundary, don't know why
    	int CIRCADIAN_TIME_SHIFTER=6; //-- 20151203 shift from 24 hour, morning
    	int i;
    	for(i=1; i<strList.size(); i++){ 
    		if(strList.get(i).length<3) continue;    		
    		String tokenHour=strList.get(i)[0];
     		String tokenPercSleep=strList.get(i)[1];
     		String tokenPercWake=strList.get(i)[2];     	    		

     		int intHour=Integer.parseInt(tokenHour);
     		
     		tokenHour=String.valueOf((intHour+CIRCADIAN_TIME_SHIFTER)%24);
     		
            Cursor cursor = mResolver.query(Circadian.CONTENT_URI, null, 
                    Circadian.COLUMN_CIRC_HOUR + "=?", new String[] {
                    tokenHour, 
            		}, null);
            		     		
            boolean isExist = (cursor != null && cursor.getCount() > 0);
            
            if(cursor!=null) cursor.close();

            ContentValues values = new ContentValues();
            values.put(Circadian.COLUMN_CIRC_HOUR, tokenHour);
            values.put(Circadian.COLUMN_CIRC_PERC_SLEEP_FACTOR, tokenPercSleep);
            values.put(Circadian.COLUMN_CIRC_PERC_WAKE_FACTOR, tokenPercWake);
            if (isExist) {
                mResolver.update(Circadian.CONTENT_URI, values, Circadian.COLUMN_CIRC_HOUR
                        + "=?", new String[] {
                        tokenHour
                		});
            } else {
                mResolver.insert(Circadian.CONTENT_URI, values);
            }
    		//-- Log.i(TAG, "Circadian token:"+i+":"+tokenHour+":"+tokenPercSleep+":"+tokenPercWake);
    		
    	}
    }
    
    public class RecActigraphy{
        private long time;
        private int movements;
        public RecActigraphy(long t, int m) {
        	time=t;
        	movements=m;
        }	
    }
    
    public Cursor getLevelPeriods(long startTime, long endTime) {
        int userID=getActiveUserID();
    	
        //-- Log.i(TAG, "getLevelPeriods:"+startTime+":"+endTime);
        
        Cursor cursor = mResolver.query(CubeLevelPeriods.CONTENT_URI,
                new String[] {                       
                        CubeLevelPeriods.COLUMN_CUBELEVEL_THRESH_ID,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START,
                        CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END
                }, 
                CubeLevelPeriods.COLUMN_CUBELEVEL_USER + "=? AND " +               
            	CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_START + ">=? AND " +
                CubeLevelPeriods.COLUMN_CUBELEVEL_DTM_END + "<=?"
                , new String[] {                       
        				String.valueOf(userID), 
                        String.valueOf(startTime), 
                        String.valueOf(endTime)
                }, null);
 
        return cursor;
    }
    
    
 
    
    //-- 150615
    public void updateLevelPeriods(long startTime, long endTime)
    {
    	final int PERIOD_SEGMENT=360; 
    	// 150620 keep reasonably high to reduce time, but a number to control memory usage 
    	
    	for(long segStartTime=startTime; segStartTime<endTime; segStartTime+=PERIOD_SEGMENT){
    		long segEndTime=segStartTime+PERIOD_SEGMENT-1;
    		if(segEndTime>endTime) segEndTime=endTime;
    		
    		updateLevelPeriodSegment(segStartTime, segEndTime);
    		
    	}
    }
    
    private void updateLevelPeriodSegment(long startTime, long endTime)
    {
    	
        int userID=getActiveUserID();
                
        Cursor cursorActivities = mResolver.query(Activities.CONTENT_URI,
                new String[] {
                        Activities.COLUMN_ACTIVITY_VALUE,
                        Activities.COLUMN_ACTIVITY_TIME,
                        Activities.COLUMN_ACTIVITY_TYPE			// 07.13
                }, 
                        "(" + 	Activities.COLUMN_ACTIVITY_USER + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TYPE + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
                        		Activities.COLUMN_ACTIVITY_TIME + "<=?" + ")"
                        		, new String[] {
                        
                        String.valueOf(userID), String.valueOf(Bins.BINS_ACTIVITY_TYPE_SLEEP), String.valueOf(startTime), String.valueOf(endTime),
                 }, Activities.DEFAULT_SORT_ORDER_TIME);
        
        if (cursorActivities == null) return; 	// nothing to do

        
    	//-- Log.i(TAG, "level segment:"+cursorActivities.getCount()+":"+startTime);

        ArrayList<RecActigraphy> recActiList=new ArrayList<RecActigraphy>();
        
        cursorActivities.moveToNext();
        while (!cursorActivities.isAfterLast()) {
        	int rawCounter=cursorActivities.getInt(0);
        	long activityTime=cursorActivities.getInt(1);
        	
        	recActiList.add(new RecActigraphy(activityTime, rawCounter));
        	cursorActivities.moveToNext();
        }
        cursorActivities.close();
                               
        int i;
        for(i=0; i<recActiList.size(); i++) {
            	int rawCounter=recActiList.get(i).movements;
            	long activityTime=recActiList.get(i).time;
            	
            	int threshId=getThresholdId(rawCounter);
            	
            	//--Log.i(TAG, " sleep rawcounter:"+activityTime+":"+rawCounter+":"+threshId);

            	if(threshId==0) continue;
            	
            	updateCubeLevelPeriods(activityTime,threshId);
            	
        }
   
        recActiList.clear();
        
        
    }
    
    private void updateCubeLevelPeriods(long activityTime, int threshId)
    {
    	//-- Log.i(TAG, "sleep updateCubeLevel:"+activityTime+":"+threshId);
    	
    	if(threshId==0) return;	// not a valid ID, just return;
    	
        int existingThreshId=getThreshFromCubeLevelPeriods(activityTime);
        if(existingThreshId!=0){
        	if(existingThreshId!=threshId){
        		int existingEntry=getEntryFromCubeLevelPeriods(activityTime);
        		splitCubeLevelPeriod(existingEntry, activityTime, threshId);
        	}
        } else {
        	Boolean isIncluded=false;
        	ThreshPair threshPair=null;
        	ThreshPair threshPairAfter=null;
        	
        	threshPair=getThreshFromCubeLevelPeriodsBefore(activityTime-1);
        	
        	if(threshPair!=null && threshPair.threshId==threshId){
        		expandCubeLevelPeriod(threshPair.entry, 0, activityTime);
        		isIncluded=true;
        	} else{
               	threshPairAfter=getThreshFromCubeLevelPeriodsAfter(activityTime+1);
                if(threshPairAfter!=null && threshPairAfter.threshId==threshId){
                	expandCubeLevelPeriod(threshPairAfter.entry, activityTime, 0);
                	isIncluded=true;
                 }
        	}
        	if(isIncluded==false){
       		 
        		if(threshPair!=null && threshPair.threshId!=0){
        			insertCubeLevelPeriod(threshPair.endTime+1, activityTime, threshId); 
        		} else {
         			insertCubeLevelPeriod(activityTime, activityTime, threshId);        			
        		}
        			
        	}
        }
    }
    
    public Boolean existingLevelPeriodsTable() {
    	if(mResolver.getType(CubeLevelPeriods.CONTENT_URI)!=null){
    		return true;
    	}
    	return false;
    }
    
    public void clearCubeLevelPeriods() {
    	
  /*  	mResolver.delete(CubeLevelPeriods.CONTENT_URI, 	
    				CubeLevelPeriods.COLUMN_CUBELEVEL_USER+"=? OR "+
    				CubeLevelPeriods.COLUMN_CUBELEVEL_USER+"<>? ",
    				new String[] { 
    						String.valueOf(0), String.valueOf(0)}
    			);
 */
    	
    	mResolver.delete(CubeLevelPeriods.CONTENT_URI, 	
				null,
				null
			);   		
    }
    
    private final int MAX_UPLOAD_ACTIVITIES_SESSION=100;

    private Boolean uploadActivitiesOfType(int uploadType) {
        
        String userUID=getUserUID(mActiveUserID);
        
        if(userUID==null || (userUID!=null && userUID.equals(""))){
            Log.i(TAG, "uploadActivities user UID not assigned yet.");
            
            return false;
        }
        
        Cursor cursor=queryActivitiesForUpload(mActiveUserID,uploadType,MAX_UPLOAD_ACTIVITIES_SESSION);
        if(cursor!=null){
            mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

            mBinsCloudServer.createUploadSession(userUID, uploadType); 
            int actValue=0;
            int actType=0;
            long actTime=0;
            int actSerialNo=0;

            int numUploading=0;
            
            //-- Log.i(TAG, "query done");
            
            cursor.moveToNext();
            while (!cursor.isAfterLast()) {
            	actTime = 	cursor.getInt(0);
                actType = 	cursor.getInt(1);
                actValue = 	cursor.getInt(2);
                actSerialNo = 	cursor.getInt(3);
                
                mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

                mBinsCloudServer.addUploadActivity(actValue,actType,actTime);

                numUploading++;

                cursor.moveToNext();
            }
            cursor.close();
            
            if(numUploading==0) return false;

            //-- 20150823
            //-- it has potential risk to lose track of status
            //-- if the data upload is not successful
            setActivitiesUploaded(mActiveUserID,uploadType,actSerialNo);
            
            return true;
        }
        
        return false;
    }

    public Boolean setActivitiesUploaded(int user, int type, int recordSerialNo) {
        
        Boolean isSuccess=true;
        
        
       	ContentValues values = new ContentValues();
    	values.put(Activities.COLUMN_ACTIVITY_UPLOADED, 1);
     
    	try{
    		mResolver.update(Activities.CONTENT_URI,
    				values,
    				"( "
    				+Activities.COLUMN_ACTIVITY_USER + "=? AND " 
    				+Activities.COLUMN_ACTIVITY_TYPE + "=? AND " 
   				 	+Activities.COLUMN_ACTIVITY_UPLOADED + "=? AND " 
   				 	+Activities._ID + "<=? "					   				 	
   				 	+")",
    				new String[] {
    					String.valueOf(user),
    					String.valueOf(type),
    					String.valueOf(0),
    					String.valueOf(recordSerialNo) 
    					
            		});
    	
    	} catch (Exception e) {
        	// nothing
    		isSuccess=false;
    		Log.i(TAG, "setActivitiesUploaded error:"+e);
    	}

        /*
        sprintf(strSqlStatement,
                "UPDATE %s  SET             \
                %s=%d                       \
                WHERE                       \
                %s=%d   AND                 \
                %s=%d   AND                 \
                %s=%d   AND                 \
                %s<=%d                      \
                ; ",
                binsData.activity->tableName,
                binsData.activity->uploaded, 1,
                binsData.activity->user, user,
                binsData.activity->type, type,
                binsData.activity->uploaded, 0,
                binsData.activity->recordID, recordSerialNo
                );
        */
          
        
        return isSuccess;
    }
   

   
    public void uploadTask() {        
   	
        if(mDataIsUploading==true){
            return;
        }
       
        Log.i(TAG,"uploadTask");
        
        if(getActiveUserType()!=1){	//-- if not regular network user        	
        	return;
        }
        	
        mDataIsUploading=true;
        
        //-- this function will create record session data of the specific activity type
        uploadActivitiesOfType(Bins.BINS_ACTIVITY_TYPE_SLEEP);
        uploadActivitiesOfType(Bins.BINS_ACTIVITY_TYPE_STEPS);
        uploadActivitiesOfType(Bins.BINS_ACTIVITY_TYPE_PULSE);
        uploadActivitiesOfType(Bins.BINS_ACTIVITY_TYPE_LIGHT);
        
        //-- merge the session data to the command header and send upload request
        
        mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

        Boolean isSuccess=mBinsCloudServer.startUploadSession();

        if(isSuccess==false) mDataIsUploading=false;
            
        //-- later delegate addedNewData is called back from the server
        //-- and uploadTask can be called again if the delegate returns success, otherwise, this uploadTask will be called after every time finish of data synchronizing
    }
    
    
    public int getActiveSystemId() {
    	String ibeaconUuid=BinsDBUtils.activeDB().getIbeaconUuid();

        Log.i(TAG, "device check="+ibeaconUuid);
        
    	if(ibeaconUuid==null || ibeaconUuid.length()<=0){
    		return -1;
    	}
    	
    	UUID deviceu= UUID.fromString(ibeaconUuid);
    	    	
        ByteBuffer uuidBytes = ByteBuffer.wrap(new byte[16]);
        uuidBytes.putLong(deviceu.getMostSignificantBits());
        uuidBytes.putLong(deviceu.getLeastSignificantBits());
    	
        // Create a NSUUID with the same UUID as the broadcasting beacon
       
        int systemId=0;
        
        for(int i=0; i<15; i++){
            //-- NSLog(@"uuid %d=%d", i, uuidBytes[i]);
        	// Log.i(TAG, "beacon id uuid "+i+"="+Integer.toHexString((int)(uuidBytes.get(i) & 0xFF)));
            if(i==0) systemId+= (uuidBytes.get(i) & 0xFF);
            if(i==1) systemId+= ((uuidBytes.get(i) & 0xFF)<<8);
            if(i==2) systemId+=  ((uuidBytes.get(i) & 0xFF)<<16);
        }
        systemId %=1000000;
        
        Log.i(TAG, "device check id="+systemId);
        
        return systemId;           
    }    
    
    //-- Find the next available time slot for the sleep record stream
    public long nextAvailableSleepTime(long sleepRecordTime) {        
        if(sleepRecordTime==0) return 0;  // special case to just return 0
                
        int user=getActiveUserID();
        
        Cursor cursor = mResolver.query(Activities.CONTENT_URI,
                		new String[] {
                        		Activities.COLUMN_ACTIVITY_TIME,
                		}, 
                        "(" + 	Activities.COLUMN_ACTIVITY_USER + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TYPE + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TIME + ">?"		 +
                        ")",
                        new String[] {
                        
                        			String.valueOf(user), 
                        			String.valueOf(Bins.BINS_ACTIVITY_TYPE_SLEEP), 
                        			String.valueOf(sleepRecordTime)
                 		}, 
                 		Activities.COLUMN_ACTIVITY_TIME +" ASC" ); //-- ORDER BY %s ASC
        
        
        if (cursor == null) return 0; 	// nothing to do

        long nextOccupiedTime=0;
        
        sleepRecordTime++;  // unit of minute
        
        cursor.moveToNext();
        while (!cursor.isAfterLast()) {
        	nextOccupiedTime = 	cursor.getLong(0);         

            if(sleepRecordTime<nextOccupiedTime){
                break;
            }else if(sleepRecordTime==nextOccupiedTime){
                sleepRecordTime=(nextOccupiedTime+1);
            }
            cursor.moveToNext();
        }
        cursor.close();

        return sleepRecordTime;
    }

    public long previousAvailableSleepTime(long sleepRecordTime) {
        if(sleepRecordTime==0) return 0;  // special case to just return 0
        
        int user=getActiveUserID();
        
        Cursor cursor = mResolver.query(Activities.CONTENT_URI,
                		new String[] {
                        		Activities.COLUMN_ACTIVITY_TIME,
                		}, 
                        "(" + 	Activities.COLUMN_ACTIVITY_USER + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TYPE + "==? AND " +
                        		Activities.COLUMN_ACTIVITY_TIME + "<?"		 +
                        ")",
                        new String[] {
                        
                        			String.valueOf(user), 
                        			String.valueOf(Bins.BINS_ACTIVITY_TYPE_SLEEP), 
                        			String.valueOf(sleepRecordTime)
                 		}, 
                 		Activities.COLUMN_ACTIVITY_TIME +" DESC" ); //-- ORDER BY %s ASC       
        
        if (cursor == null) return 0; 	// nothing to do

        long nextOccupiedTime=0;
        
        sleepRecordTime--;  // unit of minute
        
        cursor.moveToNext();
        while (!cursor.isAfterLast()) {
        	nextOccupiedTime = 	cursor.getLong(0);         

            if(sleepRecordTime>nextOccupiedTime){
                break;
            }else if(sleepRecordTime==nextOccupiedTime){
                sleepRecordTime=(nextOccupiedTime-1);
            }
            cursor.moveToNext();
        }
        cursor.close();

        return sleepRecordTime;
    }

  //--- 20151124
    public static int QUERY_COMPLETED=0;
    public static int QUERY_STOPPED=1;
    public static int QUERY_WORKING=2;
    
    
    public void insertOrUpdateActivitySetUploaded(long aTime, int aType, int aValue) {
        
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, 
        		null, 
        		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                Activities.COLUMN_ACTIVITY_TYPE + "=? AND " + 
        		Activities.COLUMN_ACTIVITY_TIME + "=?"
        		, 
        		new String[] {
                	String.valueOf(mActiveUserID), 
                	String.valueOf(aType), 
                	String.valueOf(aTime)
                }, 
                Activities.DEFAULT_SORT_ORDER);
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
        cursor.close();

        ContentValues values = new ContentValues();
        values.put(Activities.COLUMN_ACTIVITY_TYPE, aType);
        values.put(Activities.COLUMN_ACTIVITY_VALUE, aValue);
        values.put(Activities.COLUMN_ACTIVITY_TIME, aTime);
        values.put(Activities.COLUMN_ACTIVITY_USER, mActiveUserID);
        values.put(Activities.COLUMN_ACTIVITY_UPLOADED, 1);
        
        if (isExist) {
            //-- Log.d(TAG, "insertOrUpdateActivity ... UPDATE succeed: userName:"+user+" ti:"+aTime+" va:"+aValue);
            mResolver.update(Activities.CONTENT_URI, 
            		values
            		, 
            		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                    Activities.COLUMN_ACTIVITY_TYPE + "=? AND " + 
                    Activities.COLUMN_ACTIVITY_TIME + "=?"
                    , 
                    new String[] {
                    	String.valueOf(mActiveUserID), 
                    	String.valueOf(aType), 
                    	String.valueOf(aTime)
            		});
        } else {
            //-- Log.d(TAG, "insertOrUpdateActivity ... UPDATE failed, will INSERT");
            mResolver.insert(Activities.CONTENT_URI, values);
        }
        
     }

    public boolean updateThresholdItem(int threshId, String name, int movements, int sleepState) {
        
        Cursor cursor = mResolver.query(SleepThreshold.CONTENT_URI, 
        		null, 
                SleepThreshold.COLUMN_SLEEP_THRESH_ID + "=?", 
                new String[] {
                	String.valueOf(threshId) 
        		}, null);
        		    		
        boolean isExist = (cursor != null && cursor.getCount() > 0);
                    
        if(cursor!=null) cursor.close();

        ContentValues values = new ContentValues();
        values.put(SleepThreshold.COLUMN_SLEEP_THRESH_ID, threshId);
        values.put(SleepThreshold.COLUMN_SLEEP_THRESH_NAME, name);
        values.put(SleepThreshold.COLUMN_SLEEP_THRESH_MOVEMENTS, movements);
        values.put(SleepThreshold.COLUMN_SLEEP_THRESH_STATE, sleepState);
        if (isExist) {
            mResolver.update(SleepThreshold.CONTENT_URI, 
            			values, 
            			SleepThreshold.COLUMN_SLEEP_THRESH_ID + "=?", 
            			new String[] {
                    		String.valueOf(threshId)
            			});
        } else {
            mResolver.insert(SleepThreshold.CONTENT_URI, values);
        }
        
        return isExist;        
    }

    public void clearSleepTimeEffectTable(){
    	mResolver.delete(SleepTimeEffect.CONTENT_URI, 	
    			null,
    			null
    			);   		
    }
    
    public boolean updateSleepTimeEffectItem(int threshId, int hour, float recoveryFactor){
    	
        Cursor cursor = mResolver.query(SleepTimeEffect.CONTENT_URI, 
        		null
        		, 
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "=? AND " +
                SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "=?  "
                , 
                new String[] {
                	String.valueOf(threshId), String.valueOf(hour) 
        		}, null);
        		    		
        boolean isExist = (cursor != null && cursor.getCount() > 0);
                    
        if(cursor!=null) cursor.close();

        ContentValues values = new ContentValues();
        values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID, threshId);
        values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR, hour);
        values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_SLEEP_FACTOR, recoveryFactor);
        values.put(SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_PERC_WAKE_FACTOR, 0);
        if (isExist) {
            mResolver.update(SleepTimeEffect.CONTENT_URI, 
            			values
            			, 
            			SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_THRESH_ID + "=? AND "+
            		    SleepTimeEffect.COLUMN_SLEEP_TIME_EFFECT_HOUR + "=?"
            		    , new String[] {
                			String.valueOf(threshId), String.valueOf(hour) 
           				});
        } else {
            mResolver.insert(SleepTimeEffect.CONTENT_URI, values);
        }
   	
        return isExist;
    }
        
    public boolean updateCircadianItem(int circadianId, int hour, float sleepFactor, float wakeFactor){
		String tokenHour=String.valueOf(hour);
 		//-- TODO do we need this ? tokenHour=String.valueOf((intHour+CIRCADIAN_TIME_SHIFTER)%24);
 		
        Cursor cursor = mResolver.query(Circadian.CONTENT_URI, null, 
                Circadian.COLUMN_CIRC_HOUR + "=?", 
                new String[] {
                tokenHour, 
        		}, null);
        		     		
        boolean isExist = (cursor != null && cursor.getCount() > 0);
        
        if(cursor!=null) cursor.close();

        ContentValues values = new ContentValues();
        values.put(Circadian.COLUMN_CIRC_HOUR, hour);
        values.put(Circadian.COLUMN_CIRC_PERC_SLEEP_FACTOR, sleepFactor);
        values.put(Circadian.COLUMN_CIRC_PERC_WAKE_FACTOR, wakeFactor);
        if (isExist) {
            mResolver.update(Circadian.CONTENT_URI, 
            		values, 
            		Circadian.COLUMN_CIRC_HOUR+ "=?", 
            		new String[] { tokenHour }
            		);
        } else {
            mResolver.insert(Circadian.CONTENT_URI, values);
        }

        return isExist;
    }
    
    public long getActiveUserType(){
        long value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_IS_FROM_NETWORK
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getInt(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return value;      	
   }
    
    public long getActiveUserCreateTime(){
        long value=0;
       	int user=getActiveUserID();
       	try{
       		Cursor cursor =mResolver.query(User.CONTENT_URI,
           		new String[] {
                      User.COLUMN_USER_START_TIME
                  },
                  User._ID + "=? ",
                   new String[] {
                           String.valueOf(user)
                   },
                   null);
       		if (cursor != null) {
       			if (cursor.moveToFirst()) {
       				value = cursor.getLong(0);
       			} 
       			cursor.close();
       		}            	
       	} catch (Exception e) {
       		// nothing
       	}

        return value;      	
   }
    
    public int mQueryResult=0;
    
    public int getQueryResult(){
    	return mQueryResult;
    }
    
    public Cursor queryActivityRecords(int user, long fromTime, long toTime){
    	
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, 
          		new String[] {
        		Activities.COLUMN_ACTIVITY_VALUE,
        		Activities.COLUMN_ACTIVITY_TIME,
        		Activities.COLUMN_ACTIVITY_TYPE,       		
        		}
        		, 
        		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                "(" + Activities.COLUMN_ACTIVITY_TYPE + "=?  OR "+ Activities.COLUMN_ACTIVITY_TYPE + "=? ) AND " + 
        		Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
        		Activities.COLUMN_ACTIVITY_TIME + "<=?"
        		, 
        		new String[] {
                	String.valueOf(mActiveUserID), 
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_STEPS),
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_PULSE),
                	String.valueOf(fromTime), 
                	String.valueOf(toTime)
                }, 
                Activities.DEFAULT_SORT_ORDER_TIME );
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
  
        if(isExist==false){
            mQueryResult=QUERY_STOPPED;
        	cursor.close();
            cursor=null;
        }else{
            mQueryResult=QUERY_COMPLETED;
        }	
                
        return cursor;
    }

    //-- if no one existed locally then trying to get it from cloud side
    public Cursor queryActivityRecordsWithCloudLoad(int user, long fromTime, long toTime){
    	
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, 
          		new String[] {
        		Activities.COLUMN_ACTIVITY_VALUE,
        		Activities.COLUMN_ACTIVITY_TIME,
        		Activities.COLUMN_ACTIVITY_TYPE,       		
        		}
        		,
        		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                "(" + Activities.COLUMN_ACTIVITY_TYPE + "=?  OR "+ Activities.COLUMN_ACTIVITY_TYPE + "=? ) AND " + 
        		Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
        		Activities.COLUMN_ACTIVITY_TIME + "<=?"
        		, 
        		new String[] {
                	String.valueOf(mActiveUserID), 
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_STEPS),
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_PULSE),
                	String.valueOf(fromTime), 
                	String.valueOf(toTime)
                }, 
                Activities.DEFAULT_SORT_ORDER_TIME );
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
 
        int totalQueriedNumber=cursor.getCount();
                
        long currentTime=(System.currentTimeMillis() / 1000 / 60);

        boolean needLoadFromCloud=false;
        
        long  userCreateTime=getActiveUserCreateTime();
        
      	if(userCreateTime>0 && toTime<userCreateTime){
            needLoadFromCloud=true;
        }
        
      	//-- Log.i(TAG, "queryActivityRecordsWithCloud.. needLoadFromCloud:"+needLoadFromCloud+":"+totalQueriedNumber);
        
        //-- following is no need to load from cloud
        if(needLoadFromCloud==false ||
        		(
        				totalQueriedNumber>0
        				//-- if for today, always reload
        				//-- this can be optimized by checking lastSyncTime
        				&& (currentTime>=fromTime && currentTime<=toTime)==false
        		)
           )
        {   
            if(isExist==false){
                	mQueryResult=QUERY_STOPPED;
                   	cursor.close();
                	cursor=null;
            }else{
                	mQueryResult=QUERY_COMPLETED;
            }	
                
            return cursor;
        }else{
        	//-- Log.i(TAG, "queryActivityRecordsWithCloud.. calling BinsCloud");
            mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

        	if(mBinsCloudServer.loadActivityRecordsStepPage(
        							getUserUID(mActiveUserID),
        							fromTime, toTime			)==true){
        			mQueryResult=QUERY_WORKING;        		
        	}else{
        			mQueryResult=QUERY_STOPPED;        		        		
        	}
            cursor.close();
            cursor=null;
        }
        
        return null; //-- won't reach here
    }

    public Cursor querySleepRecords(int user, long fromTime, long toTime){
    	
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, 
          		new String[] {
        		Activities.COLUMN_ACTIVITY_VALUE,
        		Activities.COLUMN_ACTIVITY_TIME,
        		Activities.COLUMN_ACTIVITY_TYPE,       		
        		}
                , 
        		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                "(" + Activities.COLUMN_ACTIVITY_TYPE + "=?  OR "+ Activities.COLUMN_ACTIVITY_TYPE + "=? ) AND " + 
        		Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
        		Activities.COLUMN_ACTIVITY_TIME + "<=?"
        		, 
        		new String[] {
                	String.valueOf(mActiveUserID), 
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_SLEEP),
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_LIGHT),
                	String.valueOf(fromTime), 
                	String.valueOf(toTime)
                }, 
                Activities.DEFAULT_SORT_ORDER_TIME );
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
        
        if(isExist==false){
            mQueryResult=QUERY_STOPPED;
        	cursor.close();
            cursor=null;
        }else{
            mQueryResult=QUERY_COMPLETED;
        }	
                
        return cursor;
    }
    
	public Cursor querySleepRecordsWithCloudLoad(int user, long fromTime, long toTime){
    	
        Cursor cursor = mResolver.query(Activities.CONTENT_URI, 
          		new String[] {
        		Activities.COLUMN_ACTIVITY_VALUE,
        		Activities.COLUMN_ACTIVITY_TIME,
        		Activities.COLUMN_ACTIVITY_TYPE,       		
        		}
                , 
        		Activities.COLUMN_ACTIVITY_USER + "=? AND " +
                "(" + Activities.COLUMN_ACTIVITY_TYPE + "=?  OR "+ Activities.COLUMN_ACTIVITY_TYPE + "=? ) AND " + 
        		Activities.COLUMN_ACTIVITY_TIME + ">=? AND " +
        		Activities.COLUMN_ACTIVITY_TIME + "<=?"
        		, 
        		new String[] {
                	String.valueOf(mActiveUserID), 
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_SLEEP),
                	String.valueOf(Bins.BINS_ACTIVITY_TYPE_LIGHT),
                	String.valueOf(fromTime), 
                	String.valueOf(toTime)
                }, 
                Activities.DEFAULT_SORT_ORDER_TIME );
        
        boolean isExist = (cursor != null && cursor.getCount() > 0);
   
        int totalQueriedNumber=cursor.getCount();
        
        long currentTime=(System.currentTimeMillis() / 1000 / 60);

        boolean needLoadFromCloud=false;
        
        long  userCreateTime=getActiveUserCreateTime();
        
        if(userCreateTime>0 && toTime<userCreateTime){
            needLoadFromCloud=true;
        }
        
        //-- following is no need to load from cloud
        if(needLoadFromCloud==false ||
        		(
        				totalQueriedNumber>0
        				//-- if for today, always reload
        				//-- this can be optimized by checking lastSyncTime
        				&& (currentTime>=fromTime && currentTime<=toTime)==false
        		)
           )
        {     
            if(isExist==false){
                	cursor.close();
                	mQueryResult=QUERY_STOPPED;
                	cursor=null;
            }else{
                	mQueryResult=QUERY_COMPLETED;
            }	
                
            return cursor;
        }else{
            mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);          

        	if(mBinsCloudServer.loadActivityRecordsSleepPage(
        									getUserUID(mActiveUserID),
        									fromTime, toTime			)==true){
                mQueryResult=QUERY_WORKING;        		
        	}else{
                mQueryResult=QUERY_STOPPED;        		        		
        	}
        	
            cursor.close();
            cursor=null;
        }
        
        return null; //-- won't reach here
    }
    
 
}
