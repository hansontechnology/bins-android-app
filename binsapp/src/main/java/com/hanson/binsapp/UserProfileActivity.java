package com.hanson.binsapp;
import android.app.AlertDialog;
import android.view.MotionEvent;

import android.content.DialogInterface;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;
import android.widget.EditText;
import android.widget.TextView;

import com.hanson.binsapp.R;
import com.hanson.binsapp.provider.BinsCloudService;
import com.hanson.binsapp.provider.BinsCloudService.BinsCloudBinder;
import com.hanson.binsapp.provider.BinsCloudService.OnBinsCloudEventListener;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.provider.BinsDBUtils.OnBinsDBEventListener;
import com.hanson.binsapp.provider.BinsDBUtils.UserRecord;

import android.view.View.OnFocusChangeListener;

public class UserProfileActivity extends Activity {
    private static final String TAG = "UserProfileActivity";
    private DatePickerDialog birthDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private LinearLayout mPasswordLayout=null;

    // private Switch mGenderSwitch;
    private RadioButton mGenderRadio;
    private RadioButton mFemaleRadio;
    
    private EditText mNameEdit;    
    private EditText mPasswordEdit;    
    private ProgressBar mCloudLoadProgress;
    private EditText mFirstNameEdit;    
    private EditText mLastNameEdit;    
    private EditText mHeightEdit;
    private EditText mWeightEdit;
    private EditText mBirthEdit;
    private Button mSaveButton;
    private int mCurrentUserID;
    private Boolean mCloudUserExist=false;	//-- by definition, it only can be false for first time creation
    										//-- of the account, without cloud side registration
    private int mIsNetworkUser=0;			//-- it is 0, if user login to it using guest access right
    										//-- guest access right only can read and download data from cloud
    										//-- if it is 1, then a remote user
    										//-- if it is 2, then a local user
    private String mUserUID;
    private long mUserLastSyncTime;
    private Button mLoginButton;
    private CheckBox mShowPasswordCheckbox;
    
    private static Boolean mIsNewUserSession;
    private static Activity mActivity=null;

	private static BinsDBUtils binsData=null;
	
	private static Intent mIntentBinsCloud;
	
	//-- private static UnlockingLifeService mBinsCloudServer=null;	//-- HBP replacement
	private BinsCloudService mBinsCloudServer;
      
    boolean mBound=false; /* Flag indicating whether we have called bind on the service. */

    	
    private final int MSG_USER_EXIST=1;
    private final int MSG_CONFIRM_REMOTE_USER=2;
    private final int MSG_CONFIRM_LOCAL_USER=3;
    private final int MSG_LOAD_USER_FAILED=4;
    private final int MSG_LOAD_USER_SUCCESS=5;
    private final int MSG_DIALOG_CREATE_LOCAL_ACCOUNT=7;    
    private final int MSG_CONFIRM_CREATE_LOCAL_ACCOUNT=8;    
    private final int MSG_FINISH=6;
    
    boolean mIsForPasswordInput=false;
    
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
        	
        	if(msg.what==MSG_USER_EXIST){
        		mCloudLoadProgress.setVisibility(View.GONE);    	
        		 
    			if(mCloudUserExist==true){ 
    				mLoginButton.setVisibility(View.VISIBLE);
    		     	mSaveButton.setEnabled(false);
    		     	
    		     	//-- continue to ask type of user to login, and password input
    		     	//-- to load user profile info eventually
     	       		if(mBinsCloudServer.isSupportRemoteGuest()==true){
    	       			//-- if it support a remote-referenced user access
            			handleNetworkUser();
            		}else{        			
            			dialogConfirmExistingUser(); 
            			//-- inform there is an existing user, and
            			//-- ask user if he/she wants login 
            		}      

     			}else{
    				mLoginButton.setVisibility(View.GONE);
    			 	mSaveButton.setEnabled(true);
    			 	
    			 	//-- continue to allow input user profile details
                   	setEditFieldMode();
                   	
    			}
    			      		
        	}else if(msg.what==MSG_CONFIRM_REMOTE_USER){
                Toast toast=Toast.makeText(mActivity, R.string.enter_password, Toast.LENGTH_SHORT);                
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
                
                Log.i(TAG, "MSG_CONFIRM_REMOTE_USER :"+mNameEdit.getText().toString());
                
                mIsForPasswordInput=true;
                
                if(mPasswordEdit.requestFocus()) {
                	/*
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                	((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE))
                	.showSoftInput(mPasswordEdit, InputMethodManager.SHOW_FORCED);
                	*/
                }
        	}else if(msg.what==MSG_DIALOG_CREATE_LOCAL_ACCOUNT){
                mCloudLoadProgress.setVisibility(View.GONE);
                dialogConfirmCreateLocalUser();
        	}else if(msg.what==MSG_CONFIRM_CREATE_LOCAL_ACCOUNT){
                Log.i(TAG, "MSG_CONFIRM_CREATE_LOCAL_ACCOUNT :"+mNameEdit.getText().toString());

                mSaveButton.setEnabled(true);
    			mPasswordEdit.setVisibility(View.GONE);
    			mShowPasswordCheckbox.setVisibility(View.GONE);

                //-- NOTHING TO DO
                
        	}else if(msg.what==MSG_CONFIRM_LOCAL_USER){
                Log.i(TAG, "MSG_CONFIRM_LOCAL_USER :"+mNameEdit.getText().toString());
                
                mIsForPasswordInput=true;
                if(mPasswordEdit.requestFocus()) {
                	/*
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    
                  	((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE))
                	.showSoftInput(mPasswordEdit, InputMethodManager.SHOW_FORCED);
                	*/

                }

               
              	Toast toast=Toast.makeText(mActivity, R.string.enter_password, Toast.LENGTH_SHORT);                
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
				
        	}else if(msg.what==MSG_LOAD_USER_SUCCESS){
                Log.i(TAG, "MSG_LOAD_USER_SUCCESS");

                Bundle bundle=msg.getData();
                       
                if(bundle.getString("userName")==null || bundle.getString("userUID")==null){
                	//-- return with no result
                	Log.i(TAG, "loaded user profile, return with no result");
                	
                	return false;
                }
             	mNameEdit.setText(bundle.getString("userName"));
               	mFirstNameEdit.setText(bundle.getString("firstName"));             	
               	mLastNameEdit.setText(bundle.getString("lastName"));
               	if(bundle.getString("DOB")!=null){
               		mBirthEdit.setText(bundle.getString("DOB"));
               	}
               	if(bundle.getInt("weight")!=-1){
               		mWeightEdit.setText(String.valueOf(bundle.getInt("weight")));
               	}
               	if(bundle.getInt("height")!=-1){
               		mHeightEdit.setText(String.valueOf(bundle.getInt("height")));
               	}
               	if(bundle.getInt("isMale")!=-1){
               		int intIsMale=bundle.getInt("isMale");
               		boolean isMale;
               		if(intIsMale==0) isMale=false;
               		else isMale=true;
               		mGenderRadio.setChecked(isMale);
               		mFemaleRadio.setChecked(!(isMale));
               	}
    	        mUserUID=bundle.getString("userUID");
               	if(bundle.getLong("lastSyncTime")!=-1){
               		mUserLastSyncTime=bundle.getLong("lastSyncTime");;
               	}
               	
               	setEditFieldMode();	//-- to disable editable depends on if it is a guest mode
               	
        		mCloudLoadProgress.setVisibility(View.GONE);
          		mNameEdit.setEnabled(false);   		
        		mSaveButton.setEnabled(true);
        		
        	}else if(msg.what==MSG_LOAD_USER_FAILED){
                Log.i(TAG, "MSG_LOAD_USER_FAILED");
                mCloudLoadProgress.setVisibility(View.GONE);
                Toast toast=Toast.makeText(mActivity, R.string.notice_wrong_password, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.show();
                
                /*
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice_login_failed", nil) 
                                      message:NSLocalizedString(@"notice_wrong_password",nil) delegate:nil 
                                      cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) 
                                      otherButtonTitles:nil];
                [alert show];
                */
            
                mSaveButton.setEnabled(false);

        	}else if(msg.what==MSG_FINISH){
        		Log.i(TAG, "Unlocking MSG_FINISH");
        		/*
        		if(mBinsCloudServer!=null){
        			//-- mBinsCloudServer.unbindService(mBinsCloudConnection);        		
        			mBinsCloudServer.stopSelf();
        			mBinsCloudServer=null;
        		}
        		*/
        		finish();    		
        	}
        	
        	return false;
        }
    });
    
    private void setEditFieldMode() {
        if(mIsNetworkUser==0){	//-- if a (networked) guest user
            setEditFields(false);
        }else{	//-- if a network user or local user
            setEditFields(true);
        }
    }

    private void setEditFields(boolean newSetting) {
        mLastNameEdit.setEnabled(newSetting);
        mFirstNameEdit.setEnabled(newSetting);
        mHeightEdit.setEnabled(newSetting);
        mWeightEdit.setEnabled(newSetting);     
        mBirthEdit.setEnabled(newSetting);
        mGenderRadio.setEnabled(newSetting);
        mFemaleRadio.setEnabled(newSetting);
    }

    private void dialogConfirmExistingUser(){
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.notice);			
		builder.setMessage(R.string.notice_username_used);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	Log.i(TAG, "MSG_CONFIRM_LOCAL_USER sent");
            	
        		mHandler.sendEmptyMessage(MSG_CONFIRM_LOCAL_USER);
            }
        });
		
		builder.show();
		
    }
    
	public static void actionUserProfile(Activity fromActivity, Boolean isNewUser) {
        Intent intent = new Intent(fromActivity, UserProfileActivity.class);
        mActivity=fromActivity;
        mIsNewUserSession=isNewUser;
        
    	binsData=BinsDBUtils.activeDB();

        fromActivity.startActivity(intent);
	}

	class DialogUserSelectListener implements DialogInterface.OnClickListener {
		public void dialogUserSelectListener() {
	    }
	    public void onClick(DialogInterface dialog, int which) { 
	    	if(which==DialogInterface.BUTTON_POSITIVE){ //-- use_remote, means a guest user
	    		mIsNetworkUser=0;
	    		mHandler.sendEmptyMessage(MSG_CONFIRM_REMOTE_USER);
	    		
	    	}else if(which==DialogInterface.BUTTON_NEGATIVE){ //-- use_local, means a network regular user
	    		mIsNetworkUser=1;
	    		mHandler.sendEmptyMessage(MSG_CONFIRM_LOCAL_USER);
	    	}
	    }

	}
	
	private void handleNetworkUser () {
		mIsNetworkUser=0;    		
        //-- Can login to remote user data
		DialogUserSelectListener dialogUserSelectListener = new DialogUserSelectListener();
		
        AlertDialog.Builder alertUserSelect=new AlertDialog.Builder(this);
        alertUserSelect.setMessage(R.string.want_network_user);
        alertUserSelect.setPositiveButton(R.string.use_remote, dialogUserSelectListener); //-- means a guest user
        alertUserSelect.setNegativeButton(R.string.use_local, dialogUserSelectListener);  //-- means a network user
        alertUserSelect.create();
        
        alertUserSelect.show();               
		
	}
	
	private OnEditorActionListener  mEditorNameEditListener= new OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        	
        	Log.i(TAG,"NameEditorListener Action");

            mFirstNameEdit.setEnabled(true);
            mLastNameEdit.setEnabled(true);

            mSaveButton.setEnabled(false);
                	
            if(isValidEmail(mNameEdit.getText().toString())==false){
                Log.i(TAG,"email edit invalid address");
                
                return false;
            }
            //-- BiNS cloud part
            else if(mBinsCloudServer.checkUserExist(mNameEdit.getText().toString())==false){
            		Log.i(TAG, "Impossible to be here!");             		
            }

            mCloudLoadProgress.setVisibility(View.VISIBLE);
            mCloudLoadProgress.invalidate();
             
            return false;
        }
    };
    
    private boolean loadUserProfileFromCloud(){
    	
        if(mCloudUserExist==false) return false;
        
        //-- 20151203 TODO         		
        boolean isSuccess;
   		
       	isSuccess=mBinsCloudServer.loadUserProfile(
       			mNameEdit.getText().toString(),
       			mPasswordEdit.getText().toString() );
       		
       	if(isSuccess==true){
       			mCloudLoadProgress.setVisibility(View.VISIBLE);
                   mCloudLoadProgress.invalidate();
       	}  
       	
       	return isSuccess;	
    }
    
	private OnEditorActionListener  mEditorPasswordListener= new OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        	
        	Log.i(TAG,"PasswordEdit");
        	
        	
        	//-- previously loadUserProfileFromCloud();
        	   		
            return false;
        }
    };
    
    @Override
    protected void onStart(){
    	
    	super.onStart();
    	
        if(mNameEdit.isEnabled()==true){
        	if(mNameEdit.requestFocus()) {
        		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            	((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE))
            						.showSoftInput(mNameEdit, InputMethodManager.SHOW_FORCED);
            }
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        mGenderRadio=(RadioButton)findViewById(R.id.radioGender);
        mFemaleRadio=(RadioButton)findViewById(R.id.radioFemale);
        
        mNameEdit= (EditText)findViewById(R.id.editName);
        mPasswordEdit= (EditText)findViewById(R.id.editPassword);
        mPasswordLayout = (LinearLayout)findViewById(R.id.user_password_layout);
        mCloudLoadProgress= (ProgressBar)findViewById(R.id.cloudLoadProgress);
        mCloudLoadProgress.setVisibility(View.GONE);
        mFirstNameEdit= (EditText)findViewById(R.id.editFirstName);
        mLastNameEdit= (EditText)findViewById(R.id.editLastName);
        mBirthEdit= (EditText)findViewById(R.id.editBirth);
        mWeightEdit= (EditText)findViewById(R.id.editWeight);
        mHeightEdit= (EditText)findViewById(R.id.editHeight);
        mSaveButton= (Button)findViewById(R.id.buttonUserSave);
        mLoginButton= (Button)findViewById(R.id.buttonLogin);
        mLoginButton.setVisibility(View.GONE);
        mLoginButton.setOnClickListener(onClickListener);
        mShowPasswordCheckbox=(CheckBox)findViewById(R.id.checkboxShowPassword);
        
        mShowPasswordCheckbox.setOnClickListener(new OnClickListener() {          
        	public void onClick(View v) {       		
        		Log.i(TAG, "show password clicked");  
        			CheckBox cb=(CheckBox)v;
        			if(cb.isChecked()==false){
        				mPasswordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        			}else{
        				mPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);        				           				
        			}
        			mPasswordEdit.setSelection(mPasswordEdit.length());
                }          
        });
        mShowPasswordCheckbox.setChecked(false);

        //-- 11.20 findViewById(R.id.buttonUserAdd).setOnClickListener(onClickListener); 
        findViewById(R.id.buttonUserSave).setOnClickListener(onClickListener); 
        findViewById(R.id.buttonUserDelete).setOnClickListener(onClickListener); 
          
        //-- findViewById(R.id.buttonUserDelete).setVisibility(View.GONE); //-- 11.14 kj */
        
        mGenderRadio.setOnCheckedChangeListener(mOnCheckedChangeListener);
        mFemaleRadio.setOnCheckedChangeListener(mOnCheckedChangeListener);

        Log.d(TAG,"onCreate");
        
        mFirstNameEdit.setOnEditorActionListener(mNamesEditorActionListener);
        mFirstNameEdit.setOnClickListener(mNamesOnClickListener);
        mLastNameEdit.setOnEditorActionListener(mNamesEditorActionListener);
        mLastNameEdit.setOnClickListener(mNamesOnClickListener);


        
 
        mFirstNameEdit.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View arg0, MotionEvent arg1)
            {
            	 mNameEdit.setEnabled(false);
            	 
                 return false;
            }
        });
        
        mLastNameEdit.setOnTouchListener(new View.OnTouchListener()
        {
            public boolean onTouch(View arg0, MotionEvent arg1)
            {
            	 mNameEdit.setEnabled(false);
            	 
                 return false;
            }
        });
        
        mNameEdit.setOnEditorActionListener(mEditorNameEditListener);
        mNameEdit.setOnClickListener(new OnClickListener() {          
        	public void onClick(View v) {
        		
        		Log.i(TAG, "name edit button clicked");
        		
                mSaveButton.setEnabled(false);
                mFirstNameEdit.setEnabled(false);
                mLastNameEdit.setEnabled(false);

            }          
        });
        
        mPasswordEdit.setOnEditorActionListener(mEditorPasswordListener);
        mPasswordEdit.setOnClickListener(new OnClickListener() {          
        	public void onClick(View v) {       		
        		Log.i(TAG, "password edit button clicked");       		
                }          
        });
        
        mPasswordEdit.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            	mPasswordEdit.post(new Runnable() {
                    @Override
                    public void run() {
                    	/*
                    	if(mIsForPasswordInput==true){
                    		mIsForPasswordInput=false;
                    		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    		imm.showSoftInput(mPasswordEdit, InputMethodManager.SHOW_IMPLICIT);
                    	}*/

                    }
                });
            }
        });
        
        mNameEdit.setOnFocusChangeListener(new OnFocusChangeListener() {          

            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                   // code to execute when EditText loses focus
                	//-- 20151208 mNameEdit.setText(strLastUserName);
                	
                	Log.i(TAG, "lose focus of name edit");
                    mFirstNameEdit.setEnabled(true);
                    mLastNameEdit.setEnabled(true);                	
                	
                }
            }
        });
        // mHeightEdit.setOnEditorActionListener(mEditorActionListener);
        // mWeightEdit.setOnEditorActionListener(mEditorActionListener);        
        
   
        if(mIsNewUserSession==true)
        	editUser(0); // fill in to the text fields
        else{
        	int user=BinsDBUtils.getBluetoothLeDao(mActivity).getActiveUserID();
        	editUser(user);
        }
        
        // setup date picker
        mBirthEdit.setInputType(InputType.TYPE_NULL);
        
        mBirthEdit.setOnClickListener(onClickListener);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        birthDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {
             
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mBirthEdit.setText(dateFormatter.format(newDate.getTime()));
            }
 
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
              
        
    	Log.i(TAG, "Unlocking UnserProfileActivity onCreate:"+mBinsCloudServer);

        Intent intentBinsCloud = new Intent(this, BinsCloudService.class);  
        //-- Intent intentBinsCloud = new Intent(this, UnlockingLifeService.class);	//-- HBP replacement
        
        mIntentBinsCloud=intentBinsCloud;
        
        bindService(intentBinsCloud, mBinsCloudConnection, Context.BIND_AUTO_CREATE);
        this.startService(intentBinsCloud);        

        BinsDBUtils.activeDB().setEventListener(onBinsDBEventListener);
        
     }	//-- end of onCreate
    
    private OnBinsDBEventListener onBinsDBEventListener=new OnBinsDBEventListener(){
        
        public void binsDataChanged(){
        	Log.i(TAG, "binsDataChanged");
        	
        	mSaveToCloudCompleted=STATUS_YES;
        	chanceToExit();
        }
        
    };
    
    @Override
    protected void onStop() {
    	Log.i(TAG, "Unlocking UnserProfileActivity onStop:"+mBinsCloudServer);
    	/*
    	if(mBinsCloudServer!=null){
    		Log.i(TAG, "Unlocking: BinsCloudServer stopped by stopSelf");
    		//-- mBinsCloudServer.unbindService(mBinsCloudConnection);
    		mBinsCloudServer.stopSelf();
    		mBinsCloudServer=null;
    	}
    	*/
    	super.onStop();
    	
    	
    }

    @Override
    protected void onDestroy() {
       	super.onDestroy();
     	try {
    			if(mBound==true){
    				unbindService(mBinsCloudConnection);
    			}
    			//-- reverse of startService
           		this.stopService(mIntentBinsCloud);        			
                    
        }catch (IllegalArgumentException e){
            Log.d("userprofile ondestroy","Error: "+e.getMessage());
        }
   	
    }

    
    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mBinsCloudConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder serviceBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            BinsCloudBinder binsCloudBinder = (BinsCloudBinder) serviceBinder;
            //-- UnlockingLifeBinder binsCloudBinder = (UnlockingLifeBinder) serviceBinder;  //-- HBP replacement
            mBinsCloudServer = binsCloudBinder.getService();
            mBound = true;
            mBinsCloudServer.setConnection(mActivity);
            mBinsCloudServer.setConnectEventListener(onBinsCloudEventListner);   
            
           	Log.i(TAG, "Unlocking UnserProfileActivity onServiceConnected:"+mBinsCloudServer);

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
            mBinsCloudServer=null;
        	Log.i(TAG, "Unlocking UnserProfileActivity onServiceDisconnected:"+mBinsCloudServer);


        }
    };
    
    private OnClickListener mNamesOnClickListener= new OnClickListener() {          
    	public void onClick(View v) {
    		
    		Log.i(TAG, "mNamesOnClickListener");
    		
            mNameEdit.setEnabled(false);            
        }          
    };
    
    private OnEditorActionListener  mNamesEditorActionListener= new OnEditorActionListener() {

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        	
           mNameEdit.setEnabled(true);
            
           return false;
        }
    };
    
    private void dialogConfirmCreateLocalUser(){
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.notice);			
		builder.setMessage(R.string.notice_create_local_account);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	Log.i(TAG, "MSG_CONFIRM_CREATE_LOCAL_ACCOUNT sent");
            	mIsNetworkUser=2;	//-- means a local (not networked) user
        		mHandler.sendEmptyMessage(MSG_CONFIRM_CREATE_LOCAL_ACCOUNT);
            }
        });		
		builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
        	@Override
        	public void onClick(DialogInterface dialog, int which) {
        		return;
        	}
        });

		builder.show();
		
    }
    private OnBinsCloudEventListener onBinsCloudEventListner=new OnBinsCloudEventListener(){
    	
        public void serviceTimeout(){       	
    		Log.i(TAG, "serviceTimeout called");
            if(mCloudLoadProgress.getVisibility()==View.VISIBLE){
        		mHandler.sendEmptyMessage(MSG_DIALOG_CREATE_LOCAL_ACCOUNT);
            }        
        }

        public void hasUser(Boolean isUserExist){
           //do whatever you want to do when the event is performed.
        	Log.i(TAG, "hasUser received:"+isUserExist+":"+mNameEdit.getText().toString());
        	
            mCloudUserExist=isUserExist;
        	
            if(mCloudUserExist==false){
            	mIsNetworkUser=1; //-- means a network connected user, a standard member
            }
    		mHandler.sendEmptyMessage(MSG_USER_EXIST);
    		//-- indirectly perform dialog to ask user

        }
        
    	public void loadedUserProfile(String userName,String userUID, String firstName, String lastName,
				String dob, int weight, int height, int isMale, long lastSyncTime){ 
    		Log.i(TAG, "Unlocking called from user profile, loadedUserProfile - DOB:"+dob);
    		
    		Bundle bundle= new Bundle();
    		bundle.putString("userName", userName);
    		bundle.putString("userUID", userUID);
    		bundle.putString("firstName", firstName);
    		bundle.putString("lastName", lastName);
    		bundle.putString("DOB", dob);
    		bundle.putInt("weight", weight);
    		bundle.putInt("height", height);
    		bundle.putInt("isMale", isMale); 
    		bundle.putLong("lastSyncTime", lastSyncTime);
    		
    		
    		Message message = mHandler.obtainMessage(MSG_LOAD_USER_SUCCESS);
    		message.setData(bundle);
    		
     		mHandler.sendMessage(message); 
     		
    	}
    	public void loadUserProfileFailed(String userName){
    		Log.i(TAG, "Load user profile failed");    		
    		mHandler.sendEmptyMessage(MSG_LOAD_USER_FAILED);
    	}

        public void addedNewUser(Boolean isSuccess, String userName, String userUID){
        	Log.i(TAG, "Unlocking: addedNewUser from UserProfileActivity:"+userName+":"+userUID);

            if(isSuccess==true){             
            	BinsDBUtils.activeDB().updateUserUID(userName, userUID);
            }
            
        	mSaveToCloudCompleted=STATUS_YES;
         	chanceToExit();
            
 
        	//-- do nothing
        }
    	public void addedNewData(Boolean isSuccess, String userID){
    		
    		Log.i(TAG, "addedNewData");
    	}
    	
    	public void loadActivityCompleted(String userUID){
    		Log.i(TAG, "Unlocking loadActivityCompleted from UserProfileActivity");
    	}
    };	//-- END of BinsCloud event listeners
    
    
    private Boolean isValidEmail(String emailAddress) {
    	
    	if (TextUtils.isEmpty(emailAddress)) {
    		    return false;
    	} 
    
    	return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
    	
    }
    
    // equal to function loadUserData
    private void editUser(int user)
    {  	  	       	    	
       	Log.i(TAG, "editUser..");
        
       	mCurrentUserID=user;
       	
       	UserRecord userRecord=binsData.getUserProfile(user);
       	
       	mNameEdit.setText(userRecord.userName);
       	mPasswordEdit.setText(userRecord.userPassword);
       	mPasswordEdit.setSelection(mPasswordEdit.getText().length());	// move cursor to the end

       	mFirstNameEdit.setText(userRecord.firstName);
       	mLastNameEdit.setText(userRecord.lastName);
       	mBirthEdit.setText(userRecord.dob);
       	mWeightEdit.setText(Integer.toString(userRecord.weight));
       	mHeightEdit.setText(Integer.toString(userRecord.height));
       	
       	mIsNetworkUser=userRecord.isNetworkUser;
       	               	
        if(isValidEmail(mNameEdit.getText().toString())==true){
        	//-- if already have email as user name, no more editable
            mNameEdit.setEnabled(false);
            mCloudUserExist=true;
        }else{
            mNameEdit.setEnabled(true);
            mCloudUserExist=false; // locally available means the cloud is existing           
        }
		
        if(mIsNetworkUser==2){
			mLoginButton.setVisibility(View.GONE); 
			mPasswordEdit.setVisibility(View.GONE);
			mShowPasswordCheckbox.setVisibility(View.GONE);	
            mPasswordLayout.setVisibility(View.GONE);
            
        }else{
        	if(mCloudUserExist==true) 
        		mLoginButton.setVisibility(View.VISIBLE);
        	else 
        		mLoginButton.setVisibility(View.GONE);
        	
			mPasswordEdit.setVisibility(View.VISIBLE);
			mShowPasswordCheckbox.setVisibility(View.VISIBLE);
            mPasswordLayout.setVisibility(View.VISIBLE);			
        }
                
        mUserUID=userRecord.userUID;
        mUserLastSyncTime=userRecord.lastSyncTime;

        boolean isMale=false;
        if(userRecord.gender!=0) isMale=true;
       
        mGenderRadio.setChecked(isMale);
        mFemaleRadio.setChecked(!(isMale));
                
        setEditFieldMode();
    }
    
    private void genderSet(){   
    	boolean isMale;

    	// isMale=mGenderSwitch.isChecked();
    	isMale=mGenderRadio.isChecked();
	
  		//-- =Integer.parseInt(v.getText().toString()); 
	 	binsData.setActiveUserGender(isMale);            		

		if(mGenderRadio.isChecked()==true){
			Log.i(TAG, "Gender change to Male");
			mGenderRadio.setChecked(true);
		}
		else{
			Log.i(TAG, "Gender change to Female"); 
			mFemaleRadio.setChecked(true);
		}
    }
    private OnCheckedChangeListener mOnCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton cButton, boolean status) { 
 
        	if(cButton==mGenderRadio){
        		genderSet();
        	}
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	   	
    	Log.i(TAG, "onCreateOptionsMenu");
  
        /* 11.14 kj */ 
    	getMenuInflater().inflate(R.menu.main_activity_actions, menu);
                
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	   	      
    	
    	if(item.getItemId()==0){
    		
        	String strName=item.getTitle().toString();
        	int userID=0;
        	
        	if(strName.equalsIgnoreCase(getResources().getString(R.string.menu_add_new_user))==false){
        		userID=BinsDBUtils.getBluetoothLeDao(mActivity).getUserIdByName(strName);
        	}
        	/* 20150820
        	else{             	
        		userID=BinsDBUtils.getBluetoothLeDao(mActivity).insertNewUser();
        	}
        	*/
        	
        	editUser(userID); 	
            return super.onOptionsItemSelected(item);
    	}

        switch(item.getItemId()) {
 
            case R.id.menu_share:
            	
                SubMenu subMenu=item.getSubMenu();
                subMenu.clear();               
            	Cursor cursor=BinsDBUtils.getBluetoothLeDao(mActivity).getUsers(); 
                if (cursor != null) {
                    Log.d(TAG, "Column Count: " + cursor.getColumnCount());
                    cursor.moveToNext();
                    while (!cursor.isAfterLast()) {
                        String strUserName = cursor.getString(2);	//-- 150825 order dependent to DB
                        MenuItem menuItem=subMenu.add(strUserName);
                        if(strUserName.equalsIgnoreCase(mNameEdit.getText().toString())){
                        	menuItem.setIcon(R.drawable.white_btn);
                        }                       
 
                        cursor.moveToNext();
                    }
                    cursor.close();
                    
                    subMenu.add(getResources().getString(R.string.menu_add_new_user)); // New user                                 

                } 
                // sm5.add(0,1, Menu.NONE, "ttttt");
                // sm5.getItem(0).setTitle("alice");  

                break;

            case Menu.FIRST:
            	showMsg("New");
            	break;
            	
            	
                // Respond to the action bar's Up/Home button
            	// Back button
            case android.R.id.home:
			    String defaultName=getResources().getString(R.string.default_user_name);
			 	String userName=mNameEdit.getText().toString();
			        
			 	if(userName.equals(defaultName)!=true){	// no action if not a valid name
	            	mHandler.sendEmptyMessage(MSG_FINISH);
			 	} else {
	                Toast.makeText(this, R.string.need_change_username, Toast.LENGTH_SHORT).show();
			 	}

            	
                return true;
     
            	
         }
        return super.onOptionsItemSelected(item);
    }
    
    private void showMsg(String message) {
    	//-- Log.d(TAG, "TESTTEST");
    	
	}
    
    Calendar myCalendar = Calendar.getInstance();

    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            // updateLabel();
        }

    };
    
    private void noticeUserForDeleteDialog() {
    	
      	Cursor cursor=BinsDBUtils.activeDB().getUsers(); 
       	if(cursor==null || cursor.getCount()==0){
       		// Must have one user 
       		Log.i(TAG, "noticeUserForDelete cursor=null or "+cursor.getCount());
       		
       		if(cursor!=null) cursor.close();
       		return;
       	}    

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.notice)
                .setMessage(R.string.data_will_be_deleted)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      	Cursor cursor=BinsDBUtils.activeDB().getUsers(); 
                       	if(cursor==null || cursor.getCount()==0){
                       		// Must have one user 
                       		if(cursor!=null) cursor.close();
                       		return;
                       	}

                       	
                       	if (cursor != null) {
                            Log.d(TAG, "User Count: " + cursor.getCount());
                            if(cursor.getCount()>=1){
                            	int userID=0;
                 				BinsDBUtils.getBluetoothLeDao(mActivity).deleteUser(mCurrentUserID);   
                 				
                               	Cursor cursor2=BinsDBUtils.getBluetoothLeDao(mActivity).getUsers(); 
                               	cursor2.moveToFirst();                     	
                                if(!cursor2.isAfterLast()) {
                                    userID = cursor2.getInt(0); // TODO
                                }
                                cursor2.close();
                                
                                BinsDBUtils.activeDB().setActiveUser(userID);
                				editUser(userID);
                             }
                            cursor.close();
                            
                        } 

                    }
                })  
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                	@Override
                	public void onClick(DialogInterface dialog, int which) {
                		return;
                	}
                })
                .create();
        dialog.show();
    }

    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
        	//--**
        	if(view.getId()==R.id.editBirth){
        		String str=mBirthEdit.getText().toString();
            	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.US);
            	Date bDate= new Date();

            	  try{
            	      bDate = dateFormat.parse(str);
            	    } catch(Exception e){
            	      System.out.println(e.getMessage());
            	    }        		
        		
             	Calendar cal = Calendar.getInstance();
        		cal.setTime(bDate);
        		
        		birthDatePickerDialog.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        		birthDatePickerDialog.show();
        	}
        	else if(view.getId()==R.id.buttonLogin){
        		if(mCloudLoadProgress.getVisibility()==View.GONE){
        			loadUserProfileFromCloud();
        		}
        	}
    		else if(view.getId() == R.id.buttonUserSave) {
    			 	Log.i(TAG, "Update User");
    			 	
    			    //-- String defaultName=getResources().getString(R.string.default_user_name);
    			    
    			 	String userName=mNameEdit.getText().toString();
 			        
                    if(isValidEmail(mNameEdit.getText().toString())==false){

                    	Log.i(TAG, "save button, user name is not valid");
    			 		Toast.makeText(mActivity, R.string.need_change_username, Toast.LENGTH_SHORT);
    			 		
    			 		return;
    			 	} 
                        			 	
    			 	int foundUser=BinsDBUtils.activeDB().getUserIdByName(userName);
    			 	if(foundUser>0 && foundUser!=mCurrentUserID){
    			 		Toast.makeText(mActivity, R.string.notice_username_used, Toast.LENGTH_SHORT);
    			 		return;
    			 	}
    			 	
    			 	Boolean isSuccessCallCloud=false;
    			 	
  			 	    if(mCurrentUserID==0){    			 		
    			 		  isSuccessCallCloud=binsData.insertNewUser(
    		      					mNameEdit.getText().toString(),
    		      					mFirstNameEdit.getText().toString(),
    		      					mLastNameEdit.getText().toString(),
    		      					mBirthEdit.getText().toString(),
    		      					Integer.parseInt(mWeightEdit.getText().toString()),
    		      					Integer.parseInt(mHeightEdit.getText().toString()),
    		      					mGenderRadio.isChecked(),
    		      					mPasswordEdit.getText().toString(),
    		      					mIsNetworkUser,
    		      					mUserUID,
    		      					mUserLastSyncTime
    		  			 			);
    		  			 		
    		  			 	//-- 20151210 BUG FIX
    		  			 	mCurrentUserID=binsData.getActiveUserID();
  			 		
  			 	    } else {
    			 		  isSuccessCallCloud=binsData.updateUser(
    	  		    				mCurrentUserID,	
    	  	    					mNameEdit.getText().toString(),
    	  	    					mFirstNameEdit.getText().toString(),
    	  	    					mLastNameEdit.getText().toString(),
    	  	    					mBirthEdit.getText().toString(),
    	  	    					Integer.parseInt(mWeightEdit.getText().toString()),
    	  	    					Integer.parseInt(mHeightEdit.getText().toString()),
    	  	    					mGenderRadio.isChecked(),
    	  	      					mPasswordEdit.getText().toString(),
    	          					mIsNetworkUser,
    	          					mUserUID,
    	          					mUserLastSyncTime
    	  	    					);   			 	
 			 	    }
  			 	    
  			 	    if(isSuccessCallCloud==true){
  			 	    	Log.i(TAG, "save profile, successfully called cloud");
  			 	    	mSaveToCloudCompleted=STATUS_NO;
  			 	    }else{
  			 	    	Log.i(TAG, "save profile, failed to call cloud");
  			 	    	mSaveToCloudCompleted=STATUS_DONTCARE;		 		
  			 	    }
    			 	
    			 	if(mIsNetworkUser==2){ //-- IF A LOCAL ACCOUNT (not networked)
    			 		mSaveToCloudCompleted=STATUS_DONTCARE;
    			 	}
    			 	
    			 	if(BinsDBUtils.activeDB().getActiveDeviceID()==0  && mIsNetworkUser!=0){ // device not assigned yet
    			 	    Toast.makeText(mActivity, R.string.button_press, Toast.LENGTH_SHORT)
    			 	    	.show();
    			 		
    			 	    	//-- mDevicePairCompleted=STATUS_NO;

	   		           	BinsActivity binsActivity=(BinsActivity)(mActivity);     	
	   		           	binsActivity.stopDeviceBeaconRegion();

	    				DeviceListActivity.autoPair(mActivity, mCurrentUserID);
	    				//-- as the pairing will work independently, 
	    				//-- we can free current activity
	   			 		mDevicePairCompleted=STATUS_YES;	    					
	    			}else{
	  			 		mDevicePairCompleted=STATUS_DONTCARE;	    				
	    			}
	    			chanceToExit();
    		}
    		else if(view.getId() == R.id.buttonUserDelete) {  
    			
                	noticeUserForDeleteDialog();
               	
     		}	//-- else if delete button
        }
    };

    static int STATUS_DONTCARE=2;
    static int STATUS_YES=1;
    static int STATUS_NO=0;
    
    int mSaveToCloudCompleted=STATUS_DONTCARE;
    int mDevicePairCompleted=STATUS_DONTCARE;
    
    private void chanceToExit() {
    	
    		Log.i(TAG, "Unlocking: chanceToExit:"+mSaveToCloudCompleted+":"+mDevicePairCompleted);
            if((mSaveToCloudCompleted==STATUS_YES || mSaveToCloudCompleted==STATUS_DONTCARE)
               &&
               (mDevicePairCompleted==STATUS_YES || mDevicePairCompleted==STATUS_DONTCARE) ){
                            	
                mHandler.sendEmptyMessage(MSG_FINISH);
                
            }else{
            	
                //-- reuse this indicator to show until finish
                //-- TODO [pairRunningIndicator startAnimating];
            }
  
    }

}
