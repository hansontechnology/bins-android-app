
package com.hanson.binsapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.hanson.binsapp.R;
import com.hanson.binsapp.fragment.ActivityFragment;
import com.hanson.binsapp.fragment.BinsFragment;
import com.hanson.binsapp.fragment.DeviceInfoFragment;
import com.hanson.binsapp.fragment.SleepFragment;
import com.hanson.binsapp.provider.BinsCloudService;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.provider.BinsCloudService.BinsCloudBinder;
import com.hanson.binsapp.widget.FragmentPagerAdapter;
import com.hanson.binsapp.widget.HansonTabHost;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

// import com.facebook.*;
// import com.facebook.model.*;


public class BinsActivity extends BaseActivity implements BeaconConsumer {
    public static final String TAG = "BinsActivity";
    public static final String LABEL_BiNS = "BiNS";
    public static final String LABEL_ACTIVITY = "Activity";
    public static final String LABEL_SLEEP = "Sleep";
    public static final String LABEL_WEIGHT = "Weight";
    public static final String LABEL_FRIENDS = "Friends";
    public static final String LABEL_MORE = "More";
    public static final String LABEL_DEVICE = "Device";

    //------------------------------ 
   public static final String ACTION_DATA_SYNC_ERROR = "com.data.sync.ERROR";
    
    public static final String ACTION_DATA_SYNC_START = "com.data.sync.START";
    public static final String ACTION_DATA_SYNC_END = "com.data.sync.END";
    public static final String ACTION_DATA_SYNC_RUNNING = "com.data.sync.RUNNING";
    public static final String ACTION_DATA_SYNC_STOP = "com.data.sync.STOP";
    public static final String ACTION_PULSE_FUNCTION_START = "com.data.pulse.START";
    public static final String ACTION_PULSE_FUNCTION_STOP = "com.data.pulse.STOP";
    public static final String ACTION_PULSE_PROGRESS = "com.data.pulse.PROGRESS";
    public static final String ACTION_PULSE_RESULT = "com.data.pulse.RESULT";
    public static final String ACTION_BATTERY_LOW = "com.data.battery.BATTERYLOW";
    public static final String EXTRAS_BATTERY_LEVEL	= "com.data.battery.extras.LEVEL";
    
    public static final String ACTION_DEVICE_MODEL = "com.data.device.MODEL";
    public static final String EXTRAS_MODEL	= "com.data.device.extras.MODEL";
   
    
    public static final String ACTION_PULSE_SDNN_RESULT = "com.data.pulse.SDNNRESULT";
    public static final String ACTION_DATA_SYNC_PROGRESS = "com.data.sync.PROGRESS";

    public static final String ACTION_FINDER_LOST = "com.data.finder.LOST";
    public static final String ACTION_FINDER_STOP = "com.data.finder.STOP";
    public static final String ACTION_DATA_NOTIFY = "com.data.sync.NOTIFY";
    public static final String ACTION_SMS_RECEIVE = "com.data.sms.RECEIVE";
    public static final String ACTION_MMS_RECEIVE = "com.data.mms.RECEIVE";

    public static final String EXTRAS_SDNN_START	= "com.data.sdnn.extras.SDNN_START";
    public static final String EXTRAS_SDNN_READY	= "com.data.sdnn.extras.SDNN_READY";
    public static final String EXTRAS_SDNN_LEVEL	= "com.data.sdnn.extras.SDNN_LEVEL";

    public static final String EXTRAS_PULSE_DATA	= "com.data.pulse.extras.DATA";
    public static final String EXTRAS_PULSE_READY	= "com.data.pulse.extras.READY";
    public static final String EXTRAS_PULSE_SEQUENCE = "com.data.pulse.extras.SEQUENCE";
         
    //------------------------------
    
    public ViewPager mViewPager;
    private HansonTabHost mTabHost;
    
    private String mCurrentTabId; //-- 09.13
    private long mLastBeaconCheckForBatteryLow=0;
    
	private BeaconManager mBeaconManager;
	public static final String BEACON_REGION_CONNECT_REQUEST = "BeaconRegionConnectRequest";
	public Region gActiveBeaconRegion = null;
	public static final int BEACON_MAJOR_NORMAL = 0;
	public static final int BEACON_MAJOR_BATTERY_LOW = 1;
	public static final int BEACON_MAJOR_HEARTRATE = 2;
	public static final int BEACON_MAJOR_FREEDROP = 3;
	public static final int BEACON_MAJOR_BUTTON_SOS = 4;
	public static final int BEACON_MAJOR_CONNECT_REQUEST = 10;
	public static final int BEACON_MAJOR_HEARTRATE_CYCLE = 11;	// connect only, no data sync
	
	
	public static BinsCloudService mBinsCloudServer;
	//-- public static UnlockingLifeService mBinsCloudServer;	//-- HBP replacement

	public static BinsActivity mBinsActivity;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Use this check to determine whether BLE is supported on the device.
        // Then you can selectively disable BLE-related features.
        if (!deviceSupported()) {
            //Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            showErrorDialog();
            return;
        }
		
        mBeaconManager = BeaconManager.getInstanceForApplication(this);
        mBeaconManager.bind(this);
        
        //-- 06.08
        // View content = findViewById(R.id.view_pager);
        // content.setDrawingCacheEnabled(true);
        
        setContentView(R.layout.activity_bins);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new BinsPageAdapter());
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int state) {
                mTabHost.setCurrentTab(state);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int position) {

            }
        });

        mTabHost = (HansonTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
            	//-- Log.i(TAG, "onTabChanged tabId:"+tabId);
            	mCurrentTabId=tabId;	// 09.13
            	
                //mTitle.setText(tabId);
                if (tabId.equals(LABEL_BiNS)) {
                    mTitle.setText(R.string.tab_bins);
                    mViewPager.setCurrentItem(0);
                } else if (tabId.equals(LABEL_ACTIVITY)) {
                    mTitle.setText(R.string.tab_activity);
                    mViewPager.setCurrentItem(1);
                    
                /* 11.14 kj */
                } else if (tabId.equals(LABEL_SLEEP)) {
                    mTitle.setText(R.string.tab_sleep);
                    mViewPager.setCurrentItem(2);   
                      
                /* */
                } else {
                    mTitle.setText(R.string.tab_more);
                    mViewPager.setCurrentItem(3);	// 11.14 kj 3);
                }
                
  /*              
                if(tabId.equals(LABEL_ACTIVITY))
                {
                	if(mBinsFragment.mTimeControllerView.getTimeInMillis()!=
                			mActivityFragment.mTimeControllerView.getTimeInMillis()
                			)
                	{
                		mActivityFragment.mTimeControllerView.setTimeInMillis(
                				mBinsFragment.mTimeControllerView.getTimeInMillis()); 		
                	}
                }*/
            }
        });
        mTabHost.setup(this, getFragmentManager(), R.id.realtabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);

        //-- 09.13
        // new BinsFragment();
        // new ActivityFragment();
        // new SleepFragment();
        
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        setupBinsTab(layoutInflater);
        setupActivityTab(layoutInflater);
        /* 11.14 kj */ setupSleepTab(layoutInflater);
        //setupWeightTab(layoutInflater);
        //setupFriendsTab(layoutInflater);
        //setupMoreTab(layoutInflater);
        setupDeviceTab(layoutInflatemBinsActivityr);
        
	   
        //-- 150516 call it daily 
        //-- Intent ulIntent = new Intent(BinsActivity.this, UnlockingLifeReceiver.class);
        //-- PendingIntent pendingIntent = PendingIntent.getBroadcast(BinsActivity.this, 0, ulIntent,0);
   
        /* allow perform every day */
        // AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);        
        // alarmManager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(),  
        //        (24*60*60) * 1000, pendingIntent); 
        
        //-- 150612
        //-- 150810 
        //-- if(BinsDBUtils.getBluetoothLeDao(this).readyThresholdData()==false)
        /* 20151210
        {
            Log.i(TAG, "going to create unlocking");
            
            Intent intentBinsCloud = new Intent(this, UnlockingLifeService.class);
            
            Intent service1 = new Intent("com.hanson.binsapp.provider.UnlockingLifeService");
        	//-- Intent myIntent = new Intent(this, RangingActivity.class);
    		this.startService(service1);
    	}
    	*/
        
        Intent intentBinsCloud = new Intent(this, BinsCloudService.class);        
        //-- Intent intentBinsCloud = new Intent(mContext, UnlockingLifeService.class); //-- HBP replacement
    	
        bindService(intentBinsCloud, mBinsCloudConnection, Context.BIND_AUTO_CREATE);
		startService(intentBinsCloud);        

		mBinsActivity=this;
      
    }
       
    private ServiceConnection mBinsCloudConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder serviceBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            BinsCloudBinder binsCloudBinder = (BinsCloudBinder) serviceBinder;
            //-- UnlockingLifeBinder binsCloudBinder = (UnlockingLifeBinder) serviceBinder; //-- HBP
            mBinsCloudServer = binsCloudBinder.getService();
            mBinsCloudServer.setConnection(getApplicationContext());
            
            BinsDBUtils.setBinsCloud();	//-- as all operation is doing by DB, setting it first.
            
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };
    
 
    public void startDeviceBeaconRegion() {
    	Log.i(TAG, "startDeviceBeaconRegion");
        try {
      	  	if(gActiveBeaconRegion!=null){
      	        mBeaconManager.bind(this);
      	  		mBeaconManager.startRangingBeaconsInRegion(gActiveBeaconRegion);
      	  	}
      	  
        } catch (RemoteException e) {   }
    	
    }
    
    public void stopDeviceBeaconRegion() {
    	Log.i(TAG, "stopDeviceBeaconRegion");
        try {
      	  	if(gActiveBeaconRegion!=null){
      	  		mBeaconManager.stopRangingBeaconsInRegion(gActiveBeaconRegion);
      	        mBeaconManager.unbind(this);      	  		
      	  	}
      	  
        } catch (RemoteException e) {   }
    	
    }
    
    public void setDeviceBeaconRegion() {
    	
    	Log.i(TAG, "setDeviceBeaconRegion");
    	
        try {
      	  	if(gActiveBeaconRegion!=null){
      	  		mBeaconManager.stopRangingBeaconsInRegion(gActiveBeaconRegion);
      	  		gActiveBeaconRegion=null;
      	  	}
      	  
        } catch (RemoteException e) {   }

    	if(BinsDBUtils.activeDB()==null) return; // 0316          
        String ibeaconUuid=BinsDBUtils.activeDB().getIbeaconUuid();
        
        if(ibeaconUuid==null || (ibeaconUuid!=null && ibeaconUuid.equals(""))) 
        	return; 
          
        Log.i(TAG, "new beacon id="+ibeaconUuid);
        /*
        Region newRegion= new Region(
					BEACON_REGION_CONNECT_REQUEST, 
					Identifier.parse(ibeaconUuid), 
					null, //Identifier.fromInt(BEACON_MAJOR_CONNECT_REQUEST), // null, Identifier.fromInt(1), 
					null
        );
        */
        Region newRegion= new Region(
				BEACON_REGION_CONNECT_REQUEST, 
				Identifier.parse(ibeaconUuid), 
				Identifier.fromInt(1),	// major is set to 1 constantly 
				null
        		);
        		//-- 150715 check minor numbers to tell always
        
        // if want to add new major minor, then add new region ID
        
        try {
        	  mBeaconManager.startRangingBeaconsInRegion(newRegion);
        	  gActiveBeaconRegion=newRegion;        	  
          } catch (RemoteException e) {   }
                    
    }
    @Override 
    protected void onDestroy() {
        super.onDestroy();
        mBeaconManager.unbind(this);
        
     	try {
			unbindService(mBinsCloudConnection);                
     	}catch (IllegalArgumentException e){
     		Log.d("userprofile ondestroy","Error: "+e.getMessage());
     	}

    }
    @Override
    public void onBeaconServiceConnect() {
    	Log.i(TAG, "onBeaconServiceConnect");
    	
        mBeaconManager.setRangeNotifier(new RangeNotifier() {
        	@Override 
        	public void didRangeBeaconsInRegion(Collection beacons, Region region) {
        		if (beacons.size() > 0) {
        			Beacon beacon=((Beacon) beacons.iterator().next());
        			Log.i(TAG, "1st beacon found on "+beacon.getDistance()+" meters away."+"major="+beacon.getId2().toInt()+" minor="+beacon.getId3().toInt());
                    if(beacon.getId3().toInt()==BEACON_MAJOR_CONNECT_REQUEST){
                    	// it is a connection request
						// should avoid repeated calls
                    	// tryConnect will only do connect if not connected connecting yet. 
                    	
                    	mBinsFragment.tryConnect();	
                    	// if(mBinsFragment.checkTryConnect()==true){
                    	// 	mBinsFragment.newDataSyncSession();
                    	// }
                    }else if(beacon.getId3().toInt()==BEACON_MAJOR_HEARTRATE_CYCLE){
                    	mBinsFragment.tryConnectWithoutSync();
                    
                	}else if(beacon.getId3().toInt()==BEACON_MAJOR_BATTERY_LOW){
                 		long currentTimeInMinute=(System.currentTimeMillis() / 1000 / 60); 
                 		
                        if(currentTimeInMinute>(mLastBeaconCheckForBatteryLow+30)){
                            // if less than 30 minutes, then just skip
                            
                            mLastBeaconCheckForBatteryLow=currentTimeInMinute;
                            
                    		notifyBatteryLow();
                            
                        }
                	}                    
                }
        	}
           });
        
        setDeviceBeaconRegion();  //-- it will be called first time when startup, also
        
        /*
        try {
        	mBeaconManager.startRangingBeaconsInRegion(new Region(BEACON_REGION_CONNECT_REQUEST, null, null, null));
            // mBeaconManager.setDebug(true); // 150503 for test
            //mBeaconManager.getBeaconParsers().add(new BeaconParser().
            //        setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        } catch (RemoteException e) {   }
        */
        
    	/* Should not include monitorNotifier manually, use Bootstrap
    	 * 
        mBeaconManager.setMonitorNotifier(new MonitorNotifier() {
        @Override
        public void didEnterRegion(Region region) {
            Log.i(TAG, "I just saw an beacon for the first time!");        
        }

        @Override
        public void didExitRegion(Region region) {
            Log.i(TAG, "I no longer see an beacon");
        }

        @Override
            public void didDetermineStateForRegion(int state, Region region) {
            Log.i(TAG, "I have just switched from seeing/not seeing beacons: "+state);        
            }
        });

        try {
            mBeaconManager.startMonitoringBeaconsInRegion(new Region("myMonitoringUniqueId", null, null, null));
        } catch (RemoteException e) {    }
        */
       
    }
    
    // 20151212
    public OnUiFragmentEventListener mUiFragmentEventListener=null; //listener field

    public interface OnUiFragmentEventListener{
   	   public void binsDataChanged();
    }
    
    //setting the listener
    public void setUiFragmentEventListener(OnUiFragmentEventListener eventListener) {
 	   this.mUiFragmentEventListener=eventListener;
    }
    public void uiFragmentUpdate(){   
    	Log.i(TAG, "Unlocking entered uiFragmentUpdate of BinsActivity");
    	if(mUiFragmentEventListener!=null){
    		mUiFragmentEventListener.binsDataChanged();
    	}  	
    }
    
    public void notifyBatteryLow()
    {    	
		Notification myNotification;
		myNotification= new NotificationCompat.Builder(getApplicationContext())
		.setContentTitle(getResources().getString(R.string.notification_title_battery_less))
		.setContentText(getResources().getString(R.string.notification_content_battery_less)+" "+10+"%")
		.setTicker(getResources().getString(R.string.notification_ticker_battery))
		.setWhen(System.currentTimeMillis())
		.setDefaults(Notification.DEFAULT_SOUND)
		.setAutoCancel(true)
		.setSmallIcon(R.drawable.ic_bins)
		.build();

		NotificationManager notificationManager = 
		  (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(0, myNotification); 
	}

    // 2015.01.17
    @Override
    protected void onResume() {
        super.onResume();
    	
        if(BinsDBUtils.activeDB()==null) return; // 0316
        
        long syncedTime=BinsDBUtils.activeDB().getActiveUserLastSyncTime();
    
        if(syncedTime==0) return;

 		long currentTime=(System.currentTimeMillis() / 1000 / 60); 

 		if((currentTime-syncedTime)>5)   // after 5 minutes
 		{
 			Log.i(TAG,"active to start sync:"+currentTime);
 			mBinsFragment.backgroundSyncAndDisconnect();
 		}
 		
 		BinsDBUtils.activeDB().uploadTask();	//-- 20150825
 		
    }
    
    private void setupBinsTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_bins);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(getResources().getString(R.string.tab_bins));
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_BiNS).setIndicator(view),
                DummyFragment.class, null);
    }
  
    private void setupActivityTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_activity_man);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(getResources().getString(R.string.tab_activity));
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_ACTIVITY).setIndicator(view),
                DummyFragment.class, null);
    }

    private void setupSleepTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_moon);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(getResources().getString(R.string.tab_sleep));
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_SLEEP).setIndicator(view),
                DummyFragment.class, null);
    }
    
    /*private void setupWeightTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_weight);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(LABEL_WEIGHT);
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_WEIGHT).setIndicator(view),
                WeightFragment.class, null);
    }

    private void setupFriendsTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_friends);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(LABEL_FRIENDS);
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_FRIENDS).setIndicator(view),
                WeightFragment.class, null);
    }

    private void setupMoreTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_more);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(LABEL_MORE);
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_MORE).setIndicator(view),
                MoreBaseFragment.class, null);
    }*/

    private void setupDeviceTab(LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.tab_widget, null);
        ImageView icon = (ImageView) view.findViewById(R.id.widget_icon);
        icon.setImageResource(R.drawable.ic_more);
        TextView tv = (TextView) view.findViewById(R.id.widget_title);
        tv.setText(getResources().getString(R.string.tab_more)); //-- LABEL_DEVICE to more
        mTabHost.addTab(mTabHost.newTabSpec(LABEL_DEVICE).setIndicator(view),
                DummyFragment.class, null);
    }

    private void showErrorDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.warning)
                .setMessage(R.string.only_support_android_43_up)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setCancelable(false)
                .create();
        dialog.show();
    }

    public static class DummyFragment extends Fragment {
    }

    BinsFragment mBinsFragment;
    DeviceInfoFragment mDeviceInfoFragment;
    ActivityFragment mActivityFragment;
    SleepFragment mSleepFragment;
    
    private class BinsPageAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> mFragments = new ArrayList<Fragment>();

        public BinsPageAdapter() {
            super(getFragmentManager());
            mBinsFragment=new BinsFragment();            
            mFragments.add(mBinsFragment);
            mFragments.add((mActivityFragment=new ActivityFragment()));
            /* 11.14 kj */ mFragments.add((mSleepFragment=new SleepFragment()));
            mFragments.add((mDeviceInfoFragment=new DeviceInfoFragment()));
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
    
/*  
    private void getScreen()
    {
        View content = findViewById(R.id.view_pager);
        Bitmap bitmap = content.getDrawingCache();
        File file = new File( Environment.getExternalStorageDirectory() + "/test.png");
        try 
        {
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            bitmap.compress(CompressFormat.PNG, 100, ostream);
            ostream.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }

*/
  /*  
    protected void onActivityShare()
    {

    	// start Facebook Login   	
    	Session.openActiveSession(this, true, new Session.StatusCallback() {
    	// callback when session changes state

    		@Override
    		public void call(Session session, SessionState state, Exception exception) {
    			Log.d(TAG,"session call entered");
    			if (session.isOpened()) {
    	   			Log.d(TAG,"session call openned");
    				// make request to the /me API
    				Request.newMeRequest(session, new Request.GraphUserCallback() {
    				// callback after Graph API response with user object

    					@Override
    					public void onCompleted(GraphUser user, Response response) {
    						if (user != null) {
    							//TextView welcome = (TextView) findViewById(R.id.welcome);
    							//welcome.setText("Hello " + user.getName() + "!");
    							Log.d(TAG,"Hello "+ user.getName());
    							
    						}

    					} // end onCompleted
    				}).executeAsync();	//-- end newMeRequest

    			} // isOpened

    		} // call
    	});	// openActiveSession
    }	// on Function 
    
*/    
/*    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {	
        super.onActivityResult(requestCode, resultCode, data);
        if(Session.getActiveSession()!=null){
        	Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
        }

    }
*/
/* -- 06.08 for Facebook  */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	
    	  
    	Log.i(TAG, "onCreateOptionsMenu");
    	  
/*    	
        SubMenu sm1=menu.addSubMenu("Menu1");
        SubMenu sm2=menu.addSubMenu("Menu2");
        SubMenu sm3=menu.addSubMenu("Menu3");
        
        sm1.add(0, Menu.FIRST, 0, "new");
*/
 

/*      
   
        SubMenu sm4=menu.getItem(0).getSubMenu();

       
        MenuItem item = menu.findItem(R.id.menu_share);
        
        SubMenu sm5=item.getSubMenu();
         
        sm5.add(0,1, Menu.NONE, "ttttt");
        sm5.getItem(0).setTitle("olive");  

        sm4.addSubMenu("Sub123");
        sm4.add(0, 1, Menu.NONE, "SubSub4");
        sm4.add(0, 2, Menu.NONE, "SubSub5");
*/       
    	invalidateOptionsMenu();
    	
        return super.onCreateOptionsMenu(menu);
        
        // return true;
    }
       
    
    public void refreshViews(){
    	
    	mBinsFragment.refreshViews(); //-- always run
    	
        if(mCurrentTabId.equals(LABEL_BiNS)){
        } else if(mCurrentTabId.equals(LABEL_DEVICE)){
        	mDeviceInfoFragment.updateViews(mBinsFragment);
        } else if(mCurrentTabId.equals(LABEL_ACTIVITY)){
        	mActivityFragment.refreshViews();
        	/* 11.14 kj */
        } else if(mCurrentTabId.equals(LABEL_SLEEP)){
        	mSleepFragment.refreshViews();
        	mSleepFragment.redrawViews(); //-- 20151022
        	/* */
        } 
        
    }
    
    private void changeUser(String userName)
    {   	     
    	Log.i(TAG, "changeUser");
    	if(mBinsFragment.syncSessionRunning()==true) return;   		
 
    	BinsDBUtils.getBluetoothLeDao(this).changeActiveUserByName(userName);
    	
    	
	    setDeviceBeaconRegion(); 	// 150605
	    
    	/* TODO 11.21
        final Intent intent = new Intent(BinsActivity.ACTION_DATA_SYNC_END);        
        sendBroadcast(intent);
		*/
    	
	    refreshViews();
  
    }
    
    Boolean mHasMenuSetAutoHRD=false;

    //-- 2015.01.13 work around for Android bug, 
    // not call onPrepareOptionsMenu when first time pressing menu key.
    // call invalidateOptioneMenu first, to force onPrepareOptionsMenu been called
    boolean mIsMenuFirstClick=true;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch(keyCode) {
        case KeyEvent.KEYCODE_MENU:
            if(mIsMenuFirstClick) {
                mIsMenuFirstClick = false;
                invalidateOptionsMenu();
            }
        }
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

    	//-- Log.i(TAG, "onPrepareOptionsMenu");
    	
    	menu.clear();
    	
        if(BinsFragment.syncIsActive()==false){
         	//-- Log.i(TAG, "static Menu");
                 	
            /* 11.14 kj */ getMenuInflater().inflate(R.menu.static_menu_actions, menu);        	        	
        }
        else{
          	//-- Log.i(TAG, "runtime Menu");
            
            /* 11.14 kj */ getMenuInflater().inflate(R.menu.main_menu_actions, menu);        	        	
        }
        
        
    	if(BinsDBUtils.activeDB().getActiveDeviceType()==BinsDBUtils.BINS_MODEL_BINSX1)
    	{
    		Log.i(TAG, "menu binsx1 model");
    		menu.removeItem(R.id.menu_start_heartrate_detection);
    		//-- 01.13 menu.removeItem(R.id.menu_bins_heartrate_sleep_enable);
    		menu.removeItem(R.id.menu_bins_heartrate_step_enable);
    		menu.removeItem(R.id.menu_set_auto_heartrate_detection);
    		menu.removeItem(R.id.menu_bins_heartrate_enable);
    		menu.removeItem(R.id.menu_bins_display_brightness);
    		menu.removeItem(R.id.menu_bins_oled_blink);
    	}        

    	//-- 150825 for Z3 release, disable those menus 
		menu.removeItem(R.id.menu_set_auto_heartrate_detection);
		menu.removeItem(R.id.menu_bins_heartrate_sleep_enable);
		menu.removeItem(R.id.menu_bins_background_sync);
		menu.removeItem(R.id.menu_bins_lock_bins);
   		menu.removeItem(R.id.menu_bins_oled_blink);
   	    	
    	int sz=menu.size();

    	for (int i=0; i< sz; i++){

    		MenuItem mi=menu.getItem(i);

    		BinsFragment.gConfiguration=mBinsFragment.getConfig();     		
    		
    		if(mi==null) continue;
    		if(mi.getItemId()==R.id.menu_set_auto_heartrate_detection){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_HRD_ALWAYS_ON_MASK)==0){
    				//-- mi.setIcon(R.drawable.white_btn);
    				mi.setTitle(getResources().getString(R.string.menu_set_auto_hrd_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_reset_auto_hrd_title));
    				//-- mi.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
                }                        

    		}
    		else if(mi.getItemId()==R.id.menu_bins_heartrate_enable){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_HEARTRATE_ENABLE_MASK)==0){
    				//-- mi.setIcon(R.drawable.white_btn);
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_disable_title));
    				//-- mi.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
                }                        

    		}   
    		
    		else if(mi.getItemId()==R.id.menu_bins_heartrate_sleep_enable){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_HEARTRATE2SLEEP_MASK)==0){
    				//-- mi.setIcon(R.drawable.white_btn);
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_sleep_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_sleep_disable_title));
    				//-- mi.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
                }                        

    		}
    		
    		else if(mi.getItemId()==R.id.menu_bins_heartrate_step_enable){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_STEP_WHILE_HRD_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_step_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_heartrate_step_disable_title));
                }                        

     		} else if(mi.getItemId()==R.id.menu_bins_step_counter_reset){  // 01.09
     			boolean enabled=BinsDBUtils.activeDB().getActiveUserStepWatchEnabled();
     			
    			if(enabled==false){
    				mi.setTitle(getResources().getString(R.string.menu_bins_step_counter_reset_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_step_counter_wholeday_title));
                }                        
   			/* 01.13
    		} else if(mi.getItemId()==R.id.menu_bins_sleep_enable){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_sleep_track_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_sleep_track_disable_title));
                }                        
			*/
    		} else if(mi.getItemId()==R.id.menu_bins_phone_notice){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_phone_notice_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_phone_notice_disable_title));
                }                        
    		} else if(mi.getItemId()==R.id.menu_bins_background_sync){
    			if(BinsDBUtils.activeDB().getBackgroundSync()==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_background_sync_enable_tite));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_background_sync_disable_tite));
                }                        

    		} else if(mi.getItemId()==R.id.menu_bins_display_brightness){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_display_bright_high_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_display_bright_normal_title));
                }                        
       		} else if(mi.getItemId()==R.id.menu_bins_lock_bins){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_lock_bins_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_unlock_bins_title));
                }                        

    		} else if(mi.getItemId()==R.id.menu_bins_oled_blink){
    			if((BinsFragment.gConfiguration & BinsDBUtils.BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK)==0){
    				mi.setTitle(getResources().getString(R.string.menu_bins_oled_not_blink_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_bins_oled_do_blink_title));
                }                        
       		} else if(mi.getItemId()==R.id.menu_filter_data_not_worn){
    			if(BinsDBUtils.activeDB().getFilterUnused()==0){
    				mi.setTitle(getResources().getString(R.string.menu_filter_unused_enable_title));
    			}else{
    				mi.setTitle(getResources().getString(R.string.menu_filter_unused_disable_title));
                }                        
    		} else if(mi.getItemId()==R.id.menu_app_share_screen){
    
    			Log.i(TAG, "menu app share screen");
    			mi.setTitle(getResources().getString(R.string.menu_share_screen));
    		}
    		
    		
/*    			
 */
/*    				
    		String title=mi.getTitle().toString();
    		Log.i(TAG,"onPrepareOptions Menu bins activity tt:"+title);

    		if(mi.getTitle().toString().equals(getResources().getString(R.string.menu_set_auto_hrd_title))){
    			Log.i(TAG, "onPrepareOptions found");
    			
    		}
*/
    	}
    		
    	return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	//-- Log.i(TAG, "OptionsItemSelected Menu");
    	
      	if(item.getItemId()==0){
  
        	String strName=item.getTitle().toString();
        	if(strName.equalsIgnoreCase(getResources().getString(R.string.menu_add_new_user))==false)
        	{
        		changeUser(item.getTitle().toString());   	           	
      		}else{            
        	    //-- 08.04
        		mBinsFragment.stopDataSync();        		
            	UserProfileActivity.actionUserProfile(this, true); // a new user
        	}

            return super.onOptionsItemSelected(item);
    	}
    	
          
        switch(item.getItemId()) {
    
            case R.id.menu_share:
            	
                SubMenu subMenu=item.getSubMenu();
                subMenu.clear();               
                String strCurrentUserName=BinsDBUtils.getBluetoothLeDao(this).getActiveUserName(); 

            	Cursor cursor=BinsDBUtils.getBluetoothLeDao(this).getUsers(); 
                if (cursor != null) {
                    //-- String defaultName=getResources().getString(R.string.your_name);
                    
                    Log.d(TAG, "Column Count: " + cursor.getColumnCount()+":"+cursor.getCount());
                    cursor.moveToFirst();
                    //-- cursor.moveToNext();
                    while (!cursor.isAfterLast()) {
                        String strUserName = cursor.getString(2);	//-- 150821 dependent to DB field order
                        if(strUserName==null){
                        	Log.i(TAG,"error from user names: null");
                        }else{
                        	Log.i(TAG,"user name:"+strUserName+":"+strCurrentUserName);
                        }
                        if(strUserName==null){
                        	cursor.moveToNext();
                        	continue;
                        }
                        
                        MenuItem menuItem=subMenu.add(strUserName);
                        if(strUserName.equalsIgnoreCase(strCurrentUserName)){
                        	menuItem.setIcon(R.drawable.white_btn);
                        }                        
                        cursor.moveToNext();
                    }
                    cursor.close();
                    
                } 
                subMenu.add(getResources().getString(R.string.menu_add_new_user)); // New user                                 
                break;    

            case R.id.menu_start_heartrate_detection:
            	mBinsFragment.startHeartrateDetection();
            	break;
            	
            case R.id.menu_set_auto_heartrate_detection:

                mBinsFragment.setAutoHeartrateDetection();
            	
            	break;
            	
            case R.id.menu_bins_heartrate_enable:
            	mBinsFragment.setHeartRateEnable();
            	break;
            	
            	  
            case R.id.menu_bins_heartrate_sleep_enable:
            	mBinsFragment.setHeartRateSleepEnable();  
            	break;
           	
          case R.id.menu_bins_heartrate_step_enable:
            	mBinsFragment.setStepCheckWhenHRDEnable();
            	break;
            	
          case R.id.menu_bins_step_counter_reset:
          		mBinsFragment.setBinsDailyStepReset();   //-- 08.19 actually it is to reset counter
   
          		break;
          case R.id.menu_bins_lock_bins:
        		mBinsFragment.setBinsLock();   //-- 08.19 actually it is to reset counter
        		break;
          case R.id.menu_bins_oled_blink:
        	    mBinsFragment.setBinsX2SensorBlink();
        	    break;
        	    
        /*	    
          case R.id.menu_bins_sleep_enable:
    	    	mBinsFragment.setSleepEnable();
    			if(mCurrentTabId.equals(LABEL_SLEEP)){	// 01.13
    	        	mSleepFragment.refreshViews();
    			}
    	    	break;
		*/
          case R.id.menu_bins_phone_notice:
      	    	mBinsFragment.setPhoneNotice();
      	    	break;
          case R.id.menu_bins_background_sync:
    	    	mBinsFragment.toggleBackgroundSync();
    	    	break;
          case R.id.menu_bins_display_brightness:
    	    	mBinsFragment.setBinsX2DisplayBrightness();
    	    	break;
          case R.id.menu_filter_data_not_worn:
            	BinsDBUtils.activeDB().toggleFilterUnused(); 
            	if(mCurrentTabId.equals(LABEL_SLEEP))	//-- 20151022
            		mSleepFragment.redrawViews();    
            	break;

          case R.id.menu_app_share_screen:
    	    	Log.i(TAG, "onOptionsItemSelected menu app share screen");
    	    	
    	    	View screen;
    	    	Fragment nowFragment=null;
    	        if(mCurrentTabId.equals(LABEL_BiNS)){
    	        	nowFragment=mBinsFragment;
    	        } else if(mCurrentTabId.equals(LABEL_DEVICE)){
    	        	nowFragment=mDeviceInfoFragment;
    	        } else if(mCurrentTabId.equals(LABEL_ACTIVITY)){
    	        	nowFragment=mActivityFragment;
    	        } else if(mCurrentTabId.equals(LABEL_SLEEP)){
    	        	nowFragment=mSleepFragment;
    	        }
    	        screen=(View)nowFragment.getView();
                screen.setDrawingCacheEnabled(true);
    	        screen.buildDrawingCache();    	    	    	
                Bitmap bitmap = Bitmap.createBitmap(screen.getDrawingCache());
                screen.setDrawingCacheEnabled(false);
    	        
    	        if (bitmap != null) {
    	        			String filePath = Environment.getExternalStorageDirectory()
	                                .toString();
    	        			String fileName="/binsScreenCaptured.png";
	                        File file = new File(filePath, fileName);
    	        			
    	                    try {
    	                        Log.i(TAG, "share screen to folder:"+filePath);
    	                        
    	                        OutputStream out = null;
    	                        out = new FileOutputStream(file);
    	                        bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);
    	                        out.flush();
    	                        out.close();
    	                        bitmap.recycle();
    	                    } catch (Exception e) {
    	                        e.printStackTrace();
    	                    }
    	                    	
    	                    Uri uriScreen=Uri.fromFile(file);    	                    
    	                    Intent shareIntent = new Intent();
    	                    shareIntent.setAction(Intent.ACTION_SEND);
    	                    shareIntent.putExtra(Intent.EXTRA_STREAM, uriScreen);
    	                    shareIntent.setType("image/png");
    	                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.title_screen_shareto)));

    	        }
    	        

    	    	break;
    	    	
    	    	
/*  
 *           	
            case R.id.menu_reset_auto_heartrate_detection:
            	mBinsFragment.resetAutoHeartrateDetection();
            	break;            	

            case R.id.menu_bins_system_reset:
            	mBinsFragment.binsSystemReset();
            	break;
            case Menu.FIRST:
            	showMsg("New");
            	break;
*/            	
         }
        return super.onOptionsItemSelected(item);
    }
    
    /*
     * 	uint24 lap   00..23     uuid[0]= bd addr [00..16] bd[0:1]
     *  						uuid[1]= bd addr [17..23] bd[2]
     *  uint8  uap   24..31		uuid[2]= bd addr [24..31] bd[3]     
     *  uint16 nap   32..47		uuid[3]= bd addr [32..47]
     *  
     *  device id = (uuid[1]<<16+uuid[0])%1000000
     *  
     *  uuid[0]= bd[1]<<8+bd[0];
     *  uuid[1]= bd[3]<<8+bd[2];
     *  
     *  id = (bd[3]<<24+bd[2]<<16+bd[1]<<8+bd[0])%1000000;
     *  
     */
    public int getDeviceId(String strBluetoothAddress){
    	int deviceId=0;
    	String[] strSubAddress=strBluetoothAddress.split(":");
    	byte[] bd= new byte[6];
    	for(int i = 0; i < strSubAddress.length; i++) {
    		bd[i] = Integer.decode("0x" + strSubAddress[5-i]).byteValue();
    	    Log.i(TAG, "bd address="+Integer.toHexString(bd[i]));    
    	}
    	
    	
    	/*deviceId= bd[3];
    	deviceId<<=8;    	
    	deviceId+=0;
    	deviceId<<=8;
    	*/
    	deviceId+=(bd[2] & 0xFF);
    	deviceId<<=8;
    	deviceId+=(bd[1] & 0xFF);
    	deviceId<<=8;
    	deviceId+=(bd[0] & 0xFF);
    	
    	deviceId%=100000;
    	
	    //-- Log.i(TAG, "bd address device ID="+deviceId);    
    	
    	return deviceId;    	
    }
   
}

