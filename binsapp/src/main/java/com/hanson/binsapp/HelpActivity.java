
package com.hanson.binsapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;

import android.webkit.WebSettings;
import android.webkit.WebView;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import com.hanson.binsapp.R;

public class HelpActivity extends Activity {
	
	public static void actionHelp(Activity fromActivity) {
	        Intent intent = new Intent(fromActivity, HelpActivity.class);
	        fromActivity.startActivity(intent);
	}
	
    public static final String TAG = "HansonActivity";

    private WebView mWebView;

    private String htmlString="<html>testtest</html>";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       //--* setContentView(R.layout.app_help);
       
       View view = LayoutInflater.from(this).inflate(R.layout.app_help, null);
       getActionBar().setCustomView(view);
       
       getActionBar().setDisplayHomeAsUpEnabled(true);

       mWebView = (WebView) view.findViewById(R.id.app_help_html);
       
       WebSettings webSettings=mWebView.getSettings();
       webSettings.setAllowFileAccess(true);
       
       htmlString = getHtmlFromAsset();
       
       runOnUiThread(new Runnable() {
    	    @Override
    	    public void run() {
    	         
    	        // webView.loadUrl(localFile.toURI().toURL().toString());
    	        
    	       //mWebView.loadUrl("file:///android_asset/www/"+
    	       // 			getResources().getString(R.string.help_html));
    	       
    	       mWebView.loadDataWithBaseURL( "file:///android_asset/", htmlString, "text/html", 
    	    		   "utf-8", null ); 

    	        // Code for WebView goes here
    	    }
       });
         
        // webView.loadUrl(localFile.toURI().toURL().toString());
        
        //mWebView.loadUrl("file:///android_asset/"+
        //			getResources().getString(R.string.help_html));
       
       // mWebView.loadDataWithBaseURL( "file:///android_asset/", "html", "text/html", 
     //		   "utf-8", null ); 
       
        Log.d(TAG, "show help");
         
        setContentView(view);
       
    }

    /**
     * Gets html content from the assets folder.
     */
    private String getHtmlFromAsset() {
        InputStream is;
        StringBuilder builder = new StringBuilder();
        String htmlString = null;
        try {
            is = getAssets().open(getString(R.string.help_html));
            if (is != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                htmlString = builder.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return htmlString;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            //-- NavUtils.navigateUpFromSameTask(this);
        	finish();
        	
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
