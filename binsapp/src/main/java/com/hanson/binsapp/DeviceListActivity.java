
package com.hanson.binsapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

import com.hanson.binsapp.R;
import com.hanson.binsapp.provider.BinsDBUtils;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity for scanning and displaying available Bluetooth LE devices.
 */
public class DeviceListActivity extends ListActivity {

	private static int mUserID;	//-- 08.11
	
	private static BinsActivity mainActivity;
	private static boolean mIsAutoPair=false;
	private static Timer autoPairTimer=null;
	
	
    public static void actionDeviceList(Activity fromActivity, int user) {
        Intent intent = new Intent(fromActivity, DeviceListActivity.class);
        mUserID=user;
        mainActivity=(BinsActivity)fromActivity;
        mIsAutoPair=false;
        fromActivity.startActivity(intent);
    }

    public static void autoPair(Activity fromActivity, int user) {
        Intent intent = new Intent(fromActivity, DeviceListActivity.class);
        mUserID=user;
        mainActivity=(BinsActivity)fromActivity;
        mIsAutoPair=true;
        fromActivity.startActivity(intent);
    }
    
    protected static final String TAG = "DeviceListActivity";
    private LeDeviceListAdapter mLeDeviceListAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    
    private boolean mScanning;
    private Handler mHandler;

    private static final int REQUEST_ENABLE_BT = 1;

    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 30000; //20


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();

        // Initializes a Bluetooth adapter. For API level 18 and above, get a
        // reference to BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
        
        
        getActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                mLeDeviceListAdapter.clear();
                mLeDeviceListAdapter.notifyDataSetChanged();
                scanLeDevice(true);
                break;
            case R.id.menu_stop:
                scanLeDevice(false);
                if(autoPairTimer!=null){
                	autoPairTimer.cancel();
                	autoPairTimer=null;
                }
                mIsAutoPair=false; // once stopped, do not run auto pair again.
                break;
                
            case android.R.id.home:
                //-- NavUtils.navigateUpFromSameTask(this);
            	finish();
            	break;
        }
        return true;
    }
 
    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } else {
            scanLeDevice(true);
        }

        // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
        mLeDeviceListAdapter.clear();
        mLeDeviceListAdapter.notifyDataSetChanged();
    }

    
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final BluetoothDevice device = mLeDeviceListAdapter.getDevice(position);
        if (device == null) {
            return;
        }

        
        // TODO
/*
        final Intent intent = new Intent(this, DeviceSetupActivity.class);
        //final Intent intent = new Intent(this, DeviceControlActivity.class);
        intent.putExtra(BaseActivity.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(BaseActivity.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        if (mScanning) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mScanning = false;
        }
        startActivity(intent);
*/
        //-- to simplify the process
        //-- constructInfoValues();
        //-- String macAddress=device.getAddress();
        
        /* should not use this, 11.18 */
        /*
        mInfoValues = new ContentValues();
        mInfoValues.put(Device.COLUMN_DEVICE_NAME, "BINS");        
        mInfoValues.put(Device.COLUMN_MAC_ADDRESS, macAddress);
        getContentResolver().insert(Device.CONTENT_URI, mInfoValues);
        */
        
        BinsDBUtils.getBluetoothLeDao(this).setUserDeviceAddress(mUserID, device.getAddress()); // 08.11
        
        if(autoPairTimer!=null){
        	autoPairTimer.cancel();
        	autoPairTimer=null;
        }
        
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                scanLeDevice(true);
            }
        }
    }

    private void scanLeDevice(final boolean enable) {
    	
    	
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    if (mBluetoothAdapter != null) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);                   	
                        invalidateOptionsMenu();
                    }
                }
            }, SCAN_PERIOD);

            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
            if(mIsAutoPair==true){
            	if(autoPairTimer==null){
            		// autoPairTimer.cancel();
            		mLastDeviceFoundCount=0;
            		autoPairTimer=new Timer();		
            				//-- 20151210
            		autoPairTimer.schedule(autoPairTimerTask, 5000, 5000);	
            							//-- period time after this to finish pairing search
            							//-- delay, delay before first execution
            							//-- period, between subsequent execution 
            	}
            }
        } else {
            mScanning = false;
          	runOnUiThread(new Runnable() {
                @Override
                public void run() {  
                	if(mBluetoothAdapter!=null){
                		mBluetoothAdapter.stopLeScan(mLeScanCallback);
                	}
                }
           	});
            
        }
        invalidateOptionsMenu();
    }
    int mLastDeviceFoundCount=0;
    TimerTask autoPairTimerTask = new TimerTask()
    {
    	@Override
		public void run()
		{   
    		if(autoPairTimer==null) return;
    		
    		Log.i(TAG,"autoPairTimerTask:count="+mLastDeviceFoundCount);
    		if(mLeDeviceListAdapter.getCount()==mLastDeviceFoundCount){
    			autoPairTimer.cancel();
    			autoPairTimer=null;
    			
    			Log.i(TAG,"Unlocking: autoPairTimerTask:count="+mLastDeviceFoundCount);
    			
    		    final BluetoothDevice device = mLeDeviceListAdapter.getStrongestDevice(); 
    		    if(device!=null){
    		    	BinsDBUtils.getBluetoothLeDao(mainActivity).setUserDeviceAddress(mUserID, device.getAddress()); 
    		    }else{
    		    	Log.i(TAG, "Unlocking cannot find device");
    		    }
    		    
    		    finish();
    		}
    		if(mLeDeviceListAdapter!=null){
    			mLastDeviceFoundCount=mLeDeviceListAdapter.getCount();
    		}
		}
    	
    };
    
    @Override
    protected void onDestroy() {
        if (mScanning) {
        	mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {                    
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    mBluetoothAdapter.cancelDiscovery();
                }
           	});
        }

        mBluetoothAdapter = null;
        mLeDeviceListAdapter.clear();
        mLeDeviceListAdapter = null;
        
        if(autoPairTimer!=null){
        	autoPairTimer.cancel();
        	autoPairTimer=null;
        }
        
        
        super.onDestroy();
    }

    class LeDeviceInfo {
    	BluetoothDevice bleDevice;
    	int	rssi;
    };
    
    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        //-- private ArrayList<BluetoothDevice> mLeDevices;
        private ArrayList<LeDeviceInfo> mLeDevices;
        private LayoutInflater mInflator;
 
        public LeDeviceListAdapter() {
            super();
            //-- mLeDevices = new ArrayList<BluetoothDevice>();
            mLeDevices = new ArrayList<LeDeviceInfo>();
            mInflator = DeviceListActivity.this.getLayoutInflater();
        } 
        
        public void addDevice(BluetoothDevice device, final int rssi) {
        	int i;
        	BluetoothDevice dv;
        	Boolean isDeviceRssiChanged=false;
        	
        	for(i=0; i<mLeDevices.size(); i++)
        	{
        		dv=mLeDevices.get(i).bleDevice;
        		
        		if(dv.getAddress().equals(device.getAddress()) )
        		{
        			LeDeviceInfo deviceInfo=(LeDeviceInfo)mLeDevices.get(i);
        			if(deviceInfo.rssi!=rssi){
        				deviceInfo.rssi=rssi; 
        				isDeviceRssiChanged=true;
        			}
        			
        			break;
        		}
        	}
            if (i==mLeDevices.size()) {
            	LeDeviceInfo deviceInfo= new LeDeviceInfo();
            	deviceInfo.bleDevice=device;
            	deviceInfo.rssi=rssi;
                mLeDevices.add(deviceInfo);  
                isDeviceRssiChanged=true;
            }
            if(isDeviceRssiChanged==true){ //-- 20151023
                Collections.sort(mLeDevices,new Comparator<LeDeviceInfo>() {
                    public int compare(LeDeviceInfo dev1, LeDeviceInfo dev2) {
                        return dev2.rssi-dev1.rssi;
                    }
                });
          
            }
            
        }

        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position).bleDevice;
        }

        public void clear() {
            mLeDevices.clear();
        }

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        public BluetoothDevice getStrongestDevice() {
        	int i;
        	int maxRssi=-200;
        	LeDeviceInfo device=null;
        	LeDeviceInfo deviceWithMaxRssi=null;
        	for(i=0; i<getCount(); i++){
        		device=mLeDevices.get(i);
        		if(device.rssi>maxRssi){
        			maxRssi=device.rssi;
        			deviceWithMaxRssi=device;
        		}
        	}
        	if(deviceWithMaxRssi==null) return null;
        	
        	return deviceWithMaxRssi.bleDevice;
        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceRSSI = (TextView) view.findViewById(R.id.device_RSSI); //--**
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            LeDeviceInfo device = mLeDevices.get(i);
            String deviceName = device.bleDevice.getName();
            if (deviceName != null && deviceName.length() > 0) {
            	//-- Customize if need, deviceName="Unlocking Life Band";	//-- 20151001
                viewHolder.deviceName.setText(deviceName);
            } else {
                viewHolder.deviceName.setText(R.string.unknown_device);
            }
            //-- viewHolder.deviceAddress.setText("[ " + device.bleDevice.getAddress().substring(9) + " ]");
            int deviceId=mainActivity.getDeviceId(device.bleDevice.getAddress());
            viewHolder.deviceAddress.setText("[ " + deviceId + " ]");
            viewHolder.deviceRSSI.setText("( "+String.valueOf(device.rssi)+" )");  //-- device.rssi);

            int activeDeviceId=BinsDBUtils.activeDB().getActiveSystemId(); // 08.11
            
            Log.i(TAG, "current device id="+activeDeviceId);
            if(deviceId==activeDeviceId){
            	viewHolder.deviceAddress.setTextColor(Color.RED);
            }else{
            	if(i==0){
                	viewHolder.deviceAddress.setTextColor(Color.BLUE);           	            		
            	}else{
            		viewHolder.deviceAddress.setTextColor(Color.BLACK); 
            	}
            }
            
            return view;
        }
    }
    
    
    
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {

                	
                	 mainActivity.runOnUiThread(new Runnable() 
                	{
                        @Override
                        public void run() {
                        	                        	
                            Log.d(TAG, "onLeScan... Name = " + device.getName());
                            Log.d(TAG, "onLeScan... Address = " + device.getAddress());
                            Log.d(TAG, "onLeScan... Type = " + device.getType());
                            
                            if(device.getName()!=null && device.getName().equals("BINS"))
                            {
                            	mLeDeviceListAdapter.addDevice(device, rssi);
                            	mLeDeviceListAdapter.notifyDataSetChanged();
                            }
                            // not work device.connectGatt(null, false, mGattCallback);

                        }
                    });
                    
                    // mBluetoothGatt=;
                    // BluetoothGatt connectGatt = 
  
                }  // end of onLeScan
            };

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
        TextView deviceRSSI;
    };
    
 

}
