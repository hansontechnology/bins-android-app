
package com.hanson.binsapp.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.TimePicker;

import com.hanson.binsapp.BinsActivity;
import com.hanson.binsapp.DeviceListActivity;
import com.hanson.binsapp.HelpActivity;
import com.hanson.binsapp.R;
import com.hanson.binsapp.UserProfileActivity;
import com.hanson.binsapp.provider.BinsDBUtils;

public class DeviceInfoFragment extends  BaseFragment {
	
    private TimePickerDialog timePickerDialog;

    private static final String TAG = "DeviceInfoFragment";

    public static final String DATE_FORMAT = "HH:mm, yyyy-MM-dd";

    private RelativeLayout mAlarmLayout=null;
    private RelativeLayout mDeviceLayout=null;
    private TextView mDeviceNameView;
    private ImageView mDeviceSyncIcon;
    private ImageView mDeviceBattery;
    private TextView mBatteryLevelText;
    private EditText mDailyGoalEdit;
    //-- 08.11 private ToggleButton mFinderToggle;
    //-- private SwitchButton mFinderSwitch;
    private SeekBar mFinderSeekBar;
    private SeekBar mShakeUiSeekBar;
    private TextView mVersionNumberText;
    private EditText mAlarmEdit;
    private Switch mAlarmSwitch;


    private String mDeviceAddress = null;
    
    private int mAlarmHour, mAlarmMinute;
    
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach ...");
        
        //-- 12.07
        /*
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_DATA_SYNC_START);
        filter.addAction(ACTION_DATA_SYNC_END);
        filter.addAction(ACTION_FINDER_LOST);         
        filter.addAction(ACTION_FINDER_STOP);  
        getActivity().registerReceiver(mSyncReceiver, filter);
        */
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ...");
        //-- 150321 changed to fragment_more, disable finder
        
        {
        	int hour=0;
        	int minute=0;

        	timePickerDialog = new TimePickerDialog(getActivity(), timePickerListener, hour, minute, false);
        }
        
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener 
    = new TimePickerDialog.OnTimeSetListener() {
        
    public void onTimeSet(TimePicker view, int hour, int minute) {            
            mAlarmHour=hour;
            mAlarmMinute=minute;
            
            mAlarmEdit.setText(String.format("%02d:%02d",hour,minute));
            
            mAlarmSwitch.setChecked(false);
            
            int alarmValue=((hour<<8)+minute);
            
            //-- Log.i(TAG, "alarm set to :"+hour+":"+minute+":"+alarmValue+":"+(hour<<8));

            BinsDBUtils.activeDB().setActiveUserAlarm(alarmValue, 
    													mAlarmSwitch.isChecked());

            //-- newDate.set(year, monthOfYear, dayOfMonth);
            //-- mTimeControllerView.setDate(newDate);
        }

    };
    
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
    	 @Override       
    	    public void onStopTrackingTouch(SeekBar seekBar) {      
    	        // TODO Auto-generated method stub      
    	    }       

    	    @Override       
    	    public void onStartTrackingTouch(SeekBar seekBar) {     
    	        // TODO Auto-generated method stub      
    	    }       

    	    @Override       
    	    public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {     
    	        // TODO Auto-generated method stub      

    	        if(seekBar==mFinderSeekBar){
       	        	BinsDBUtils.activeDB().setActiveFinderThreshold(progress);
       	        	//-- Log.i(TAG,"finder:"+progress);
       	            	        	
    	        }
    	        else if(seekBar==mShakeUiSeekBar){
    	        	
    	        	int threshNow=BinsDBUtils.activeDB().getActiveShakeUiThreshold();
    	        	
    	        	if(threshNow!=progress){
    	        		BinsDBUtils.activeDB().setActiveShakeUiThreshold(progress);
    	        		//-- Log.i(TAG,"shake:"+progress);    	        
    	        		activeBinsFragment.writeShakeUiThreshold();
    	        	}
    	        }
    	    }       
    };
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated ...");
        mAlarmLayout = (RelativeLayout) view.findViewById(R.id.shake_ui_layout);
        mDeviceLayout = (RelativeLayout) view.findViewById(R.id.device_container);
        mDeviceNameView = (TextView) view.findViewById(R.id.device_name);
        mDeviceSyncIcon = (ImageView) view.findViewById(R.id.device_sync_icon);
        mDeviceBattery = (ImageView) view.findViewById(R.id.device_battery);
        mBatteryLevelText = (TextView) view.findViewById(R.id.battery_level_text);
        //-- 07.21 mDeviceBattery.setVisibility(View.GONE);
        //-- mFinderSwitch= (SwitchButton) view.findViewById(R.id.switchFinder);
        //-- mFinderSeekBar= (SeekBar) view.findViewById(R.id.seekBarFinder);
        //-- mFinderSeekBar.setMax(BinsDBUtils.BINS_FINDER_LEVEL_MAX);
        //-- mFinderSeekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);

        mShakeUiSeekBar= (SeekBar) view.findViewById(R.id.seekBarShakeUi);

        mShakeUiSeekBar.setMax(BinsDBUtils.BINS_SHAKE_LEVEL_MAX);

        mShakeUiSeekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
        
        mAlarmLayout.setVisibility(View.GONE);	//-- 150713
        mAlarmEdit= (EditText) view.findViewById(R.id.editAlarm);
        mAlarmSwitch= (Switch)view.findViewById(R.id.switchAlarm);
        mDailyGoalEdit=(EditText) view.findViewById(R.id.edit_goal);
        
        // 12.09 App software version
        mVersionNumberText=(TextView)view.findViewById(R.id.version_number);
        PackageManager pManager = getActivity().getPackageManager();
        String pName=getActivity().getPackageName();
        String versionString="";
		try {
			versionString = pManager.getPackageInfo(pName, 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}      
        mVersionNumberText.setText("v "+versionString);
        
        
 /*       
        mFinderSeekBar.setOnSeekBarChangeListener( new OnSeekBarChangeListener() {       

             @Override       
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {     
                // TODO Auto-generated method stub      

                //t1.setTextSize(progress);
                //Toast.makeText(getApplicationContext(), String.valueOf(progress),Toast.LENGTH_LONG).show();

            }       
        });    
        
        mShakeUiSeekBar.setOnSeekBarChangeListener(this);
 */
        
        //-- 08.11 mFinderToggle= (ToggleButton) view.findViewById(R.id.finder_toggleButton);
        //-- mFinderToggle.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFFAA0000));
        
           
          

        //-- 08.11 mFinderToggle.setTextColor(csl);
        
        
        //-- view.findViewById(R.id.setup_device_info).setOnClickListener(onClickListener);
        view.findViewById(R.id.setup_device_new).setOnClickListener(onClickListener);
        view.findViewById(R.id.setup_device_new).setOnLongClickListener(onLongClickListener);
        
        view.findViewById(R.id.more_help).setOnClickListener(onClickListener); //-- 
        //-- view.findViewById(R.id.more_treadmill).setOnClickListener(onClickListener); //-- 
        //-- view.findViewById(R.id.more_finder).setOnClickListener(onClickListener); //-- 
        //-- 08.11 view.findViewById(R.id.finder_toggleButton).setOnClickListener(onClickListener); //-- 
        view.findViewById(R.id.user_profile).setOnClickListener(onClickListener); //-- 
        
        mAlarmEdit.setInputType(InputType.TYPE_NULL);        
        mAlarmEdit.setOnClickListener(onAlarmSetClickListener);
        
        int alarm=BinsDBUtils.activeDB().getActiveUserAlarm();
        
        
        Log.i(TAG, "alarm time retrived"+Integer.toHexString(alarm));
        
        int hour=(alarm&0x7F00)>>8;
    	int minute=(alarm&0xFF); 
    	if(minute>59) minute=59;
    	
    	mAlarmHour=hour;
    	mAlarmMinute=minute;
    	
    	mAlarmEdit.setText(String.format("%02d:%02d",hour,minute)); //-- 20151022
        
        if((alarm&0x8000)!=0){
        	mAlarmSwitch.setChecked(true);
        } else {
        	mAlarmSwitch.setChecked(false);
        }
 
/*
        mDailyGoalEdit.addTextChangedListener(new TextWatcher() {
        	
        
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            	Log.d(TAG,"after edit goal: " +s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            	Log.d(TAG,"before edit goal: " +s.toString());

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            	Log.d(TAG,"edit goal: " +s.toString());
            	
            }
        });
        
        
        mDailyGoalEdit.setOnFocusChangeListener(new OnFocusChangeListener() {
        	@Override
        	public void onFocusChange(View v, boolean hasFocus) {
        	    if(hasFocus){
        	        Log.d(TAG, "focus");
        	        
        	    }else {
        	        Log.d(TAG, " Not focus");
        	        
        	    }
        	   }
        	});
        // textMessage.getText().toString().length()
*/
        
        mDailyGoalEdit.setOnEditorActionListener(new OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            	
            	Log.i(TAG,"EditGoal");

            	//-- triggered but not this, if (actionId == EditorInfo.IME_ACTION_GO)

            	int newGoal=Integer.parseInt(v.getText().toString());
            	if(newGoal>99999){
                    int lastGoal=BinsDBUtils.activeDB().getActiveUserDailyGoal();
                    String goalString= String.valueOf(lastGoal);
                    v.setText(goalString);                    
            	}else{
            		gDailyGoal=Integer.parseInt(v.getText().toString()); 
            		BinsDBUtils.activeDB().setActiveDailyGoal(gDailyGoal);  
            		activeBinsFragment.updateDailyGoal();            		
                 }
                return false;
            }
        });
        
        String goalString= String.valueOf(gDailyGoal);
        mDailyGoalEdit.setText(goalString);
        
        mAlarmSwitch.setOnCheckedChangeListener(mOnAlarmSwitchCheckedChangeListener);
        //-- mFinderSwitch.setOnCheckedChangeListener(mOnCheckedChangeListener);
        
        //-- if(gFinderSetting==0) mFinderSwitch.setChecked(true);
        //-- else mFinderSwitch.setChecked(false);
        
    }

    private OnCheckedChangeListener mOnAlarmSwitchCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton cButton, boolean status) { 
            Log.d(TAG, "alarm switch checked: "+status);              		
            BinsDBUtils.activeDB().setActiveUserAlarmEnable(status);            
            activeBinsFragment.updateAlarm(); //-- TODO            
        }
    };
    
    /* 150321
    private OnCheckedChangeListener mOnCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton cButton, boolean status) { 
        	boolean isRunning;
        	isRunning=mFinderSwitch.isChecked();        	
        	if(isRunning && gFinderSetting==1) {
               	Log.d(TAG, "findme checked:"+isRunning);              		
         		gFinderSetting=0;        		
                BinsDBUtils.activeDB().setActiveFinderSetting(gFinderSetting);
        		activeBinsFragment.stopFinder();
        		
        	} else if(isRunning==false && gFinderSetting==0){
        		gFinderSetting=1;
                BinsDBUtils.activeDB().setActiveFinderSetting(gFinderSetting);
                activeBinsFragment.startFinder(getActivity());
        	}
        }
    };
    */
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated ...");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d(TAG, "onViewStateRestored ...");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart ...");
        //-- 12.07
    }

    public void updateViews(BinsFragment mBinsF)
    {
    	
    		activeBinsFragment=mBinsF;
    		
    		checkUserChanged();
    		checkDeviceChanged();
    		
			Cursor cursor = queryData();                       	   				
			if(cursor!=null) {
				handleData(cursor);
				update();
			}                     	                                	   				
			refreshViews();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ...");
        refreshViews();
        
/*        
        if(gFinderSetting!=0){
        	Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.refresh);
           	mFinderRunningIcon.startAnimation(animation);
        }
        else{
    		mFinderRunningIcon.setAnimation(null);
        }
*/
        
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.d(TAG, "setMenuVisibility ...");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ...");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ...");
        //-- 12.07 getActivity().unregisterReceiver(mSyncReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView ...");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState ...");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy ...");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach ...");
    }

    private void refreshViews() {
        Log.d(TAG, "refreshViews ...");
        
        if(mDeviceLayout==null) return;
        
        BinsActivity binsActivity=(BinsActivity)(getActivity());
        
        mDeviceAddress = BinsDBUtils.activeDB().getActiveDeviceAddress();
        
        int currentUser=BinsDBUtils.activeDB().getActiveUserID();
        
        if(currentUser==0)
        {
            //-- mFinderSwitch.setEnabled(false);
            mDailyGoalEdit.setEnabled(false);  
            mAlarmEdit.setEnabled(false);
            mAlarmSwitch.setEnabled(false);
            
        }
        else{
            //-- mFinderSwitch.setEnabled(true);
            mDailyGoalEdit.setEnabled(true);  
            mAlarmEdit.setEnabled(true);
            mAlarmSwitch.setEnabled(true);
        }
        
        if (mDeviceAddress == null) {
            //-- mFinderSeekBar.setEnabled(false);
            mShakeUiSeekBar.setEnabled(false);
            mDeviceLayout.setVisibility(View.GONE);

        }else{
            //-- mFinderSeekBar.setEnabled(true);
            mShakeUiSeekBar.setEnabled(true);
            mDeviceLayout.setVisibility(View.VISIBLE);
            
            // Set device name
            Log.i(TAG, "mDeviceAddress:"+mDeviceAddress);
            
            mShakeUiSeekBar.setProgress(BinsDBUtils.activeDB().getActiveShakeUiThreshold());
            
            int systemId=BinsDBUtils.activeDB().getActiveSystemId();
            if(systemId==-1){
                //-- strDeviceID =  mDeviceAddress.substring(9); //-- enforce to use this name
            	systemId=binsActivity.getDeviceId(mDeviceAddress);
            }   
            String strDeviceID=String.valueOf(systemId);           
            
            String deviceName = getResources().getString(R.string.paired_device) + " [ " + strDeviceID + " ]"; //-- enforce to use this name

            mDeviceNameView.setText(deviceName);
            Log.d(TAG, "refreshViews ... mDeviceName = " + deviceName);

            // Set battery

            int batteryLevel = BinsDBUtils.activeDB().getActiveBatteryLevel();
      
            if(activeBinsFragment.mBinsType==0){	//-- if bins 1 type
            	mDeviceBattery.setVisibility(View.GONE);
            	mBatteryLevelText.setVisibility(View.GONE);
            	
            }
            else{
            	mDeviceBattery.setVisibility(View.VISIBLE);
            	mDeviceBattery.getDrawable().setLevel(batteryLevel);
            	if(batteryLevel>0){
                	mBatteryLevelText.setVisibility(View.VISIBLE);
            		mBatteryLevelText.setText(String.valueOf(batteryLevel)+"%");
            	}
            	else{
                   	mBatteryLevelText.setVisibility(View.GONE);                              		
            	}
            	
            	Log.d(TAG, "refreshViews ... batteryLevel = " + batteryLevel);
            }
      

        }
      
         
        if(currentUser==0) return;
        
        gDailyGoal=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserDailyGoal();
        
        String goalString= String.valueOf(gDailyGoal);
        mDailyGoalEdit.setText(goalString);
  
        //------
        int alarm=BinsDBUtils.activeDB().getActiveUserAlarm();
        
        
        Log.i(TAG, "alarm time retrived 222+current User:"+currentUser+":"+Integer.toHexString(alarm));
        
        int hour=(alarm&0x7F00)>>8;
    	int minute=(alarm&0xFF); 
    	if(minute>59) minute=59;
    	
    	mAlarmHour=hour;
    	mAlarmMinute=minute;

    	mAlarmEdit.setText(String.format("%02d:%02d",hour,minute));
        
        if((alarm&0x8000)!=0){
        	mAlarmSwitch.setChecked(true);
        } else {
        	mAlarmSwitch.setChecked(false);
        }
        
        //--
        
        //-- gFinderSetting=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveFinderSetting();
        
        //-- Log.d(TAG, "daily goal from" + gDailyGoal);
                
    }

    /* 20151230
    private void syncViews(boolean sync) {
        Log.d(TAG, "syncViews ... sync = " + sync);
        if (sync) {
            mDeviceSyncTime.setText(R.string.sync_now);
            Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.refresh);
            mDeviceSyncIcon.startAnimation(animation);
        } else {
            long time = BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserLastSyncTime();
            mDeviceSyncTime.setText(DateFormat.format(DATE_FORMAT, time * 60 * 1000));
            mDeviceSyncIcon.setAnimation(null);
        }
    }
    */

    /* 20151230
    private BroadcastReceiver mSyncReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive ... action = " + action);
            if (action.equals(ACTION_DATA_SYNC_START)) {
                syncViews(true);
            } else if (action.equals(ACTION_DATA_SYNC_END)) {
                syncViews(false);
            
        	} else if (action != null && action.equals(ACTION_FINDER_LOST)){
        		final boolean status = intent.getBooleanExtra(EXTRA_STATUS, false);
            	Log.d(TAG, "onReceive DeviceInfo - Finder lost... status = " + status);
         	}        

        }
    };
    */

    private OnClickListener onAlarmSetClickListener = new OnClickListener() {

    	@Override
    	public void onClick(View view) {
    		int hour=mAlarmHour;
    		int minute=mAlarmMinute;
    		// Log.i(TAG, "clicked");
    		// Calendar cal=mTimeControllerView.getDate();
  			timePickerDialog.updateTime(hour, minute);
  			timePickerDialog.show();

    	}	
    };
    
    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.setup_device_new) {
            	//-- no condition, if(mIsSyncRunning==true) 
            	{
            		Log.d(TAG,"before device new to stop datasync ");
            		//-- DataSyncService.stopFinder(getActivity());
            		//--**
            		activeBinsFragment.stopDataSync();
            		mDeviceSyncIcon.setAnimation(null);
            	}
                BinsActivity binsActivity=(BinsActivity)(getActivity());
				binsActivity.stopDeviceBeaconRegion();	//-- 20151217
				
                DeviceListActivity.autoPair(getActivity(), mCurrentUserID); 
                
            } else if (view.getId() == R.id.user_profile) {
            	Log.d(TAG, "go user profile");
            	
                BinsActivity binsActivity=(BinsActivity)(getActivity());
				binsActivity.stopDeviceBeaconRegion();
				
            	UserProfileActivity.actionUserProfile(getActivity(), false); // not a new user
            	
            } else if (view.getId() == R.id.more_help) {
            	Log.d(TAG, "go help");
            	    
            	HelpActivity.actionHelp(getActivity());
            }
/*            	
            else if(view.getId() == R.id.more_treadmill) { 
            	Log.d(TAG, "start treadmill");

            	if (mDeviceAddress != null) {
                     	TreadmillActivity.startTreadmill(getActivity(), mDeviceAddress);            			
            	}
*/
/*             	
            } else if (view.getId() == R.id.finder_toggleButton) {
            	Log.d(TAG, "findme toggle");      
            	
            	if(mFinderToggle.isChecked()) {
                   	Log.d(TAG, "findme toggle checked");      
            		
             		gFinderSetting=1;
            		
                    BinsDBUtils.getBluetoothLeDao(getActivity()).setFinderSetting(gFinderSetting);
                    //--**
            		activeBinsFragment.startFinder(getActivity());
            		//-- mFinderRunningIcon.startAnimation(animation);
           		
            	} else {
            		gFinderSetting=0;
            		
           		
                    BinsDBUtils.getBluetoothLeDao(getActivity()).setFinderSetting(gFinderSetting);

                    activeBinsFragment.stopFinder(getActivity());
            		//-- mFinderRunningIcon.setAnimation(null);

            	}
            	
            }
*/                        
        }
    };
    
 
    private void releaseDeviceDialog() {
    	
       AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.notice)
                .setMessage(R.string.will_release_device)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	BinsDBUtils.activeDB().releaseUserDevice(mCurrentUserID);
                    	BinsActivity binsActivity=(BinsActivity)(getActivity());     	
                 		binsActivity.stopDeviceBeaconRegion();
                        binsActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {  
                             	refreshViews();
                            	mDeviceNameView.invalidate(); 
                            }});                    	                    	
                    }
                })  
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                	@Override
                	public void onClick(DialogInterface dialog, int which) {
                		return;
                	}
                })
                .create();
       
        dialog.show();
    }

    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {

        @Override
        public boolean onLongClick(View view) {
            if (view.getId() == R.id.setup_device_new) {
            	Log.d(TAG,"release device");
                releaseDeviceDialog();                    	
            }  
            return true;
        }
    };

    @Override
    protected void update() {
    	
    };
    
    @Override 
    protected synchronized void queryTaskUiBeforeProgress(){
    	//-- TODO
    }
 
    @Override
    protected Cursor queryData() {
    	Cursor cursor=null;
   
        return cursor;	
    };
        
    @Override
    protected synchronized void handleData(Cursor cursor) {
        	  
    };    
 

}
