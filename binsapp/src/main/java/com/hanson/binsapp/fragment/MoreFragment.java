package com.hanson.binsapp.fragment;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.hanson.binsapp.R;

public class MoreFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.more_preference);
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listview = (ListView) getView().findViewById(android.R.id.list);
        listview.setPaddingRelative(0, listview.getPaddingTop(), 0, listview.getPaddingBottom());
        listview.setSelector(R.drawable.bg_white_box_button);
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        Log.d("TEST", "KEY2: " + preference.getKey());
        if (preference.getKey().equals("alarm")) {
            setPreferenceScreen((PreferenceScreen) preference);
            return true;
        }

        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}