      
package com.hanson.binsapp.fragment;
import java.text.SimpleDateFormat;
import java.util.Timer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;
import java.util.UUID;  

     
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hanson.binsapp.BinsActivity;
import com.hanson.binsapp.DeviceListActivity;
import com.hanson.binsapp.GattAttributes;
import com.hanson.binsapp.R;
import com.hanson.binsapp.UserProfileActivity;
import com.hanson.binsapp.provider.Bins;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.provider.LevelPeriodBuilder;
import com.hanson.binsapp.provider.MMSReceiver;
import com.hanson.binsapp.provider.SMSReceiver;
import com.hanson.binsapp.provider.TelephonyStateReceiver;
import com.hanson.binsapp.sync.DataSyncFormat;
import com.hanson.binsapp.sync.DataSyncRoutine;
import com.hanson.binsapp.util.ProgressButton;
import com.hanson.binsapp.widget.TimeControllerView;
import com.pixart.alg.*;

@SuppressLint("ResourceAsColor")
public class BinsFragment
 extends BaseFragment {
	
    private DatePickerDialog datePickerDialog;

    public static final String DATE_FORMAT = "HH:mm, yyyy-MM-dd";
    // Stops scanning after 60 seconds.
    private static final long SCAN_PERIOD = 30000;
    private boolean mScanning;
    private boolean mUpdating;
    private volatile boolean mFound = false;;
    private RelativeLayout mDeviceSyncLayout;
    private int colorOrigin=0;
    private Drawable syncCellBackgroundOrigin=null;
    private TextView mDeviceSyncName;
    private TextView mDeviceSyncTime;
    private ProgressBar mDeviceSyncBar;
    private Button mButtonBypass;
    
    private ImageView mFinderRunningIndicator;

    private TextView mTitleSteps;
    private TextView mTitleDistances;
    private TextView mTitleActive;
    private TextView mTitleCalories;

    private ImageView mIconSteps;
    private ImageView mIconDistances;
    private ImageView mIconActive;
    private ImageView mIconCalories;

    private ProgressBar mProgressSteps;
    private ProgressBar mProgressDistances;
    private ProgressBar mProgressActive;
    private ProgressBar mProgressCalories;

    private TextView mCurrentSteps;
    private TextView mCurrentDistances;
    private TextView mCurrentActive;
    private TextView mCurrentCalories;

    private TextView mMaxSteps;
    private TextView mMaxDistances;
    private TextView mMaxActive;
    private TextView mMaxCalories;

    private ProgressBar mSyncProgress;
    
    // Mental Effect
    private RelativeLayout mMentalEffectLayoutView;
    private TextView mMentalEffectValue;
    private ProgressBar mMentalEffectProgress;
    private int gMentalEffectValue=0;
    
    //-- Heart rate related
    private TextView mHeartRateTextView;
    private RelativeLayout mHeartRateLayoutView;
    private TextView mHeartRateMessageView; 
    private TextView mHeartRatePPMView;
    private ProgressButton mSdnnStartButtonView;
    private TextView mSdnnMessageView;
    private TextView mSdnnStressTitleView;
    private int mCurrentStepsValue = -1;
    private int mCurrentActiveValue = -1;
    private float mCurrentDistanceSum = -1;
    
    private boolean isSyncRunning = false;
    
    private boolean hasAlertMessage=false;
    private boolean hasAlertCall=false;
    private boolean alertCallEnable=false;
    private boolean isFirstHandleFromSamsungError=false;
    
    private IntentFilter mSmsIntentFilter=null; //-- 09.20
    private IntentFilter mMmsIntentFilter=null; //-- 09.20
    private IntentFilter mCallIntentFilter=null; //-- 09.20
    
    private Runnable endSyncCompletionRoutine=null;	// 12.26
   
//------------------------------------------------------------------------------------------
    
    private static boolean mFinderIsRunning = false;
    private static Timer mRssiTimer=null;
    private static boolean mFinderJustCalled = false;
    private static int mRssiAlarmActivated=0;
    private static BluetoothGattCharacteristic mLinklossCharacteristic=null;
    private static int mLinklossInformed=0;
    private static Boolean mIsSyncSession;
    private static Boolean mDoNotCheckMentalEffect=false;
    
    
    private static BluetoothGattCharacteristic mHeartrateCharacteristic=null;

    public static final String TAG = "BinsFragment";
    public static Activity mActivity = null;

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt=null;
    private String mBluetoothDeviceAddress = null;
    private static String mCurrentBluetoothDeviceAddress = null;
    private static int mDailyGoal;

    private DataSyncRoutine mDataSyncModel = null;
    
    public static final String ACTION_DATA_SYNC_START = "com.data.sync.START";
    public static final String ACTION_DATA_SYNC_END = "com.data.sync.END";
    public static final String ACTION_DATA_SYNC_RUNNING = "com.data.sync.RUNNING";
    public static final String ACTION_DATA_SYNC_STOP = "com.data.sync.STOP";

    public static final String ACTION_FINDER_LOST = "com.data.finder.LOST";
    public static final String ACTION_FINDER_STOP = "com.data.finder.STOP";
    public static final String ACTION_DATA_NOTIFY = "com.data.sync.NOTIFY";
    public static final String ACTION_SMS_RECEIVE = "com.data.sms.RECEIVE";
    public static final String ACTION_MMS_RECEIVE = "com.data.mms.RECEIVE";
    public static final String ACTION_CALL_RECEIVE = "com.data.telephony.RECEIVE";
    
    public static final String EXTRA_STATUS = "status";
    private volatile boolean mSucceed = true;
    private volatile boolean mDestroyed = false;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
 
    private static int mConnectionState = STATE_DISCONNECTED;
    
    private static int mRssiLastTime=0;
       
    private Timer mPeriodicSyncTimer=null;
    private int mCountFromLastSync=0;
    private boolean mIsPeriodicScanning;
    private boolean mIsConnectOnly;

    public static final int MSG_CONNECT = 0;
    public static final int MSG_SERVICE_DISCOVERED = 1;
    public static final int MSG_TIMEOUT = 2;
    public static final int MSG_ERROR = 3;
    public static final int MSG_DISCONNECT = 4;
    public static final int MSG_FINDER_TASK = 5;
    public static final int MSG_PERIODIC_SCAN_TIMEOUT = 6;

    public static final int MSG_DATA_SYNC_START = 10;
    public static final int MSG_DATA_SYNC_RUNNING = 11;
    public static final int MSG_DATA_SYNC_STOP = 12;
    public static final int MSG_DATA_SYNC_END = 14;
    
    public static final int MSG_FINDER_LOST = 15;
    public static final int MSG_FINDER_STOP = 16;
    public static final int MSG_DATA_NOTIFY = 17;
    public static final int MSG_SYNC_STOP_END = 18;
    public static final int MSG_CONNECT_GATT = 19;
    public static final int MSG_READ_ERROR = 20;    
    public static final int MSG_READ_FW_REVISION = 21;
    public static final int MSG_GATT_SERVICE_DISCOVERY = 23;
    public static final int MSG_GATT_DISCONNECT = 24;
    public static final int MSG_GATT_READ = 25;
    public static final int MSG_BLUETOOTH_RESET = 26;
    
    public static final int MSG_BLUETOOTH_RESET_FINISH = 27;
    public static final int MSG_BLUETOOTH_RESET_END = 28;
    
    public static final int MSG_DELAYED_LINKLOSS_ALARM = 29;
    
    public static final int MSG_BINS_DISCONNECT = 30;
    public static final int MSG_NEW_SYNC_SESSION = 31;
    public static final int MSG_MENTAL_EFFECT = 32;
    public static final int MSG_REFRESH_MENTAL_EFFECT = 33;
    public static final int MSG_UPDATE_MENTAL_EFFECT = 34;
    public static final int MSG_NEW_CONNECT_ONLY_SESSION = 35;
       
    
    
    public static final int ALERT_TYPE_LINKLOSS = 0x00;  // 12.21 Backward compatible, 0x80
    public static final int ALERT_TYPE_CALL = 0x00; // 150616 to reuse link lose part
    public static final int ALERT_TYPE_MESSAGE = 0x40;
    public static final int ALERT_COMMAND_RESET = 0x00;
    public static final int ALERT_COMMAND_ENABLE = 0x01;
    


    
    
    
    
    private static MediaPlayer mMediaPlayer=null;
    private static int mRssiSetCounter=0;
    private static int mRssiGetCounter=0;

    //-- heart rate
 	public int iHR = 0 ;
 	boolean bStartHRTest = false ;
 	int iReadyFlag = 0;
 	int iMotionFlag = 0;
 	int [] iHRInfo ;
 	int iStableTime = 0 ;
 	int iTouchFlag = 0;
 	boolean gIsNewPulseSession;

    float[] g_dDisplayBuffer;

  
    BluetoothGattCharacteristic gHeartRateUpdateCharacteristic;
    static BluetoothGattCharacteristic gConfigurationCharacteristic;
    BluetoothGattCharacteristic gBatteryCharacteristic;
    BluetoothGattCharacteristic gHardwareRevisionCharacteristic;
    BluetoothGattCharacteristic gActivityTimeValueCharacteristic;
    BluetoothGattCharacteristic gGoalCharacteristic;
    BluetoothGattCharacteristic activeCharacteristic;
    BluetoothGattCharacteristic gSleepStartTimeCharacteristic;
    BluetoothGattCharacteristic gSleepRecordsCharacteristic;
    BluetoothGattCharacteristic gIbeaconUuidCharacteristic;
    BluetoothGattCharacteristic gDebugOutputCharacteristic;
    BluetoothGattCharacteristic gAlarmCharacteristic;
    
    
    Boolean mRunningHeartrateFunction=false;
    
    Boolean mIsSyncSessionRunning=false;
    
    public int mBinsType=0;	// assume Bins 1
    
    private final int DISPLAY_NAME_LENGTH_MAX=20;
        
    public Boolean mHadNewDataRead=false;	// 08.31
    
    public String binsFirmwareRevision="";	// 09.11
    
    //-- 2015120
    private ProgressBar mWorkingProgress;
    private boolean mIsCloudWorking=false;

//-----------------------------------------------------------------------------------------
    

    public void startFinder(Activity fromActivity) {
    	
    	Log.d(TAG, "startFinder()");
    	
    	if(mConnectionState==STATE_CONNECTED){
    		mFinderIsRunning=true;
    		
    		setupFinderTimer();
    		
    		return;
    		
    	}
    	else{
    		return;
    	}

    }
 

    public static Boolean syncSession() {
    	return mIsSyncSession;
    }

    public Boolean periodicScanSession(){
    	return mIsPeriodicScanning;
    }
    
    public void startDataSync(Activity fromActivity, int dgoal, int finderOn) {
    	
    	mActivity = fromActivity;
    	mDailyGoal= dgoal;
    	    	
    	Log.i(TAG, "startDataSync : " + finderOn + " : " + mFinderIsRunning);
    	
    	endSyncCompletionRoutine=null; // 12.26
    	
    	//-- 08.12
    	checkUserChanged();
    	checkDeviceChanged();
    	
    	
    	if(mIsSyncSessionRunning==true) return;    		
    	mIsSyncSessionRunning=true;	//-- 08.11
    	
    	mIsSyncSession=true; // 12.26 will check if it is an active session
    	
    	// if(finderOn==0 || mRssiTimer!=null)

		if(mRssiTimer!=null) {
			mRssiTimer.cancel(); 
			mRssiTimer=null;
		}

    	if(finderOn==0)
    	{
    		mFinderJustCalled=false;
    	} 
    	else {
        	mFinderIsRunning=true;
        	mFinderJustCalled=true;
        	
        	mRssiSetCounter=0;
        	mRssiGetCounter=0;
        	mRssiAlarmActivated=0;
    	}
    		
    
        //--** Intent service = new Intent(fromActivity, DataSyncService.class);
        //--** fromActivity.startService(service);
    	syncStartCommand();
    	
    	
    }
    
 
    
    //-- 
    
    public void stopDataSync() {
    	
    	mFinderIsRunning=false;	// 12.17 to stop running finder anymore, actively disconnect
     	connectionClose();    	 
   		onEventReceive(ACTION_DATA_SYNC_END, true);

    }
    
    

    /**
     * Initializes a reference to the local Bluetooth adapter.
     * 
     * @return Return true if the initialization is successful.
     */
    private boolean initialize() {
        Log.d(TAG, "initialize ...");
    
        return true;
    }

    public int syncStartCommand() {
        Log.d(TAG, "syncStartCommand ...");
        
        if(mFinderJustCalled==false) {
        	sendDataSyncStartBroadcast();
    		Toast toast = Toast.makeText(mActivity, R.string.sync_start, Toast.LENGTH_SHORT);
    		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);		           
            toast.show();
        }
 
        boolean success = initialize();
        
        mBluetoothDeviceAddress = BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveDeviceAddress();
        
        if(mCurrentBluetoothDeviceAddress!=null && mCurrentBluetoothDeviceAddress.equals(mBluetoothDeviceAddress)==false)
        {
        	if(mConnectionState!=STATE_DISCONNECTED)
        	{
        		Log.d(TAG, "change address, disconnect first");
        		//-- disconnect();
        		//-- close();
        		connectionClose();
        		
        	}
        }
        
        
        mCurrentBluetoothDeviceAddress = mBluetoothDeviceAddress;
        
        // TODO for test
        //mBluetoothDeviceAddress = "10:36:5B:00:15:10";
        if (!success || mBluetoothDeviceAddress == null) {
            meetError();
            //-- return;
        }
        else {
        	if(mFinderJustCalled==true){
        		connect();
        	}
        	else{
        		mHandler.sendEmptyMessage(MSG_CONNECT);
        	}
        }
        
        return 0;
    }

    public String getDeviceAddress() {
        return mBluetoothDeviceAddress;
    }
    
    public static boolean isConnected() {
    	if(mConnectionState==STATE_CONNECTED) {
    		return true;
    	}
    	return false;
    }

    public static boolean isConnectAndNotifying() {
    	if(mConnectionState==STATE_CONNECTED && mCheckNotificationTimer!=null) {
    		return true;
    	}
    	return false;
    }
    
    public static int foundAndRssi() {
    	if(mConnectionState==STATE_CONNECTED) {
    		if(mFinderIsRunning==true && mRssiLastTime<0){
    			return mRssiLastTime;
    		}
    	}    	
		return -200; // small enough
    }

      
    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     * 
     * @param address The device address of the destination device.
     * @return Return true if the connection is initiated successfully. The
     *         connection result is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect() {
        Log.d(TAG, "connect ...");
        isFirstHandleFromSamsungError=true;
        return connectInside();
    }
    public void tryConnect() {
    	if(mConnectionState!=STATE_CONNECTING && mConnectionState!=STATE_CONNECTED)
    	{
    		//-- connect();
    		mHandler.sendEmptyMessage(MSG_NEW_SYNC_SESSION);
    		
    	}
    }
    
    public void tryConnectWithoutSync() {
    	if(mConnectionState!=STATE_CONNECTING && mConnectionState!=STATE_CONNECTED)
    	{
    		//-- connect();
    		mHandler.sendEmptyMessage(MSG_NEW_CONNECT_ONLY_SESSION);
    		
    	}
    }
    
    public boolean checkTryConnect() {
    	if(mConnectionState!=STATE_CONNECTING && mConnectionState!=STATE_CONNECTED)
    		return true;
    	return false;
    	
    }
    public boolean connectInside() {
        Log.d(TAG, "connectInside ...");

    
        final Activity nowActivity=mActivity; // 12.17
        if(nowActivity==null) return false; // not normal case
        
    	  
        //-- mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();	//-- 09.01, prevent fall to return startleScan():null

        if (mBluetoothAdapter == null) {
            
            final BluetoothManager bluetoothManager =
                     (BluetoothManager) nowActivity.getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
            if(mBluetoothAdapter==null){
            	Log.w(TAG, "connect(): BluetoothAdapter not assigned");
            	return false;
            }
            
        }
          
        if(mConnectionState==STATE_CONNECTED && mBluetoothGatt!=null) {
        	Log.d(TAG, "Already connected. directly start discover services");
            nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {               	
            	mBluetoothGatt.discoverServices();
              	mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 60 * 1000); //-- 60*1000
            }});
        	return true;
        }
  
        //-- 150505
        // read again just in case called by device initiated connect request (beacon)
        mBluetoothDeviceAddress = BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveDeviceAddress();

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mBluetoothDeviceAddress);
        
        if (device == null) {
            Log.w(TAG, "Device object is not allocated");
            meetError();
            return false;
        }
        
        //-- try if we can find the hardware, but set the timeout
        mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 20 * 1000); // 20 ?
        
        // We want to directly connect to the device, so we are setting the
        // autoConnect
        // parameter to false.
        //-- mBluetoothGatt = device.connectGatt(this, true, mGattCallback);

        //-- 11.19 mBluetoothAdapter.startLeScan(mLeScanCallback);

        nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() { 
            	mBluetoothGatt = device.connectGatt(nowActivity, false, mGattCallback); // 11.19
            }
        });
        
        mUpdating= false; // always false;
                
        Log.d(TAG, "Trying to create a new connection.");
        mConnectionState = STATE_CONNECTING;

        return true;
    }

    
    public boolean connectPeriodic() {
        Log.d(TAG, "connectPeriodic ...");
              
        final Activity nowActivity=getActivity();
        if(nowActivity==null) return false;	// not normal case

        //-- mBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();	//-- 09.01, prevent fall to return startleScan():null
        //-- 09.10
        // 12.13 if(mBluetoothAdapter==null)
        {           
        	final BluetoothManager bluetoothManager =
                (BluetoothManager) nowActivity.getSystemService(Context.BLUETOOTH_SERVICE);
        	mBluetoothAdapter = bluetoothManager.getAdapter();
        }
        
        if (mBluetoothAdapter == null) {
            Log.w(TAG, "connectPeriodic(): BluetoothAdapter not assigned");
            return false;
        }

        String bluetoothDeviceAddress = BinsDBUtils.activeDB().getActiveDeviceAddress();
        if(bluetoothDeviceAddress==null){
            Log.w(TAG, "No bluetooth device assgined");
        	return false;
        }
        
        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bluetoothDeviceAddress);       
        if (device == null) {
            Log.w(TAG, "Device object is not allocated");
            meetError();
            return false;
        }
 
        //-- try if we can find the hardware, but set the timeout
        mHandler.sendEmptyMessageDelayed(MSG_PERIODIC_SCAN_TIMEOUT, 20 * 1000); // 20 ?

        mUpdating= false; // always false;
        
       	mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	//-- 11.20 mBluetoothAdapter.startLeScan(mLePeriodicScanCallback);
                mBluetoothGatt = device.connectGatt(nowActivity, false, mGattCallback); // 11.19
            }
       	});
        
        Log.d(TAG, "Trying to create a new connection.");
        mConnectionState = STATE_CONNECTING;
        return true;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read
     * result is reported asynchronously through the
     * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     * 
     * @param characteristic The characteristic to read from.
     */
    public void readCharacteristic(final BluetoothGattCharacteristic characteristic) {
        //Log.d(TAG, "readCharacteristic ... ");
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "read Character: BluetoothAdapter not initialized");
            meetError();
            return;
        }
        
        if(mConnectionState!=STATE_CONNECTED){
            mHandler.sendEmptyMessage(MSG_TIMEOUT);
        	return;	//-- 08.11
        }
        
        activeCharacteristic=characteristic;
        mHandler.sendEmptyMessage(MSG_GATT_READ);
        
        /*
        //-- 09.09
        Activity nowActivity=getActivity();
        if(nowActivity==null){
        	Log.i(TAG, "error to get Activity while write characteristic, abort");
        	return;
        }
        
      	nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {         		
                mBluetoothGatt.readCharacteristic(characteristic);
            }
       	});        
      	
        //-- mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 60 * 1000); // 60*1000      	
      	mHandler.sendEmptyMessageDelayed(MSG_READ_ERROR, 8 * 1000); // 09.14
      	*/
      	
    }

    public void writeCharacteristic(final BluetoothGattCharacteristic characteristic) {
        //Log.d(TAG, "writeCharacteristic ... ");
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "write character: BluetoothAdapter not initialized");
            meetError();
            return;
        }
        if(mConnectionState!=STATE_CONNECTED){
            mHandler.sendEmptyMessage(MSG_TIMEOUT);
        	return;	//-- 08.11
        }
        /* 12.17
        Activity nowActivity=getActivity();
        if(nowActivity==null){
        	Log.i(TAG, "error to get Activity while write characteristic, abort");
        	return;
        }
        */
      	mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	try{
            		if(mBluetoothGatt!=null){
            			mBluetoothGatt.writeCharacteristic(characteristic);
            			mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 20 * 1000);
            		}
            	}catch(Exception e){
            		Log.i(TAG, "writeCharacteristic exception:"+e);
            	}
            }
       	});

    }
    

    //-- private BluetoothGatt mLastBluetoothGatt=null;
    //-- private Timer mDisconnectSwitchTimer=null;
    
    private void connectionClose() {
    	
    	Log.i(TAG, "connectionClose()");
    	 
    	/* 150616 for incoming call alert, disable this part, real link losee is depreciated
        if(mLinklossCharacteristic!=null)
     	{
       		  byte[] bytes = new byte[1];
       		  
       		  bytes[0]=(byte)(ALERT_TYPE_LINKLOSS | ALERT_COMMAND_RESET); //-- 09.20 s(0x00); 
       		  mLinklossCharacteristic.setValue(bytes);    	
       		  writeCharacteristic(mLinklossCharacteristic);
       		  
       		  mLinklossCharacteristic=null;
        }               	 
		*/
    	
        mLinklossCharacteristic=null; //-- no more valid;
                
        if(mCheckNotificationTimer!=null){
        	mCheckNotificationTimer.cancel();
        	mCheckNotificationTimer=null;
  	    	   
         }	
  
        //-- 11.14 if(finderIsActive()==true) return; //-- no need to disconnect if finder is running
        if(finderIsActive()==true){	// 12.17, only terminate if enforce to stop it
        	stopFinder();
        }
        
        if (mBluetoothAdapter != null) {  	//-- 09.09 in case
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        
        //----------------------
        		         			        
       	mHandler.removeMessages(MSG_TIMEOUT);  //-- clear timeout handling always in the end
        
        mConnectionState = STATE_DISCONNECTED;

        final Activity nowActivity=getActivity();
        
        if(mBluetoothGatt!=null && nowActivity!=null){ 
        	nowActivity.runOnUiThread(new Runnable() {
        	@Override
            public void run() {
        		
        		mBluetoothGatt.close();	
        		mBluetoothGatt=null;
        	}
        	});
        	
            //-- 20151002
     		BinsActivity binsActivity;
     		binsActivity=(BinsActivity)(nowActivity);     	
     		binsActivity.startDeviceBeaconRegion();

        }

        
		mHandler.sendEmptyMessage(MSG_GATT_DISCONNECT);  // update UI

        
        //---------------------
/*        
     	//-- mLastBluetoothGatt=mBluetoothGatt;
     	//-- mBluetoothGatt=null;	//-- 09.10
     	
     	
        Activity nowActivity=mActivity; // 12.17 getActivity();
        if(nowActivity!=null){
      	  	nowActivity.runOnUiThread(new Runnable() {
      		  @Override
            	public void run() {
                	mHandler.removeMessages(MSG_TIMEOUT);  //-- clear timeout handling always in the end
                	try{
                 		if(mConnectionState==STATE_CONNECTED && mBluetoothGatt!=null){
                 			mBluetoothGatt.disconnect();
                 			
                            // 150610 send it anyway 
                			mHandler.sendEmptyMessage(MSG_GATT_DISCONNECT);
                			         			
                 			
                 		}
                	} catch (Exception e) {
                		Log.i(TAG, "error while disconnecting Gatt.");
      				}         
                
            	}
      	  	});
        }
        
        mConnectionState=STATE_DISCONNECTED;
	
	    TimerTask timerTask = new TimerTask()
	    {
	    	@Override
			public void run()
			{
	    		Activity nowActivity=mActivity; //-- 12.17 getActivity();
	    		if(nowActivity==null) return;
	    		
	         	nowActivity.runOnUiThread(new Runnable() {
	                @Override
	                public void run() {
	                	try{
	                		if(mLastBluetoothGatt!=null){
	                			mLastBluetoothGatt.close();
	                			mLastBluetoothGatt=null;
	                		}
	                		else{
	                			Log.w(TAG, "Last BluetoothGatt not initialized");	                 		
	                		}
	                	} catch (Exception e) {
	                		Log.i(TAG, "error while delayed close of Gatt.");
          				}         

	                }
	           	});
			}
		};
*/
		/*
     	mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	try{
            		if(mLastBluetoothGatt!=null){
            			mLastBluetoothGatt.close();
            			mLastBluetoothGatt=null;
            		}
            		else{
            			Log.w(TAG, "Last BluetoothGatt not initialized");	                 		
            		}
            	} catch (Exception e) {
            		Log.i(TAG, "error while delayed close of Gatt.");
  				}         

            }
       	});

     	*/
     	
		/* 150505 
		if(mDisconnectSwitchTimer==null) 
		{ 
			mDisconnectSwitchTimer = new Timer();
    	}
		
		//--** 4.15 mDisconnectSwitchTimer.schedule(timerTask, 1000); //-- 04.05
		mDisconnectSwitchTimer.schedule(timerTask, 5000);	// 12.25 1000 to 500	
		*/
    }
    
    /**
     * Disconnects an existing connection or cancel a pending connection. The
     * disconnection result is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    //-- private to public
    public void disconnect() {
    	
        mHandler.removeMessages(MSG_TIMEOUT);  //-- clear timeout handling always in the end
        
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized mBluetoothAdapter:"+mBluetoothAdapter+" mBluetoothGatt:"+mBluetoothGatt);
            return;
        }
        mLinklossCharacteristic=null; //-- no more valid;
        if(mCheckNotificationTimer!=null){
        	mCheckNotificationTimer.cancel();
        	mCheckNotificationTimer=null;
        }

        
        Activity nowActivity=mActivity; //-- 12.17 getActivity();
        if(nowActivity==null) return;
        
        //-- 20151001
 		BinsActivity binsActivity;
 		binsActivity=(BinsActivity)(nowActivity);     	
 		binsActivity.startDeviceBeaconRegion();
 		
       	nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mBluetoothGatt!=null){ 
                	mBluetoothGatt.disconnect();
                }
            }
       	});

    }  
    
    /**
     * After using a given BLE device, the app must call this method to ensure
     * resources are released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        
        Activity nowActivity=mActivity; // 12.17 getActivity();
        if(nowActivity==null) return;

       	nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	mBluetoothGatt.close();
            	mBluetoothGatt=null;

            }
       	});

        
        //-- force to rest status
        mConnectionState=STATE_DISCONNECTED;
                
    }

    private void startSync() {
  

        final Activity nowActivity=mActivity; // 12.17 getActivity();
        if(nowActivity==null){
        	Log.d(TAG, "get activity returned null, error");
            meetError();
        	return;
        }

        
    	if(mBluetoothGatt== null){
            meetError();
    		return;
    	}
    	
    	if(mCheckNotificationTimer!=null){	//-- 09.09
    		mCheckNotificationTimer.cancel();
    		mCheckNotificationTimer=null;
    	}
    	
        mDataSyncModel = new DataSyncRoutine(this);

        // ---- start to use UI thread 
        // ---------------------------------------------------------------------------
        mActivity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
        	
        	   	
        BluetoothGattService batteryService = mBluetoothGatt.getService(UUID
                .fromString(GattAttributes.SERVICE_BATTERY));
        if (batteryService == null) {
            Log.e(TAG, "startSync... Can not read batteryService");
            meetError();
            return;
        }

        BluetoothGattCharacteristic batteryCharacter = batteryService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_BATTERY));
        if (batteryCharacter == null) {
            Log.e(TAG, "startSync... Can not read batteryCharacter");
            meetError();
            return;
        }
        gBatteryCharacteristic=batteryCharacter;

        BluetoothGattService deviceInfoService = mBluetoothGatt.getService(UUID
                .fromString(GattAttributes.SERVICE_DEVICE_INFORMATION));
        if (deviceInfoService == null) {
            Log.e(TAG, "startSync... Can not read deviceInfoService");
            meetError();
            return;
        }

        BluetoothGattCharacteristic hardwareRevisionCharacteristic = deviceInfoService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_DI_FW_REVISION));  //-- 09.11 CHARACTERISTIC_DI_HW_REVISION
        if (hardwareRevisionCharacteristic == null) {
            Log.e(TAG, "startSync... Can not read hardware revision");
            meetError();
            return;
        }
        gHardwareRevisionCharacteristic=hardwareRevisionCharacteristic;
        
        BluetoothGattService activityService = mBluetoothGatt.getService(UUID
                .fromString(GattAttributes.SERVICE_ACTIVITY_TRACKING));
        if (activityService == null) {
            Log.e(TAG, "startSync... Can not read activityService");
            meetError();
            return;
        }
     
        mHeartrateCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_HEARTRATE_STREAM));
        //-- if BiNS 1, there is no this characteristic.
                
        if(mHeartrateCharacteristic==null) Log.i(TAG, "Cannot find Heartrate detector");
        else Log.i(TAG, "Found Heartrate detector");
        
        if(mHeartrateCharacteristic==null){
        	mBinsType=BinsDBUtils.BINS_MODEL_BINSX1;	// 0, Bins 1
        }
        else{
        	mBinsType=BinsDBUtils.BINS_MODEL_BINSX2;
        }
        BinsDBUtils.activeDB().setActiveDeviceType(mBinsType);
        
        BluetoothGattCharacteristic valueCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_VALUE));
        if (valueCharacter == null) {
            Log.e(TAG, "startSync... Can not read valueCharacter");
            meetError();
            return;
        }

        BluetoothGattCharacteristic typeCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_TYPE));
        if (typeCharacter == null) {
            Log.e(TAG, "startSync... Can not read typeCharacter");
            meetError();
            return;
        }

        BluetoothGattCharacteristic timeCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_TIME));
        if (timeCharacter == null) {
            Log.e(TAG, "startSync... Can not read timeCharacter");
            meetError();
            return;
        }

        //--
        BluetoothGattCharacteristic timeValueCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_TIME_VALUE));
        if (timeValueCharacter == null) {
            Log.e(TAG, "startSync... Can not read timeValueCharacter");
            meetError();
            return;
        }
        gActivityTimeValueCharacteristic=timeValueCharacter;
        
        BluetoothGattCharacteristic configurationCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_CONFIGURATION));
        if (configurationCharacter == null) {
            Log.e(TAG, "startSync... Can not find configuration characteristic");
            meetError();
            return;
        }

        BluetoothGattCharacteristic localClockCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_LOCAL_CLOCK));
        if (localClockCharacter == null) {
            Log.e(TAG, "startSync... Can not find local clock characteristic");
            meetError();
            return;
        }

        BluetoothGattCharacteristic goalCharacter = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_GOAL));
        if (goalCharacter == null) {
            Log.e(TAG, "startSync... Can not find activity goal characteristic");
            meetError();
            return;
        }   
        gGoalCharacteristic=goalCharacter;
                  
        mLinklossCharacteristic = activityService.getCharacteristic(UUID
                 .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_LINK_LOSS));
                 
        if (mLinklossCharacteristic == null) {
        	Log.e(TAG, "startSync... Can not find activity link loss characteristic");
   	  	}
        
        gHeartRateUpdateCharacteristic = activityService.getCharacteristic(UUID
               .fromString(GattAttributes.CHARACTERISTIC_HEARTRATE_HOST_UPDATE));
    
        gSleepStartTimeCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_SLEEP_START_TIME));
        
        gSleepRecordsCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_SLEEP_RECORDS));
     
        gIbeaconUuidCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_IBEACON_UUID));
 
        gDebugOutputCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_DEBUG_OUTPUT));
 
        gAlarmCharacteristic = activityService.getCharacteristic(UUID
                .fromString(GattAttributes.CHARACTERISTIC_ALARM_CLOCK));

        gConfigurationCharacteristic = configurationCharacter;
        
        if (mDataSyncModel != null) {
            mDataSyncModel.stop();
        }
        // Convert to minute
        mDataSyncModel.start(mIsConnectOnly, batteryCharacter, valueCharacter, typeCharacter, timeCharacter, timeValueCharacter,
        		localClockCharacter,
        		goalCharacter,
        		gAlarmCharacteristic,
        		configurationCharacter,
        		gSleepStartTimeCharacteristic,
        		gSleepRecordsCharacteristic,
        		gIbeaconUuidCharacteristic);
        
        }}); 
        // ---------------------------------------------------------------------------
       	// ---------  end of using UI thread 
        
        //-- 20151129 try to enable heart rate directly without need of waiting sync completed
        if(mHeartrateCharacteristic!=null){
        	mBluetoothGatt.setCharacteristicNotification(mHeartrateCharacteristic, true); //-- 06.19 TBC 
    	  	nowActivity.runOnUiThread(new Runnable() 
      	  	{
      	  		@Override
            	public void run() {
      	  			if(mHeartrateCharacteristic!=null) {
      	  				showHeartRateViews();
      	  			}  
            	}
      	  	});                	
    	  	openHeartrate();	//-- open heart rate library function
            
        }
   
    
    }

    private static Timer mCheckNotificationTimer=null;
    
    public int getBinsFirmwareRevisionMajor()
    {
    	Integer version;
    	
    	//-- Log.i(TAG, "Firmware Major rev:"+binsFirmwareRevision);
    	if(binsFirmwareRevision==null || binsFirmwareRevision.length()<21) // give one more chance to read
    	{
    		binsFirmwareRevision=BinsDBUtils.activeDB().getBinsFirmwareRevision();	
    	}
    	
    	if(binsFirmwareRevision==null || binsFirmwareRevision.length()<21){
    		version=3;	//-- 09.21, 0, assume it is the latest one
    	}
    	else{
    		version=Integer.parseInt(binsFirmwareRevision.substring(16,17));
    	}
    	//-- Log.i(TAG, "Firmware Major:"+version);
    	// Log.i(TAG, "Firmware version:"+binsFirmwareRevision);
        
    	
    	return version;
    }
    
    public int getBinsFirmwareRevisionMinor()
    {
    	if(binsFirmwareRevision==null || binsFirmwareRevision.length()<21) // give one more chance to read
    	{
    		binsFirmwareRevision=BinsDBUtils.activeDB().getBinsFirmwareRevision();	
    	}
    	
    	if(binsFirmwareRevision==null || binsFirmwareRevision.length()<21) return 0;
    	
    	Integer version=Integer.parseInt(binsFirmwareRevision.substring(18,19));
    	    	
    	//-- Log.i(TAG, "Firmware Minor:"+version);

    	return version;
    }
    
    private void openHeartrate()
    {
        	final byte TYPE_HRD_GS =  (byte)0xB8 ;    

        	int odr=20;
        	PXIALGMOTION.Close();
        	PXIALGMOTION.Open(odr);
        	PXIALGMOTION.EnableFastOutput(true);
        	
			PXIALG.Close();
			PXIALG.Open(TYPE_HRD_GS);
			
	        //-- 09.08
     		int user=BinsDBUtils.activeDB().getActiveUserID();

	        String strDOB=BinsDBUtils.activeDB().getUserBirth(user); // return default if not yet set
        	Calendar cal = Calendar.getInstance();
        	Calendar calNow = Calendar.getInstance();
        	int age;
        	try {
            	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        		Date bDate=dateFormat.parse(strDOB);
        		cal.setTime(bDate);
         	} catch (java.text.ParseException e) {
			} finally {  
				age=calNow.get(Calendar.YEAR)-cal.get(Calendar.YEAR);
				Log.i(TAG,"Year Now:"+calNow.get(Calendar.YEAR)+",DOB:"+cal.get(Calendar.YEAR)+",Age:"+age);								
        	}	        
			PXIALG.SetAge(age);

    }
    
    public void endSyncNotify() {

       	Log.i(TAG, "endSyncNotify");
        
     	endRead();
     	         
        sendDataSyncEndBroadcast();
	             
        if(mIsPeriodicScanning==true){
        	Log.i(TAG, "ending periodic sync");
        	resetPeriodicSyncTimer();  // reset counter and this flag
        	if(endSyncCompletionRoutine!=null){
        		endSyncCompletionRoutine.run();
            	mHandler.sendEmptyMessageDelayed(MSG_BINS_DISCONNECT, 1000); // 01.15 from 500 to 1000, avoid racing
        	} else {
               	connectionClose();       		 
        	}
        	return;	//-- 09.19
        }    

        if(isHeartRateStreaming()==false){
        	BinsDBUtils.activeDB().uploadTask(); // 150825
        }
        
     	if(syncSession()==false) //-- means not an active sync, is a background sync action, no need to set notify
     	{
           	connectionClose();  //-- 20151002
     		return;
     	}

     	
     	if(mIsConnectOnly==false){ //-- 150712
     		//-- 150518 TODO ! should set this, BUT HOW ? for first time, setDeviceBeaconRegion();     	
     		BinsActivity binsActivity;
     		binsActivity=(BinsActivity)(mActivity);     	
     		binsActivity.setDeviceBeaconRegion();
     	
     		binsActivity.stopDeviceBeaconRegion();	//-- 150924 stop it
     		
            //-- 20151129 set user's sync time, different to device sync time.
     	    long currentTime=(System.currentTimeMillis() / 1000 / 60);
     		BinsDBUtils.activeDB().setActiveUserLastSyncTime(currentTime);
     	
     		//150513
     		if(gDebugOutputCharacteristic!=null){
     			Log.i(TAG, "debug output notify set"); 
     			mBluetoothGatt.setCharacteristicNotification(gDebugOutputCharacteristic, true);  
     		}
     		mBluetoothGatt.setCharacteristicNotification(gActivityTimeValueCharacteristic, true); 
     		if(gSleepRecordsCharacteristic!=null) 
     			mBluetoothGatt.setCharacteristicNotification(gSleepRecordsCharacteristic, true);  
                
     		//-- 09.11 delay running after receiving gHardwareRevision, readCharacteristic(gBatteryCharacteristic);       
     		Log.i(TAG,"start read hardware revision");
        	binsFirmwareRevision="";
        
        	//-- 09.16, no need mHandler.sendEmptyMessageDelayed(MSG_READ_FW_REVISION, 2000);	//-- after 2 seconds        
        	readCharacteristic(gHardwareRevisionCharacteristic);
        
        	//-- 09.16 readCharacteristic(gBatteryCharacteristic);  

     	}	// mIsConnectOnly==false
     	
     	//-- heart rate
        // 09.04
        if(mHeartrateCharacteristic!=null) mBluetoothGatt.setCharacteristicNotification(mHeartrateCharacteristic, true); //-- 06.19 TBC 
        mBluetoothGatt.setCharacteristicNotification(gBatteryCharacteristic, true); // 07.21
        //-- mBluetoothGatt.setCharacteristicNotification(gHardwareRevisionCharacteristic, true); // 09.11

        //-- 09.08
        openHeartrate();
        
        if(mCheckNotificationTimer!=null){
        	mCheckNotificationTimer.cancel();
        	mCheckNotificationTimer=null;
        }
               
        mCheckNotificationTimer = new Timer();
        /* 20151230
        TimerTask timerTask= new TimerTask()
	    {
	    	@Override
			public void run()
			{
	        	Log.i(TAG, "Noti Timer:"+mActivityCheckCounter);
	    	    if(hasActivityNotified==false){
	    	        mActivityCheckCounter++;
	    	        if(mActivityCheckCounter>=(10*60/5)){  //--10,  5 is enough, 10 minutes
	    	        	mCheckNotificationTimer.cancel();
	    	        	mCheckNotificationTimer=null;

	    	        	if(mBluetoothGatt!=null){
	    	        		Activity nowActivity=mActivity; // 12.17 getActivity();
	    	        		if(nowActivity==null) Log.i(TAG, "Noti Timer: Activity returns null");
	    	        		else
	    	        		{
	                      	  nowActivity.runOnUiThread(new Runnable() {
	                          @Override
	                          public void run() {
	    	    	             if(mHeartrateCharacteristic!=null && mBluetoothGatt!=null)  // 12.13
	    	    	                	mBluetoothGatt.setCharacteristicNotification(mHeartrateCharacteristic, false); //-- 06.19 TBC 
	    	    	          }
	                       	  });
	    	        		}

	    	        	}
	    	        	Log.i(TAG, "disable Notification, and stop sync, reason of timeout");	 
	    	        	
	                	//-- STOP sync force 04.05
	                    onEventReceive(ACTION_DATA_SYNC_STOP, false);	    	        	
	    	        	//-- sendDataSyncEndBroadcast(); //-- 04.05
	    	        	
	    	        	stopDataSync();	//-- 11.17, run
  
	    	        }
	    	    }
	    	    else{
	    	        mActivityCheckCounter=0;
	    	        hasActivityNotified=false;
	    	    }
			}
		};
		*/
		
		/* 150917 no need ?! 
        if(mIsSyncSession==true && mCheckNotificationTimer!=null){ //-- 04.05
        	mCheckNotificationTimer.schedule(timerTask,  5000, 5000); 
        }
        */
		
        Activity nowActivity=mActivity; // 12.17 getActivity();
        if(nowActivity==null) return;
        
 	  	//-- 09.03  mActivity.runOnUiThread(new Runnable() 
 	  	nowActivity.runOnUiThread(new Runnable() 
  	  	{
  	  		@Override
        	public void run() 
  		  	{

  	  	      if(mHeartrateCharacteristic!=null) 
  	  	    	     /* 09.10 no need, && mRunningHeartrateFunction==false){ */
  	  	      {
  	  	    	  showHeartRateViews();
   	          }  
        	}
  	  	});   
 	  	 	  	
     
    }
    
    private void showHeartRateViews() {
         Log.i(TAG, "Connected to Heart Rate");
     	
  		mHeartRateTextView.setVisibility(View.VISIBLE);
  		mHeartRateTextView.setText("");
  		//-- mHeartRateLayoutView.setVisibility(View.VISIBLE);
  		mHeartRateMessageView.setText(getResources().getString(R.string.message_press_check_pulse)); 
 		mHeartRateMessageView.setVisibility(View.VISIBLE); 
 		
 		//--??
 		mHeartRatePPMView.setVisibility(View.GONE);
  		mHeartRateTextView.setVisibility(View.GONE); 
 		mHeartRateMessageView.setVisibility(View.VISIBLE);     
  		mHeartRateMessageView.setText(getResources().getString(R.string.message_press_check_pulse)); 
    	mHeartRateLayoutView.setVisibility(View.VISIBLE); 
 		 
  		//-- mHeartRateLayoutView.invalidate();	   

    }
    
    
    /* Following routines use to set BiNS configurations 
    */
    
    public int getConfig() {
    	return BinsDBUtils.activeDB().getBinsConfiguration();
    }
    
    public void setSleepCheckPeriod(int minute)
    {    	
    	if(mConnectionState!=STATE_CONNECTED) return;
    	
		byte[] bytes = new byte[2];
		bytes[0]=(byte)(minute & 0x00FF);
		bytes[1]=(byte)(minute & 0x00FF);
		
		gSleepStartTimeCharacteristic.setValue(bytes);    	
		writeCharacteristic(gSleepStartTimeCharacteristic);	
    }
    
    public void startHeartrateDetection()
    {
    	
    	if(mConnectionState!=STATE_CONNECTED) return;
    	
    	int tmpConfiguration;

    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	tmpConfiguration= gConfiguration | (BinsDBUtils.BINS_CONFIG_COMMAND_HRD_START_MASK);     	
    	//-- only one time, BinsDBUtils.getBluetoothLeDao(getActivity()).setBinsConfiguration(gConfiguration);
    	
		byte[] bytes = new byte[2];
		bytes[0]=(byte)(tmpConfiguration & 0x00FF);
		bytes[1]=(byte)((tmpConfiguration & 0xFF00)>>8);
		
		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
				
		mRunningHeartrateFunction=true;
	
    }
    
    public void setAlertCall(boolean enable)
    {   	
    	
    	Log.i(TAG, "setAlertCall");

    	gConfiguration=getConfig();    	
    	gConfiguration &= 0xFFF0;    	

    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK )==0)
    	{
    		return;
    	}
    	
    	if(mConnectionState!=STATE_CONNECTED){	// connect-set-and disconnect
    		hasAlertCall=true;				// set flag first
    		alertCallEnable=enable;
    		backgroundSyncAndDisconnect();   	// and connect background to set	
    		return;
    	}
   	  
 		byte[] bytes = new byte[1];
   		
 		if(enable==true)
 			bytes[0]=(byte)(ALERT_TYPE_CALL | ALERT_COMMAND_ENABLE); 
 		else
 			bytes[0]=(byte)(ALERT_TYPE_CALL | ALERT_COMMAND_RESET); 

 		mLinklossCharacteristic.setValue(bytes);
 		writeCharacteristic(mLinklossCharacteristic);	
 		hasAlertCall=false;
    }  
    
    public void setAlertMessage()
    {   	
    	    	
    	gConfiguration=getConfig();    	
    	gConfiguration &= 0xFFF0;    	
    	Log.i(TAG, "setAlertMessage:"+gConfiguration);

    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK )==0)
    	{
    		return;
    	}
   	
    	
    	if(mConnectionState!=STATE_CONNECTED){	// connect-set-and disconnect
    		hasAlertMessage=true;				// set flag first
    		backgroundSyncAndDisconnect();   	// and connect background to set	
    		return;
    	}
   	  
 		byte[] bytes = new byte[1];
   		  
 		bytes[0]=(byte)(ALERT_TYPE_MESSAGE | ALERT_COMMAND_ENABLE); //-- 09.22 enable (0x01); 
 		mLinklossCharacteristic.setValue(bytes);
 		writeCharacteristic(mLinklossCharacteristic);	
 		hasAlertMessage=false;
    }  
    

    public void setHeartRateEnable()
    {   	
    	
    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_HEARTRATE_ENABLE_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_HEARTRATE_ENABLE_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_HEARTRATE_ENABLE_MASK));       		
    	}
    	
    	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);    	
    	
    	// Only continue if connected already
    	if(mConnectionState!=STATE_CONNECTED){
    		updateBinsConfiguration();
    		return;
    	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);

		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
						
    }  
    
    /* This bit means when pulse data cannot be detected over a period (30 seconds),
     * BiNS will continue to wait for it, otherwise will stop waiting 
     * and touch sensor will go down idle.
     */
    
    public void setAutoHeartrateDetection()
    {
    	
    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_HRD_ALWAYS_ON_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_HRD_ALWAYS_ON_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_HRD_ALWAYS_ON_MASK));       		
    	}    	
    	
     	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);
    	    	
     	Log.i(TAG,"Set Always On Detection "+gConfiguration);
     	
     	//-- not continue if not connected yet
    	if(mConnectionState!=STATE_CONNECTED){
    		updateBinsConfiguration();
    		return;
    	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);

		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
						
    }  

    public void setBinsX2DisplayBrightness(){

    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK)==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK));       		
    	}    	
    	
     	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);
    	    	
     	Log.i(TAG,"Display Brightness:"+gConfiguration);

     	//-- not continue if not connected yet
      	if(mConnectionState!=STATE_CONNECTED){
      		updateBinsConfiguration();
      		return;
      	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
 
		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
				
    }

    public void setPhoneNotice(){

    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK));       		
    	}    	
    	
     	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);
    	    	
     	Log.i(TAG,"Phone Notice:"+gConfiguration);

     	//-- not continue if not connected yet
      	if(mConnectionState!=STATE_CONNECTED){
      		updateBinsConfiguration();
      		return;
      	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
 
		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
				
    }

    public void setSleepEnable(){

    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK));       		
    	}    	
    	
     	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);
    	    	
     	Log.i(TAG,"Sleep ENable :"+gConfiguration);

     	//-- not continue if not connected yet
      	if(mConnectionState!=STATE_CONNECTED){
    		noticeForConnection();	//-- 20151022  		
      		updateBinsConfiguration();
      		return;
      	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
 
		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
    }

	
	
    public void setBinsX2SensorBlink(){

    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK));       		
    	}    	
    	
     	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);
    	    	
     	Log.i(TAG,"Touch Blink :"+gConfiguration);

     	//-- not continue if not connected yet
      	if(mConnectionState!=STATE_CONNECTED){
      		updateBinsConfiguration();
      		return;
      	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
 
		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
    }

    
    public void setStepCheckWhenHRDEnable()
    {   	
    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_STEP_WHILE_HRD_MASK)==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_STEP_WHILE_HRD_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_STEP_WHILE_HRD_MASK));       		
    	}
    	
    	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);    	
    	
    	//-- not continue if not connected yet
       	if(mConnectionState!=STATE_CONNECTED){
       		updateBinsConfiguration();
       		return;
       	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);

		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
		
    }   

    private void ShowSDNN(String msgSDNN)
    {
    	//-- Log.i(TAG, msgSDNN);
 		//-- 09.21 
    	mSdnnStartButtonView.setVisibility(View.GONE); 
    	mSdnnMessageView.setVisibility(View.VISIBLE);
    	mSdnnStressTitleView.setVisibility(View.VISIBLE);
    	mSdnnMessageView.setText(msgSDNN);
    	
    	//-- 09.08
    }
    
    public void setHeartRateSleepEnable()
    {   	

    	gConfiguration=getConfig();
    	gConfiguration &= 0xFFF0;
    	
    	if((gConfiguration & BinsDBUtils.BINS_CONFIG_HEARTRATE2SLEEP_MASK )==0){
    		gConfiguration |= (BinsDBUtils.BINS_CONFIG_HEARTRATE2SLEEP_MASK);   
    	}else{
    		gConfiguration &= (~(BinsDBUtils.BINS_CONFIG_HEARTRATE2SLEEP_MASK));       		
    	}
    	
    	BinsDBUtils.activeDB().setBinsConfiguration(gConfiguration);    	
    	
    	//-- continue only if device is connected
    	if(mConnectionState!=STATE_CONNECTED){
    		updateBinsConfiguration();
    		return;
    	}

		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);

		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);
		
    }   
    
    //BITS 15, 11, 4

    public int setShakeThresholdToConfig(int threshValue, int configOrigin)
    {
    	int config=configOrigin;
     
    	if((threshValue & 0x04)!=0) config |= 0x8000; else config &= (~0x8000);
    	if((threshValue & 0x02)!=0) config |= 0x0800; else config &= (~0x0800);
    	if((threshValue & 0x01)!=0) config |= 0x0010; else config &= (~0x0010);
     
    	return config;
    }
    
    //10.11
    public int getShakeThresholdFromConfig(int config) 
    {
     int threshValue=0;
     
     if((config & 0x8000)!=0) threshValue |= 0x4;
     if((config & 0x0800)!=0) threshValue |= 0x2;
     if((config & 0x0010)!=0) threshValue |= 0x1;
     
     return threshValue;
    }

    public void updateBinsConfiguration(){
       	if(mConnectionState!=STATE_CONNECTED){  
       		noticeForConnection();
       		backgroundSyncAndDisconnect(new Runnable(){
       		    @Override
       		    public void run()
       		    {       			
       		    	writeBinsConfiguration();
       		    }});
       		return;
       	}
       	writeBinsConfiguration();       	
    }
  
    //-- 2015.01.13
    public void updateBinsConfiguration(final int binsCommand){
       	if(mConnectionState!=STATE_CONNECTED){   
       		noticeForConnection();
       		backgroundSyncAndDisconnect(new Runnable(){
       		    @Override
       		    public void run()
       		    {       			
       		    	writeBinsConfiguration(binsCommand);
       		    }});
       		return;
       	}
       	writeBinsConfiguration();       	
    }
    //-- 2015.01.13
    public void writeBinsConfiguration(int binsCommand){
       	if(mConnectionState!=STATE_CONNECTED) return;
    	 
   	    int shakeUiThresh=BinsDBUtils.activeDB().getActiveShakeUiThreshold();
   	    final byte[] bytes = new byte[4];
   	 
        int deviceConfig=BinsDBUtils.activeDB().getBinsConfiguration();

        deviceConfig |= BinsDBUtils.BINS_CONFIG_ACTIVITY_NOTIFY_MASK; // 11.19, it must be set 
        
    	deviceConfig &= 0xFFF0;
    	
    	deviceConfig |= BinsDBUtils.BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK;
    	deviceConfig |= binsCommand;

    	deviceConfig=setShakeThresholdToConfig(shakeUiThresh, deviceConfig);
                
        DataSyncFormat.bleSet16(bytes, deviceConfig);
        gConfigurationCharacteristic.setValue(bytes);    	
        writeCharacteristic(gConfigurationCharacteristic);

    }

    public void writeBinsConfiguration(){
       	if(mConnectionState!=STATE_CONNECTED) return;
    	 
   	    int shakeUiThresh=BinsDBUtils.activeDB().getActiveShakeUiThreshold();
   	    final byte[] bytes = new byte[4];
   	 
        int deviceConfig=BinsDBUtils.activeDB().getBinsConfiguration();

        deviceConfig |= BinsDBUtils.BINS_CONFIG_ACTIVITY_NOTIFY_MASK; // 11.19, it must be set 
        
    	deviceConfig &= 0xFFF0;
    	
    	deviceConfig |= BinsDBUtils.BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK;

    	deviceConfig=setShakeThresholdToConfig(shakeUiThresh, deviceConfig);
        
    	/* this bit will be set from getBinsConfiguration function inside
    	 * 
    	boolean isSleepEnabled=BinsDBUtils.activeDB().getActiveSleepEnable();
    	
    	if(isSleepEnabled==true){
        	deviceConfig |= (BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK);   
        }else{
        	deviceConfig &= (~(BinsDBUtils.BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK));       		
    	}
    	*/
    	
        Log.i(TAG, "write ui:config="+shakeUiThresh+":"+deviceConfig);
        
        DataSyncFormat.bleSet16(bytes, deviceConfig);
        gConfigurationCharacteristic.setValue(bytes);    	
        writeCharacteristic(gConfigurationCharacteristic);

    }
    
    public void updateDailyGoal() {
    	if(mConnectionState!=STATE_CONNECTED){
    		noticeForConnection();
    	    backgroundSyncAndDisconnect(new Runnable(){
    	       	@Override
    	       	public void run()
    	       	{       			
    	       		writeDailyGoal();
    	       	}});
    	    return;
    	}
    	writeDailyGoal();
    }
    public void writeDailyGoal(){    	
    	 if(mConnectionState!=STATE_CONNECTED) return;
    	
    	 int goal=BinsDBUtils.activeDB().getActiveUserDailyGoal();

    	 final byte[] bytes = new byte[4];

     	 DataSyncFormat.bleSet16(bytes, goal);
     	 gGoalCharacteristic.setValue(bytes);
     	 writeCharacteristic(gGoalCharacteristic);        
    }

    private void noticeForConnection() {
    	Toast toast = Toast.makeText(mActivity, R.string.suggest_connect, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);		           
    	toast.show();
    }
    
    public void updateAlarm() {
    	if(mConnectionState!=STATE_CONNECTED){
    		noticeForConnection();
    	    backgroundSyncAndDisconnect(new Runnable(){
    	       	@Override
    	       	public void run()
    	       	{       			
    	       		writeAlarm();
    	       	}});
    	    return;
    	}
    	writeDailyGoal();
    }
    public void writeAlarm(){    	
   	 if(mConnectionState!=STATE_CONNECTED) return;
   	 
   	 if(gAlarmCharacteristic==null) return;
   	
   	 int alarm=BinsDBUtils.activeDB().getActiveUserAlarm();

   	 final byte[] bytes = new byte[4];

    	 DataSyncFormat.bleSet16(bytes, alarm);
    	 gAlarmCharacteristic.setValue(bytes);
    	 writeCharacteristic(gAlarmCharacteristic);        
   }
    
    public void writeShakeUiThreshold(){
    	
    	if(mConnectionState!=STATE_CONNECTED) return;
    	
    	 int shakeUiThresh=BinsDBUtils.activeDB().getActiveShakeUiThreshold();
    	 final byte[] bytes = new byte[4];
    	 
         int deviceConfig=BinsDBUtils.activeDB().getBinsConfiguration();
         
     	 deviceConfig &= 0xFFF0;

     	 deviceConfig=setShakeThresholdToConfig(shakeUiThresh, deviceConfig);
         
         DataSyncFormat.bleSet16(bytes, deviceConfig);
         gConfigurationCharacteristic.setValue(bytes);    	
         writeCharacteristic(gConfigurationCharacteristic);
    }
  
    public void setBinsDailyStepReset()
    {    	   	
      	// 2015.01.13
    	if(mConnectionState!=STATE_CONNECTED){
    		noticeForConnection();    		
     		backgroundSyncAndDisconnect(new Runnable(){
       		    @Override
       		    public void run()
       		    {       			
       		    	toggleBinsDailyStepReset();
       		    }});    			   
    		return;
    	}
    	toggleBinsDailyStepReset();
   	
   	}
    
    public void toggleBinsDailyStepReset()
    {
    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
    	
    	boolean enabled=BinsDBUtils.activeDB().getActiveUserStepWatchEnabled();
    	if(enabled==true){
    		BinsDBUtils.activeDB().setActiveUserStepWatchEnable(false);
            gConfiguration |= BinsDBUtils.BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK; // 11.19, it must be set 
            gConfiguration |= BinsDBUtils.BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK; // 01.09, additional flag to reset it
    	} else {
    		BinsDBUtils.activeDB().setActiveUserStepWatchEnable(true);  
            gConfiguration |= BinsDBUtils.BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK; // 11.19, it must be set 

            long startTime=(System.currentTimeMillis() / 1000 / 60);
            //-- Log.i(TAG, "new stepWatchStartTime="+startTime);
            BinsDBUtils.activeDB().setActiveUserStepWatchStartTime(startTime); 
    	}
    	
		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
		
 		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);    	
    }
 
    
    public void toggleBackgroundSync()
    {    	
    	int enable=BinsDBUtils.activeDB().getBackgroundSync();
    	    	
    	if(enable==0) enable=1;
    	else enable=0;
    	
    	BinsDBUtils.activeDB().setBackgroundSync(enable);
    	
   }
    
    public void setBinsLock()
    {    	
    	
    	gConfiguration=getConfig();
    	
    	gConfiguration &= 0xFFF0;
  
        gConfiguration |= BinsDBUtils.BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK; 

    	if(mConnectionState!=STATE_CONNECTED){
    		updateBinsConfiguration(gConfiguration & 0x000F);
    		return;
    	}

    	
		byte[] bytes = new byte[2];
		bytes[0]=(byte)(gConfiguration & 0x00FF);
		bytes[1]=(byte)((gConfiguration & 0xFF00)>>8);
		
 		gConfigurationCharacteristic.setValue(bytes);    	
		writeCharacteristic(gConfigurationCharacteristic);    	
    }
    
    /* Following are the heart rate related functions
     * 
     */
    
    int hrdViewUpdateCyclicCounter=0;
    Boolean hadHeartRateResult=false;
    
   	float gLastMemsData [] = new float[] {0, 0, 0};
   	Boolean gHadPulseResultLastTime=false;
   	
   	Boolean gIsNewFirmwareForPulse;
   	
    //-- for heart rate
    public void updateHeartrateStream(final byte[] txValue)
    {    
    	  //-- 09.03
    	 
    	  mActivity.runOnUiThread(new Runnable() {
    	  public void run() {
            try {
           	float mems_data [] = new float[] {0, 0, 0};
           	char ppg_data [] = new char [20];
           	// byte ppg_data[] = new byte[20];
           	

         	//-- Log.i(TAG, "MEMS="+txValue[13]+":"+txValue[14]+":"+txValue[15]+":"+txValue[16]+":");

         	//-- Log.i(TAG, "MEMS="+txValue[13]+":"+txValue[14]+":"+txValue[15]+":"+txValue[16]+":"+txValue[17]+":"+txValue[18]);
 /*          	
         	Log.i("BINS-HR", String.format("%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x",
         									txValue[0],txValue[1],txValue[2],txValue[3],txValue[4],txValue[5],
         									txValue[6],txValue[7],txValue[8],txValue[9],txValue[10],txValue[11],
         									txValue[12])
         		);
 */
            	for(int i=0; i<(13); i++)
            		ppg_data[i] = (char)txValue[i];
/*        	
         	for(int i=0; i<(3); i++){
         		short gdata;
          		gdata= (short) (txValue[13+i*2] + (txValue[13+i*2+1]<<8));
          		gdata=(short) (gdata>>2);
          		mems_data[i]=gdata;
         	}
*/

           	           	
/*           	
        	Log.i("BINS-HR", 
 							mems_data[0]+","+
 							mems_data[1]+","+
 							mems_data[2]
 							);
*/           	
         	        	
        	
        	
/*
    		if(heartRateDetectionSessionCounter<60){
        		heartRateDetectionSessionCounter++;
        		Log.i("BINS-HR","Detection bypass");
    			return;
    		}
*/    		

         	for(int i=0; i<(3); i++){
         		short gdata;
          		gdata= (short) (txValue[13+i*2] + (txValue[13+i*2+1]<<8));
          		gdata=(short) (gdata);
          		gLastMemsData[i]=gdata;
         	}

         	//-- 09.08
       		if(hrdViewUpdateCyclicCounter==0){
       			gIsNewPulseSession=true;
       			
               	
                if(getBinsFirmwareRevisionMajor()>3 ||
                		(getBinsFirmwareRevisionMajor()==3 && getBinsFirmwareRevisionMinor()>=5) ){
                	gIsNewFirmwareForPulse=true;
                } else {
                	gIsNewFirmwareForPulse=false;
                }
                

       			openHeartrate();
       		}
    		//-- 08.20
         	//-- observed that, without mems data, it is easier a lot to fix to the result and sooner.
         	//-- if fixed already, it is easy to keep the pulse result continuously
         	//-- solution is not to use MEMS until
          	
       		int pdata [] = new int [20];
        	for(int i=0; i<(13); i++)
        		pdata[i] = (int) (txValue[i] & 0xFF);

          	/* 
        	Log.i("BINSHR", String.format("PPG_GSENSOR_RAW_DATA, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
						(byte)ppg_data[0],(byte)ppg_data[1],(byte)ppg_data[2],(byte)ppg_data[3],(byte)ppg_data[4],(byte)ppg_data[5],
						(byte)ppg_data[6],(byte)ppg_data[7],(byte)ppg_data[8],(byte)ppg_data[9],(byte)ppg_data[10],(byte)ppg_data[11],
						(byte)ppg_data[12],(int)gLastMemsData[0],(int)gLastMemsData[1],(int)gLastMemsData[2]));
			*/
        	
        	/* 150320
        	 * When let hand down at static state, X absolute data must be largest.
        	 * When let hand horizontal on the table at static state, Z absolute value must be largest.
        	 */
        	/*
        	{
        		float temp;
        		temp=gLastMemsData[0];
        		gLastMemsData[0]=gLastMemsData[1];
        		gLastMemsData[1]=temp;
        	}
        	*/
        	/*
        	Log.i("BINSHR", String.format("PPG_GSENSOR_RAW_DATA, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
						(int)pdata[0],(int)pdata[1],(int)pdata[2],(int)pdata[3],(int)pdata[4],(int)pdata[5],
						(int)pdata[6],(int)pdata[7],(int)pdata[8],(int)pdata[9],(int)pdata[10],(int)pdata[11],
						(int)pdata[12],(int)gLastMemsData[0],(int)gLastMemsData[1],(int)gLastMemsData[2]));
			*/
        	
			

        	// Log.i("BINS-HR", String.format("%d,%d",(int)ppg_data[0],(int)ppg_data[1]));
        	/*
        	if(ppg_data[9]>80){
        		ppg_data[9]=80;
            	Log.i("BINSHR", String.format("PPG-1, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
    						(int)pdata[0],(int)pdata[1],(int)pdata[2],(int)pdata[3],(int)pdata[4],(int)pdata[5],
    						(int)pdata[6],(int)pdata[7],(int)pdata[8],(int)pdata[9],(int)pdata[10],(int)pdata[11],
    						(int)pdata[12],(int)gLastMemsData[0],(int)gLastMemsData[1],(int)gLastMemsData[2]));

        	} else {
        		// -- Log.i("BINSHR2", String.format("PPG, %d", (int)pdata[0]));
        	}
        	*/
        	
         	if(gHadPulseResultLastTime==false)
         	{
         		iHR = PXIALGMOTION.Process(ppg_data, mems_data);
         	}else
         	{
         		iHR = PXIALGMOTION.Process(ppg_data, gLastMemsData);
         	}
 
         	//-- 09.08 HRV
        	{
        		int [] data = new int[1];
        		int touch_flag = (txValue[11] == (byte)0x80) ? 1 : 0; 	//-- 09.08, touch flag
		  		data[0]= (int)txValue[4]&0xff ;
		  		data[0]= (data[0] << 8) + ((int)txValue[3]&0xff) ;
		  		data[0]= (data[0] << 8) + ((int)txValue[2]&0xff) ;
		  		data[0]= (data[0] << 8) + ((int)txValue[1]&0xff) ;
		  		
        		PXIALG.SetData(touch_flag, 0, data, 1);

        	}

           	
   			//-- g_dDisplayBuffer = PXIALGMOTION.GetDisplayBuffer();
            	
         	// Log.i(TAG, "Heart Rate=" + String.valueOf(iHR) +":"+txValue[13]+":"+txValue[14]+":"+txValue[15]+":"+txValue[16]+":");
        	// Log.i(TAG, "Heart Rate=" + String.valueOf(iHR) +":"+mems_data[0]+":"+mems_data[1]+":"+mems_data[2]);
            // Log.i(TAG, "Heart Rate=" + String.valueOf(iHR) + "ver:" + PXIALGMOTION.GetVersion());
                     	
       		iStableTime = PXIALGMOTION.DrvGetStableTime();
       		iReadyFlag = PXIALGMOTION.GetReadyFlag();
       		iMotionFlag = PXIALGMOTION.GetMotionFlag();
       		iTouchFlag = PXIALGMOTION.GetTouchFlag();
       		//-- Log.i(TAG, "HRD stable:ready:motion:touch=" + String.valueOf(iStableTime)+":"+String.valueOf(iReadyFlag)+":"+String.valueOf(iMotionFlag)+":"+String.valueOf(iTouchFlag));

       		// 08.20
         	if(iHR>0 && iReadyFlag==1){
         		gHadPulseResultLastTime=true;
         	}
         	else
         	{
         		gHadPulseResultLastTime=false;         		
         	}
           	
      		
       		if((hrdViewUpdateCyclicCounter%20)==0){
       			
       			
   				if(hrdViewUpdateCyclicCounter>250){	//-- 20151108
   					if( (iHR>55 && iHR<110) ||
   						(iHR>45 && iHR<180 && iReadyFlag==1))
   					{
           				mDataSyncModel.insertHeartRateRecord(iHR);       					
       				}
       			}       			
   				updateHeartRateView(iHR,iReadyFlag, hrdViewUpdateCyclicCounter/20 );
   				
   				if(iHR>0 &&
   						(
   						   (gIsNewFirmwareForPulse==true && (gIsNewPulseSession==true || hrdViewUpdateCyclicCounter%300==0)) ||
   						   (gIsNewFirmwareForPulse==false && (gIsNewPulseSession==true || hrdViewUpdateCyclicCounter%100==0)) 
   						)
   					)
   				{  //-- 150603 to reduce the overload from 100 to 300
   					
   					gIsNewPulseSession=false; //-- 150603 mark to have it already
   					
   					byte[] bytes = new byte[1];
   					if(iHR>0 && iReadyFlag==1){
   						bytes[0]=(byte)(iHR);   						
   	   					gHeartRateUpdateCharacteristic.setValue(bytes);    	
   	   					writeCharacteristic(gHeartRateUpdateCharacteristic);
   	   					//-- Log.i(TAG, "HRD writeback");
   					}else if(iHR>0 && iReadyFlag==0){
   						bytes[0]=(byte)(iHR);   						
   	   					gHeartRateUpdateCharacteristic.setValue(bytes);    	
   	   					writeCharacteristic(gHeartRateUpdateCharacteristic);
   					}else{
   						bytes[0]=0;
   						//-- no need to write to BiNS
   					}
   				}

   				
   				//-- may changed for SDNN measuring
        		iHR = PXIALG.GetHR(); 
        		
        		//-- 09.08
        		
        		if(hrdViewUpdateCyclicCounter==0){
        			//-- Log.i(TAG, "SDNN Button Start");
        	 		mSdnnStartButtonView.setVisibility(View.VISIBLE); 
        	    	mSdnnMessageView.setVisibility(View.GONE);
        	    	mSdnnStressTitleView.setVisibility(View.GONE);	    	
        			mSdnnStartButtonView.startMeasure();
    				mSdnnStartButtonView.setProgress((int)0, 200);
        			mSdnnStartButtonView.setData(PXIALG.GetHR());
        		}
        		else{
        			//-- Log.i(TAG, "SDNN Button Continue");

        			mSdnnStartButtonView.setData(PXIALG.GetHR());
        			if(PXIALG.GetHRReadyFlag() != 0)
        				mSdnnStartButtonView.setStable(true);
        			else
        				mSdnnStartButtonView.setStable(false);
            	
        			float hrv_ready = PXIALG.GetHRVReadyFlag();
        			if (hrv_ready<200){
        				//-- Log.i(TAG,"SDNN GetHRVReadyFlag:"+hrv_ready);
        				mSdnnStartButtonView.setProgress((int)hrv_ready, 200);
        			}
        			else{
        				mSdnnStartButtonView.setProgress((int)hrv_ready, 200);

        				//-- mSdnnStartButtonView.stopMeasure();
        				PXIALG.HRVProcess();
        				final int sdnn_level = PXIALG.GetStress();
        				//-- Log.i(TAG,"SDNN sdnn_level:"+sdnn_level);
            		
        				switch(sdnn_level)
        				{
            				case 0:
            				ShowSDNN(getResources().getString(R.string.sdnn_level1_comment_1st));
            				break;
            				case 1:
            				ShowSDNN(getResources().getString(R.string.sdnn_level2_comment_1st));
            				break;
            				case 2:
            				ShowSDNN(getResources().getString(R.string.sdnn_level3_comment_1st));
            				break;
            				case 3:
            				ShowSDNN(getResources().getString(R.string.sdnn_level4_comment_1st));
            				break;
        				}
        			}	
            	}

    			
/*       			
       			if(iHR>0 && iReadyFlag==1){
       				
       				mDataSyncModel.insertHeartRateRecord(iHR);
       				hadHeartRateResult=true;
    
       				updateHeartRateView(iHR,iReadyFlag, hrdViewUpdateCyclicCounter/20 );
       			}
       				
      			}else{
       				updateHeartRateView(0, iReadyFlag, hrdViewUpdateCyclicCounter/20);       				
       			}
*/       			

       		}   
       		
       		hrdViewUpdateCyclicCounter++;
       		
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
          }
    	});
    }
    
    public Boolean isHeartRateStreaming() {
    	if(hrdViewUpdateCyclicCounter>0){
    		return true;
    	}
    	return false;
    }
    
    Timer mHeartRateViewTimeoutTimer=null;
	
    public void updateHeartRateView(int currentHR, int statusReady, int timeShift)
    { 	
    	//-- Log.i(TAG,"updateHeartRateView");
    	
    	if(statusReady==1){
    		mHeartRateTextView.setTextColor(Color.BLACK);
    	}
    	else{  
    		//-- mHeartRateTextView.setTextColor(getResources().getColor(android.R.color.holo_orange_dark)  );
    		mHeartRateTextView.setTextColor(Color.rgb(0xCD, 0x32, 0x78));
    	}
   	
    	if(currentHR==0){
        	mHeartRateLayoutView.setVisibility(View.VISIBLE); 
    		mHeartRateTextView.setVisibility(View.GONE);
     		mHeartRatePPMView.setVisibility(View.GONE);
    		mHeartRateMessageView.setVisibility(View.VISIBLE);     
    		mHeartRateMessageView.setText(getResources().getString(R.string.reading_hrd_data)+" : "+timeShift);    		
    	}
    	else  
    	{
    		hadHeartRateResult=true;
    		
    		//-- Log.i(TAG, "updateHeartRateView HRD"); 
    		
     		mHeartRatePPMView.setVisibility(View.VISIBLE);
    		mHeartRateTextView.setVisibility(View.VISIBLE);
    		mHeartRateTextView.setText(String.valueOf(currentHR));
    		mHeartRateLayoutView.setVisibility(View.VISIBLE);
    		mHeartRateMessageView.setVisibility(View.GONE);     		   
    	}	  
		
   	    TimerTask mHeartRateViewTimeoutTimerTask = new TimerTask()
	    {  
	    	@Override
			public void run()
			{
	         	mActivity.runOnUiThread(new Runnable() {
	                @Override
	                public void run() {
	    	    		Log.i(TAG, "HRD View Timeout"); 
	    	    		
	    	        	//-- 07.31 mHeartRateLayoutView.setVisibility(View.GONE);	      	    		
	      	    		mHeartRateMessageView.setText(getResources().getString(R.string.message_press_check_pulse)); 
	     	    		mHeartRateMessageView.setVisibility(View.VISIBLE); 
	    	    		mHeartRateTextView.setVisibility(View.GONE);
                 		mHeartRatePPMView.setVisibility(View.GONE);
                 		
                 		mSdnnStartButtonView.setBackgroundResource(R.drawable.upload_rate_start_button_press);
                 		//-- 09.21 
                 		mSdnnStartButtonView.setVisibility(View.GONE);
                 		PXIALG.Close();
	    	    		
	    	    	
	    	        	if(hrdViewUpdateCyclicCounter>180 && hadHeartRateResult==false)
	    	        	{
/*	 08.05 ui optimizing, not good for using   	        		
	    	        		Toast toast;
	    	        		toast=Toast.makeText(getActivity(), R.string.toast_hrd_retry, Toast.LENGTH_LONG);
	    	        		toast.setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL, 0, 0);
	    	        		LinearLayout toastLayout = (LinearLayout) toast.getView();
	    	        		TextView toastTV = (TextView) toastLayout.getChildAt(0);
	    	        		toastTV.setTextSize(18); 
	    	        		toast.show();
*/	    	        		
	    	        	}
	    	        	hrdViewUpdateCyclicCounter=0;
	    	        	hadHeartRateResult=false;
	    	        	mRunningHeartrateFunction=false;
	                }
	           	});
			}
    		
		};
		
		if(mHeartRateViewTimeoutTimer!=null){
        	mHeartRateViewTimeoutTimer.cancel();            	
        }   		
		
		mHeartRateViewTimeoutTimer= new Timer();
		
		mHeartRateViewTimeoutTimer.schedule(mHeartRateViewTimeoutTimerTask, 3000); //-- 04.05
 
    }

    public void updateActivityViews() {
        
        mDoNotCheckMentalEffect=true;  //-- 150825
        onEventReceive(ACTION_DATA_NOTIFY, mSucceed);

    }
    public void endRead() {
        // Data read end
        Log.d(TAG, "endRead ..");
        mSucceed = true;

        // if reach here, will be ok to continue the rest.
        if(mFinderIsRunning==true && mFinderJustCalled==true) {  // mFinderJustCalled means the first time reached here, to setup in this case
        	mFinderJustCalled=false;
        	                 
        	setupFinderTimer();
        }	

    }

    public void meetError() {
        Log.e(TAG, "meetError ...");
        
        if(mIsPeriodicScanning==true) mIsPeriodicScanning=false; //-- 09.12
        
        if(isAdded()==false) return;	// 12.17 07.07  in case, cannot get the activity
           
        if(mConnectionState!=STATE_CONNECTING){	// if connecting, means not found, no specially popup required.
        	Toast.makeText(mActivity, R.string.sync_problem, Toast.LENGTH_LONG).show();
        } else { // 12.12 means error found during connecting, no connection yet, reset it
        	mConnectionState=STATE_DISCONNECTED;	
        }
                 
        onEventReceive(ACTION_DATA_SYNC_END, false);
 
    }


    
    /* Following routines for Message utilities to update different stages of sync running
     * 
     */

    
    private void sendDataSyncStartBroadcast() {
    	// Log.d(TAG, "sent data sync start event DATA_SYNC_START !!!!");
    	
        onEventReceive(ACTION_DATA_SYNC_START, true);
    }

    private void sendDataSyncEndBroadcast() {
       	// Log.d(TAG, "sync stop broadcast");
        onEventReceive(ACTION_DATA_SYNC_END, mSucceed);
    }
   
    
    private void sendFinderLostBroadcast() {
    	// Log.d(TAG, "finder lost message");
    	onEventReceive(ACTION_FINDER_LOST, mSucceed);
    }
    
    
//-----------------------------------------------------------------------------------------
    
    /* start of Fragment related function
     * 
     */
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        //-- 08.28, otherwise, sometimes it will duplicate sync task, generate error...
        if(syncIsActive()==true){
        	// Log.i(TAG, "onAttach to quit directly");
        	return;	// not the first and the sync is running
        }
        
        //--**
        mActivity=activity;	// 12.17
        activeBinsFragment=this;  //-- for use from other fragment
          
        // Initializes a Bluetooth adapter. For API level 18 and above, get a
        // reference to BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        mUpdating= false; // always false;
        mBluetoothAdapter = bluetoothManager.getAdapter();
/* 09.17        
        if (!mUpdating && mDeviceAddress != null && mBluetoothAdapter.isEnabled()) {
            mHandler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    //-- no need to scan first, 
                	//-- scanLeDevice(true);
                	Log.d(TAG, "onAttach, before call startDataSync");
                	
                    gFinderSetting=BinsDBUtils.getBluetoothLeDao(getActivity()).getFinderSetting();
                    
                    newDataSyncSession();	// replaced on 09.17
                    
                }  
            }, 500);
        }
*/
        //-- 12.07 moved codes to base fragments
        /*
    	if(mIntentFilter==null) {
    		mIntentFilter = new IntentFilter();
            mIntentFilter.addAction(ACTION_DATA_SYNC_START);
            mIntentFilter.addAction(ACTION_DATA_SYNC_END);
            mIntentFilter.addAction(ACTION_FINDER_LOST);
            mIntentFilter.addAction(ACTION_DATA_NOTIFY);
            //-- 04.04
            mIntentFilter.addAction(ACTION_DATA_SYNC_RUNNING);
            mIntentFilter.addAction(ACTION_DATA_SYNC_STOP);
            getActivity().registerReceiver(mSyncReceiver, mIntentFilter);
    	}
        getActivity().registerReceiver(mSyncReceiver, mIntentFilter);
         */
    	if(mSmsIntentFilter==null){
    		mSmsIntentFilter = new IntentFilter();
            //-- 09.20
            mSmsIntentFilter.addAction(ACTION_SMS_RECEIVE);

    	}
        mActivity.registerReceiver(mSmsReceiver, mSmsIntentFilter);	//-- 09.20

    	if(mMmsIntentFilter==null){
    		mMmsIntentFilter = new IntentFilter();
            //-- 09.20
            mMmsIntentFilter.addAction(ACTION_MMS_RECEIVE);
     	}
        mActivity.registerReceiver(mMmsReceiver, mMmsIntentFilter);	//-- 09.20

       	if(mCallIntentFilter==null){
    		mCallIntentFilter = new IntentFilter();
            //-- 09.20
            mCallIntentFilter.addAction(ACTION_CALL_RECEIVE);
     	}
        mActivity.registerReceiver(mCallReceiver, mCallIntentFilter);	//-- 09.20
        
 /*       
        IntentFilter filter1 = new IntentFilter(BluetoothDevice.ACTION_ACL_CONNECTED);
        IntentFilter filter2 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        IntentFilter filter3 = new IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        activity.registerReceiver(mBluetoothReceiver, filter1);
        activity.registerReceiver(mBluetoothReceiver, filter2);
        activity.registerReceiver(mBluetoothReceiver, filter3);
*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "bins Fragment onDetach ...");
        
        try {
        	//-- getActivity().unregisterReceiver(mSyncReceiver);	//-- 12.07
        	mActivity.unregisterReceiver(mSmsReceiver);	//-- 12.07
        	mActivity.unregisterReceiver(mMmsReceiver);	//-- 12.07
        	mActivity.unregisterReceiver(mCallReceiver);
        /*        
        getActivity().unregisterReceiver(mBluetoothReceiver);
        getActivity().unregisterReceiver(mBluetoothReceiver);
        getActivity().unregisterReceiver(mBluetoothReceiver);
        */
        } catch (Exception e) {}  //- 2015.01.07
        
    }
    
   /* 20151230
   // -- not work ! 12.13 
   //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            }
            else if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
             }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {            
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
 
           }           
        }
    };
    */
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ...");
        datePickerDialog = new DatePickerDialog(getActivity(), datePickerListener,2000, 1,1);

        return inflater.inflate(R.layout.fragment_bins, container, false);
        
        
    }
    
    private DatePickerDialog.OnDateSetListener datePickerListener 
    = new DatePickerDialog.OnDateSetListener() {
        
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            mTimeControllerView.setDate(newDate);
        }

    };
    
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    	Log.i(TAG, "onViewCreated");
    	
        super.onViewCreated(view, savedInstanceState);
        mTimeControllerView = (TimeControllerView) view.findViewById(R.id.time_controller);
        mTimeControllerView.setOnDateChangedListener(mListener);
        mFinderRunningIndicator= (ImageView) view.findViewById(R.id.bins_finder_indicator);
        mFinderRunningIndicator.setVisibility(View.GONE);
        
        mTimeControllerView.setOnClickListener(onClickListener);					  
               
        setupDeviceSyncViews(view);
        setupMentalEffectViews(view); //-- 150615
        
		mHandler.sendEmptyMessage(MSG_REFRESH_MENTAL_EFFECT);
      
        setupSteps(view.findViewById(R.id.steps));
        setupDistances(view.findViewById(R.id.distances));
        setupActive(view.findViewById(R.id.active_minutes));
        setupCalories(view.findViewById(R.id.calories));
        setDeviceName();
        
        //-- Heart rate
        mHeartRateTextView=(TextView)view.findViewById(R.id.device_heartrate_value);
       	//-- Log.i(TAG,"onViewCreated mHeartrateLayoutView assigned");
        
        mHeartRateLayoutView=(RelativeLayout)view.findViewById(R.id.device_heartrate);
        //-- initial       
        mHeartRateLayoutView.setOnClickListener(onClickListener);
        mHeartRateMessageView=(TextView)view.findViewById(R.id.device_heartrate_message);
        mHeartRatePPMView=(TextView)view.findViewById(R.id.textview_message_ppm);
 		mHeartRatePPMView.setVisibility(View.GONE);	//-- 08.20
 		mHeartRateTextView.setVisibility(View.GONE);
 		      
 		//-- 09.08
 		mSdnnStartButtonView = (ProgressButton)view.findViewById(R.id.imageButtonSdnn);  
 		mSdnnMessageView = (TextView)view.findViewById(R.id.textview_message_sdnn);
 		mSdnnStressTitleView = (TextView)view.findViewById(R.id.textview_stress_level);
 		//-- 09.21
 		mSdnnStartButtonView.setVisibility(View.GONE); 
 		//-- 09.21 
 		mSdnnMessageView.setVisibility(View.GONE); 
 		mSdnnStressTitleView.setVisibility(View.GONE);
 		
        mWorkingProgress=(ProgressBar)view.findViewById(R.id.progressBinsWorking);        
        mWorkingProgress.setVisibility(View.GONE);
 
 		
    	Log.i(TAG, "onViewCreated"); 
        
    	//-- 08.12       
        
        checkUserChanged();
        checkDeviceChanged();
        
        if(mBinsType==0){
        	//-- Log.i(TAG,"onViewCreated mHeartrateLayoutView GONE");
        	mHeartRateLayoutView.setVisibility(View.GONE);
        }
        else if(mConnectionState!=STATE_CONNECTED){
           	//-- Log.i(TAG,"onViewCreated !=STATE_CONNECTED mHeartrateLayoutView GONE");
        	mHeartRateLayoutView.setVisibility(View.GONE);        	
        }
        else{
           	//-- Log.i(TAG,"onViewCreated connected mHeartrateLayoutView VISIBLE");
            
    		mHeartRateTextView.setVisibility(View.GONE);
    		mHeartRatePPMView.setVisibility(View.GONE);
    		mHeartRateMessageView.setVisibility(View.VISIBLE);
			mHeartRateMessageView.setText(getResources().getString(R.string.message_press_check_pulse)); 
        	mHeartRateLayoutView.setVisibility(View.VISIBLE);

        }
            	
        view.findViewById(R.id.device_sync).setOnClickListener(onClickListener);
        mSdnnStartButtonView.setOnClickListener(onSdnnButtonClickListener);
        
        // view.findViewById(R.id.device_sync).setBackgroundResource(drawable.list_selector_background);
        
        // view.findViewById(R.id.device_sync).setBackgroundResource(R.drawable.tableviewcell_background);
        // 12.14   
    }
            
    
    String strMeter="AA";
    String strMinutes="33";
    String strKcal="Kcal";

    @Override
    public void onStart() {
    	Log.i(TAG, "onStart");
        strMeter=(getResources().getString(R.string.meter));
    	strMinutes=(getResources().getString(R.string.minutes));
        strKcal=(getResources().getString(R.string.kcal));

        //-- 12.07 move the registerReceiver to base fragment

        super.onStart();
        
        Log.i(TAG, "before onStart");
        
        //-- 150830 initialize it
        mBinsType=BinsDBUtils.activeDB().getActiveDeviceType();
        
        startQuery();
        
        int userID=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserID();
        gFinderSetting=BinsDBUtils.getBluetoothLeDao(getActivity()).getUserFinderSetting(userID);
        //--**
        /* 11.14 
        if(gFinderSetting!=0 && finderIsActive()==false) {
        	int device=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveDeviceID();
        	if(device>0){	//-- means a device is assigned
        		Log.d(TAG, "Finder start");
        		startFinder(getActivity());
        	}
        }
        else{
        	Log.d(TAG, "Finder to keep as it was");       	
        }
        */
        //syncViews(DataSyncService.sDataSyncService != null);
        
        //-- 08.14 make sure it to hide
    	if(syncIsActive()==false){
    		if(mHeartRateLayoutView!=null)
    		{ 
    	       	//-- Log.i(TAG,"onStart mHeartrateLayoutView GONE");
    	        
    			mHeartRateLayoutView.setVisibility(View.GONE);
    		}
    		else{
    			//-- Log.i(TAG, "not set the mHeartRateLayoutView, fix it");
    		}
    	}
        
        if(mPeriodicSyncTimer!=null)
        	return;
        
        Log.i(TAG, "Create periodic sync timer");
        
        /* 150321
         * Disable periodic sync, only sync when app resumed
         * To reduce the app overhead
         * To reduce the app complexity
         */
        if(mPeriodicSyncTimer==null) return;
        
        // NEVER reach following code 
        
        mPeriodicSyncTimer = new Timer();
        TimerTask periodicTimerTask= new TimerTask()
	    {
	    	@Override
			public void run()
			{
	        	Log.i(TAG, "Sync Timer:"+mCountFromLastSync);
	        	
	        	if(isConnectAndNotifying()==true || 
	        			BinsDBUtils.activeDB().getBackgroundSync()==0){ // 12.07 disable background mode
	        		mCountFromLastSync=0;	//-- reset it

	        		return;
	        	}
	          	
	        	//-- Log.i(TAG, "Sync Timer count :"+mCountFromLastSync+":"+mIsPeriodicScanning+":"+mConnectionState);
	        		        	
	        	if(mCountFromLastSync>=5)	
	        	{
	        		backgroundSyncAndDisconnect();
	        		mCountFromLastSync=0;  // 12.13 every n minutes
	    	    } else{
	    	    	mCountFromLastSync++;
	    	    }
			}
		};
		
		resetPeriodicSyncTimer();
        if(mPeriodicSyncTimer!=null){
        	mPeriodicSyncTimer.schedule(periodicTimerTask,  90000, 90000); //-- 09.02 check every 90 seconds
        }
 
    }

	private void resetPeriodicSyncTimer() {
		mCountFromLastSync=0; //-- start with 0 for next time
		mIsPeriodicScanning=false;
	}
	
	public void backgroundSyncAndDisconnect(Runnable completionRoutine) {
		endSyncCompletionRoutine=completionRoutine;
		backgroundSyncAndDisconnectInside();
	}
	public void backgroundSyncAndDisconnect() {
		endSyncCompletionRoutine=null;
		backgroundSyncAndDisconnectInside();	
	}	
    public void backgroundSyncAndDisconnectInside() {

    	if(mConnectionState==STATE_DISCONNECTED)
    	{		    	    		
	    	//-- final Activity mActivity=getActivity();
	    	if(mActivity!=null){
          	  	mActivity.runOnUiThread(new Runnable() {
          	  		@Override
                	public void run() {
	    	    		mIsPeriodicScanning=true; // 09.17 moved from upper level
	    	    		mIsConnectOnly=false; // 150712
		    	    	mFinderJustCalled=false;
		    	    	mIsSyncSession=false;	// it is NOT an active sync session
    	            	if(connectPeriodic()==false) {	//-- special handling first for background sync
    	            		mIsPeriodicScanning=false;	// 09.01, reset if not actually run sync
    	            	}
                	}
          	  	});
	    	}

    	}
    }
    
    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, " binsFragment onResume");
        //--**
        if(finderIsActive()==false){
        	mFinderRunningIndicator.setVisibility(View.GONE);
           	mFinderRunningIndicator.setAnimation(null);	
                   	
        }																																															
        else{
            Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.refresh);
        	mFinderRunningIndicator.startAnimation(animation);	
        	mFinderRunningIndicator.setVisibility(View.VISIBLE);
        }
        setDeviceName();
       
    	if(mConnectionState==STATE_CONNECTED) {
    		mDeviceSyncLayout.setBackgroundColor(getResources().getColor(R.color.bins_connected_color));
        	
    	} else {
    		//-- mDeviceSyncLayout.setBackgroundColor(colorOrigin);  
    		mDeviceSyncLayout.setBackground(syncCellBackgroundOrigin);
    		
    	}
		
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mScanning) {
            scanLeDevice(false);
        }
        // getActivity().unregisterReceiver(mSyncReceiver);
        // getActivity().unregisterReceiver(mSmsReceiver);	//-- make it responsible at background 09.20
        // getActivity().unregisterReceiver(mMmsReceiver);	//-- 09.20

    }

       
    private void setDeviceName() {
    	
    	
    	Log.d(TAG, "setDeviceName");
    	
       	String userName=BinsDBUtils.activeDB().getActiveUserName();
    	int device=BinsDBUtils.activeDB().getActiveDeviceID();


        if (device==0) {	// device is not assigned yet
        	
            mDeviceSyncLayout.setVisibility(View.VISIBLE);
            //-- final String deviceName = getResources().getString(R.string.device_not_assigned);

            //-- 20150821 mDeviceSyncName.setText(deviceName);
            mDeviceSyncTime.setText(getResources().getString(R.string.press_to_setup));
        	
        }
        if(userName==null){
        	mDeviceSyncLayout.setVisibility(View.VISIBLE);
            String displayName;
           	displayName=getResources().getString(R.string.device_not_assigned); 
            mDeviceSyncName.setText(displayName);
        }else{
        	mDeviceSyncLayout.setVisibility(View.VISIBLE);         	
            String displayName;
            if(userName.length()>DISPLAY_NAME_LENGTH_MAX){ 
            	displayName= userName.substring(0, DISPLAY_NAME_LENGTH_MAX);
                displayName= displayName+"..";
            }
            else{
               	displayName= userName;            	
            }   
            
            mDeviceSyncName.setText(displayName);
            
            if(device!=0){
            	if (mUpdating) {
            		mDeviceSyncBar.setVisibility(View.VISIBLE);
            		mDeviceSyncTime.setText(R.string.sync_now);
            	} else {
            		showSyncTimeMessage();
            	}
            }
        }
    }  

    private void setupMentalEffectViews(View view) {
    	mMentalEffectLayoutView= (RelativeLayout) view.findViewById(R.id.device_mental_effect);
    	mMentalEffectValue = (TextView) view.findViewById(R.id.device_mental_effect_value);
       	mMentalEffectLayoutView.setVisibility(View.VISIBLE);
       	mMentalEffectProgress=(ProgressBar) view.findViewById(R.id.device_mental_effect_progress);
       	mMentalEffectLayoutView.setOnClickListener(onClickListener);
    }

    private void setupDeviceSyncViews(View view) {
    	mDeviceSyncLayout= (RelativeLayout) view.findViewById(R.id.device_sync);
    	//-- colorOrigin=Color.rgb(245, 245, 245);
    	if(colorOrigin==0){
    		syncCellBackgroundOrigin=view.findViewById(R.id.device_sync).getBackground();
				
    	    colorOrigin=Color.rgb(245, 245, 245);
    	}
    	mDeviceSyncName = (TextView) view.findViewById(R.id.device_sync_name);
        mDeviceSyncTime = (TextView) view.findViewById(R.id.device_sync_time);
        mDeviceSyncBar = (ProgressBar) view.findViewById(R.id.device_sync_progress);
        mButtonBypass = (Button) view.findViewById(R.id.buttonSyncBypass);
        
        mButtonBypass.setOnClickListener(onClickListener); 

        mSyncProgress = (ProgressBar)view.findViewById(R.id.syncProgress);
        //.setOnClickListener(mScanListener);
        mSyncProgress.setVisibility(View.GONE);
    }

    /*private OnClickListener mScanListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (mBluetoothAdapter.isEnabled()) {
                scanLeDevice(!mScanning);
            }
        }
    };*/
  
    final float DEFAULT_PROGRESSBAR_HEIGHT = 0.34f;  
    private void setupSteps(View view) {
        mTitleSteps = (TextView) view.findViewById(R.id.card_title);
        mTitleSteps.setText(R.string.title_steps);
        mIconSteps = (ImageView) view.findViewById(R.id.card_icon);
        mIconSteps.setImageResource(R.drawable.ic_steps_hanson);
        mCurrentSteps = (TextView) view.findViewById(R.id.card_current);
        mMaxSteps = (TextView) view.findViewById(R.id.card_max);
        mProgressSteps = (ProgressBar) view.findViewById(R.id.card_progress);
        mProgressSteps.setScaleY(DEFAULT_PROGRESSBAR_HEIGHT);
    }

    private void setupDistances(View view) {
        mTitleDistances = (TextView) view.findViewById(R.id.card_title);
        mTitleDistances.setText(R.string.title_distances);
        mIconDistances = (ImageView) view.findViewById(R.id.card_icon);
        mIconDistances.setImageResource(R.drawable.ic_distance_hanson);
        mCurrentDistances = (TextView) view.findViewById(R.id.card_current);
        mMaxDistances = (TextView) view.findViewById(R.id.card_max);
        mProgressDistances = (ProgressBar) view.findViewById(R.id.card_progress);
        mProgressDistances.setScaleY(DEFAULT_PROGRESSBAR_HEIGHT);
        ((TextView)(view.findViewById(R.id.card_unit))).setText(R.string.card_unit_meter);
        
    }       
  
    private void setupActive(View view) {
        mTitleActive = (TextView) view.findViewById(R.id.card_title);
        mTitleActive.setText(R.string.title_active);
        mIconActive = (ImageView) view.findViewById(R.id.card_icon);
        mIconActive.setImageResource(R.drawable.ic_active_minute_hanson);
        mCurrentActive = (TextView) view.findViewById(R.id.card_current);
        mMaxActive = (TextView) view.findViewById(R.id.card_max);
        mProgressActive = (ProgressBar) view.findViewById(R.id.card_progress);
        mProgressActive.setScaleY(DEFAULT_PROGRESSBAR_HEIGHT);  
        ((TextView)(view.findViewById(R.id.card_unit))).setText(R.string.card_unit_minute);
       
    }

    private void setupCalories(View view) {
        mTitleCalories = (TextView) view.findViewById(R.id.card_title);
        mTitleCalories.setText(R.string.title_calories);
        mIconCalories = (ImageView) view.findViewById(R.id.card_icon);
        mIconCalories.setImageResource(R.drawable.ic_calories_hanson);
        mCurrentCalories = (TextView) view.findViewById(R.id.card_current);
        mMaxCalories = (TextView) view.findViewById(R.id.card_max);
        mProgressCalories = (ProgressBar) view.findViewById(R.id.card_progress);
        mProgressCalories.setScaleY(DEFAULT_PROGRESSBAR_HEIGHT);
        ((TextView)(view.findViewById(R.id.card_unit))).setText(R.string.card_unit_calorie);


    }
    
    public Boolean syncSessionRunning(){
    	return  mIsSyncSessionRunning;    		
    }
    
    public void newDataSyncSession()
    {	    	
    	Log.i(TAG, "newDataSyncSession()");
	    {

		    mIsPeriodicScanning=false;	// flag must be set to tell background sync or not    			    		
	    	mIsConnectOnly=false;
	    	isSyncRunning=true;
	    	if(mDeviceSyncBar!=null) //-- 20151022 don't know why
	    		mDeviceSyncBar.setVisibility(View.VISIBLE);
    		startDataSync(mActivity, gDailyGoal, gFinderSetting);

    	}      
    	
    }
    public void newConnectOnlySession()
    {
	    mIsPeriodicScanning=false;	// flag must be set to tell background sync or not    			    		
    	mIsConnectOnly=true;
    	isSyncRunning=true;
    	mDeviceSyncBar.setVisibility(View.VISIBLE);
		startDataSync(mActivity, gDailyGoal, gFinderSetting);   	
    }
    public void refreshViews()
    {
    		Log.i(TAG, "refreshViews");
    		
    		Boolean hadUserChanged=false;
    		
    		hadUserChanged=checkUserChanged();
    		
    		if(checkDeviceChanged()==true || hadUserChanged==true){ // 12.12 
    			int user=BinsDBUtils.getBluetoothLeDao(mActivity).getActiveUserID();
    	    	int device=BinsDBUtils.getBluetoothLeDao(mActivity).getActiveDeviceID();

    		    if (device==0) {
                    DeviceListActivity.autoPair(getActivity(), user);
    		    }
    		    else {
       		    	if(isSyncRunning==true || isConnectAndNotifying()==true){
       		    		Log.i(TAG,"isSyncRunning false");
    		    		isSyncRunning=false;
    		    		stopDataSync();
    		    		
    		    	}
       		    	
       		    	newDataSyncSession();
      		    }
    		    
    		    gMentalEffectValue=0;
    			mHandler.sendEmptyMessage(MSG_REFRESH_MENTAL_EFFECT);	//-- 150618
    			 
    		}
    		else{
    			if(hadUserChanged==true){ 		
    	    		//-- to update views
    				syncViews(false); // 12.12 fix for same device different user case
    				Cursor cursor = queryData();                       	   				
    				if(cursor!=null) {
    					handleData(cursor);
    					update();
    				}      				
    			}
    		}

    }
    
    // This is the actual screen update for activity data changes
    // 06.21
    @Override
    protected void update() {
    	   	
      	if(mIsCloudWorking==false){ 
    		mWorkingProgress.setVisibility(View.GONE);      		 
      	}else{
    		//-- still allow redraw, return; 
      		// working on cloud side, waiting for the result later      		
      	}
  
    	//-- 20150825 
    	if(mDoNotCheckMentalEffect==false){
    		Log.i(TAG,  "update() to MSG_MENTAL_EFFECT");
    		mHandler.sendEmptyMessage(MSG_MENTAL_EFFECT);    			
    	}
    	mDoNotCheckMentalEffect=false;
    	
    	int maxSteps = mCurrentStepsValue + mCurrentStepsValue / 5;
    	if(maxSteps<gDailyGoal) maxSteps=gDailyGoal;
        mProgressSteps.setMax(maxSteps);
        mProgressSteps.setProgress(mCurrentStepsValue);
        mMaxSteps.setText(String.valueOf(mProgressSteps.getMax()));
        mCurrentSteps.setText(String.valueOf(mProgressSteps.getProgress()));

        int distanceCurrent=(int)mCurrentDistanceSum; //-- 01.09 (mCurrentStepsValue)*6/10;
        //-- int distanceCurrent=(mCurrentStepsValue)*6/10;

        int maxDistance=distanceCurrent*12/10;
        if(maxDistance<(maxSteps*0.6)) maxDistance=(int) (maxSteps*0.6);  // 01.09
        mProgressDistances.setMax(maxDistance);
        mProgressDistances.setProgress(distanceCurrent);
        mMaxDistances.setText(String.valueOf(mProgressDistances.getMax()));
        mCurrentDistances.setText(String.valueOf(mProgressDistances.getProgress()));        

        mProgressActive.setMax(DAY_MINUTES/3); // assume 1/3 as ideal max per day
        mProgressActive.setProgress(mCurrentActiveValue);
        mMaxActive.setText(String.valueOf(mProgressActive.getMax()));
        mCurrentActive.setText(String.valueOf(mProgressActive.getProgress()) );
        
        		

        double fCaloryCurrent=(mCurrentStepsValue)*0.03;
        
        int caloryCurrent=(int)fCaloryCurrent;
        //-- int maxCalory=caloryCurrent*13/10;
        int maxCalory=513; //-- 513 Kcal per day, if want to lose 1 kg in 15 days
        
        mProgressCalories.setMax(maxCalory);
        mProgressCalories.setProgress(caloryCurrent);
        mMaxCalories.setText(String.valueOf(mProgressCalories.getMax()));
        mCurrentCalories.setText(String.valueOf(mProgressCalories.getProgress()));
    
        
    }
 
    
    long mStepWatchStartTime; 	// 01.09
    
    @Override 
    protected synchronized void queryTaskUiBeforeProgress(){
       	if(mIsCloudWorking==true){
       		mWorkingProgress.setVisibility(View.VISIBLE);
        }else{
        	mWorkingProgress.setVisibility(View.GONE);      		 
        }           	
    }
    
    @Override
    protected Cursor queryData() {
    	
       Calendar queryStartTime=mTimeControllerView.startOfTheDay();
       
       long currentTime = (long) (queryStartTime.getTimeInMillis() / MINUTE_MILLIS);

       
       // 01.09
       int queryPeriod=24*60;	// in minutes
       mStepWatchStartTime=0;
       if(BinsDBUtils.activeDB().getActiveUserStepWatchEnabled()==true){
    	   long stepWatchStartTime=BinsDBUtils.activeDB().getActiveUserStepWatchStartTime();
    	   
    	   //-- Log.i(TAG, "stepWatchStartTime="+stepWatchStartTime);
    	   long startTodayTime=(mTimeControllerView.startOfToday().getTimeInMillis() / MINUTE_MILLIS);
    	   if(stepWatchStartTime<startTodayTime){
    		   BinsDBUtils.activeDB().setActiveUserStepWatchEnable(false);
    	   }else{
    		   if(stepWatchStartTime>currentTime && currentTime>=startTodayTime){
    			   queryPeriod -=(stepWatchStartTime-currentTime);
    			   currentTime=stepWatchStartTime;
    			   mStepWatchStartTime=stepWatchStartTime;
    		   }
    	   }
       }

       Log.d(TAG, "queryData ...");
              
       Cursor cursor;        

       long showingStartTime=currentTime;
       long showingEndTime=currentTime+queryPeriod;

       if(mIsCloudWorking==true){    
       	mIsCloudWorking=false;
           	cursor = BinsDBUtils.activeDB().queryActivityRecords(mCurrentUserID, showingStartTime, showingEndTime);

       }else{
       		cursor = BinsDBUtils.activeDB().queryActivityRecordsWithCloudLoad(mCurrentUserID, showingStartTime, showingEndTime);
       }
       
       int queryResult=BinsDBUtils.activeDB().getQueryResult();
       mIsCloudWorking=(queryResult==BinsDBUtils.QUERY_WORKING);        
         
       return cursor;
    }

    final int WALK_STEPS_PER_MINUTE_HEIGHT172CM = 96;
    final int RUN_STEPS_PER_MINUTE_HEIGHT172CM = 163;
    final float WALK_SPEED_KM_PER_HOUR_HEIGHT172CM = 4.8f;
    final float RUN_SPEED_KM_PER_HOUR_HEIGHT172CM = 12f;
    final float COMPENSATE_FOR_BINS_WALK_STEPS_PER_MINUTE = 0.13f;
    final int WALK_DISTANCE_PER_WALK_HEIGHT172CM = 83;
    final int WALK_DISTANCE_PER_WALK_HEIGHT155CM = 60;
    final float BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM = WALK_STEPS_PER_MINUTE_HEIGHT172CM*(1-COMPENSATE_FOR_BINS_WALK_STEPS_PER_MINUTE);
    
    
    // 01.09
    float distanceOfStepsPerMinute(float stepsOfMinute) {
    	int userHeight=BinsDBUtils.activeDB().getActiveUserHeight();
    	float stepsPerMinute=70; // steps in minute for lowest speed
    	if(userHeight<140) userHeight=140;	// height in CM
    	if(stepsOfMinute>stepsPerMinute) stepsPerMinute=stepsOfMinute;
    	
    	float factor_x= (RUN_SPEED_KM_PER_HOUR_HEIGHT172CM-WALK_SPEED_KM_PER_HOUR_HEIGHT172CM)/
    						(RUN_STEPS_PER_MINUTE_HEIGHT172CM-BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM);
    	float factor_y= WALK_SPEED_KM_PER_HOUR_HEIGHT172CM-(BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM*factor_x);
    	
    	float speedInDefaultHeight=(stepsPerMinute*factor_x+factor_y); //-- (stepsPerMinute*0.134f-9.895f);
    	float ratioToDefaultHeight=(userHeight*1.15f-118.25f)/ (172*1.15f-118.25f);
    	
    	float speedOfHeight=speedInDefaultHeight*ratioToDefaultHeight;
    	
    	float distancePerMinute=speedOfHeight*1000/60; // in meter
    	
    	float distanceForSteps=distancePerMinute*stepsOfMinute/96;
    	//-- Log.i(TAG, "Distance Minute:"+stepsOfMinute+":"+distanceForSteps+":"+mCurrentDistanceSum);
    	
    	return distanceForSteps;
    }   
        
    
    private boolean goalAchievedNotified(){
    	BinsDBUtils binsDB=BinsDBUtils.activeDB();
    	
    	long startTodayTime=(mTimeControllerView.startOfToday().getTimeInMillis() / MINUTE_MILLIS);
    	int dailyGoal=binsDB.getActiveUserDailyGoal(); 
    	
    	long goalAchieveTime=binsDB.getActiveUserGoalAchieveTime();
    	if(goalAchieveTime>startTodayTime){
        	long goalAchieveValue=binsDB.getActiveUserGoalAchieveValue();
        	if(goalAchieveValue>dailyGoal){
        		return true;
        	}
    	}
    	
    	return false;
    }
    
    final static int MAX_STEP_SAMPLE_WINDOW_MINUTES=5;
    //-- 08.28
    Boolean mHasGoalAchievedNotified=false;
    @Override
    protected synchronized void handleData(Cursor cursor) {
        
        Log.d(TAG, "Unlocking handleData ..."+mIsCloudWorking);

       	if(mIsCloudWorking==true){      	  
       		return; // working on cloud side, waiting for the result later
        }
       	
        long nowActivityTime=0;
        long lastActivityTime=0;
        long samplePeriod=0;
        mCurrentStepsValue = 0;
        mCurrentActiveValue = 0;
        mCurrentDistanceSum = 0;
        if (cursor != null) {
            //-- Log.d(TAG, "Column Count: " + cursor.getColumnCount());
            cursor.moveToNext();
            while (!cursor.isAfterLast()) {
            	int stepsThisMinute	=	cursor.getInt(0);
            	nowActivityTime		=	cursor.getInt(1);
                int type  			= 	cursor.getInt(2);
            	
                if(type!=Bins.BINS_ACTIVITY_TYPE_STEPS) continue;
                
            	if((nowActivityTime-lastActivityTime)>MAX_STEP_SAMPLE_WINDOW_MINUTES){
            		samplePeriod=MAX_STEP_SAMPLE_WINDOW_MINUTES;
            	}else{
            		samplePeriod=(nowActivityTime-lastActivityTime);
            	}
                if(samplePeriod<=0) samplePeriod=1;

            	lastActivityTime=nowActivityTime;
            	
              	//-- Log.i(TAG, "handleData time-steps-distance:"+nowActivityTime+":"+samplePeriod+":"+stepsThisMinute+":"+mCurrentActiveValue);

                if(mStepWatchStartTime!=0 && mStepWatchStartTime==nowActivityTime){
                	int deltaStepsFromStartMinute= BinsDBUtils.activeDB().getActiveUserStepWatchDelta();
                	//-- Log.i(TAG, "handleData deltaSteps:"+stepsThisMinute+":"+deltaStepsFromStartMinute);
                	stepsThisMinute-= deltaStepsFromStartMinute;
                }
                
                mCurrentStepsValue 	+= 	stepsThisMinute;
                mCurrentDistanceSum +=	distanceOfStepsPerMinute(stepsThisMinute/samplePeriod)*samplePeriod; // sample every 5 min. now
                
                if (DEBUG) {
                    Log.d(TAG, "Value: " + cursor.getInt(0));
                    Log.d(TAG, "Time : "
                            + DateFormat.format("yyyy-MM-dd HH:mm:ss",
                                    ((long) cursor.getInt(1)) * MINUTE_MILLIS).toString());
                }
                
                //-- if above a threshold 100 steps over one sampling period, t
                float avgStepsPerMinute=((float)stepsThisMinute/samplePeriod);
                if(avgStepsPerMinute>20) mCurrentActiveValue+= samplePeriod;	//-- 150908
                
                cursor.moveToNext();
            }
            cursor.close();
            
            if(mDailyGoal!=0 && mCurrentStepsValue>mDailyGoal){
            	if(mHadNewDataRead==true){	//-- 08.30
            		if(goalAchievedNotified()==false){	//-- 01.09
            			Log.d(TAG, "pass goal: mDailyGoal:"+mDailyGoal);
            			Notification myNotification;
            			myNotification= new NotificationCompat.Builder(mActivity.getApplicationContext())
            			.setContentTitle(getResources().getString(R.string.notification_title_goal_pass))
            			.setContentText(getResources().getString(R.string.notification_content_goal_pass))
            			.setTicker(getResources().getString(R.string.notification_ticker))
            			.setWhen(System.currentTimeMillis())
            			.setDefaults(Notification.DEFAULT_SOUND)
            			.setAutoCancel(true)
            			.setSmallIcon(R.drawable.ic_bins)
            			.build();
            	
            			//.setContentIntent(pendingIntent)
            	
            			NotificationManager notificationManager = 
            			  (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);

            			notificationManager.notify(0, myNotification); 
            			
            			//-- 01.09
            			BinsDBUtils.activeDB().setActiveUserGoalAchieve(nowActivityTime, mCurrentStepsValue);
            		}
            	}
    			mHasGoalAchievedNotified=true;
            }
        	else{
        		mHasGoalAchievedNotified=false;
        	}	
            
        } else {
            Log.d(TAG, "handleData ... No Data");
        }
        
        mHadNewDataRead=false;	//-- 08.30 no more need, reset it.
        
        Log.i(TAG, "handleData end");            
    }

    private Boolean updateActigraphy() 
    { 
    	
        BinsDBUtils.activeDB().clearCubeLevelPeriods();    
        
        Calendar queryEndTime=mTimeControllerView.endOfTheDay();
 
        /* force to start and stop at the day boundary
        
        Calendar currentTime= Calendar.getInstance();
        
        if(currentTime.before(queryEndTime)){
        	queryEndTime=currentTime;
        }
        */
        
        int queryPeriod=24*60*3;	// in minutes
        
        long endTime = (long) (queryEndTime.getTimeInMillis() / MINUTE_MILLIS);
        long startTime=endTime-queryPeriod; //-- from previous three days max.
   
        final Activity nowActivity=getActivity();
        if(nowActivity==null) return false;
        
        Cursor cursor;
        
        cursor = BinsDBUtils.activeDB().querySleepRecords(mCurrentUserID, startTime, endTime);

        LevelPeriodBuilder levelPeriods= new LevelPeriodBuilder(startTime, endTime, cursor);
        
        float availPercent=levelPeriods.getDataAvailability(); //-- 20151023
        Log.i(TAG, "updateActigraphy movement avail="+availPercent);
        if(availPercent<0.7){
        	if(availPercent==0) return false;
        	/* 20151103 too much interfere
     	  	mActivity.runOnUiThread(new Runnable()  {
       	  		@Override
             	public void run() {
       	    		Toast toast = Toast.makeText(mActivity, R.string.lack_actigraphy, Toast.LENGTH_SHORT);
       	    		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);		           
       	            toast.show();

            }});
     	  	*/	
     	    return false;
        }
        
        levelPeriods.storeToDB();        
        
        return true;
        
    }
    
    public class LevelPeriod{
        private int threshId;
        private long startTime;        
        private long endTime;      

        public LevelPeriod(int th, long st, long et) {
        	threshId=th;
        	startTime=st;
        	endTime=et;
        }
    }
    //-- 150615
    public void updateMentalEffect() {
    	float mentalEffectTotal=0;
        Calendar queryStartTime=mTimeControllerView.startOfTheDay();
        long startTime = (long) (queryStartTime.getTimeInMillis() / MINUTE_MILLIS);
        int queryPeriod=24*60;	// 3 days as a period to measure, in minutes
        long endTime=startTime+queryPeriod;
        startTime-=24*60*1;	/* previous one day */       
    	
    	Cursor cursor=BinsDBUtils.activeDB().getLevelPeriods(startTime, endTime);
    	
    	//-- Log.i(TAG, "mentaleffect cursor:"+cursor+":"+startTime+":"+endTime);

        if (cursor == null) return; 	// nothing to do

        ArrayList<LevelPeriod> levelPeriodList=new ArrayList<LevelPeriod>();
        
        cursor.moveToNext();
        while (!cursor.isAfterLast()) {
        	int threshId=cursor.getInt(0);
        	long timeStart=cursor.getInt(1);
        	long timeEnd=cursor.getInt(2);
        	
        	//-- Log.i(TAG, "mentaleffect list:"+threshId+":"+timeStart+":"+timeEnd);
        	
        	levelPeriodList.add(new LevelPeriod(threshId, timeStart, timeEnd)); 
        	
        	cursor.moveToNext();
        }
        cursor.close();
        
        int numLevelPeriods=levelPeriodList.size();
        int i;
        for(i=0; i<numLevelPeriods; i++){
        	float periodDelta=0;
        	int threshId=levelPeriodList.get(i).threshId;
        	long timeStart=levelPeriodList.get(i).startTime;
        	long timeEnd=levelPeriodList.get(i).endTime;
        	int  stateSleep=BinsDBUtils.activeDB().getSleepStateFromThreshold(threshId);
        	//-- Log.i(TAG, "circadian sleep state:"+threshId+":"+stateSleep);
        	if(threshId<=4) stateSleep=1;  // TODO 20150810
        	
        	long timePeriod=(timeEnd-timeStart+1);
        	float hourFraction;       	
        	
        	for(int theHour=0; theHour*60<=timePeriod; theHour++){
        		if((theHour+1)*60<timePeriod){
        			hourFraction=1;
        		}else{
        			hourFraction=((float)(timePeriod-theHour*60)/60);
        		}
/**/      		
                //-- int cirHour=(int)(((timeStart+theHour*60)/60+18)%24);
                Date cirDate= new Date((timeStart+theHour*60)*60*1000); // Date as millisecond
                Calendar cirCal = Calendar.getInstance();
                cirCal.setTime(cirDate);
                int cirHour=cirCal.get(Calendar.HOUR_OF_DAY);
                //-- Log.i(TAG, "circadian hour of the day:"+cirHour);
                
                cirHour+=1; //-- Nth hour 0-23 changes to 1-24
                if(cirHour>24){ cirHour-=24; }
                //-- Log.i(TAG, "circadian time:"+timeStart+":"+timeEnd+":"+theHour+":"+(timeStart+theHour*60)/60+":"+((timeStart+theHour*60)/60)%24);
        		Cursor cursorCir=BinsDBUtils.activeDB().getCircadian(cirHour);
        		if(cursorCir==null){
        			Log.i(TAG, "circadian no entries");
        			
        		}
        		if(cursorCir!=null){
           		  if (cursorCir.moveToFirst()) {

        			//-- cursorCir.moveToNext();
        			float cirPercSleepFactor=cursorCir.getFloat(0);
        			float cirPercWakeFactor=cursorCir.getFloat(1); 
        			
        			if(stateSleep!=0){
        				periodDelta+=hourFraction*cirPercSleepFactor;
        			} else {
        				periodDelta+=hourFraction*cirPercWakeFactor;        				
        			}
        			
        			Log.i(TAG,"circadian factors de:hr:fr:sl:wa="+periodDelta+":"+cirHour+":"+hourFraction+":"+cirPercSleepFactor+":"+cirPercWakeFactor);
            		
           		  } else {
           			  Log.i(TAG, "circadian not found entry");
           			  
           		  }
        		}
        		if(cursorCir!=null) cursorCir.close();
/* */
/* */       		
        		//-- 20151203 time effect 
        		int timeEffectHour=theHour+1; // hour started from 1
        		Cursor cursorTime=BinsDBUtils.activeDB().getTimeEffect(threshId,timeEffectHour);
        		if(cursorTime!=null && cursorTime.getCount()>0){
        			cursorTime.moveToNext();
        			float timePercSleepFactor=cursorTime.getFloat(0);
        			float timePercWakeFactor=cursorTime.getFloat(1); 
        			if(timePercSleepFactor!=0){
        				periodDelta+=hourFraction*timePercSleepFactor;
        			} 
        			if(timePercWakeFactor !=0){
        				periodDelta+=hourFraction*timePercWakeFactor;        				
        			}
        			
        			Log.i(TAG,"timeeffect factors de:th:hr:fr:sl:wa="+periodDelta+":"+threshId+":"+timeEffectHour+":"+hourFraction+":"+timePercSleepFactor+":"+timePercWakeFactor);

        		}
        		if(cursorTime!=null) cursorTime.close();
/* */        		
        	} // loop for every hour of the cube level period
        	
        	//-- Log.i(TAG, "mentaleffect periodDelta:"+periodDelta);
        	
        	mentalEffectTotal+=periodDelta;
        	
           	//-- should use message, mMentalEffectValue.setText(String.valueOf((int)(mentalEffectTotal))+"%");
                    	
        	if(mentalEffectTotal>100) mentalEffectTotal=100;
        	if(mentalEffectTotal<0) mentalEffectTotal=0;
        	
        	
        	
        }	// loop for every cube level period of the period of time. 
        
    	//-- this mentalEffectTotal should be output to UI
        
        gMentalEffectValue=(int)mentalEffectTotal;
        
        Log.i(TAG, "new mentaeffect value is:"+gMentalEffectValue);
        
        levelPeriodList.clear();
        
    }
 
    private void scanLeDevice(final boolean enable) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            mHandler.postAtTime(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    if (mBluetoothAdapter != null) {
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    }
                    stopScanning();
/*
                    if(mIsPeriodicScanning==true)
                    {
                    	mIsPeriodicScanning=false;
                    	mCountFromLastSync=0;
                    }
*/
                }
            }, this, SystemClock.uptimeMillis() + SCAN_PERIOD);

            mScanning = true;
            
            //-- mBluetoothAdapter.stopLeScan(mLeScanCallback); // just in case;
            
            boolean result=mBluetoothAdapter.startLeScan(mLeScanCallback);
            Log.i(TAG, "startlescan result :"+result);
            
            //-- startScanning();
        } else {
            mHandler.removeCallbacksAndMessages(this);
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            //-- stopScanning();
        }
    }

    Runnable mentalEffectTask = new Runnable() {  
    	
        @Override  
        public void run() {  
   			if(updateActigraphy()==true){
   				updateMentalEffect(); 
   			}else{
   				gMentalEffectValue=0;
   			}
  			mHandler.sendEmptyMessage(MSG_REFRESH_MENTAL_EFFECT);
       }  
    };  

    //-----------------------------------------------------------------------------
    static boolean mIsRestartingBluetooth;	// 12.14
    
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
        	
        	if(msg.what==MSG_DATA_SYNC_START){
        		onEventRun(ACTION_DATA_SYNC_START, true);
        	} else if(msg.what==MSG_MENTAL_EFFECT){
      			//-- Log.i(TAG, "message mental effect");
 				mMentalEffectValue.setText("---");
    			mMentalEffectProgress.setVisibility(View.VISIBLE);    			
    			mHandler.sendEmptyMessage(MSG_UPDATE_MENTAL_EFFECT);     			
      			
        	} else if(msg.what==MSG_UPDATE_MENTAL_EFFECT){
        	   	new Thread(mentalEffectTask).start();        	    
        	} else if(msg.what==MSG_REFRESH_MENTAL_EFFECT){
     			mMentalEffectProgress.setVisibility(View.GONE);     			        		
       			if(gMentalEffectValue!=0){
      				mMentalEffectValue.setText(String.valueOf(gMentalEffectValue)+"%");
      			}else{
      				mMentalEffectValue.setText("---");
      			}
   
        	} else if(msg.what==MSG_DATA_SYNC_RUNNING){
        		onEventRun(ACTION_DATA_SYNC_RUNNING, true);
       		       	
        	} else if(msg.what==MSG_DATA_SYNC_STOP){
        		onEventRun(ACTION_DATA_SYNC_STOP, true);
       		       	
       		} else if(msg.what==MSG_DATA_SYNC_END){
        		onEventRun(ACTION_DATA_SYNC_END, true); 
        		
        	} else if(msg.what==MSG_FINDER_LOST){
        		onEventRun(ACTION_FINDER_LOST, true);
       		       	
        	} else if(msg.what==MSG_FINDER_STOP){
        		onEventRun(ACTION_FINDER_STOP, true);

        	} else if(msg.what==MSG_DATA_NOTIFY){
        		onEventRun(ACTION_DATA_NOTIFY, true);
        		
        	} else if (msg.what == MSG_CONNECT) {
        		mIsRestartingBluetooth=false;
                connect();
                mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 40 * 1000);
                return true;
                
        	} else if (msg.what == MSG_CONNECT_GATT) { // 09.14
        		mIsRestartingBluetooth=false;
        		connectInside();
                mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 40 * 1000);                   		
                return true;
        	} else if (msg.what == MSG_GATT_SERVICE_DISCOVERY) { // 09.14
        		Log.i(TAG, "MSG_GATT_SERVICE_DISCOVERY");
        		/*
				final Activity nowActivity=getActivity();	//-- 09.21			
				if(nowActivity!=null){
         	  	  	nowActivity.runOnUiThread(new Runnable()  {
           	  		@Override
                 	public void run() {
                 	*/
        				mDeviceSyncLayout.setBackgroundColor(getResources().getColor(R.color.bins_connected_color));          	  	  
        				//-- 09.19, try to set it earlier
           	  			if(mBluetoothGatt!=null){
            	  			mBluetoothGatt.discoverServices(); 
           	  			}   
           	  	/*		
                 	 }});
				} else {
					Log.i(TAG,"no activity returned error");
				}
				*/
           	  			
                mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 16 * 1000); //-- 03.31 16 60*1000
                return true;
                
        	} else if(msg.what == MSG_GATT_DISCONNECT){
                // 08.11
                if(mDataSyncModel!=null) 
                	mDataSyncModel.stopRead();
                           	  	
           	  		if(mIsRestartingBluetooth==false){
           	  			//-- mDeviceSyncLayout.setBackgroundColor(colorOrigin);  // 11.19
           	  			mDeviceSyncLayout.setBackground(syncCellBackgroundOrigin);
           	  		}
           	  		
                    if(mHeartrateCharacteristic!=null){
                            Log.i(TAG, "Close Heart Rate");                            	 
                     		mHeartRateTextView.setVisibility(View.VISIBLE);
                     		mHeartRatePPMView.setVisibility(View.VISIBLE);
                     		mHeartRateMessageView.setVisibility(View.GONE);
                     		mHeartRateLayoutView.setVisibility(View.GONE);
                     		
              	    		//-- mHeartRateLayoutView.invalidate();	//-- 09.14

                     		//-- Heart rate  06.19
                      		PXIALGMOTION.Close();
                    }
                    
            	  	if(mBluetoothGatt!=null){
           	  			mBluetoothGatt.close();
           	  			mBluetoothGatt=null;
           	  		}
         	  			
           // 12.17     }});    
         	  	
           	} else if (msg.what == MSG_READ_ERROR) { // 09.14
                mDataSyncModel.readAgain();            	               
                return true;
                
            } else if (msg.what == MSG_TIMEOUT) {
                Log.d(TAG, "Timeout ...............");
                mHandler.removeMessages(MSG_TIMEOUT);
 
                if(mConnectionState==STATE_CONNECTING){
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    connectionClose();	// 12.14 remove deadlock case                    
                }
                if(mIsPeriodicScanning==false) //-- if not the Periodic scanning case, 
                	meetError();
                else{ //-- if it is a periodic scanning case
                	mIsPeriodicScanning=false;
                }
                
                return true;
            } else if(msg.what == MSG_PERIODIC_SCAN_TIMEOUT) {
                Log.d(TAG, "Periodic Scan Timeout..");
                mHandler.removeMessages(MSG_PERIODIC_SCAN_TIMEOUT);
                if(mConnectionState==STATE_CONNECTING){
                    mBluetoothAdapter.stopLeScan(mLePeriodicScanCallback);
                }
                if(mIsPeriodicScanning==true){
                	mIsPeriodicScanning=false;
                	//-- give up this time, as not success
                }                
                mConnectionState=STATE_DISCONNECTED;	// 11.20
                
                return true;
            } else if(msg.what == MSG_GATT_READ){
                //-- 09.09
            	/* 12.17
                Activity nowActivity=getActivity();
                if(nowActivity==null){
                	Log.i(TAG, "error to get Activity while write characteristic, abort");
                
                } else {
                
                	nowActivity.runOnUiThread(new Runnable() {
                	@Override
                    public void run() {         		
                        mBluetoothGatt.readCharacteristic(activeCharacteristic);
                    }
                	});
                	
                  	mHandler.sendEmptyMessageDelayed(MSG_READ_ERROR, 8 * 1000); // 09.14

                }
                */
            	
            	if(activeCharacteristic!=null){
            		try{
            			mBluetoothGatt.readCharacteristic(activeCharacteristic);
            			mHandler.sendEmptyMessageDelayed(MSG_READ_ERROR, 8 * 1000); // 09.14
            			
            		} catch (Exception e) {
        			}         

            	}	


            } else if (msg.what == MSG_SERVICE_DISCOVERED) {
            	Log.d(TAG, "MSG_SERVICE_DISCOVERED");
            	
            	//-------------------------------------------------------------------
            	//----------------- Run from UI Thread
            	/* 12.17
         	  	getActivity().runOnUiThread(new Runnable() {
         	  	@Override
                public void run() 
          		{
          		*/
         	  		//-- this is called to have chance update UI for the sync running status
         	  		onEventReceive(ACTION_DATA_SYNC_RUNNING, mSucceed);
               	
    				startSync();
          		// }});
         	  	//-------------------------------------------------------
         	  	//------ end to use UI thread
         	  	        	  	
                return true;
                
            } else if (msg.what == MSG_BLUETOOTH_RESET ){
            	
            	//-- 12.14 keep continue to show busy working
        		mIsRestartingBluetooth=true;
        		
            	BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            	// boolean isEnabled = bluetoothAdapter.isEnabled();
            	bluetoothAdapter.disable();
            	mHandler.sendEmptyMessageDelayed(MSG_BLUETOOTH_RESET_FINISH, 3000);
            } else if (msg.what == MSG_BLUETOOTH_RESET_FINISH ){
            	Log.i(TAG, "done bluetooth off");
            	
            	BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            	// boolean isEnabled = bluetoothAdapter.isEnabled();
            	bluetoothAdapter.enable();            	
               	mHandler.sendEmptyMessageDelayed(MSG_BLUETOOTH_RESET_END, 4000);
              
            } else if (msg.what == MSG_BLUETOOTH_RESET_END ){
            	Log.i(TAG, "done bluetooth on");
            	
            	mIsRestartingBluetooth=false;
            	mHandler.sendEmptyMessage(MSG_CONNECT_GATT);	// re-do connect 
            } else if (msg.what == MSG_ERROR) {
                if (!mDestroyed) {
                    Log.d(TAG, "Error ...............");
                    meetError();
                }
                return true;
            } else if(msg.what == MSG_DISCONNECT) {

            	disconnect();
            	close(); 	
            } else if(msg.what == MSG_SYNC_STOP_END){	//-- 09.14
            	mDeviceSyncLayout.setEnabled(true);	//-- 09.19
            	isSyncStopTaskRunning=false;
            } else if(msg.what == MSG_READ_FW_REVISION){	//-- 09.16
             	readCharacteristic(gHardwareRevisionCharacteristic);
             	
            } else if(msg.what == MSG_DELAYED_LINKLOSS_ALARM) {
            	if(mConnectionState!=STATE_CONNECTED){
            		mRssiAlarmActivated=1;           		
            	}
        	} else if(msg.what == MSG_FINDER_TASK) {
            	if(mConnectionState==STATE_CONNECTED && mBluetoothGatt!=null){
                    // boolean result=
                    mBluetoothGatt.readRemoteRssi(); 
                	// Log.i(TAG, "readRemoteRssi:"+result);
            	}
        	}else if(msg.what == MSG_BINS_DISCONNECT) {	// 12.25
        		Log.i(TAG, "MSG_DISCONNECT");
        		connectionClose();	       		
            }else if(msg.what == MSG_NEW_SYNC_SESSION){ //150505
            	newDataSyncSession();
            }else if(msg.what == MSG_NEW_CONNECT_ONLY_SESSION){ //150712
            	newConnectOnlySession();
            }
        	
            return false;
        }
    });
  
    public void resetBluetooth() {
    	if(isFirstHandleFromSamsungError==true){
    		isFirstHandleFromSamsungError=false;
    		mHandler.sendEmptyMessage(MSG_BLUETOOTH_RESET);	
    	} else {
    		meetError();
    	}
    }
    
    public static boolean syncIsActive() {    	
    	if(mCheckNotificationTimer!=null){
    		return true;
    	}
    	return false;
    }
    public static boolean finderIsActive() {
    	
    	if(mRssiTimer!=null){
    		return true;
    	}
    	return false;
    }
    public void stopFinder() {
    	
    	//-- 12.17, if(mFinderIsRunning==true) 
		{
			//-- 12.17, 
			mFinderIsRunning=false; //-- 01.13
			
        	mFinderRunningIndicator.setVisibility(View.GONE);
           	mFinderRunningIndicator.setAnimation(null);	                  	

			if(mFinderIsRunning==false && mRssiTimer!=null) {
				mRssiTimer.cancel(); 
				mRssiTimer=null;
			}
			   
			if(mLinklossCharacteristic!=null)
			{
				byte[] bytes = new byte[1];
			   		  
				bytes[0]=(byte)(ALERT_TYPE_LINKLOSS | ALERT_COMMAND_RESET); //-- 09.20 s(0x00); 
			    mLinklossCharacteristic.setValue(bytes);
			    //--**
			   	writeCharacteristic(mLinklossCharacteristic);
			   		  
			}
			   
			   
			/* 11.14
			//-- 4.15
		    if(isSyncRunning==true){
		       return;		// no need to disconnect/close
		    }
 
			disconnect();
			close();	
			 */	    	         		   
		}		   
    }   
    
    private void finderReconnect() {

		Log.d(TAG, "reset finder");
		disconnect();
		close();	    			
    	mFinderIsRunning=true;
    	mFinderJustCalled=true;
    	//-- 11.19 no change to the current setting, mIsSyncSession=false;
    	mRssiSetCounter=0;
    	mRssiGetCounter=0;
    	
		connect(); 	
		
    }
    private void setupFinderTimer() {
    	
    	if(mRssiTimer!=null) return;
    	
	    TimerTask timerTask = new TimerTask()
	    {
	    	@Override
			public void run()
			{
	    		
	    		Log.i(TAG, "finderTimer:"+mConnectionState+":"+mRssiAlarmActivated);
	    			    		
	    		if(mRssiAlarmActivated==1){	// only true of the following conditions ok 
	    			finderLostNotify();
	    		}
	    		
	    		// if disconnected, then try to connect again, if it is in connecting already, then do nothing
	    		
	    			// the case of suddenly disconnected happens, then 
    	    	if(mRssiSetCounter>15 && mConnectionState==STATE_DISCONNECTED){ 
 
	    	    	if(((mRssiSetCounter-mRssiGetCounter)<=1) && strongRssiHistory()==true){
	    	    		// 12.21 means suddenly disconnected
	    	    		Log.i(TAG, "MSG_DELAYED_LINKLOSS_ALARM posted");
	    	    		mHandler.sendEmptyMessageDelayed(MSG_DELAYED_LINKLOSS_ALARM, 18*1000); // 18 seconds
	    	    	} else {
	    	    		mRssiAlarmActivated=1;	    	   		
	    	    	}
	    	    	finderReconnect();	// try to reconnect, 
	    	    						// next time, the connect state will change to STATE_CONNECTING
	    	    						// and if reconnect success in short time, 
	    	    						// then ignore the alarming this time
	    	    						
	    	    	// for a while, if connected successfully, then case below
	    	    	// 				if failed, then come here again
	    	    	
    	    	} else if(mRssiSetCounter>15 && (mRssiSetCounter-mRssiGetCounter)>3){ 
    	    		// it means it is failed to read RSSI for several times
    	    		// , can be connected or connecting (cannot be disconnected, above case covered)
    	    		// and after a while from the starting time window
    	    		// and missed several times from to read rssi successfully  	  
    	    		
    	    		Log.i(TAG,  "rssi set:get:state="+mRssiSetCounter+":"+mRssiGetCounter+" : "+mConnectionState);
    	    		
	    	    	if(mConnectionState==STATE_CONNECTED){ 
	    	    		//-- if it is connected already, NOT in connecting
	    	    		//-- mRssiIssueWorkaround==0 means it is a case from suddenly disconnected
	    	    		//-- work around case of happened to NEXUS case - suddenly disconnect
	    	    		//-- then stop the media player soon as possible, and recover to normal
 
	    	    		if(mRssiAlarmActivated==1){	 // only run this one time   	    	    	
	    	    			mRssiAlarmActivated=0;
	    	    			
	    	    			if(mMediaPlayer.isPlaying()==true) mMediaPlayer.stop();	// immediate stop it.
	    	    		}
	    	    		
	    	    		// 12.19 should be done by rssi receiver, if(mConnectionState==STATE_CONNECTED) mRssiGetCounter=mRssiSetCounter;	// 12.17 reset them
	    	    		
	        	    	// as it is connected, still try to check rssi
		    	    	mHandler.sendEmptyMessage(MSG_FINDER_TASK);
		
	    	    	}
	    	    	else{ // if connecting  
	    	    		mRssiAlarmActivated=1;	 // alarming soon as possible
	    	    	}
	    	    	
	    	    	
 	    		}
	    		else{ // connected and work normal, let rssi receiver to check later
	    	    	mHandler.sendEmptyMessage(MSG_FINDER_TASK);
	    			mRssiAlarmActivated=0; 
	    			if(mRssiSetCounter==mRssiGetCounter && mRssiSetCounter>8000){
	    				mRssiSetCounter=8000;
	    				mRssiGetCounter=8000;
	    				// prevent overflow;
	    			}
	    		}
	    		
	    		//-- Log.d(TAG, "rssi timer : set and get="+mRssiSetCounter +" : "+ mRssiGetCounter + ":" + mRssiAlarmActivated + ":" + mRssiIssueWorkaround);
	    		
				mRssiSetCounter++;
				
				
			}
		};
		
    	mRssiSetCounter=0;
    	mRssiGetCounter=0;
		rssiRecordReset();
		mRssiTimer = new Timer();
		mRssiTimer.schedule(timerTask, 1000, 1000);	//-- 09.22, from 1000 to 500 ms
	}
    
    private void finderLostNotify()
    {
    	
      Log.i(TAG, "finderLostNotify");
      
  	  try {
          
          if(mMediaPlayer!=null) {
          	mMediaPlayer.stop(); 
          	mMediaPlayer.release();
          	mMediaPlayer=null;
          }
              
          
          {
              mMediaPlayer = MediaPlayer.create(mActivity.getApplicationContext(), R.raw.burglar_alarm);
          }
          
          
          mMediaPlayer.setOnCompletionListener(new OnCompletionListener(){
        	  int count = 0; // initialize somewhere before, not sure if this will work here
        	  int maxCount = 4;

        	  @Override
        	  public void onCompletion(MediaPlayer mediaPlayer) {
        	    if(count < maxCount) {
        	      count++;
        	      mediaPlayer.seekTo(0);
        	      mediaPlayer.start();
        	    }
        	}});          
          
           
          if(mMediaPlayer!=null){
             mMediaPlayer.start();                        
          }
            
       } catch (Exception e) {} 
          	

   	  sendFinderLostBroadcast();

   	  if(mConnectionState==STATE_CONNECTED && mLinklossCharacteristic!=null)
   	  {
   		  byte[] bytes = new byte[1];
   		  
   		  bytes[0]=(byte)(ALERT_TYPE_LINKLOSS | ALERT_COMMAND_ENABLE);  //-- 09.20 (0x80 | 0x01); 
   		  mLinklossCharacteristic.setValue(bytes);    	
   		  writeCharacteristic(mLinklossCharacteristic);
   		  if(mLinklossInformed<50) mLinklossInformed++;
   		  
   	  }

    }
    // Implements callback methods for GATT events that the app cares about. For
    // example,
    // connection change and services discovered.


    private static final int FINDER_TRACK_MAX = 5;
    //-- private static final int FINDER_ALARM_RSSI_THRESHOLD_BINSX2 = -85; // 07.21 // -99; // -100; // -85; // -95; // -85;
    //-- private static final int FINDER_ALARM_RSSI_THRESHOLD_BINS_ONE = -90; //-- 09.22 from -99 for Nexus 5, // 09.09

    private static final int BINS_FINDER_RSSI_MIN   =  (-105);
    private static final int BINS_FINDER_RSSI_MAX   =  (-90);
    private static final float BINS_FINDER_RSSI_STEP  =  
    		((float)(BINS_FINDER_RSSI_MAX-BINS_FINDER_RSSI_MIN)/BinsDBUtils.BINS_FINDER_LEVEL_MAX);

    private int[] rssiRecord = new int[FINDER_TRACK_MAX];
    int rssiRecordHead=0;

    private int finderAlarmRssiThreshold()
    {
        int level=BinsDBUtils.activeDB().getActiveFinderThreshold();

    	    
    	int finderRssiThresh= (int)((float)BINS_FINDER_RSSI_MAX-(level)*BINS_FINDER_RSSI_STEP);
    	
    	//-- Log.i(TAG, "fi th:"+finderRssiThresh);
    	
    	return finderRssiThresh;
    	
    	/* 
  	    if(mHeartrateCharacteristic!=null){	//-- means BiNS X2 model
  	    	  return FINDER_ALARM_RSSI_THRESHOLD_BINSX2;
  	    }
  	    
    	return FINDER_ALARM_RSSI_THRESHOLD_BINS_ONE;
    	*/
    }
   

    private void rssiRecordReset()
    {
        int i;
        for(i=0; i<FINDER_TRACK_MAX; i++)
            rssiRecord[i]=0;
        rssiRecordHead=0;
    }

    private BluetoothAdapter.LeScanCallback mLePeriodicScanCallback = new BluetoothAdapter.LeScanCallback() {
    	@Override
    	public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                  if(device.getName().equals("BINS"))               
                  {  
                    Log.d(TAG, "onPeriodicLeScan");
                	if(device.getAddress().equals(mBluetoothDeviceAddress))
                	{                   	                 		
                        mHandler.removeMessages(MSG_PERIODIC_SCAN_TIMEOUT); //-- clear them from registered previously
                		mBluetoothAdapter.stopLeScan(mLePeriodicScanCallback); 

                        final Activity nowActivity=getActivity();
                        if(nowActivity==null){
                        	Log.i(TAG, "Periodic scan onLeScan: activity null error");
                        }	
                        
                        if(mBluetoothGatt!=null && nowActivity!=null){ 
                        	nowActivity.runOnUiThread(new Runnable() {
                        	@Override
                            public void run() {                       
                        		mBluetoothGatt.close();	// 09.20 put here, //-- 09.02, in case, to make sure
                            }
                        	});
                        }
                        
                		if(nowActivity!=null && rssi>-97){
                            mHandler.sendEmptyMessageDelayed(MSG_TIMEOUT, 30 * 1000); //-- should be enough
                            
                           	nowActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                	try{
                                		mBluetoothGatt = device.connectGatt(nowActivity, false, mGattCallback); 
                                	} catch (Exception e) {
                                		Log.i(TAG, "Periodic scan, onLeScan error while call close and connectGatt");
                                		mIsPeriodicScanning=false;
                        			}         
                                }
                           	});
                		}
                		else{
                            mIsPeriodicScanning=false;
                            //-- give up scanning this time, but continue trying next time 
                		}
               		
                	}
                	
                  }
 
    	}    
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
    	@Override
    	public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
                  if(device.getName().equals("BINS"))               
                  {  
                    Log.d(TAG, "onLeScan... Address = " + device.getAddress());
                	if(device.getAddress().equals(mBluetoothDeviceAddress))
                	{
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        
                        final Activity nowActivity=getActivity();
                        if(nowActivity==null){
                        	Log.i(TAG, "onLeScan: activity null error");
                        }	

                        if(mBluetoothGatt!=null && nowActivity!=null){ 
                        	nowActivity.runOnUiThread(new Runnable() {
                        	@Override
                            public void run() {                       
                        		mBluetoothGatt.close();	//-- 09.20 place to here for SAMSUNG N2 stability
														//-- 09.02 in case
                            }
                        	});
                        }
   
                        if(nowActivity!=null){
                       	    nowActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            	try{                           	
                            		mBluetoothGatt = device.connectGatt(nowActivity, false, mGattCallback);
                            	} catch(Exception e){
                            		Log.i(TAG, "LeScan call close connectGatt error");
                            	}
                            }
                       	   });
                        }

                	}
                  }
 
    	}    
    };
    
    private Boolean strongRssiHistory() {
        int nowFinderAlarmThresh=finderAlarmRssiThreshold();    	
        int signalWeakCount=0;
        for(int i=0; i<FINDER_TRACK_MAX; i++){
            if(rssiRecord[i]<nowFinderAlarmThresh ){
           	 	signalWeakCount++;
            }
        }

        if(signalWeakCount<=2) return true;
    	return false;
    }
    
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
    	@Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
    		
            int nowFinderAlarmThresh=finderAlarmRssiThreshold();

             Log.i(TAG,"rssi = " + rssi + " : " + nowFinderAlarmThresh);
             
             mRssiLastTime=rssi;
             
             // if(rssi<FINDER_ALARM_RSSI_THRESHOLD ) { //-- -80 -70
             //	 finderLostNotify();
             // }
 			 mRssiAlarmActivated=0;
             mRssiGetCounter=mRssiSetCounter;
             
             rssiRecord[rssiRecordHead]=rssi;
             
             rssiRecordHead++;
             if(rssiRecordHead>=FINDER_TRACK_MAX) rssiRecordHead=0;
             
             Boolean isTimeToClear=true;
             int signalWeakCount=0;
             for(int i=0; i<FINDER_TRACK_MAX; i++){
                 if(rssiRecord[i]<nowFinderAlarmThresh ){
                	 signalWeakCount++;
                	 isTimeToClear=false;
                 }
             }
               
             //if(rssiNow < -100)
             if(rssi<nowFinderAlarmThresh  && signalWeakCount>2 && mRssiTimer!=null)
             {
                 finderLostNotify();
             }
             /* 12.19 too sensitive
             else if(rssi<(nowFinderAlarmThresh+2) && 
            		 rssi>=nowFinderAlarmThresh &&
            		 mLinklossCharacteristic!=null			)
           	 */
             else if(rssi>=103 && 	// almost disconnected then, alert LED anyway
            		 mLinklossCharacteristic!=null			)
             {
            	 //-- only flash LED 
            	 byte[] bytes = new byte[1];
                		  
            	 bytes[0]=(byte)(ALERT_TYPE_LINKLOSS | ALERT_COMMAND_ENABLE); //-- 09.20 (0x81); 
            	 mLinklossCharacteristic.setValue(bytes);    	
            	 writeCharacteristic(mLinklossCharacteristic);
            	 
            	 if(mLinklossInformed<50) mLinklossInformed++;
             }
             else {	//-- 09.23 series of good signal level, then
                 if(mMediaPlayer!=null) {
                	 
                  	mMediaPlayer.stop(); 
                    mMediaPlayer.release();
                    mMediaPlayer=null;
                 }
                 
                 if(mLinklossInformed>0 && isTimeToClear==true)
                 {                 	
                 	mLinklossInformed--;                  	
                 	if(mLinklossCharacteristic!=null)
                 	{
                   		  byte[] bytes = new byte[1];
                   		  
                   		  bytes[0]=(byte)(ALERT_TYPE_LINKLOSS | ALERT_COMMAND_RESET); //-- 09.20 s(0x00); 
                   		  mLinklossCharacteristic.setValue(bytes);    	
                   		  writeCharacteristic(mLinklossCharacteristic);
                    }               	 
                 }
             }

        }
    	
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        	
        	Log.i(TAG, "onConnectionStateChange:"+newState);
        	
        	//-- 20150924
     		BinsActivity binsActivity;
     		binsActivity=(BinsActivity)(mActivity);     	
        	
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				
				binsActivity.stopDeviceBeaconRegion();

                if(status!=0)
                {	// or 129 something wrong with the connection, Samsung phone !
                	
                	//-- 150609
 					
                    if(getBinsFirmwareRevisionMajor()>3 ||
                    		(getBinsFirmwareRevisionMajor()==3 && getBinsFirmwareRevisionMinor()>=5) )
                    {
                		//-- 133 case is GATT Error, if not found the device
                		mHandler.sendEmptyMessage(MSG_TIMEOUT);  // trigger now             		
                    } else 
                    	if(isFirstHandleFromSamsungError==true){
                		isFirstHandleFromSamsungError=false;
                        mHandler.removeMessages(MSG_TIMEOUT);

                		Log.i(TAG, "Error from Samsung phone, reset Bluetooth");
                		mHandler.sendEmptyMessage(MSG_BLUETOOTH_RESET);
                	} else{
                		//-- 133 case is GATT Error, for major
                		mHandler.sendEmptyMessage(MSG_TIMEOUT);  // trigger now             		
                	}
                	return;                	
                }
                
                mHandler.removeMessages(MSG_TIMEOUT);

				mHandler.sendEmptyMessage(MSG_GATT_SERVICE_DISCOVERY);
				
	  	   	    Log.i(TAG, "Connected to GATT server.");
                
                	
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                //-- ?? mBluetoothGatt=null; //--
                Log.i(TAG, "Disconnected from GATT server.");
                
         		binsActivity.startDeviceBeaconRegion();
                
    			mHandler.sendEmptyMessage(MSG_GATT_DISCONNECT);
    			         			
                mConnectionState = STATE_DISCONNECTED;
                if(isSyncRunning==true || mCheckNotificationTimer!=null){	//-- 09.14
                    if(mCheckNotificationTimer!=null){               	
                    	mCheckNotificationTimer.cancel();
                    	mCheckNotificationTimer=null;
                    }                	
                	//-- force to STOP sync 04.05
                	//-- 11.19 cancel, onEventReceive(ACTION_DATA_SYNC_STOP, true);
            		sendDataSyncEndBroadcast(); //-- means in real time sync, update to parent-level 
                }
                
               	mHandler.removeMessages(MSG_TIMEOUT);  //-- clear timeout handling always in the end               
                
            } 
            else
            {
            	Log.w(TAG, "onConnectionStateChange ..??");
            }
			
	
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG, "onServicesDiscovered received: " + status);

            mHandler.removeMessages(MSG_TIMEOUT);
            mHandler.removeMessages(MSG_PERIODIC_SCAN_TIMEOUT);

            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                mHandler.sendEmptyMessage(MSG_SERVICE_DISCOVERED);

                mConnectionState = STATE_CONNECTED;
                
                
                /*
                //-- this is called to have chance update UI for the sync running status
                onEventReceive(ACTION_DATA_SYNC_RUNNING, mSucceed);
                
                if(mLinklossCharacteristic==null){
                    BluetoothGattService activityService = mBluetoothGatt.getService(UUID
                            .fromString(GattAttributes.SERVICE_ACTIVITY_TRACKING));
                    
                	mLinklossCharacteristic = activityService.getCharacteristic(UUID
                        .fromString(GattAttributes.CHARACTERISTIC_ACTIVITY_LINK_LOSS));
                
                
                	if (mLinklossCharacteristic == null) {
                		Log.e(TAG, "startSync... Can not find activity link loss characteristic");
                    
                		//-- no need to set error, 
                	}
                }

                //-- heart rate 06.19
                {
                	BluetoothGattService activityService = mBluetoothGatt.getService(UUID
                        .fromString(GattAttributes.SERVICE_ACTIVITY_TRACKING));
  
                	mHeartrateCharacteristic = activityService.getCharacteristic(UUID
                        .fromString(GattAttributes.CHARACTERISTIC_HEARTRATE_STREAM));
                	
                	//-- 09.02
                	if(mHeartrateCharacteristic==null) Log.i(TAG, "heart rate tracking is not available.");
                }
                
                {
                                
                	BluetoothGattService deviceInfoService = mBluetoothGatt.getService(UUID
                            .fromString(GattAttributes.SERVICE_DEVICE_INFORMATION));

                	gHardwareRevisionCharacteristic = deviceInfoService.getCharacteristic(UUID
                        .fromString(GattAttributes.CHARACTERISTIC_DI_FW_REVISION));  //-- 09.11 CHARACTERISTIC_DI_HW_REVISION
                	
                }
                
                */
/*                
                // if reach here, will be ok to continue the rest.
                if(mFinderIsRunning==true && mFinderJustCalled==true) {
                	mFinderJustCalled=false;
                	                 
                	setupFinderTimer();
                }	
*/                	 
                
            } 
            else 
            {
                Log.w(TAG, "onServicesDiscovered error: " + status);
                mHandler.sendEmptyMessage(MSG_ERROR);
             }
        }
  
        //-- READ CHARACTERISTICS from BiNS 
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                BluetoothGattCharacteristic characteristic,
                int status) {
            mHandler.removeMessages(MSG_TIMEOUT);
          	mHandler.removeMessages(MSG_READ_ERROR); // 09.14

            if (status == BluetoothGatt.GATT_SUCCESS) {
            	  
            	if(characteristic==gHardwareRevisionCharacteristic){ // 07.28 not using
            		final byte[] bytes = characteristic.getValue();
            		
            		Log.i(TAG,"hardware ID length:"+bytes.length);
            		if(bytes!=null && bytes.length>0){
    
            			final StringBuilder stringBuilder = new StringBuilder(bytes.length+1);
            			for(byte byteChar:bytes){
              				stringBuilder.append(String.format("%c",byteChar));   
            				//-- Log.i(TAG,"rev char:"+byteChar);
            			}
            			
            			Log.i(TAG, "hardware revision:"+stringBuilder);
            			
            			binsFirmwareRevision=stringBuilder.toString();
            			
              			Log.i(TAG, "binsFirmwareRevision:"+binsFirmwareRevision);
              	        
              			//-- 20150609
              			BinsDBUtils.activeDB().setFirmwareRevision(binsFirmwareRevision);
            		}
            		//-- read characteristic must be one by one.
                    readCharacteristic(gBatteryCharacteristic);

            	}else{
            		
            		//-- update to UI, and continue process 
            		if (characteristic==gActivityTimeValueCharacteristic) {
            			mActivity.runOnUiThread(new Runnable()
               	  		{
               	  			@Override
               	  			public void run() 
               	  			{
               	  				//-- 12.06, change from false, true to true, true
               	  				//-- 1st parameter: is sync session running, say yes here.
               	  				//-- 2nd parameter: received a new data record?, say yes here.
               	  				syncCountViewUpdate(true, true);	//-- 08.23 to update the sync status for user
               	  			}
               	  		});    
            		} else if(characteristic==gSleepRecordsCharacteristic){
                  			
            		}
            		
            		mDataSyncModel.onDataRead(characteristic);
            	}
            } else {
                //-- Log.w(TAG, "onCharacteristicRead error " + status);
                mDataSyncModel.readAgain();            	

            }
        }
       
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                BluetoothGattCharacteristic characteristic, int status) {
            //-- Log.d(TAG, "onCharacteristicWrite status: " + status);
            mHandler.removeMessages(MSG_TIMEOUT);
            if(characteristic==mLinklossCharacteristic){
            	
            	Log.d(TAG,"reset Linkloss alert remotely......SUCCESS :" + mLinklossInformed);
            	
            	
            }
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //-- Log.d(TAG, "write success");
                mDataSyncModel.onDataWrite(characteristic);

            }
            else {
            	Log.d(TAG, "write failed");
            	//-- 11.19 meetError();	// don't forget the end of the session?
            }
        }

        public void onCharacteristicChanged (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            mDataSyncModel.onCharacteristicChanged(characteristic); 
            
        }
    };
private void stopScanning() {
        Log.d(TAG, "stopScanning ...");
        if (!mFound) {
            //mDeviceSyncTime.setText(R.string.range_out);
            mDeviceSyncBar.setVisibility(View.GONE);
        }
    }
    
    /* This function solely send handler message to let it be processed 
     * while next time UI thread is available
     */
    public void onEventReceive(final String action, Boolean status) {
        Log.d(TAG, "onEventReceive ... action = " + action);
        
        if (action.equals(ACTION_DATA_SYNC_START)) {
        	boolean result=status;
        	
        	if(result==false){
	    		Log.i(TAG,"isSyncRunning false 2");        	
        		isSyncRunning=false;
        	}
        	mHandler.sendEmptyMessage(MSG_DATA_SYNC_START);
        	
        } else if (action.equals(ACTION_DATA_SYNC_END)) {
        	mHandler.sendEmptyMessage(MSG_DATA_SYNC_END);
        } else if (action.equals(ACTION_DATA_SYNC_RUNNING)) {
        	mHandler.sendEmptyMessage(MSG_DATA_SYNC_RUNNING);
        
        } else if (action.equals(ACTION_DATA_SYNC_STOP)) {
        	mHandler.sendEmptyMessage(MSG_DATA_SYNC_STOP);

        } else if (action.equals(ACTION_FINDER_LOST)) {
        	mHandler.sendEmptyMessage(MSG_FINDER_LOST);
        } else if (action.equals(ACTION_FINDER_STOP)) {
        	mHandler.sendEmptyMessage(MSG_FINDER_STOP);
        } else if (action.equals(ACTION_DATA_NOTIFY)) {
        	mHandler.sendEmptyMessage(MSG_DATA_NOTIFY);
        }
    }

    /* the function is called from message handler when UI thread takes control
     * to process broadcast message that were sent during sync process ran previously
     */
    public void onEventRun(final String action, Boolean status) {
        Log.d(TAG, "onEventRun ... action = " + action + "syncRunning:"+isSyncRunning);
        
        if (action.equals(ACTION_DATA_SYNC_START)) {
            syncViews(true); //-- true, to say the sync session is running, or start to run.
        } else if (action.equals(ACTION_DATA_SYNC_END)) {
        	
        	//if(mConnectionState==STATE_CONNECTED && mCheckNotificationTimer!=null) {
            if(mConnectionState==STATE_CONNECTED) {

        		mTimeControllerView.moveToToday();	// 12.12
        	}
        	
            startQuery();
            syncViews(false); //-- false, to say the sync session is not running anymore
            mIsSyncSessionRunning=false;
            
        } else if(action.equals(ACTION_DATA_SYNC_RUNNING)){
        	        
        	if(hasAlertMessage==true){	//-- 09.20, actual delivery of alert message by this timings
        		setAlertMessage();
        	}
        	if(hasAlertCall==true){	// 150617
        		setAlertCall(alertCallEnable);
        	}

        } else if (action.equals(ACTION_DATA_SYNC_STOP)) {
           	
        	//-- 11.19 isSyncRunning=false;	//-- force to stop the sync 
            // done by _SYNC_END syncViews(false);	//-- 09.14
      	    
        } else if(action.equals(ACTION_DATA_NOTIFY)) {
        	startQuery();
            //-- syncViews(true);
        } else if(action.equals(ACTION_FINDER_LOST)){

        } else if(action.equals(ACTION_FINDER_STOP)){
        }
          
                
    }
    
    /* Phone notice process routines 
     *
     */
    
    private SMSReceiver mSmsReceiver = new SMSReceiver() {
  	  // Retrieve SMS
    	@Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra("get_msg");

       		Log.i(TAG, "sms receiver onReceive");
       		
            // Process the sms format and extract body &amp; phoneNumber
       		if(msg==null) return; //-- in case
       		
            msg = msg.replace("\n", "");
            String body = msg.substring(msg.lastIndexOf(":") + 1,
                          msg.length());
            String pNumber = msg.substring(0, msg.lastIndexOf(":"));

            // Add it to the list or do whatever you wish to
            Log.i(TAG, "" + msg + body + pNumber);

      		setAlertMessage();    		
      	  
      }
    	
    };
    
    private MMSReceiver mMmsReceiver = new MMSReceiver() {
       	@Override
        public void onReceive(Context context, Intent intent) {
       		
       		Log.i(TAG, "mms receiver onReceive");
       		String msg = intent.getStringExtra("get_msg");
       		
       		// Process the sms format and extract body &amp; phoneNumber
       		msg = msg.replace("\n", "");
       		String body = msg.substring(msg.lastIndexOf(":") + 1,
                      msg.length());
       		String pNumber = msg.substring(0, msg.lastIndexOf(":"));

       		// Add it to the list or do whatever you wish to
       		Log.i(TAG, "" + msg + body + pNumber);

       		setAlertMessage();
       	}
    };
    
    private TelephonyStateReceiver mCallReceiver = new TelephonyStateReceiver() {
       	@Override
        public void onReceive(Context context, Intent intent) {
       		
       		Log.i(TAG, "telephony onReceive");
       		String event = intent.getStringExtra("call_event");
       		 if(event.equalsIgnoreCase("hang")==true){
       			//-- setAlertCall(false); 
       		 }else{	// incoming call
        		setAlertCall(true);        			       			 
       		 }

       		
       	}
    };
    /* 20151230
    private BroadcastReceiver mSyncReceiver = new BroadcastReceiver() {

        @Override 
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive ... action = " + action);
            if (action.equals(ACTION_DATA_SYNC_START)) {
                syncViews(true);
            } else if (action.equals(ACTION_DATA_SYNC_END)) {

            	boolean result=intent.getBooleanExtra("status", true);
            	
            	if(result==false){
            		Log.i(TAG, "isSyncRunning 8");
            		isSyncRunning=false;
   
            	}
                syncViews(false);
                
            } else if(action.equals(ACTION_DATA_SYNC_RUNNING)){

            } else if (action.equals(ACTION_DATA_SYNC_STOP)) {
            	isSyncRunning=false;	//-- force to stop the sync 
	        	          	 
            }
        }
    };
    */
    /* UI update to reflect the sync processing status
     * 
     */
    private void showSyncTimeMessage() {
    	    	
        long time = BinsDBUtils.activeDB().getActiveUserLastSyncTime();
                
        if(time==0){
        	mDeviceSyncTime.setText("---");
        }else{
           	if(isAdded()==true){ // 12.17 to avoid exception when fragment not attached to activity
           		mDeviceSyncTime.setText(DateFormat.format(DATE_FORMAT, time * 60 * 1000) +"   " + 
           								getResources().getString(R.string.last_sync_time));
           	} else {
            	mDeviceSyncTime.setText("---");
           	}
        }
    }

    public void syncSleepRecordViewUpdate(final long sessionCount, final long sessionTotal)
    {
		mActivity.runOnUiThread(new Runnable()
   	  	{
   	  		@Override
   	  		public void run() 
   	  		{
   	  			String showMessage;
   	  			showMessage= getResources().getString(R.string.sync_read_sleep_record) +
   	    											"   " + 
   	    											sessionCount + 
   	    											" of "+
   	    											sessionTotal;
   	  			mDeviceSyncTime.setText(showMessage);
   	  			mDeviceSyncBar.setVisibility(View.GONE);
   	    	
   	  			//-- mButtonBypass.setVisibility(View.VISIBLE);    
   	  			
   	  			mSyncProgress.setVisibility(View.VISIBLE);
   	  			mSyncProgress.setMax((int)sessionTotal);
   	  			mSyncProgress.setProgress((int)sessionCount);
   	  		}
   	  	});    

    }
     
    //-- 08.23
    int syncUploadCounter=0;    
    private void syncCountViewUpdate(Boolean isRunning, Boolean isReadData)
    {
    	//-- Log.i(TAG, "reading view:"+isReadData+":"+syncUploadCounter);
    	
    	if(isRunning==true){	// while running sync session, updating status
    		
        	if(isAdded()==false) return; // 12.17, if fragment is not attached, then just return; 
  
    		String showMessage;
    		if(syncUploadCounter==0 || isReadData==false){
        		showMessage= getResources().getString(R.string.connecting_now);    			
    	  		mSyncProgress.setVisibility(View.GONE);
    		}
    		else{
    			showMessage= getResources().getString(R.string.sync_now) + "   " + syncUploadCounter;
        	   	//-- 20151209 no need
    	  		mSyncProgress.setVisibility(View.VISIBLE);
    	  		mSyncProgress.setMax(100);
    	  		mSyncProgress.setProgress(syncUploadCounter%100);

    		}
    		if(isReadData==true) syncUploadCounter++;

    		mDeviceSyncTime.setText(showMessage);
    		mDeviceSyncBar.setVisibility(View.VISIBLE);
    	   	mButtonBypass.setVisibility(View.GONE);   
    	    
    	}
    	else{  //-- end of sync session, to show last sync time, and stop sync indicator
    		
    		showSyncTimeMessage();
            syncUploadCounter=0;
            mDeviceSyncBar.setVisibility(View.GONE); 
           	mButtonBypass.setVisibility(View.GONE);
    	   	mSyncProgress.setVisibility(View.GONE);    	               
            
    	}
    }
    private void syncViews(boolean syncRunning) {
        Log.d(TAG, "syncViews ... sync = " + syncRunning);
        
        String displayName;
        int user=BinsDBUtils.activeDB().getActiveUserID();
      	String userName=BinsDBUtils.activeDB().getUserName(user);
      	
      	Log.i(TAG, "syncViews user and userName="+user+":'+userName");
      	
      	if(userName==null){
      		if(isAdded()==true){ // 12.17
      			displayName= getResources().getString(R.string.device_not_assigned);
      		}else{
      			displayName= ".....";      				
      		}
      	} else if(userName.length()>DISPLAY_NAME_LENGTH_MAX){ 
        	displayName= userName.substring(0, DISPLAY_NAME_LENGTH_MAX);
            displayName= displayName+"..";
        }
        else{ 
           	displayName= userName;            	
        }   
        mDeviceSyncName.setText(displayName);
        

        if (syncRunning==true) {
            syncCountViewUpdate(syncRunning, false); //-- 08.23
            
        } else {	// if false, means the sync session is not running anymore,and end of sync session
            //--**
        	syncCountViewUpdate(syncRunning, false);	//-- 08.23
            if(finderIsActive()==false){
            	mFinderRunningIndicator.setAnimation(null);  
            	gFinderIsActive=false;
            	mFinderRunningIndicator.setVisibility(View.GONE);
            }
            else{
            	// 12.17
            	if(isAdded()==true){
            		Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.refresh);
            		mFinderRunningIndicator.setAnimation(animation);  
            	}
                mFinderRunningIndicator.setVisibility(View.VISIBLE);  
                
             	gFinderIsActive=true;
            }
            

        }
    };
   
    Boolean isSyncStopTaskRunning=false;
	Boolean hadCancelPrevious=false;
	
    private void runDataSync(){
      		int device=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveDeviceID();
      		int userID=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserID();
      		Log.i(TAG, "runDataSync user/device:"+userID+":"+device);
      		if(userID==0){
      			UserProfileActivity.actionUserProfile(getActivity(), true);
      		} else if (device==0) {
                DeviceListActivity.autoPair(getActivity(), userID);
		    }
		    else {
		    	
		    	if(mConnectionState==STATE_DISCONNECTED){
		    		Log.i(TAG, "from STATE_DISCONNECTED");
	    			newDataSyncSession();	//-- added 09.17
		    	}else if(mConnectionState==STATE_CONNECTED ||
		    			(mConnectionState==STATE_CONNECTING && hadCancelPrevious==true))	
		    		{	
		    		if(hadCancelPrevious==true) hadCancelPrevious=false;
		    		Log.i(TAG, "from STATE_CONNECTED");
			    	mDeviceSyncLayout.setEnabled(false);	//-- 09.19
	                mHandler.sendEmptyMessageDelayed(MSG_SYNC_STOP_END, 1000);	//-- 09.14
	                stopDataSync();
	  		        mDeviceSyncBar.setVisibility(View.GONE);
		    	}else if(mConnectionState==STATE_CONNECTING){
		    		Toast toast = Toast.makeText(mActivity, R.string.sync_is_busy,Toast.LENGTH_SHORT);
		    		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);		           
		            toast.show();
		    		Log.i(TAG, "STATE_CONNECTING, cancel");	
		    		hadCancelPrevious=true;  // 12.19, press again to cancel next time
		    	}
/* 11.19		    	
		    	Log.i(TAG, "running:stopTaskRunning="+isSyncRunning+":"+isSyncStopTaskRunning);
		    	
		    	if(isSyncRunning==false && isConnectAndNotifying()==false) {
		    		if(isSyncStopTaskRunning==false){
		    			newDataSyncSession();	//-- added 09.17
		    			
		    		}
		    	}      
		    	else if(isSyncRunning==true && isSyncStopTaskRunning==false){
		    		isSyncStopTaskRunning=true;
		    		Log.i(TAG, "isSyncRunning 10");
		    		isSyncRunning=false;
		    		mDeviceSyncLayout.setEnabled(false);	//-- 09.19
                    mHandler.sendEmptyMessageDelayed(MSG_SYNC_STOP_END, 1000);	//-- 09.14
		    		stopDataSync(getActivity());
  		        	mDeviceSyncBar.setVisibility(View.GONE);
  		        	
		    	}
*/
		    }
	
    }
    
    private OnClickListener onSdnnButtonClickListener = new OnClickListener() {
    	@Override
        public void onClick(View view) {
    		  Dialog dialog = new AlertDialog.Builder(getActivity())
    		    .setTitle(getResources().getString(R.string.sdnn_dialog_title))
    		    .setMessage(getResources().getString(R.string.sdnn_dialog_message))
    		    .setPositiveButton(getResources().getString(R.string.confirm_ok), new DialogInterface.OnClickListener() {
    		     
    		     @Override
    		     public void onClick(DialogInterface dialog, int which) {
    		     }
    		    }).create();
    		  
    		    dialog.show();
    			
    	}    	
    };
    
    public Boolean isSleepRecordReadBypass=false;
    
    private OnClickListener onClickListener = new OnClickListener() {

        @Override
        public void onClick(View view) {
        	//--**        	        	
    		if (view.getId() == R.id.device_sync) {
    			
    	       // view.setBackgroundResource(R.drawable.tableviewcell_background);

    			runDataSync();	//-- 08.04
    			
    		}
    		else if(view.getId()==R.id.device_mental_effect){
    			Log.i(TAG, "enter mental effect");    			   			
    			mHandler.sendEmptyMessage(MSG_MENTAL_EFFECT);    			
    		}
    		else if(view.getId()==R.id.device_heartrate){
    			if(mConnectionState==STATE_CONNECTED){
    				mHeartRateMessageView.setText(getResources().getString(R.string.message_pulse_preparing_read)); 
    				mHeartRateMessageView.setVisibility(View.VISIBLE); 
  
    				mDoNotCheckMentalEffect=true; //-- 20150825
    				startHeartrateDetection();
    				hrdViewUpdateCyclicCounter=0; // reset the counter
    				//-- mHeartRateViewTimeoutTimer.schedule(mHeartRateViewTimeoutTimerTask, 5000); //-- 	07
    			}
    			
    		}
    		else if(view.getId()==R.id.time_controller){
        		Calendar cal=mTimeControllerView.getDate();
      			datePickerDialog.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
      			datePickerDialog.show();

    		} else if(view.getId()==R.id.buttonSyncBypass)	{
      			isSleepRecordReadBypass=true;      			
    		} 
        }
    };
    
    
}
