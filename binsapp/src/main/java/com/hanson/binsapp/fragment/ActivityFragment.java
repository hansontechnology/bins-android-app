package com.hanson.binsapp.fragment;

import java.util.Calendar;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.hanson.binsapp.BinsActivity;
import com.hanson.binsapp.R;
import com.hanson.binsapp.provider.Bins;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.widget.StepsView;
import com.hanson.binsapp.widget.TimeControllerView;

public class ActivityFragment extends BaseFragment {
    private DatePickerDialog datePickerDialog;

    private StepsView mStepsView;

    private float oldDist;
    private View fragmentView;
    private TextView mPulseTitleView;
    private TextView mStepTitleView;
    private float  startX;
    private float lastX;
    private int touchMode=0; 
    private Boolean hasTouchChangeDateActivated;
    
    private int showingMode=0;
    private int mStartViewHour=0;
    
    RadioButton mActivityUnitDayRadio; //radioGroupActivityUnit, radioActivityUnitDay
    RadioButton mActivityUnitMonthRadio; //radioGroupActivityUnit, radioActivityUnitDay
    
    private ProgressBar mWorkingProgress;
    
    private boolean mIsCloudWorking=false;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ...");
        
        View theView=inflater.inflate(R.layout.fragment_activity, container, false);
        
        fragmentView=theView;
       
        mActivityUnitDayRadio=(RadioButton) theView.findViewById (R.id.radioActivityUnitDay); 
		//radioGroupActivityUnit, radioActivityUnitDay
        mActivityUnitMonthRadio=(RadioButton)theView.findViewById(R.id.radioActivityUnitMonth); 
        //radioGroupActivityUnit, radioActivityUnitDay

        mActivityUnitDayRadio.setOnCheckedChangeListener(mOnCheckedChangeListener);
        mActivityUnitMonthRadio.setOnCheckedChangeListener(mOnCheckedChangeListener);

        mWorkingProgress= (ProgressBar)theView.findViewById (R.id.progressActivityWorking);
        mWorkingProgress.setVisibility(View.GONE);
        
        datePickerDialog = new DatePickerDialog(getActivity(), datePickerListener,2000, 1,1);
 
      
        

        //-- 08.03 to implement touch two fingers for two view resolutions        
        final GestureDetector gesture = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {
        	
            		@Override
            		public boolean onDown(MotionEvent e) {
            			return true;
            		}

                    //@Override
                    //public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    //	return true;
                    //}

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                        float velocityY) {

                        try {
/*                        	
                            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                                return false;
                            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Log.i(Constants.APP_TAG, "Right to Left");
                            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                                Log.i(Constants.APP_TAG, "Left to Right");
                                titles.showDetails(getPosition() - 1);
                            }
*/                            
                        } catch (Exception e) {
                            // nothing
                        }
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

            theView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                	
                	//-- Log.i(TAG, "touch: "+event.getX()+":"+event.getY());
                	
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    
                    case MotionEvent.ACTION_POINTER_DOWN:
                    	   oldDist = spacing(event);
                    	   if (oldDist > 10f) {
                    	      touchMode = 1;
                    	      Log.d(TAG, "mode=ZOOM" );
                    	      
                    	      hasTouchChangeDateActivated=false;
                    	   }
                     	   fragmentView.getParent().requestDisallowInterceptTouchEvent(true);
                    	                       	                       	   
                    	   lastX=(event.getX(0)+event.getX(1))/2;
                    	   
                    	   startX=lastX;
                    	   
                    	   break;

                    case MotionEvent.ACTION_MOVE:
                    	
                    	   if(event.getPointerCount()<2) break;
                    	   
                    	   if (touchMode == 1) 
                    	   {
                    		   float newDist = spacing(event);
                    		   float scale = newDist / oldDist;
                    	         
                    		   if((newDist > 10f) && (scale>1.5 || scale<0.7) && hasTouchChangeDateActivated==false){
                    			   if(scale>1.5){
                    					   if(showingMode<1){ 
                    						   showingMode++;
                    						   mStartViewHour=(int)((startX/v.getWidth())*24-1);
                    						   if(mStartViewHour<0) mStartViewHour=0;
                    						   
                    						   Log.i(TAG,"touch: start:"+mStartViewHour);
                    					   }
                    			   }
                    			   else if(scale<0.7){
                    					   if(showingMode>0) showingMode--;
                    			   }
                    			   //-- mTimeControllerView.changeDate(0);
                    			   hasTouchChangeDateActivated=true; 
                    	        	 
                    			   Log.i(TAG, "touch: zoom scale inside:"+scale);
                    			   
             	   					Cursor cursor = queryData();                       	   				
               	   					if(cursor!=null) {
               	   						handleData(cursor);
               	   						update();
               	   					}                     	                                	   				

                    			} // if scale >2 or < 0.5
                    		   	else if(hasTouchChangeDateActivated==false){
                    		   		float gapX;
                   	      	    
                    		   		float currentX=(event.getX(0)+event.getX(1))/2;
                    		   		
                    		   		gapX=currentX-lastX;
                    		   		
                    		   		if(showingMode==0){
                    		   			if((gapX<-50 ||  gapX>50) &&
                       	   					// (gapY<10 && gapY>-10) && 
                       	   					hasTouchChangeDateActivated==false)
                    		   			{
                    		   				Log.i(TAG, "touch: swing test:" );
                    		   				if(gapX<-50)
                    		   					mTimeControllerView.changeDate(1);
                    		   				else if(gapX>50)
                    		   					mTimeControllerView.changeDate(-1);                       	   				
                       	   				
                    		   				//-- hasTouchChangeDateActivated=true;
                    		   				lastX=currentX;
                    		   			}
                    		   		}else{	// if showingMode > 0
                    		   			if((gapX<-50 ||  gapX>50) &&
                       	   					// (gapY<10 && gapY>-10) && 
                       	   					hasTouchChangeDateActivated==false)
                    		   			{
                    		   				if(gapX>50){
                    		   					if(mStartViewHour>0) mStartViewHour--;
                    		   				}
                    		   				else if(gapX<-50){
                    		   					if(mStartViewHour<24) mStartViewHour++;
                    		   				}                       	   				                       	   				
                    		   				Log.i(TAG, "touch: hour:"+mStartViewHour );

                    		   				//-- mTimeControllerView.changeDate(0);
                       	   					Cursor cursor = queryData();                       	   				
                       	   					if(cursor!=null) {
                       	   						handleData(cursor);
                       	   						update();
                       	   					}                     	                                	   				
                    		   				//-- hasTouchChangeDateActivated=true;
                    		   				lastX=currentX;
                        		   			} // end if gap > -50 or < -50
                    		   		} // end if showing mode >0                   	      	
                    		   	} // if not scale > 2 or < 0.5
                    	   }	// if touchMode==1
                     	   break;
                    	   
                        case MotionEvent.ACTION_CANCEL:  
                            fragmentView.getParent().requestDisallowInterceptTouchEvent(false); 
                            touchMode=0;
                            break;  

                    }
               	
                    return gesture.onTouchEvent(event);
                    
                   
                }
                
                 
            });
        
        return theView;
    }
    
    @Override
    public void onStart(){
    	
    	super.onStart();
    	
        //-- 20151212
        BinsActivity binsActivity=(BinsActivity)getActivity();
        binsActivity.setUiFragmentEventListener(onUiFragmentEventListener);
    }
    
    
    private DatePickerDialog.OnDateSetListener datePickerListener 
    = new DatePickerDialog.OnDateSetListener() {
        
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            mTimeControllerView.setDate(newDate);
        }

    };
    
    private OnCheckedChangeListener mOnCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton cButton, boolean status) { 
        	boolean isDay;
        	// isMale=mGenderSwitch.isChecked();
        	 isDay=mActivityUnitDayRadio.isChecked();
        	 
        	 if(isDay==true){
        		 mActivityUnitMonthRadio.setChecked(false);
        	 }
        	 else{
           		 mActivityUnitMonthRadio.setChecked(true);        		 
        	 }
        	 
        	 if(isDay){
        		 mTimeControllerView.setDisplayMode(TimeControllerView.VIEW_MODE_UNIT_DAY);
        	 }else{
        		 mTimeControllerView.setDisplayMode(TimeControllerView.VIEW_MODE_UNIT_MONTH);		 
        	 }
        }
    };
    
    public void refreshViews()
    {
    	
    		checkUserChanged();
    		
    		
			Cursor cursor = queryData();                       	   				
			if(cursor!=null) {
				handleData(cursor);
				update();
			} 
			
	 }
    
     private float spacing(MotionEvent event) {
    	
    	if(event.getPointerCount()<2) return 0;

    	float x = event.getX(0) - event.getX(1);
    	float y = event.getY(0) - event.getY(1);
    	return FloatMath.sqrt(x * x + y * y);
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTimeControllerView = (TimeControllerView) view.findViewById(R.id.steps_controller);
        mTimeControllerView.setOnDateChangedListener(mListener);
        mStepTitleView=(TextView) view.findViewById(R.id.steps_title);
        mPulseTitleView=(TextView) view.findViewById(R.id.heartrate_title); 
        mPulseTitleView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
        mPulseTitleView.setVisibility(View.GONE);
        mStepsView = (StepsView) view.findViewById(R.id.steps_view);
        
        mTimeControllerView.setOnClickListener(onClickListener);
  
      }

    @Override
    protected void update() {    	
    	mStepsView.invalidate(); 
    }
    
    int showingStartTime=0, showingEndTime=0;

    private void updateDisplayTimePeriod(int currentTime) {
    	
        if(mTimeControllerView.getDisplayMode()==TimeControllerView.VIEW_MODE_UNIT_DAY)
        {
        	showingStartTime=currentTime;
        	showingEndTime=currentTime+24*60;
        }
        else if(mTimeControllerView.getDisplayMode()==TimeControllerView.VIEW_MODE_UNIT_MONTH){
           	showingStartTime=currentTime;
           	
           	int days=mTimeControllerView.actualDaysOfTheMonth();
            showingEndTime=currentTime+days*24*60;
        }
        else
        {
        	showingStartTime=currentTime+(mStartViewHour*60);
        	showingEndTime=showingStartTime+1*60; // 14*60;
        }
       
    }
    
    @Override
    protected Cursor queryData() {
        Log.d(TAG, "queryData ...");
        
        Calendar queryStartTime;
        if(mTimeControllerView.getDisplayMode()==TimeControllerView.VIEW_MODE_UNIT_DAY){
        	queryStartTime=mTimeControllerView.startOfTheDay();
        }else{
        	queryStartTime=mTimeControllerView.startOfTheMonth();
        }
        
        int currentTime = (int) (queryStartTime.getTimeInMillis() / MINUTE_MILLIS);
                    
        updateDisplayTimePeriod(currentTime);
         
        Cursor cursor;
        
        if(mIsCloudWorking==true){    
        	mIsCloudWorking=false;
            cursor = BinsDBUtils.activeDB().queryActivityRecords(mCurrentUserID, showingStartTime, showingEndTime);

        }else{
        	cursor = BinsDBUtils.activeDB().queryActivityRecordsWithCloudLoad(mCurrentUserID, showingStartTime, showingEndTime);
        }
        
        int queryResult=BinsDBUtils.activeDB().getQueryResult();
        mIsCloudWorking=(queryResult==BinsDBUtils.QUERY_WORKING);
        
        return cursor;
    }

    @Override 
    protected synchronized void queryTaskUiBeforeProgress(){
       	if(mIsCloudWorking==true){
       		mWorkingProgress.setVisibility(View.VISIBLE);
        }else{
      		mWorkingProgress.setVisibility(View.GONE);      		 
        }           	
    }
    
    @Override
    protected synchronized void handleData(Cursor cursor) {
        Log.d(TAG, "Unlocking handleData ..."+mIsCloudWorking);
        
        if (mStepsView==null || mPulseTitleView==null) return;	// not initiated yet

        // cursor already has the data list from the time period
        
                
       	if(mIsCloudWorking==true){
      		mStepsView.resetSteps(mTimeControllerView.getDisplayMode(), mStartViewHour, mPulseTitleView);
          	mStepTitleView.setVisibility(View.INVISIBLE);
      	  
       		return; // working on cloud side, waiting for the result later
        }
        
  		mWorkingProgress.setVisibility(View.GONE);      		 
       	
        if (cursor != null) {
        	mStepTitleView.setVisibility(View.VISIBLE);
    		mStepsView.resetSteps(mTimeControllerView.getDisplayMode(), mStartViewHour, mPulseTitleView);
            cursor.moveToNext();
            while (!cursor.isAfterLast()) {
                int steps = cursor.getInt(0);
                int time = cursor.getInt(1);
                int type  = cursor.getInt(2);	//-- 07.13
                if (DEBUG) 
                {
                    Log.d(TAG, "Unlocking Values: " + time+":" +type + ":"+ steps);          
                }
                if(type==Bins.BINS_ACTIVITY_TYPE_STEPS){
                	mStepsView.setSteps(time - showingStartTime, steps);
                } else if(type==Bins.BINS_ACTIVITY_TYPE_PULSE){
                	mStepsView.setHeartRate((time/60) - showingStartTime, steps);  // translate to minute from second              	
                }
                cursor.moveToNext();
            }
            cursor.close();
        }else{
            mStepsView.resetSteps(mTimeControllerView.getDisplayMode(), mStartViewHour, mPulseTitleView);
           	mStepTitleView.setVisibility(View.INVISIBLE);
        }
        
        mStepsView.calculateMaxAndMinValue();
        
    }


    private OnClickListener onClickListener = new OnClickListener() {

    	@Override
    	public void onClick(View view) {
    		// Log.i(TAG, "clicked");
    		Calendar cal=mTimeControllerView.getDate();
  			datePickerDialog.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
  			datePickerDialog.show();

    	}	
    };

}
