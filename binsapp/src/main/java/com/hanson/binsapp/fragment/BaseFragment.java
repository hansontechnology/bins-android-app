
package com.hanson.binsapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hanson.binsapp.BinsActivity.OnUiFragmentEventListener;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.widget.TimeControllerView;
import com.hanson.binsapp.widget.TimeControllerView.OnDateChangedListener;
  
public abstract class BaseFragment extends Fragment {
	
	
    public static final String ACTION_DATA_SYNC_START = "com.data.sync.START";
    public static final String ACTION_DATA_SYNC_END = "com.data.sync.END";
    public static final String ACTION_DATA_SYNC_RUNNING = "com.data.sync.RUNNING";
    public static final String ACTION_DATA_SYNC_STOP = "com.data.sync.STOP";

    public static final String ACTION_FINDER_LOST = "com.data.finder.LOST";
    public static final String ACTION_FINDER_STOP = "com.data.finder.STOP";
    public static final String ACTION_DATA_NOTIFY = "com.data.sync.NOTIFY";
    public static final String EXTRA_STATUS = "status";

    
    public static final int DAY_MINUTES = 24 * 60;
    public static final int MINUTE_MILLIS = 60 * 1000;
    public static final boolean DEBUG = false;

    protected String TAG = getClass().getSimpleName();
    protected int mCurrentDeviceID;
    protected int mCurrentUserID;
    protected ProgressDialog mProgressDialog;
    public TimeControllerView mTimeControllerView=null;

    private volatile boolean created = false;
    public static long gDateSelected=0;
    public static  int gDailyGoal=2340;
    public static  int gFinderSetting=0;
    public static  boolean gFinderIsActive=false;
    public static  int gConfiguration;
    
    public static BinsFragment activeBinsFragment;
    
    
    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d(TAG, "onReceive ... " + action);
            if (action != null && action.equals(ACTION_DATA_SYNC_END)) {
                //-- final boolean status = intent.getBooleanExtra(EXTRA_STATUS, false);
                //-- Log.d(TAG, "onReceive ... status = " + status);
                startQuery();
            } else if (action != null && action.equals(ACTION_DATA_NOTIFY)){
                final boolean status = intent.getBooleanExtra(EXTRA_STATUS, false);
                Log.d(TAG, "onReceive notify = " + status);
                startQuery();
            } else if (action != null && action.equals(ACTION_FINDER_LOST)){
                final boolean status = intent.getBooleanExtra(EXTRA_STATUS, false);

                Log.d(TAG, "onReceive Finder lost... status = " + status);
/* Done by background service                
            	try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    
                    MediaPlayer mp = MediaPlayer.create(context, notification);
                    mp.start();
                    
                } catch (Exception e) {}         
*/   	
            }
            
            
        }
    };

    protected OnDateChangedListener mListener = new OnDateChangedListener() {

        @Override
        public void OnDateChanged(TimeControllerView timeView, long timeInMillis) {
            //-- Log.d(TAG, "OnDateChanged ... timeInMillis = " + timeInMillis);
            
            //-- 
            gDateSelected=timeInMillis;
            
            startQuery();
        }
    };

    private QueryTask mQueryTask = null;
    private class QueryTask extends AsyncTask<Void, Void, Void> {
    	
    	Cursor mActiveCursor=null;
    	
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "QueryTask ... onPreExecute");
            showProgressDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.d(TAG, "Unlocking QueryTask ... doInBackground");
            
            
            Cursor cursor = queryData();
            if (!isCancelled() ) //-- 20151212 otherwise will not work following && cursor!=null) 
            {
                //-- handleData(cursor);
            	mActiveCursor=cursor;
            } 
            publishProgress();            

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            Log.d(TAG, "QueryTask ... onProgressUpdate");
            
            queryTaskUiBeforeProgress();	//-- 20151212 to show UI elements before query start
        }
        
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.d(TAG, "QueryTask ... onPostExecute");
            hideProgressDialog();
            handleData(mActiveCursor);
            update();
        }
 
        
        @Override
        protected void onCancelled() {
            super.onCancelled();
            Log.d(TAG, "QueryTask ... onCancelled");
            hideProgressDialog();
        }

 
   };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCurrentDeviceID = BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveDeviceID();
        gDailyGoal = BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserDailyGoal();
        //-- gConfiguration=BinsDBUtils.getBluetoothLeDao(getActivity()).getBinsConfiguration();
        mCurrentUserID=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserID(); // return default if not yet set
        
        Log.d(TAG, "base onAttach ... mCurrentDeviceID = " + mCurrentDeviceID);
              
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_DATA_SYNC_START);
        intentFilter.addAction(ACTION_DATA_SYNC_END);
        intentFilter.addAction(ACTION_DATA_SYNC_RUNNING);
        intentFilter.addAction(ACTION_FINDER_LOST);  
        intentFilter.addAction(ACTION_FINDER_STOP);
        intentFilter.addAction(ACTION_DATA_NOTIFY);
        getActivity().registerReceiver(mReceiver, intentFilter);  //-- 12.07

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate ...");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ...");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated ...");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated ...");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d(TAG, "onViewStateRestored ...");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart ...");
        checkUserChanged();
        checkDeviceChanged();
        //--
        if(mTimeControllerView!=null)
        {
        	if(gDateSelected==0) {
        		gDateSelected=mTimeControllerView.getTimeInMillis();;
        	}
        	else
        	{        		
        		mTimeControllerView.setTimeInMillis(gDateSelected); 
        	}
        }

        //-- 150618 startQuery();
        
        //-- 12.07
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ...");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ...");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ...");
        //-- 09.21 remark for background concern getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView ...");
        created = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState ...");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy ...");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach ...");
        
        getActivity().unregisterReceiver(mReceiver);	//-- 12.07

    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
       	
        if (menuVisible && created) {
            onStart();
            onResume();
        }
        
        //-- Log.d(TAG, "setMenuVisibility ... menuVisible = " + menuVisible + ", created = " + created);
        
        if(mTimeControllerView!=null && menuVisible==true) //-- 09.13
        {
        	if(gDateSelected==0) {
        		gDateSelected=mTimeControllerView.getTimeInMillis();
        	}
        	else
        	{

        		//-- 4.15 if(gDateSelected!=mTimeControllerView.getTimeInMillis())
        		{
        			//-- Log.d(TAG,"set Time try");
    		
        			mTimeControllerView.setTimeInMillis(gDateSelected); 
        			checkUserChanged();	//-- 09.16
        			startQuery();
        		}	
        				
        	}

        }
    }

    //-- 20151212
    public OnUiFragmentEventListener onUiFragmentEventListener=new OnUiFragmentEventListener(){
        
        public void binsDataChanged(){
        	Log.i(TAG, "Unlocking OnUiFragmentEventListner binsDataChanged");        	
        	
        	startQuery();
        }
        
    };
    
    public Boolean checkUserChanged(){
    	Log.d(TAG, "checkUserChanged");
        //-- 08.4
        int userID=BinsDBUtils.getBluetoothLeDao(getActivity()).getActiveUserID(); // return default if not yet set
         
        if(userID!=0 && mCurrentUserID!=userID){
        	Log.i(TAG, "user name changed: user:"+mCurrentUserID+":"+userID);
        	mCurrentUserID=userID;
        	
        	return true;       
        }	
        	
        return false;
    }
    public Boolean checkDeviceChanged() {
        Log.d(TAG, "checkDeviceChanged ...");
        final int deviceID = BinsDBUtils.getBluetoothLeDao(getActivity())
                .getActiveDeviceID();
        if (deviceID!=0 && mCurrentDeviceID!=deviceID) {
            Log.d(TAG, "checkDeviceChanged ... Device has been changed");
            mCurrentDeviceID = deviceID;
            return true;
         } 
 
        return false;
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            //-- mProgressDialog.setMessage(getResources().getString(R.string.loading));
        }

        //-- mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    protected void startQuery() {
    	
    	/* cannot return here, must give chance to refresh view from child 
        if (mCurrentDeviceID == 0) {
            Log.w(TAG, "startQuery ... mCurrentDeviceID == 0");
            return;
        }
        */

        Log.d(TAG, "Unlocking startQuery ...");
        // Cancel and restart it
        if (mQueryTask != null && mQueryTask.getStatus() == AsyncTask.Status.RUNNING) {
            Log.w(TAG, "Unlocking startQuery ... mQueryTask cancel");
            mQueryTask.cancel(true);
        }

        Log.w(TAG, "Unlocking startQuery ... mQueryTask begin");
        mQueryTask = new QueryTask();
        mQueryTask.execute();
    }

    protected abstract void update();
    protected abstract Cursor queryData();
    protected abstract void handleData(Cursor cursor);
    protected abstract void queryTaskUiBeforeProgress();
    
    
}