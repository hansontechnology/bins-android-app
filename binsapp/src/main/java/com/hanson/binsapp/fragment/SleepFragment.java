package com.hanson.binsapp.fragment;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.FloatMath;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.hanson.binsapp.R;
import com.hanson.binsapp.provider.BinsDBUtils;
import com.hanson.binsapp.provider.LevelPeriodBuilder;
import com.hanson.binsapp.widget.AmbientLightView;
import com.hanson.binsapp.widget.SleepView;
import com.hanson.binsapp.widget.TimeControllerView;

public class SleepFragment extends BaseFragment {
	private DatePickerDialog datePickerDialog;

    private SleepView mSleepView;
    private AmbientLightView mLightView;
    
    private TextView mLightSleepTimeText;
    private TextView mDeepSleepTimeText;
    private TextView mTotalSleepTimeText;
    
    private boolean mLastSleepQueryHadRecord=false;
    private RadioButton mSleepEnableRadioButton;
    private RadioButton mSleepDisableRadioButton;
    private ProgressBar mWorkingProgress;
    
    private boolean mIsCloudWorking=false;
    

    private float oldDist;
    private View fragmentView;
    private float lastX;
    private int touchMode=0; 
    private Boolean hasTouchChangeDateActivated;
    
    private int showingMode=0;
    private int mStartViewHour=0;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView ...");      
        //-- View theView=inflater.inflate(R.layout.fragment_sleep_hbp, container, false);  /* 20151230 HBP replacement to fragment_sleep_hbp */
        View theView=inflater.inflate(R.layout.fragment_sleep, container, false);
        
        fragmentView=theView;
        
        datePickerDialog = new DatePickerDialog(getActivity(), datePickerListener,2000, 1,1);
        
        
        //-- 08.03 to implement touch two fingers for two view resolutions        
        final GestureDetector gesture = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {       	
            		@Override
            		public boolean onDown(MotionEvent e) {
            			return true;
            		}
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                        float velocityY) {

                        return super.onFling(e1, e2, velocityX, velocityY);
                    }
                });

        
        theView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                	
                	//-- Log.i(TAG, "touch: "+event.getX()+":"+event.getY());
                	
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    
                    case MotionEvent.ACTION_POINTER_DOWN:
                    	   oldDist = spacing(event);
                    	   if (oldDist > 10f) {
                    	      touchMode = 1;
                    	      Log.d(TAG, "mode=ZOOM" );
                    	      
                    	      hasTouchChangeDateActivated=false;
                    	   }
                     	   fragmentView.getParent().requestDisallowInterceptTouchEvent(true);
                    	   
                    	   //-- startX=event.getX(0);
                    	   //-- startY=event.getY(0);
                    	                       	   
                    	   lastX=(event.getX(0)+event.getX(1))/2;
                    	   
                    	   break;

                    case MotionEvent.ACTION_MOVE:
                    	
                    	   if(event.getPointerCount()<2) break;
                    	   
                    	   if (touchMode != 1) break;
                    	   
                           float newDist = spacing(event);
                		   float scale = newDist / oldDist;
                	         
                		   if((newDist > 10f) && (scale>1.5 || scale<0.7) && hasTouchChangeDateActivated==false)
                		   {
                			   if(scale>1.5){
                				   /* 09.23 TODO later
                					   if(showingMode<1){ 
                						   showingMode++;
                						   mStartViewHour=(int)((startX/v.getWidth())*24-1)+(12); //-- 09.23
                						   if(mStartViewHour<0) mStartViewHour=0+(12);//-- 09.23
                						   if(mStartViewHour>24) mStartViewHour -= (24);
                						  
                						   Log.i(TAG,"touch: start:"+mStartViewHour);
                					
                					   }
                					*/
                			   }
                			   else if(scale<0.7){
                					   //-- 09.23 TODO later, if(showingMode>0) showingMode--;
                			   }
                			   //-- mTimeControllerView.changeDate(0);
                			   hasTouchChangeDateActivated=true; 
                	        	 
                			   Log.i(TAG, "touch: zoom scale inside:"+scale);
                			   
         	   					Cursor cursor = queryData();                       	   				
           	   					if(cursor!=null) {
           	   						handleData(cursor);
           	   						update();
           	   					}                     	                                	   				

                			} // if scale >2 or < 0.5
                		   	else if(hasTouchChangeDateActivated==false){
                		   		float gapX;
               	      	    
                		   		float currentX=(event.getX(0)+event.getX(1))/2;
          		
                		   		gapX=currentX-lastX;
                		   		if(showingMode==0)
                		   		{
                		   			if((gapX<-50 ||  gapX>50) &&
                   	   					// (gapY<10 && gapY>-10) && 
                   	   					hasTouchChangeDateActivated==false)
                		   			{
                		   				Log.i(TAG, "touch: swing test:" );
                		   				if(gapX<-50)
                		   					mTimeControllerView.changeDate(1);
                		   				else if(gapX>50)
                		   					mTimeControllerView.changeDate(-1);                       	   				
                   	   				
                		   				//-- hasTouchChangeDateActivated=true;
                		   				lastX=currentX;
                		   			}
                		   			
                		   		}else{	// if showingMode > 0
                		   			if((gapX<-50 ||  gapX>50) &&
                   	   					// (gapY<10 && gapY>-10) && 
                   	   					hasTouchChangeDateActivated==false)
                		   			{
                		   				if(gapX>50){
                		   					if(mStartViewHour==0) mStartViewHour=24;  //-- 09.23
                		   					if(mStartViewHour!=12) mStartViewHour--;	//-- 09.23
                		   				}
                		   				else if(gapX<-50){
                   		   					if(mStartViewHour!=11) mStartViewHour++;
                 		   					if(mStartViewHour==24) mStartViewHour=0;  //-- 09.23
                		   				}                       	   				                       	   				
                		   				Log.i(TAG, "touch: hour:"+mStartViewHour );

                		   				//-- mTimeControllerView.changeDate(0);
                   	   					Cursor cursor = queryData();                       	   				
                   	   					if(cursor!=null) {
                   	   						handleData(cursor);
                   	   						update();
                   	   					}                     	                                	   				
                		   				//-- hasTouchChangeDateActivated=true;
                		   				lastX=currentX;

                		   			} // end if gap > -50 or < -50
                		   		} // end if showing mode >0 
                		   			
                		   			
                		   	} // if not scale > 2 or < 0.5
                   		   		
                     	   break;
                    	   
                        case MotionEvent.ACTION_CANCEL:  
                            fragmentView.getParent().requestDisallowInterceptTouchEvent(false); 
                            touchMode=0;
                            break;  

                    }
               	
                    return gesture.onTouchEvent(event);
                    
                   
                }
                
                 
        });

        /*          
        // 01.05
        View view = inflater.inflate(R.layout.popup, null);  
        // 创建PopupWindow对象  
        final PopupWindow pop = new PopupWindow(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, false);  
        // Button btn = (Button) findViewById(R.id.btn);  
        // 需要设置一下此参数，点击外边可消失  
        pop.setBackgroundDrawable(new BitmapDrawable());  
        //设置点击窗口外边窗口消失  
        pop.setOutsideTouchable(true);  
        // 设置此参数获得焦点，否则无法点击  
        pop.setFocusable(true);  
        
        pop.showAsDropDown(theView);

        btn.setOnClickListener(new OnClickListener() {  
              
            @Override  
            public void onClick(View v) {  
                if(pop.isShowing()) {  
                    // 隐藏窗口，如果设置了点击窗口外小时即不需要此方式隐藏  
                    pop.dismiss();  
                } else {  
                    // 显示窗口  
                    pop.showAsDropDown(v);  
                }  
                  
            }  
        });  
         */
        
        // 01.05 showPopUp(theView);
        
        return theView;
    }
    
    /* 01.05
    private void showPopUp(View v) {  
        LinearLayout layout = new LinearLayout(getActivity());  
        layout.setBackgroundColor(Color.GRAY);  
        TextView tv = new TextView(getActivity());  
        tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));  
        tv.setText("I'm a pop -----------------------------!");  
        tv.setTextColor(Color.WHITE);  
        layout.addView(tv);  
  
        final PopupWindow popupWindow = new PopupWindow(layout,120,120);  
          
        popupWindow.setFocusable(true);  
        popupWindow.setOutsideTouchable(true);  
        popupWindow.setBackgroundDrawable(new BitmapDrawable());  
          
        int[] location = new int[2];  
        v.getLocationOnScreen(location);  
          
        popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0], location[1]-popupWindow.getHeight());  
    } 
    */
    
    private DatePickerDialog.OnDateSetListener datePickerListener 
    = new DatePickerDialog.OnDateSetListener() {
        
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            mTimeControllerView.setDate(newDate);
        }

    };
    public void refreshViews()
    {   	
    	
    		boolean sleepEnabled=BinsDBUtils.activeDB().getActiveSleepEnable();
    		mSleepEnableRadioButton.setChecked(sleepEnabled);
    		mSleepDisableRadioButton.setChecked(!(sleepEnabled));
    		
    		if(BinsDBUtils.activeDB().getActiveUserID()==0){
    			mSleepEnableRadioButton.setEnabled(false);
    			mSleepDisableRadioButton.setEnabled(false);
    		} else {
    			mSleepEnableRadioButton.setEnabled(true);
    			mSleepDisableRadioButton.setEnabled(true);			
    		}
    		
    		checkUserChanged();
    		
    		/*
			Cursor cursor = queryData();                       	   				
			if(cursor!=null) {
				handleData(cursor);
				update();
			} 
			*/                    	                                	   				
    }
    public void redrawViews() {	//-- 20151022
    	Cursor cursor = queryData();                       	   				
		if(cursor!=null) {
			handleData(cursor);
			update();
		}                     	                                	   				
    }
    private float spacing(MotionEvent event) {
    	
    	if(event.getPointerCount()<2) return 0;

    	float x = event.getX(0) - event.getX(1);
    	float y = event.getY(0) - event.getY(1);
    	return FloatMath.sqrt(x * x + y * y);
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTimeControllerView = (TimeControllerView) view.findViewById(R.id.sleep_controller);
        mTimeControllerView.setOnDateChangedListener(mListener);
        mSleepView = (SleepView) view.findViewById(R.id.sleep_view);
        mLightView = (AmbientLightView) view.findViewById(R.id.ambient_light_view);
        
        // RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mSleepView.getLayoutParams();
        // params.height = 300;
        // mSleepView.setLayoutParams(params);
        
        mTimeControllerView.setOnClickListener(onClickListener);
        
        // light_sleep_time_text
        mDeepSleepTimeText=(TextView)view.findViewById(R.id.deep_sleep_time_text);
        mLightSleepTimeText=(TextView)view.findViewById(R.id.light_sleep_time_text);
        mTotalSleepTimeText=(TextView)view.findViewById(R.id.total_sleep_time_text);
        mWorkingProgress=(ProgressBar)view.findViewById(R.id.sleep_view_progress);        
        mWorkingProgress.setVisibility(View.GONE);
        
        
        mSleepEnableRadioButton=(RadioButton)view.findViewById(R.id.sleep_enable_radio_button);
        mSleepDisableRadioButton=(RadioButton)view.findViewById(R.id.sleep_disable_radio_button);
        
        mSleepEnableRadioButton.setOnCheckedChangeListener(mOnCheckedChangeListener);
        mSleepDisableRadioButton.setOnCheckedChangeListener(mOnCheckedChangeListener);
        
        
  		boolean sleepEnabled=BinsDBUtils.activeDB().getActiveSleepEnable();
		mSleepEnableRadioButton.setChecked(sleepEnabled);
		mSleepDisableRadioButton.setChecked(!(sleepEnabled));
		
		if(BinsDBUtils.activeDB().getActiveUserID()==0){
			mSleepEnableRadioButton.setEnabled(false);
			mSleepDisableRadioButton.setEnabled(false);
		} else {
			mSleepEnableRadioButton.setEnabled(true);
			mSleepDisableRadioButton.setEnabled(true);			
		}

		/* 2015.01.05
		mSleepView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                
                    
                case MotionEvent.ACTION_UP:
                  	Log.i(TAG, "teddddddd");
                    showPopUp(v);
                  	break;                	

                }
                return true;
                
            }
		  }
	    );
		
        showPopUp(mTimeControllerView);
        */
    }
    
    //-- 150618 
    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart sleep");
        startQuery();
    }
    
    private OnCheckedChangeListener mOnCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton cButton, boolean status) { 
        	boolean enable;
        	// isMale=mGenderSwitch.isChecked();
        	
        	if(cButton==mSleepEnableRadioButton){
        		
        		Log.i(TAG, "radio enable:"+mSleepEnableRadioButton.isChecked()+":"+mSleepDisableRadioButton.isChecked());
        		
        		enable=mSleepEnableRadioButton.isChecked();
        	 
           	 	if(enable==true){
           	 		mSleepDisableRadioButton.setChecked(false);
           	 	}
           	 	else{
           	 		mSleepDisableRadioButton.setChecked(true);        		 
           	 	}
           	 
           	 	if(BinsDBUtils.activeDB().getActiveSleepEnable()!=enable){
           	 		// 150321, only set if different from current setting
           	 		// Otherwise, always set when fragment page switch
           	 		BinsDBUtils.activeDB().setActiveSleepEnable(enable);
           	 		activeBinsFragment.updateBinsConfiguration();
           	 	}
         		
        	}

       }
    };

    @Override
    protected void update() {
    	       
        mSleepView.invalidate();
        mLightView.invalidate();

		// Log.i(TAG, "from update:"+mLastSleepQueryHadRecord);
		Activity nowActivity=getActivity();
		if(nowActivity==null) return;
		
     	nowActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
 
    	  if(mLastSleepQueryHadRecord==true){
    		// Log.i(TAG, "from update");
    		
    		mDeepSleepTimeText.setVisibility(View.VISIBLE);
    		mLightSleepTimeText.setVisibility(View.VISIBLE);
    		mTotalSleepTimeText.setVisibility(View.VISIBLE);
    	    	
    		String stringDeepSleepTime=stringTimeFormatFromMinute(mSleepView.mDeepSleepTime);
    		String stringLightlSleepTime=stringTimeFormatFromMinute(mSleepView.mLightSleepTime);
    		String stringTotalSleepTime=stringTimeFormatFromMinute(mSleepView.mTotalSleepTime);
 
    		// Log.i(TAG, "from update:"+stringDeepSleepTime+":"+stringLightlSleepTime);
    		
    		mDeepSleepTimeText.setText(stringDeepSleepTime);
    		mLightSleepTimeText.setText(stringLightlSleepTime);
    		mTotalSleepTimeText.setText(stringTotalSleepTime);
       	 
    	  } else {
    		mDeepSleepTimeText.setVisibility(View.GONE);
    		mLightSleepTimeText.setVisibility(View.GONE);
    		mTotalSleepTimeText.setVisibility(View.GONE);
    	    }
         }
       	});
    }
    
        
    @Override
    protected void startQuery() {
    	
    	super.startQuery();
    	
    	//-- mSleepView.resetSleep(showingMode, 1);
    	mHandler.sendEmptyMessage(MSG_GRAPH_REDRAW);
    	
    	Log.i(TAG, "sleepFragment startQuery");
    }
    
    String stringTimeFormatFromMinute(int totalMinutes)
	{
 		int hour=totalMinutes /60;
		int minute=totalMinutes-hour*60;
		String message="";
		
		if(hour!=0){
			message += String.valueOf(hour) + " "+getResources().getString(R.string.hour);
		}
		if(minute!=0){
			message += " " + String.valueOf(minute) + " "+getResources().getString(R.string.minute);        			
		}   
		
		if(totalMinutes==0){
			message +=" 0 ";
		}
		
		return message;
	}
    
    public static final int MSG_GRAPH_REDRAW = 01;

    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
        	
        	if(msg.what==MSG_GRAPH_REDRAW){
        		
        		String stringDeepSleepTime=stringTimeFormatFromMinute(mSleepView.mDeepSleepTime);
        		String stringLightlSleepTime=stringTimeFormatFromMinute(mSleepView.mLightSleepTime);
        		String stringTotalSleepTime=stringTimeFormatFromMinute(mSleepView.mTotalSleepTime);

        		mDeepSleepTimeText.setText(stringDeepSleepTime);
        		mLightSleepTimeText.setText(stringLightlSleepTime);
        		mTotalSleepTimeText.setText(stringTotalSleepTime);

        		mSleepView.invalidate();   
        		mLightView.invalidate();
        	}
        	
        	return true;
        }
    });
        
    int showingStartTime=0, showingEndTime=0;

    @Override
    protected Cursor queryData() {
        Calendar queryStartTime=mTimeControllerView.startOfTheDay();
        
        int currentTime = (int) (queryStartTime.getTimeInMillis() / MINUTE_MILLIS);	//-- 09.23

        Calendar nowCalendar= Calendar.getInstance();
        int hour=nowCalendar.get(Calendar.HOUR_OF_DAY);
        
        //-- shift 12 hours for night daily showing
        if(hour>=12){
        	currentTime=currentTime+(12*60);
        }
        else{
        	currentTime=currentTime-(12*60);        	
        }
         
        {
        	showingStartTime=currentTime;
        	showingEndTime=currentTime+24*60;
        }
 
        Cursor cursor;
        
        if(mIsCloudWorking==true){    
        	mIsCloudWorking=false;
            cursor = BinsDBUtils.activeDB().querySleepRecords(mCurrentUserID, showingStartTime, showingEndTime);

        }else{
        	cursor = BinsDBUtils.activeDB().querySleepRecordsWithCloudLoad(mCurrentUserID, showingStartTime, showingEndTime);
        }
        
        int queryResult=BinsDBUtils.activeDB().getQueryResult();
        mIsCloudWorking=(queryResult==BinsDBUtils.QUERY_WORKING);
 
                       
        return cursor;

    }

    @Override 
    protected synchronized void queryTaskUiBeforeProgress(){
       	if(mIsCloudWorking==true){
       		mWorkingProgress.setVisibility(View.VISIBLE);
        }else{
        	mWorkingProgress.setVisibility(View.GONE);      		 
        }           	
    }
    
    @Override
    protected synchronized void handleData(Cursor cursor) {
        Log.d(TAG, " sleep handleData ...:"+cursor);
    	
        Boolean hasRecord=false;

       	if(mIsCloudWorking==true){
       		
       		return; // working on cloud side, waiting for the result later
        }

 		mWorkingProgress.setVisibility(View.GONE);      		 

        LevelPeriodBuilder levelPeriod=new LevelPeriodBuilder(showingStartTime, showingEndTime, cursor);
        
        if(levelPeriod.getCount()>0){
        	hasRecord=true;
            levelPeriod.display(mSleepView, mLightView);        	
        }
        
    	mLastSleepQueryHadRecord=hasRecord; // used from update     	      
    }
    
    
    private OnClickListener onClickListener = new OnClickListener() {

    	@Override
    	public void onClick(View view) {
    	    Log.i(TAG, "clicked");
    		Calendar cal=mTimeControllerView.getDate();
  			datePickerDialog.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
  			datePickerDialog.show();
    	}	
    };
    
    
}   
   
