package com.hanson.binsapp;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.hanson.binsapp.R;


public class BaseActivity extends Activity {
    public static final String TAG = "HansonActivity";
    public static final int REQUEST_CODE_BLUETOOTH = 1001;

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    protected TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowCustomEnabled(true);
        View view = LayoutInflater.from(this).inflate(R.layout.custom_title, null);
        //-- getActionBar().setCustomView(view);
        mTitle = (TextView) view.findViewById(R.id.title);  
        //-- 06.08
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        	     ActionBar.LayoutParams.MATCH_PARENT, 
        	     ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER ); 
        mTitle.setText(getTitle());
        getActionBar().setCustomView(view, params);
        
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if(deviceSupported()){
            if (adapter != null && !adapter.isEnabled()) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(intent, REQUEST_CODE_BLUETOOTH);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_BLUETOOTH) {
            if (resultCode == RESULT_CANCELED) {
                Log.w(TAG, "Bluetooth's state is off ...");
                Toast.makeText(this, R.string.bluetooth_hint, Toast.LENGTH_LONG).show();
            } else {
                Log.d(TAG, "Bluetooth's state is ok ...");
            }
        }  
    }
    
    // Must meet the following two conditions
    protected boolean deviceSupported() {
        boolean supported = getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
        return supported;
    }
}
