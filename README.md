# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Android app for BiNS wearble device
* BiNS is a product and service to combind wristband wearable and mobule app and cloud services to monitor user's health
* This iOS app connects to wearable device through Bluetooth LE, and exchange activity data / settings.
* Product document can be found from [here](/bins-documents/BiNS-Z3-Users-Manual-v2-150831.pdf), and [others](/bins-documents).

![BiNS Z3 Product](/BiNS-Z3-product-photos/BiNS-Z3-photo-2.JPG)

* Other product photos can be found [here](/BiNS-Z3-product-photos)

![BiNS App for Android](/bins-app-android-screenshots/bins-app-android-1.png)

* Screenshots of mobile app for Android is located [here](/bins-app-android-screenshots).
* Screenshots of mobile app for iOS is located [here](/bins-app-ios-screenshots).

### How do I get set up? ###

* This version was built on Eclipse for Android environment previously.
* It has been adjusted for Android Studio configuration, however is not tested yet over Android Studio environment.

### Contribution guidelines ###

* Read together with embedded part of the project can help understand the flow of works.

### Who do I talk to? ###

* Please contact denniskung68@hotmail.com for any suggestions.
